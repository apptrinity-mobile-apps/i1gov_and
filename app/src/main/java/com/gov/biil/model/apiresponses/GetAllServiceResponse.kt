package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class GetAllServiceResponse {
     val result: String? = null

     val data: Array<GetAllServiceDataResponse>? = null

     val status: String? = null


}
