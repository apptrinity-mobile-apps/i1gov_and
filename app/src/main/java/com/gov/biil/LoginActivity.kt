package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.Toast
import com.google.firebase.iid.FirebaseInstanceId
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.ConnectionManager
import com.gov.biil.model.APIResponse.UserLoginResponse
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.DeviceTokenModel
import com.gov.biil.model.apiresponses.UserInfo
import retrofit2.Call
import retrofit2.Callback


class LoginActivity : AppCompatActivity() {
    internal lateinit var et_user_name_id: EditText
    internal var et_pass_id:EditText? = null
    internal lateinit var tv_forgot_pass_id: CustomTextView
    internal lateinit var tv_sign_in_id:CustomTextView
    internal lateinit var tv_forgot_user_name_id:CustomTextView
    internal lateinit var tv_help_id:CustomTextView
    internal lateinit var user_name_stg: String
    internal lateinit var password_stg:String
    internal lateinit var device_id:String
    internal lateinit var sessionManager: SessionManager
    internal lateinit var tv_register:CustomTextView
    internal lateinit var tv_privacy:CustomTextView
    internal lateinit var tv_terms:CustomTextView

    internal lateinit var deviceTokenModel: DeviceTokenModel
    internal lateinit var refreshedToken: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sessionManager = SessionManager(this)
        if (sessionManager.isLoggedIn) {
            val home_intent = Intent(this@LoginActivity, MainActivity::class.java)
            finish()
            startActivity(home_intent)
        }else {
            setContentView(R.layout.activity_new_login)
            myloading()


            val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
            setSupportActionBar(toolbar)

            var drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.login_menu);
            toolbar.setOverflowIcon(drawable)


           // toolbar.setNavigationIcon(android.R.drawable.ic_dialog_alert);

            et_user_name_id = findViewById(R.id.et_user_name) as EditText
            et_pass_id = findViewById(R.id.et_pass_id) as EditText
            tv_forgot_pass_id = findViewById(R.id.tv_forgot_pass_id) as CustomTextView
            tv_help_id = findViewById(R.id.tv_help_id) as CustomTextView
            tv_sign_in_id = findViewById(R.id.tv_sign_in_id) as CustomTextView

            tv_register = findViewById(R.id.tv_sign_up) as CustomTextView
            var rootview = findViewById(R.id.activity_login_id) as RelativeLayout



           // someTask().execute()


            tv_sign_in_id.setOnClickListener(View.OnClickListener {
                user_name_stg = et_user_name_id.getText().toString().trim({ it <= ' ' })
                password_stg = et_pass_id!!.getText().toString().trim({ it <= ' ' })
                if (hasValidCredentials()) {
                    if(ConnectionManager.checkConnection(applicationContext)) {

                        var iid = FirebaseInstanceId.getInstance()
                        var refreshedToken = iid.token

                        while (refreshedToken == null) {
                            iid = FirebaseInstanceId.getInstance()
                            refreshedToken = iid.token
                        }

                        Log.e("refreshedToken",refreshedToken );

                        callLoginAPI(user_name_stg, password_stg,refreshedToken.toString(),"ANDROID")
                    }else{
                        ConnectionManager.snackBarNetworkAlert_Relative(rootview,applicationContext)
                    }
                }
            })

            tv_forgot_pass_id.setOnClickListener(View.OnClickListener {
                val intent = Intent(this@LoginActivity, ForgotPasswordActivity::class.java)
                startActivity(intent)
            })

            tv_help_id.setOnClickListener(View.OnClickListener {
                val intent = Intent(this@LoginActivity, ContactUsActivity::class.java)
                startActivity(intent)
            })


            tv_register.setOnClickListener(View.OnClickListener {
                val intent = Intent(this@LoginActivity, RegisterActivity2::class.java)
                startActivity(intent)
            })
        }



       /* var iid = FirebaseInstanceId.getInstance()
        var refreshedToken = iid.token

        while (refreshedToken == null) {
            iid = FirebaseInstanceId.getInstance()
            refreshedToken = iid.token
        }

        Log.e("refreshedToken",refreshedToken );*/


        //refreshedToken = FirebaseInstanceId.getInstance().token
        //Log.e("refreshedToken",refreshedToken );
        /*deviceTokenModel = DeviceTokenModel.getInstance()
        device_id = deviceTokenModel.getDevicetoken()
        Log.e("device_id",device_id );*/

    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun callLoginAPI(user_name_stg: String, password_stg: String,refreshedToken: String,device_type:String) {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.login(user_name_stg,password_stg,refreshedToken,"ANDROID")
        Log.d("REQUEST_LOGIN", call.toString() + "----"+refreshedToken)
        call.enqueue(object : Callback<UserLoginResponse> {
            override fun onResponse(call: Call<UserLoginResponse>, response: retrofit2.Response<UserLoginResponse>?) {
                Log.w("LoginResponse","Result : Success"+response!!.body()!!.status)
                my_loader.dismiss()
                if (response != null && response.body()!= null  && response.body()!!.status.equals("1")) {
                    if(response.body()!!.user_info!= null){
                        if(response.body()!!.user_info!!.mobile_status.equals("1")) {
                            if (response.body()!!.user_info!!.email_status.equals("1")) {
                                if (response.body()!!.user_info!!.reg_status.equals("1")){
                                    if(response.body()!!.user_info!!.admin_status.equals("1")){
                                        storeLoginData(response.body()!!.user_info)
                                        val intent = Intent(this@LoginActivity, MainActivity::class.java)
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                                        startActivity(intent)
                                    }else {
                                        Toast.makeText(applicationContext,"Please Approve from Admin",Toast.LENGTH_SHORT).show()
                                    }


                                }else{
                                    val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
                                    intent.putExtra("user_id", response.body()!!.user_info!!.id.toString())
                                    intent.putExtra("email", response.body()!!.user_info!!.email.toString())
                                    intent.putExtra("phone", response.body()!!.user_info!!.phone_number.toString())
                                    startActivity(intent)
                                }

                            } else {
                                val intent = Intent(this@LoginActivity, RegisterActivity3::class.java)
                                intent.putExtra("email", response.body()!!.user_info!!.email.toString())
                                intent.putExtra("user_id", response.body()!!.user_info!!.id.toString())
                                intent.putExtra("phonenumber", "")
                                intent.putExtra("password", "")
                                intent.putExtra("otp", "")
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                                startActivity(intent)
                            }
                        }else{
                           /* val intent = Intent(this@LoginActivity, RegisterActivity3::class.java)
                            intent.putExtra("email", response.body()!!.user_info!!.email.toString())
                            intent.putExtra("phonenumber", response.body()!!.user_info!!.phone_number.toString())
                            intent.putExtra("user_id", response.body()!!.user_info!!.id.toString())
                            intent.putExtra("password", "")
                            intent.putExtra("otp", "")
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)*/
                            Toast.makeText(applicationContext,"Please Register to verify Mobile",Toast.LENGTH_SHORT).show()
                        }
                    }
                }
                else{
                    Toast.makeText(applicationContext,"Invalid user credentails",Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<UserLoginResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed"+t.toString())
            }
        })
    }

    private fun storeLoginData(user_info: UserInfo?)  {
        if(user_info !=null) {
            try {
               sessionManager.storeLoginInfoSession(user_info)
            }
            catch (e: Exception) {
                Log.w("exception",""+e.printStackTrace())
            }

        }

    }


    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(user_name_stg))
            et_user_name_id.setError("Username Required")
        else if (TextUtils.isEmpty(password_stg))
            et_pass_id!!.setError("Password Required")
        else if (et_pass_id != null && et_pass_id!!.length() < 6)
            et_pass_id!!.setError("Invalid Password")
        else
            return true
        return false
    }

    override fun onBackPressed() {
       /* val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)*/
        val intent = Intent(this@LoginActivity, LaunchingActivity::class.java)
        startActivity(intent)
    }


   /* class someTask() : AsyncTask<Void, Void, String>() {
        override fun doInBackground(vararg params: Void?): String? {
            // ...


            return
        }

        override fun onPreExecute() {
            super.onPreExecute()
            // ...
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            // ...
        }
    }*/


    override
    fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override
    fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        when (id) {
            R.id.faq -> {
                val intent = Intent(this@LoginActivity, FaqActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.feedback -> {
                val intent = Intent(this@LoginActivity, FeedBackActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.contact -> {
                val intent = Intent(this@LoginActivity, ContactUsActivity::class.java)
                startActivity(intent)
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }


}
