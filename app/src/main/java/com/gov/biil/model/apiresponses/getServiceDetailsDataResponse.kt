package com.gov.biil.model.apiresponses

/**
 * Created by indo67 on 7/31/2018.
 */
class getServiceDetailsDataResponse {

    var id: String? = null
    var user_id: String? = null
    var service_type: String? = null
    var service_id: String? = null
    var city_id: String? = null
    var name: String? = null
    var account: String? = null


}