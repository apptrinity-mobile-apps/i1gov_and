package com.gov.biil.model.apiresponses

class SaveVoteDataResponse {

    val votecount: Array<SaveVoteListDataResponse>?=null

    val total: String? = null
    val city: String? = null
    val state: String? = null

}
