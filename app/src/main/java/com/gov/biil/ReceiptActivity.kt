package com.gov.biil

import Helper.CustomTextView
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.confirmResponse
import com.gov.biil.model.confirmData
import model.apiresponses.getAdminStatusInfo
import model.apiresponses.startParkingResponse
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*


class ReceiptActivity : AppCompatActivity() {
    var vehicle_number = ""
    var vehicle_state = ""
    var vehicle_state_id = ""
    var vehicle_zone = ""
    var vehicle_address = ""
    var vehicle_time = ""
    var vehicle_time_only = ""
    var vehicle_price = ""
    var vehicle_price_id = ""
    var vehicle_type = ""
    var zone_id = ""
    var lot_rateid = ""
    var vehicle_from_time = ""
    var vehicle_to_time = ""
    var total_main_balance = ""
    var commercial_price = ""
    var personal_price = ""
    lateinit var tv_toT_id: TextView
    lateinit var tv_fromT_id: TextView
    lateinit var tv_receiptzone_id: TextView
    lateinit var tv_cost_id: TextView
    lateinit var tv_total_price_id: TextView
    lateinit var tv_confirm_id: CustomTextView
    internal lateinit var sessionManager: SessionManager
    lateinit var stop_Dialog: Dialog
    lateinit var tv_ok_id: CustomTextView
    lateinit var tv_cancel_id: CustomTextView
    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receipt)
        sessionManager = SessionManager(this)


        myloading()

        getAdminStatusInfo()
        Stopalert()
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        vehicle_number = intent.getStringExtra("vehicle_no")
        vehicle_state = intent.getStringExtra("vehicle_state")
        vehicle_state_id = intent.getStringExtra("vehicle_stateid")
        vehicle_zone = intent.getStringExtra("vehicle_zone")
        vehicle_address = intent.getStringExtra("vehicle_address")
        vehicle_time = intent.getStringExtra("vehicle_time")
        vehicle_time_only = intent.getStringExtra("vehicle_time_only")
       // vehicle_price = intent.getStringExtra("vehicle_price")
        vehicle_price_id = intent.getStringExtra("vehicle_price_id")
        vehicle_type = intent.getStringExtra("vehicle_type")
        zone_id = intent.getStringExtra("zone_id")
        lot_rateid = intent.getStringExtra("lot_rateid")
        total_main_balance = intent.getStringExtra("total_main_balance")

        Log.d("RECEIPTINTENT", sessionManager.isId+""+vehicle_number + "---"+vehicle_state+"---"+vehicle_state_id+"---"+vehicle_zone+"---"+vehicle_address+"---"+vehicle_time+"---"+vehicle_time_only+"---"+vehicle_price_id+"---"+vehicle_type+"---"+zone_id+"---"+lot_rateid+"---"+total_main_balance)


        tv_toT_id = findViewById(R.id.tv_toT_id)
        tv_fromT_id = findViewById(R.id.tv_fromT_id)
        tv_receiptzone_id = findViewById(R.id.tv_receiptzone_id)
        tv_cost_id = findViewById(R.id.tv_cost_id)
        tv_total_price_id = findViewById(R.id.tv_total_price_id)
        tv_confirm_id = findViewById(R.id.tv_confirm_id)
        if (vehicle_zone.equals("")) {
            tv_receiptzone_id.setText("Address #" + vehicle_address)

        } else {
            tv_receiptzone_id.setText("Zone #" + vehicle_zone)
        }


        callStartParkingAPI()

        tv_confirm_id.setOnClickListener {
            calltoConfirmAPI()
        }

        val sdf = SimpleDateFormat("hh:mm aa")
        val currentDateandTime = sdf.format(Date())

        val date = sdf.parse(currentDateandTime)
        val calendar = Calendar.getInstance()

        calendar.setTime(date)
        calendar.add(Calendar.HOUR, vehicle_time_only.toInt())

        System.out.println("Time here " + sdf.format(calendar.getTime()) + "----" + vehicle_time_only.toInt())


        val dateandtimeFormat = SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        val presentDateTime = dateandtimeFormat.format(Date())

        val date_time = dateandtimeFormat.parse(presentDateTime)
        val new_calendar = Calendar.getInstance()

        new_calendar.setTime(date_time)
        new_calendar.add(Calendar.HOUR, vehicle_time_only.toInt())
        System.out.println("Time here now" + dateandtimeFormat.format(new_calendar.getTime()))

        tv_fromT_id.setText(presentDateTime)
        tv_toT_id.setText(dateandtimeFormat.format(new_calendar.getTime()).toString())
        vehicle_from_time = presentDateTime
        vehicle_to_time = dateandtimeFormat.format(new_calendar.getTime()).toString()
    }
    //postconfirmreceipt

    private fun calltoConfirmAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.postconfirmreceipt(id = sessionManager.isId, no_plate = vehicle_number,
                vehicle_type = vehicle_type,  parking_zone_street = vehicle_zone,parking_lot_street = vehicle_address,
                rate_price = vehicle_price_id, state = vehicle_state, state_code = vehicle_state_id,hours = vehicle_time_only,amount = vehicle_price)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<confirmResponse> {
            override fun onResponse(call: Call<confirmResponse>, response: retrofit2.Response<confirmResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        Log.e("Result", response.body()!!.result.toString())
                        val list: confirmData? = response.body()!!.data!!


                        Log.d("ZONE_LOTRATE", list!!.zone_id + "-------" + list!!.lot_id)

                        val intent = Intent(this@ReceiptActivity, TimerActivity::class.java)
                        intent.putExtra("timer_hrs", vehicle_time_only)
                        intent.putExtra("receipt_id", list!!.receipt_id)
                        intent.putExtra("user_id", list!!.user_id)
                        intent.putExtra("no_plate", list!!.no_plate)
                        intent.putExtra("state", list!!.state)
                        intent.putExtra("vehicle_type", list!!.vehicle_type)
                        intent.putExtra("zone_id", list!!.zone_id)
                        intent.putExtra("lot_id", list!!.lot_id)
                        intent.putExtra("parkig_start_date", list!!.parkig_start_date)
                        intent.putExtra("parkig_end_date", list!!.parkig_end_date)
                        intent.putExtra("from", "parking")
                        intent.putExtra("total_main_balance",total_main_balance)
                        startActivity(intent)
                    } else if (response.body()!!.status.equals("2")) {
                        my_loader.dismiss()
                        Toast.makeText(applicationContext,response.body()!!.result, Toast.LENGTH_LONG).show()
                    }else if(response.body()!!.status.equals("3")){
                        stop_Dialog.show()

                    }
                }
            }

            override fun onFailure(call: Call<confirmResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
                my_loader.dismiss()
            }


        })
    }


    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    fun Stopalert(){
        stop_Dialog = Dialog(this)
        stop_Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        stop_Dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        stop_Dialog.setCancelable(false)
        stop_Dialog.setContentView(R.layout.stop_dialog_alert)
        val tv_message_id=stop_Dialog.findViewById<CustomTextView>(R.id.tv_message_id)
        tv_message_id.setText("Insufficient funds")
        tv_ok_id=stop_Dialog.findViewById(R.id.tv_ok_id)
        tv_ok_id.setText("Load Funuds")
        tv_cancel_id=stop_Dialog.findViewById(R.id.tv_cancel_id)
        tv_cancel_id.setOnClickListener {
            stop_Dialog.dismiss()
        }
        tv_ok_id.setOnClickListener {
            val intent = Intent(this, FoundsLoad::class.java)
            intent.putExtra("total_main_balance",total_main_balance)
            startActivity(intent)
            stop_Dialog.dismiss()
        }

    }


    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }





    private fun callStartParkingAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.startParking(hour_id = vehicle_price_id, zone_id = zone_id,
                vehicle_type = vehicle_type,lot_id = lot_rateid)
        Log.d("StartParking_REQUEST", vehicle_price_id+"-----"+zone_id+"------"+vehicle_type+"------"+lot_rateid)
        call.enqueue(object : Callback<startParkingResponse> {
            override fun onResponse(call: Call<startParkingResponse>, response: retrofit2.Response<startParkingResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.status.equals("1")) {
                            if (response.body()!!.data != null) {
                                Log.w("Result_Address_Profile", "Result : " + response.body()!!.data)

                                if(response.body()!!.data!!.vehicle_type!!.equals("Commercial")){

                                    commercial_price = response!!.body()!!.data!!.commercial_price.toString()
                                    commercial_price = response!!.body()!!.data!!.commercial_price.toString()
                                    vehicle_price = commercial_price

                                    tv_cost_id.setText("$ " + commercial_price)
                                    tv_total_price_id.setText("$ " + commercial_price)
                                }else if(response.body()!!.data!!.vehicle_type!!.equals("Personal")){

                                    personal_price = response!!.body()!!.data!!.personal_price.toString()
                                    vehicle_price = personal_price

                                    tv_cost_id.setText("$ " + personal_price)
                                    tv_total_price_id.setText("$ " + personal_price)
                                }



                            }

                        }
                    } else if (response.body()!!.status.equals("2")) {
                        my_loader.dismiss()
                        Toast.makeText(applicationContext,"Currently Not available", Toast.LENGTH_LONG).show()
                        tv_confirm_id.visibility = View.INVISIBLE
                    }else if(response.body()!!.status.equals("3")){
                        stop_Dialog.show()

                    }
                }
            }

            override fun onFailure(call: Call<startParkingResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
                my_loader.dismiss()
            }


        })
    }




}
