package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.ConnectionManager
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.RegistrationStepOneResponse
import com.gov.biil.model.apiresponses.RegisterUserInfo
import retrofit2.Call
import retrofit2.Callback


class RegisterActivity2 : AppCompatActivity() {


    var dialog_list: Dialog? = null
    internal lateinit var sessionManager: SessionManager

    internal lateinit var reg_cellphone: EditText
    internal  var  otp: String?= ""

    internal lateinit var reg_email: EditText
    internal lateinit var reg_password: EditText
    internal lateinit var create_account: CustomTextView
    internal lateinit var login_account: CustomTextView
    internal lateinit var tv_privacy:CustomTextView
    internal lateinit var tv_terms:CustomTextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_new_account)
        sessionManager = SessionManager(this)


        var rootview = findViewById(R.id.activity_login_id) as RelativeLayout

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        var drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.login_menu);
        toolbar.setOverflowIcon(drawable)

        tv_privacy = findViewById(R.id.tv_privacy) as CustomTextView
        tv_terms = findViewById(R.id.tv_terms) as CustomTextView

        dialog_list = Dialog(this)
        dialog_list!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list!!.setContentView(R.layout.dialog_view)
        dialog_list!!.setCancelable(true)


        reg_email = findViewById(R.id.et_user_name) as EditText
        reg_cellphone = findViewById(R.id.et_number_id) as EditText
        reg_password = findViewById(R.id.et_pass_id) as EditText
        create_account = findViewById(R.id.tv_sign_in_id) as CustomTextView
        login_account = findViewById(R.id.tv_sign_in) as CustomTextView
        myloading()




        tv_privacy.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@RegisterActivity2, PrivacyWebActivity::class.java)
            intent.putExtra("privacy","privacy")
            startActivity(intent)

        })

        tv_terms.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@RegisterActivity2, PrivacyWebActivity::class.java)
            intent.putExtra("privacy","terms")
            startActivity(intent)

        })


        create_account.setOnClickListener(View.OnClickListener {
            if (hasValidCredentials()) {
                if (true) {
                    if (ConnectionManager.checkConnection(applicationContext)) {
                        callRegisterAPI()

                    } else {
                        ConnectionManager.snackBarNetworkAlert_Relative(rootview, applicationContext)
                    }
                } else {
                    Toast.makeText(applicationContext, "Please select terms and conditions", Toast.LENGTH_SHORT).show()
                }
            }
        })

        login_account.setOnClickListener {
            val intent = Intent(this@RegisterActivity2, LoginActivity::class.java)
            startActivity(intent)
        }


    }


      private fun callRegisterAPI() {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.mobileVerification(reg_cellphone.text.toString(),reg_email.text.toString())
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<RegistrationStepOneResponse> {
            override fun onResponse(call: Call<RegistrationStepOneResponse>, response: retrofit2.Response<RegistrationStepOneResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                   // Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                         otp = response.body()!!.otp
                        val intent = Intent(this@RegisterActivity2, RegisterActivity3::class.java)
                        intent.putExtra("email",reg_email.text.toString())
                        intent.putExtra("phonenumber",reg_cellphone.text.toString())
                        intent.putExtra("password",reg_password.text.toString())
                        intent.putExtra("otp",otp)
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)

                    } else  if (response.body()!!.status.equals("3")){
                        Toast.makeText(applicationContext, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(applicationContext, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<RegistrationStepOneResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


  /*  private fun callRegisterAPI() {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.saveUserStepOne(reg_email.text.toString(), reg_cellphone.text.toString(), reg_password.text.toString())
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<RegistrationStepOneResponse> {
            override fun onResponse(call: Call<RegistrationStepOneResponse>, response: retrofit2.Response<RegistrationStepOneResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.usetr_info != null) {

                            val email = response.body()!!.usetr_info!!.email
                            val phonenumber = response.body()!!.usetr_info!!.phone_number
                           // storeRegistrationData(response.body()!!.usetr_info)
                            val intent = Intent(this@RegisterActivity2, RegisterActivity3::class.java)
                            intent.putExtra("email",email)
                            intent.putExtra("phonenumber",phonenumber)
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                        }
                    } else {
                        Toast.makeText(applicationContext, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<RegistrationStepOneResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }*/

    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(reg_email.text.toString())) {
            reg_email.setError("Enter Email ")
        } else if (TextUtils.isEmpty(reg_cellphone.text.toString())) {
            reg_cellphone.setError("Enter phone number")
        } else if (TextUtils.isEmpty(reg_password.text.toString())) {
            reg_password.setError("Enter Password ")
        } else
            return true
        return false
    }

    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun storeRegistrationData(user_info: RegisterUserInfo?) {
        if (user_info != null) {
            try {
                sessionManager.storeLoginInfoSession(user_info)
            } catch (e: Exception) {
                Log.w("exception", "" + e.printStackTrace())
            }

        }

    }


    override
    fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override
    fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        when (id) {
            R.id.faq -> {
                val intent = Intent(this@RegisterActivity2, FaqActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.feedback -> {
                val intent = Intent(this@RegisterActivity2, FeedBackActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.contact -> {
                val intent = Intent(this@RegisterActivity2, ContactUsActivity::class.java)
                startActivity(intent)
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

}
