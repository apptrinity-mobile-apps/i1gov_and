package com.gov.biil.model.apiresponses

/**
 * Created by indo67 on 7/24/2018.
 */
class paymentdataResponse {
    var id: String? = null
    var user_id: String? = null
    var amount: String? = null
    var currency: String? = null
    var trans_id: String? = null
    var payment_status: String? = null
    var created_date: String? = null
}