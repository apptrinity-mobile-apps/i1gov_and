package com.gov.biil

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.biil.adapter.SearchListAdapter
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.SearchListDataResponse
import com.gov.biil.model.apiresponses.SearchListResponse
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback


class SearchActivity : AppCompatActivity() {

    private var lv_userlist: ListView? = null
    internal lateinit var search_autocomplete: EditText
    internal lateinit var btn_search: Button
    internal lateinit var search_string:String
    internal lateinit var my_loader: Dialog
    private var mSearchListAdapter: SearchListAdapter? = null
    internal lateinit var sessionManager: SessionManager
    public var searchlistdata: ArrayList<SearchListDataResponse> = ArrayList<SearchListDataResponse>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        myloading()
        sessionManager = SessionManager(this)
        getAdminStatusInfo()
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        search_autocomplete = findViewById(R.id.search_autocomplete) as EditText
        lv_userlist = findViewById(R.id.lv_userlist) as ListView
        btn_search = findViewById(R.id.btn_search) as Button

        btn_search.setOnClickListener {

            search_string = search_autocomplete.text.toString().trim()

            if(search_string.equals("")){
                Toast.makeText(applicationContext,"Please Enter Name",Toast.LENGTH_SHORT).show()
            }else{
                callSearchList()
            }

        }


       /* val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        search_autocomplete.setIconifiedByDefault(false)
        search_autocomplete.setSearchableInfo(searchManager
                .getSearchableInfo(componentName))
        search_autocomplete.setMaxWidth(Integer.MAX_VALUE)

        search_autocomplete.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                //FILTER AS YOU TYPE
               // adapter.getFilter().filter(query)

                return false
            }
        })*/



    }


    private fun callSearchList() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.usersearch(search_string)
        Log.d("REQUEST", call.toString() + "------"+search_string)
        searchlistdata.clear()
        call.enqueue(object : Callback<SearchListResponse> {
            override fun onResponse(call: Call<SearchListResponse>, response: retrofit2.Response<SearchListResponse>?) {
                my_loader.dismiss()
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        my_loader.dismiss()
                        val list: Array<SearchListDataResponse> = response.body()!!.data!!

                        for (item: SearchListDataResponse in list.iterator()) {
                            searchlistdata.add(item)
                            setProductAdapter(searchlistdata)
                        }
                        if (searchlistdata.size == 0) {

                        }
                    }

                }else {
                    Toast.makeText(applicationContext,"Failed to Respond Data",Toast.LENGTH_SHORT).show()
                }
            }
            override fun onFailure(call: Call<SearchListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    private fun setProductAdapter(searchlistdata: ArrayList<SearchListDataResponse>) {
        mSearchListAdapter = SearchListAdapter(searchlistdata, this)
        lv_userlist!!.adapter = mSearchListAdapter
        mSearchListAdapter!!.notifyDataSetChanged()
    }



    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


}
