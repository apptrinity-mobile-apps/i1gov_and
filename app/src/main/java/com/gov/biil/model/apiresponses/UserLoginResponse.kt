package com.gov.biil.model.APIResponse

import com.gov.biil.model.apiresponses.UserInfo
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class UserLoginResponse {
     val user_info: UserInfo? = null

     val result: String? = null

     val status: String? = null

}
