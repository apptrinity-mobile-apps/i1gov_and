package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.view.animation.AnimationSet
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.CustomTextViewBold


class LaunchingActivity : AppCompatActivity() {
    internal lateinit var tv_login_id: CustomTextViewBold
    internal lateinit var tv_createaccount: CustomTextViewBold
    internal lateinit var tv_help: CustomTextViewBold
    internal var et_pass_id:EditText? = null
    internal lateinit var tv_forgot_pass_id: CustomTextView
    internal lateinit var tv_sign_in_id:CustomTextView
    internal lateinit var tv_privacy:CustomTextView
    internal lateinit var tv_terms:CustomTextView
    internal lateinit var iv_launching_logo:ImageView
    internal lateinit var iv_menu_launching:ImageView
    internal lateinit var ll_logincreateaccount:LinearLayout
    internal lateinit var sessionManager: SessionManager
    internal lateinit var my_loader: Dialog
    var arrowdownup = false



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sessionManager = SessionManager(this)
        if (sessionManager.isLoggedIn) {
            val home_intent = Intent(this@LaunchingActivity, MainActivity::class.java)
            finish()
            startActivity(home_intent)
        }else {
            setContentView(R.layout.activity_register_login_screen)

            val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
            setSupportActionBar(toolbar)

          var drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.login_menu);
            toolbar.setOverflowIcon(drawable);

            myloading()

            tv_login_id = findViewById(R.id.tv_login_id) as CustomTextViewBold
            tv_createaccount = findViewById(R.id.tv_createaccount) as CustomTextViewBold
            tv_help = findViewById(R.id.tv_help) as CustomTextViewBold
            iv_launching_logo = findViewById(R.id.iv_launching_logo) as ImageView
            ll_logincreateaccount = findViewById(R.id.ll_logincreateaccount) as LinearLayout
            tv_privacy = findViewById(R.id.tv_privacy) as CustomTextView
            tv_terms = findViewById(R.id.tv_terms) as CustomTextView

            val mAnimationSet = AnimationSet(false)
            val mAnimationSet2 = AnimationSet(false)
            val fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.slidedown)
            val fadeInAnimation2 = AnimationUtils.loadAnimation(this, R.anim.slideup)
            mAnimationSet.addAnimation(fadeInAnimation)
            mAnimationSet2.addAnimation(fadeInAnimation2)



            // someTask().execute()
            iv_launching_logo.setOnClickListener(View.OnClickListener {

              /*  if (arrowdownup == false) {
                    ll_logincreateaccount.startAnimation(mAnimationSet);
                    iv_launching_logo.startAnimation(mAnimationSet2);
                    ll_logincreateaccount.visibility = View.VISIBLE
                    arrowdownup = true

                }*/

            })

            tv_privacy.setOnClickListener(View.OnClickListener {
                val intent = Intent(this@LaunchingActivity, PrivacyWebActivity::class.java)
                intent.putExtra("privacy","privacy")
                startActivity(intent)

            })

            tv_terms.setOnClickListener(View.OnClickListener {
                val intent = Intent(this@LaunchingActivity, PrivacyWebActivity::class.java)
                intent.putExtra("privacy","terms")
                startActivity(intent)

            })

            tv_login_id.setOnClickListener(View.OnClickListener {
                val intent = Intent(this@LaunchingActivity, LoginActivity::class.java)
                startActivity(intent)

            })

            tv_createaccount.setOnClickListener(View.OnClickListener {
                val intent = Intent(this@LaunchingActivity, RegisterActivity2::class.java)
                startActivity(intent)
            })

            tv_help.setOnClickListener(View.OnClickListener {
                val intent = Intent(this@LaunchingActivity, ContactUsActivity::class.java)
                startActivity(intent)
            })



        }




    }

    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }



    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

override
    fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override
    fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        when (id) {
            R.id.faq -> {
                val intent = Intent(this@LaunchingActivity, FaqActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.feedback -> {
                val intent = Intent(this@LaunchingActivity, FeedBackActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.contact -> {
                val intent = Intent(this@LaunchingActivity, ContactUsActivity::class.java)
                startActivity(intent)
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }


}
