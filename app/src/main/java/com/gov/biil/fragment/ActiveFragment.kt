package indo.com.graceprinting.Fragment

import Helper.CustomTextView
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.biil.Helper.Helper
import com.gov.biil.R
import com.gov.biil.ServicesDetailView
import com.gov.biil.adapter.GetAllServiceListAdapter
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.*
import retrofit2.Call
import retrofit2.Callback
import java.util.*
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass.
 */
class ActiveFragment : Fragment(), AdapterView.OnItemSelectedListener {


    lateinit var fragmentName: Fragment
    var uri: Uri? = null
    var city_id: String = ""
    var county_id: String = ""
    var state_id: String = ""
    var pay_type_stg: String = ""
    var service_amount_stg: String = ""
    var final_service_id: String = ""
    var service_update_stg: String = ""
    lateinit var search_autocomplete: AutoCompleteTextView
    lateinit var search_autocomplete_state: AutoCompleteTextView
    lateinit var search_autocomplete_county: AutoCompleteTextView


    lateinit var listview_searchcity: ListView
    lateinit var listview_getallservice: RecyclerView
    internal lateinit var my_loader: Dialog
    internal var paylink: String? = ""
    internal var selected_service_type: String? = ""
    internal var selected_service_id: String? = ""
    internal var selected_service_id1: String? = ""
    internal lateinit var sessionManager: SessionManager
    private var listview_citypayforservice: ExpandableListView? = null

    private var no_service: TextView? = null
    lateinit var listAdapter: ExpListViewAdapterWithCheckbox
    lateinit var listDataHeader: java.util.ArrayList<String>
    lateinit var listDataHeader1: java.util.ArrayList<String>
    lateinit var listData_paytype: ArrayList<String>
    lateinit var listData_serviceamount: ArrayList<String>
    lateinit var listDataChild: HashMap<String, java.util.ArrayList<GetAllServiceDataResponse>>
    private var lastExpandedPosition = -1

    lateinit var spinner_type_id: Spinner
    lateinit var spinner_service_type_id: Spinner
    lateinit var ll_city_search_id: LinearLayout
    lateinit var rv_serv_layout_id: RelativeLayout
    lateinit var service_spinner_adapter: ArrayAdapter<String>
    lateinit var spinner_list_array: ArrayList<String>
    lateinit var spinner_list_array_ids: ArrayList<String>
    lateinit var spinner_services_array: ArrayList<String>
    lateinit var spinner_services_array_ids: ArrayList<String>
    var service_type_id = ""
    var timer: Timer? = null

    var search_string: String = ""
    var aPosition: Int = 0
    internal lateinit var name_string: String
    internal lateinit var account_string: String
    internal lateinit var selected_position: String
    // private var mSearchListAdapter: SearchCityListAdapter? = null
    private var mSeviceListByIdListAdapter: ServiceListByIdListAdapter? = null
    private var mGetAllServicetypesListAdapter: GetAllServiceListAdapter? = null
    public var searchlistdata: java.util.ArrayList<SearchCityListDataResponse> = java.util.ArrayList<SearchCityListDataResponse>()
    public var searchstatelistdata: java.util.ArrayList<SearchStateListDataResponse> = java.util.ArrayList<SearchStateListDataResponse>()
    public var searchcountylistdata: java.util.ArrayList<SearchCountyListDataResponse> = java.util.ArrayList<SearchCountyListDataResponse>()
    public var serviceListdata: java.util.ArrayList<getServiceListByIdDataResponse> = java.util.ArrayList<getServiceListByIdDataResponse>()
    public var getservicelisttdata: java.util.ArrayList<GetAllServiceTypesDataResponse> = java.util.ArrayList<GetAllServiceTypesDataResponse>()
    var search_temp = false
    lateinit var search_names: ArrayList<String>
    lateinit var search_names_state: ArrayList<String>
    lateinit var search_names_county: ArrayList<String>
    lateinit var search_ids: ArrayList<String>
    lateinit var search_ids_state: ArrayList<String>
    lateinit var search_ids_county: ArrayList<String>
    lateinit var adapteo: ArrayAdapter<String>
    lateinit var adapteo_state: ArrayAdapter<String>
    lateinit var adapteo_county: ArrayAdapter<String>

    var getServiceId_stg = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        val rootView = LayoutInflater.from(activity).inflate(R.layout.city_payforservice_fragment, container, false)
        search_autocomplete = rootView.findViewById<AutoCompleteTextView>(R.id.search_autocomplete)
        search_autocomplete_state = rootView.findViewById<AutoCompleteTextView>(R.id.search_autocomplete_state)
        search_autocomplete_county = rootView.findViewById<AutoCompleteTextView>(R.id.search_autocomplete_county)
        listview_searchcity = rootView.findViewById(R.id.listview_searchcity)
        listview_getallservice = rootView.findViewById<RecyclerView>(R.id.listview_getallservice)
        listview_citypayforservice = rootView!!.findViewById(R.id.listview_citypayforservice) as ExpandableListView
        ll_city_search_id = rootView!!.findViewById(R.id.ll_city_search_id) as LinearLayout

        spinner_type_id = rootView.findViewById<Spinner>(R.id.spinner_type_id)
        spinner_service_type_id = rootView.findViewById<Spinner>(R.id.spinner_service_type_id)
        rv_serv_layout_id = rootView.findViewById<RelativeLayout>(R.id.rv_serv_layout_id)
        spinner_type_id.setOnItemSelectedListener(this)
        spinner_service_type_id.setOnItemSelectedListener(this)
        myloading()
        sessionManager = SessionManager(this.activity!!)
        search_names = ArrayList()
        search_names_state = ArrayList()
        search_names_county = ArrayList()
        search_ids = ArrayList()
        search_ids_state = ArrayList()
        search_ids_county = ArrayList()
        spinner_list_array = ArrayList()
        spinner_list_array_ids = ArrayList()
        spinner_services_array = ArrayList()
        spinner_services_array_ids = ArrayList()
        //  my_loader.show()

        //my_loader.dismiss()


        //callCitySearchList()
        //callStateSearchList()
        CallgetListOfCategories()
        //  ll_city_search_id.setBackgroundColor(Color.GRAY)

        rv_serv_layout_id.visibility = View.GONE
        spinner_service_type_id.visibility = View.GONE
        adapteo = ArrayAdapter(context, android.R.layout.simple_dropdown_item_1line, search_names)
        adapteo_state = ArrayAdapter(context, android.R.layout.simple_dropdown_item_1line, search_names_state)
        adapteo_county = ArrayAdapter(context, android.R.layout.simple_dropdown_item_1line, search_names_county)

        search_autocomplete.setOnItemClickListener { parent, view, position, id ->

            aPosition = search_names.indexOf(adapteo.getItem(position).toString())
            Log.e("names_ids", aPosition.toString() + "--" + search_ids.get(aPosition))
            city_id = search_ids.get(aPosition)
            /*else if(selected_position.equals("county")){
             state_id = search_ids.get(aPosition)
             Log.e("SELECTEDPOSITION_STATE", selected_position + "--" + search_ids.get(aPosition))
         }else if(selected_position.equals("federal")){
             state_id = search_ids.get(aPosition)
             Log.e("SELECTEDPOSITION_STATE", selected_position + "--" + search_ids.get(aPosition))
         }*/



            rv_serv_layout_id.visibility = View.VISIBLE
            spinner_service_type_id.visibility = View.VISIBLE

        }

        search_autocomplete.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                search_string = search_autocomplete.text.toString().trim()

                if (search_string.equals("")) {
                    rv_serv_layout_id.visibility = View.GONE
                    spinner_service_type_id.visibility = View.GONE

                    service_spinner_adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, spinner_services_array)
                    service_spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    spinner_service_type_id.adapter = service_spinner_adapter
                    service_spinner_adapter.notifyDataSetChanged()

                    listview_citypayforservice!!.visibility = View.GONE
                }


            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {

            }
        })



        search_autocomplete_state.setOnItemClickListener { parent, view, position, id ->

            aPosition = search_names_state.indexOf(adapteo_state.getItem(position).toString())

            city_id = search_ids_state.get(aPosition)
            Log.e("names_ids", aPosition.toString() + "--" + search_ids_state.get(aPosition)+"-----"+city_id)




            rv_serv_layout_id.visibility = View.VISIBLE
            spinner_service_type_id.visibility = View.VISIBLE

        }

        search_autocomplete_state.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                search_string = search_autocomplete_state.text.toString().trim()

                if (search_string.equals("")) {
                    rv_serv_layout_id.visibility = View.GONE
                    spinner_service_type_id.visibility = View.GONE

                    service_spinner_adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, spinner_services_array)
                    service_spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    spinner_service_type_id.adapter = service_spinner_adapter
                    service_spinner_adapter.notifyDataSetChanged()

                    listview_citypayforservice!!.visibility = View.GONE
                }


            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {

            }
        })




        search_autocomplete_county.setOnItemClickListener { parent, view, position, id ->

            aPosition = search_names_county.indexOf(adapteo_county.getItem(position).toString())
            city_id = search_ids_county.get(aPosition)
            Log.e("County_names_ids", aPosition.toString() + "--" + search_ids_county.get(aPosition)+"-----"+city_id)

            rv_serv_layout_id.visibility = View.VISIBLE
            spinner_service_type_id.visibility = View.VISIBLE

        }

        search_autocomplete_county.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                search_string = search_autocomplete_county.text.toString().trim()

                if (search_string.equals("")) {
                    rv_serv_layout_id.visibility = View.GONE
                    spinner_service_type_id.visibility = View.GONE

                    service_spinner_adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, spinner_services_array)
                    service_spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    spinner_service_type_id.adapter = service_spinner_adapter
                    service_spinner_adapter.notifyDataSetChanged()

                    listview_citypayforservice!!.visibility = View.GONE
                }


            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {

            }
        })



        // Listview Group click listener
        listview_citypayforservice!!.setOnGroupClickListener { parent, v, groupPosition, id ->
            // Toast.makeText(getApplicationContext(),
            // "Group Clicked " + listDataHeader.get(groupPosition),
            // Toast.LENGTH_SHORT).show();


            false
        }

        // Listview Group expanded listener
        listview_citypayforservice!!.setOnGroupExpandListener { groupPosition ->

            if (lastExpandedPosition != -1
                    && groupPosition != lastExpandedPosition) {
                listview_citypayforservice!!.collapseGroup(lastExpandedPosition);
            }
            lastExpandedPosition = groupPosition;


        }

        // Listview Group collasped listener
        listview_citypayforservice!!.setOnGroupCollapseListener { groupPosition ->


        }

        /* listview_citypayforservice!!.setOnGroupClickListener { parent, v, groupPosition, id ->
             pay_type_stg=listData_paytype.get(groupPosition)
             service_amount_stg=listData_serviceamount.get(groupPosition)
             Log.e("pay_type_type",pay_type_stg+"---"+service_amount_stg)
             true
         }*/

        // Listview on child click listener
        listview_citypayforservice!!.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
            // TODO Auto-generated method stub
            /* pay_type_stg = listData_paytype.get(groupPosition)
             service_amount_stg = listData_serviceamount.get(groupPosition)
             Log.e("pay_type_type_ll", pay_type_stg + "---" + service_amount_stg)*/
            false
        }

        return rootView

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        when (parent!!.getId()) {
            R.id.spinner_type_id ->

                if (spinner_list_array[position].equals("Select Any")) {
                    // ll_city_search_id.setBackgroundColor(Color.GRAY)

                    search_autocomplete.setFocusable(false)
                    search_autocomplete.setFocusableInTouchMode(false);
                    search_autocomplete.setClickable(false)
                    listview_citypayforservice!!.visibility = View.GONE

                } else {
                    search_autocomplete.visibility = View.VISIBLE

                    search_autocomplete.setFocusable(true)
                    search_autocomplete.setFocusableInTouchMode(true);
                    search_autocomplete.setClickable(true)
                    search_autocomplete.setText("")
                    search_autocomplete.setHint("Search " + spinner_list_array[position])
                    if (spinner_list_array[position].equals("City")) {

                        callCitySearchList()
                        selected_position = "city"
                        search_autocomplete_state.visibility = View.GONE
                        search_autocomplete_county.visibility = View.GONE
                        // adapteo.clear()

                    } else if (spinner_list_array[position].equals("County")) {
                        selected_position = "county"
                        //search_autocomplete.visibility = View.GONE
                        search_autocomplete_county.text.clear()
                        callCountySearchList()
                        search_autocomplete_state.visibility = View.GONE
                        search_autocomplete.visibility = View.GONE
                        search_autocomplete_county.visibility = View.VISIBLE
                    } else if (spinner_list_array[position].equals("State")) {
                        //adapteo.clear()
                        search_autocomplete_state.text.clear()
                        callStateSearchList()
                        selected_position = "state"
                        search_autocomplete.visibility = View.GONE
                        search_autocomplete_state.visibility = View.VISIBLE
                        search_autocomplete_county.visibility = View.GONE

                    } else if (spinner_list_array[position].equals("Federal")) {
                        selected_position = "federal"
                        search_autocomplete_state.visibility = View.GONE
                        search_autocomplete_county.visibility = View.GONE
                    }
                    //  ll_city_search_id.setBackgroundResource(R.drawable.seacrhbar_bg)
                    // Toast.makeText(context, spinner_list_array.get(position) + "--" + spinner_list_array_ids.get(position) + "---" + selected_position, Toast.LENGTH_LONG).show()
                    //callgetAllService(spinner_list_array_ids.get(position))
                    spinner_services_array.clear()
                    spinner_services_array_ids.clear()
                    callGetAllServiceTypesList(spinner_list_array_ids.get(position))
                }

            R.id.spinner_service_type_id ->

                if (spinner_services_array[position].equals("Select Service Type")) {
                    listview_citypayforservice!!.visibility = View.GONE
                } else {
                    service_type_id = spinner_services_array_ids.get(position)
                    //Toast.makeText(context, spinner_services_array_ids.get(position), Toast.LENGTH_LONG).show()
                    callgetAllService(spinner_services_array_ids.get(position))
                }
        }
    }

    private fun myloading() {
        my_loader = Dialog(this.activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


    private fun callgetServiceListById(serviceid: String?) {
        my_loader.show()
        serviceListdata.clear()
        val apiService = ApiInterface.create()
        val call = apiService.getServiceListById(sessionManager.isId, serviceid.toString())
        Log.d("CALLGETSERVICEBYID", sessionManager.isId + "------------" + serviceid)
        call.enqueue(object : Callback<getServiceListByIdResponse> {
            override fun onResponse(call: Call<getServiceListByIdResponse>, response: retrofit2.Response<getServiceListByIdResponse>?) {
                my_loader.dismiss()
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        my_loader.dismiss()
                        val list: Array<getServiceListByIdDataResponse> = response.body()!!.data!!
                        for (item: getServiceListByIdDataResponse in list.iterator()) {
                            serviceListdata.add(item)

                        }
                        setAdapterServiceListById(serviceListdata)
                        if (serviceListdata.size == 0) {

                        }
                        val param: ViewGroup.LayoutParams = listview_citypayforservice!!.getLayoutParams()

                        param.height = 800 * list.size
                    }


                } else {
                    //Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<getServiceListByIdResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    private fun CallgetListOfCategories() {
        my_loader.show()

        val apiService = ApiInterface.create()
        val call = apiService.getListOfCategoriesAPI("")
        call.enqueue(object : Callback<getListOfCategoriesResponse> {
            override fun onResponse(call: Call<getListOfCategoriesResponse>, response: retrofit2.Response<getListOfCategoriesResponse>?) {
                my_loader.dismiss()
                // Log.w("Status", "*** " + response!!.body()!!.status)
                if (response!!.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        my_loader.dismiss()
                        val list: Array<getListOfCategoriesDataResponse> = response.body()!!.data!!
                        spinner_list_array.add("Select Any")
                        spinner_list_array_ids.add("0")
                        for (item: getListOfCategoriesDataResponse in list.iterator()) {

                            Log.e("item.typr", item.type!! + "--" + item.id)
                            spinner_list_array.add(item.type!!)
                            spinner_list_array_ids.add(item.id!!)

                        }
                        // spinner_list = Array(spinner_list_array.size, { i -> spinner_list_array.get(i) })

                        val aa = ArrayAdapter(context, android.R.layout.simple_spinner_item, spinner_list_array)
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        spinner_type_id.adapter = aa
                    }

                } else {
                    //Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<getListOfCategoriesResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    private fun setAdapterServiceListById(serviceListdata: java.util.ArrayList<getServiceListByIdDataResponse>) {
        mSeviceListByIdListAdapter = ServiceListByIdListAdapter(serviceListdata, activity!!)
        childViewHolder!!.listview_billspayforserv!!.adapter = mSeviceListByIdListAdapter
        Helper.getListViewSize(childViewHolder!!.listview_billspayforserv)
        mSeviceListByIdListAdapter!!.notifyDataSetChanged()
    }


    inner class ServiceListByIdListAdapter(val mServiceList: java.util.ArrayList<getServiceListByIdDataResponse>, val activity: Activity) : BaseAdapter() {


        override fun getItem(position: Int): Any {

            return mServiceList.get(position)
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return mServiceList.size
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view: View = View.inflate(activity, R.layout.list_item_billpayforservice, null)
            val tv_name_list = view.findViewById(R.id.tv_name_list) as CustomTextView
            val tv_account_list = view.findViewById(R.id.tv_account_list) as CustomTextView
            val ll_name_account_id = view.findViewById(R.id.ll_name_account_id) as LinearLayout
            val img_edit_id = view.findViewById(R.id.img_edit_id) as ImageView
            val et_delete_id = view.findViewById(R.id.et_delete_id) as ImageView

            myloading()
            tv_name_list!!.text = mServiceList.get(position).name
            tv_account_list!!.text = mServiceList.get(position).account

            ll_name_account_id.setOnClickListener {
                val intent = Intent(context, ServicesDetailView::class.java)
                intent.putExtra("id", mServiceList.get(position).id)
                intent.putExtra("service_id", mServiceList.get(position).service_id)
                intent.putExtra("pay_type", pay_type_stg)
                intent.putExtra("servive_amount", service_amount_stg)
                startActivity(intent)
            }

            img_edit_id.setOnClickListener {
                val getname_stg = mServiceList.get(position).name
                val getaccount_stg = mServiceList.get(position).account
                getServiceId_stg = mServiceList.get(position).id!!

                service_update_stg = "update_service"
                name_string = childViewHolder!!.et_bill_name!!.setText(getname_stg).toString()
                account_string = childViewHolder!!.et_bill_account!!.setText(getaccount_stg).toString()
            }

            et_delete_id.setOnClickListener {

                calldeleteAccountAPI(mServiceList.get(position).id!!)
            }

            return view
        }

        internal lateinit var my_loader: Dialog
        private fun myloading() {
            my_loader = Dialog(activity)
            my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
            my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            my_loader.setCancelable(false);
            my_loader.setContentView(R.layout.mkloader_dialog)
        }


    }


    private var childViewHolder: ExpListViewAdapterWithCheckbox.ChildViewHolder? = null

    inner class ExpListViewAdapterWithCheckbox/*  Here's the constructor we'll use to pass in our calling
         *  activity's context, group items, and child items
        */
    (// Define activity context
            private val mContext: Context, // ArrayList that is what each key in the above
            // hashmap points to
            private val mListDataGroup: ArrayList<String>, /*
         * Here we have a Hashmap containing a String key
         * (can be Integer or other type but I was testing
         * with contacts so I used contact name as the key)
        */
            private val mListDataChild: HashMap<String, ArrayList<GetAllServiceDataResponse>>) : BaseExpandableListAdapter() {

        // Hashmap for keeping track of our checkbox check states
        private val mChildCheckStates: HashMap<Int, BooleanArray>
        private val mGroupCheckStates: HashMap<Int, BooleanArray>

        // Our getChildView & getGroupView use the viewholder patter
        // Here are the viewholders defined, the inner classes are
        // at the bottom
        // private var childViewHolder: ChildViewHolder? = null
        private var groupViewHolder: GroupViewHolder? = null

        /*
              *  For the purpose of this document, I'm only using a single
         *	textview in the group (parent) and child, but you're limited only
         *	by your XML view for each group item :)
        */
        private var groupText: String? = null
        private var childText: String? = null
        private var str_width_height: String? = null

        init {

            // Initialize our hashmap containing our check states here
            mChildCheckStates = HashMap()
            mGroupCheckStates = HashMap()
        }

        fun getNumberOfCheckedItemsInGroup(mGroupPosition: Int): Int {
            val getChecked = mChildCheckStates[mGroupPosition]
            var count = 0
            if (getChecked != null) {

                for (j in getChecked.indices) {
                    if (getChecked[j] == true) count++
                }
            }
            return count
        }

        fun getNumberOfGroupCheckedItems(mGroupPosition: Int): Int {
            val getChecked = mGroupCheckStates[mGroupPosition]
            var count = 0
            if (getChecked != null) {
                for (j in getChecked.indices) {
                    if (getChecked[j] == true) count++
                }
            }
            return count
        }

        override fun getGroupCount(): Int {
            return mListDataGroup.size
        }

        /*
         * This defaults to "public object getGroup" if you auto import the methods
         * I've always make a point to change it from "object" to whatever item
         * I passed through the constructor
        */
        override fun getGroup(groupPosition: Int): String {
            return mListDataGroup[groupPosition]
        }

        override fun getGroupId(groupPosition: Int): Long {
            return groupPosition.toLong()
        }


        override fun getGroupView(groupPosition: Int, isExpanded: Boolean,
                                  convertView: View?, parent: ViewGroup): View {
            var convertView = convertView


            //  I passed a text string into an activity holding a getter/setter
            //  which I passed in through "ExpListGroupItems".
            //  Here is where I call the getter to get that text
            groupText = getGroup(groupPosition)

            if (convertView == null) {

                val inflater = mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.list_group_layout, null)

                // Initialize the GroupViewHolder defined at the bottom of this document


                groupViewHolder = GroupViewHolder()

                groupViewHolder!!.tv_headername_city_payforser = convertView!!.findViewById(R.id.tv_headername_city_payforser) as CustomTextView
                groupViewHolder!!.tv_headername_city_payforser!!.setSelected(isExpanded)


                /*if (isExpanded) {
                    groupViewHolder!!.img_layout!!.setImageResource(R.drawable.down_arrow)
                } else {
                    groupViewHolder!!.img_layout!!.setImageResource(R.drawable.right_arrow)
                }*/


                convertView.tag = groupViewHolder
            } else {

                groupViewHolder = convertView.tag as GroupViewHolder
            }

            //groupViewHolder!!.mGroupText!!.text = groupText
            groupViewHolder!!.tv_headername_city_payforser!!.text = listDataHeader1.get(groupPosition)



            return convertView
        }

        override fun getChildrenCount(groupPosition: Int): Int {
            return mListDataChild[mListDataGroup[groupPosition]]!!.size
        }

        /*
         * This defaults to "public object getChild" if you auto import the methods
         * I've always make a point to change it from "object" to whatever item
         * I passed through the constructor
        */
        override fun getChild(groupPosition: Int, childPosition: Int): GetAllServiceDataResponse {
            return mListDataChild[mListDataGroup[groupPosition]]!!.get(childPosition)
        }

        override fun getChildId(groupPosition: Int, childPosition: Int): Long {
            return childPosition.toLong()
        }

        override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView


            callgetServiceListById(listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).id!!)
            final_service_id = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).id!!.toString()
            pay_type_stg = listData_paytype.get(groupPosition)

            //selected_service_id = list.get(j).id.toString()

            service_amount_stg = listData_serviceamount.get(groupPosition)
            Log.e("pay_type_type", pay_type_stg + "---" + service_amount_stg)

            if (convertView == null) {


                val inflater = this.mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.list_item_citypayforservchilditem, null)

                childViewHolder = ChildViewHolder()
                childViewHolder!!.et_bill_name = convertView!!.findViewById(R.id.et_bill_name)
                childViewHolder!!.et_bill_account = convertView!!.findViewById(R.id.et_bill_account)
                childViewHolder!!.tv_submit = convertView!!.findViewById(R.id.tv_submit)
                childViewHolder!!.listview_billspayforserv = convertView!!.findViewById(R.id.listview_billspayforserv)



                childViewHolder!!.tv_submit!!.setOnClickListener(View.OnClickListener {

                    name_string = childViewHolder!!.et_bill_name!!.text.toString()
                    account_string = childViewHolder!!.et_bill_account!!.text.toString()

                    Log.e("CHILD_ITEM_NAMES", "" + name_string + "------" + account_string)

                    if (hasValidCredentials()) {

                        if (service_update_stg.equals("update_service")) {
                            callupdateAccountAPI(name_string, account_string)
                        } else {
                            callInsertData(name_string, account_string)
                        }


                    }

                })



                convertView.setTag(R.layout.list_item_citypayforservchilditem, childViewHolder)

            } else {

                childViewHolder = convertView
                        .getTag(R.layout.list_item_citypayforservchilditem) as ChildViewHolder


                childViewHolder = ChildViewHolder()
                childViewHolder!!.et_bill_name = convertView!!.findViewById(R.id.et_bill_name)
                childViewHolder!!.et_bill_account = convertView!!.findViewById(R.id.et_bill_account)
                childViewHolder!!.tv_submit = convertView!!.findViewById(R.id.tv_submit)
                childViewHolder!!.listview_billspayforserv = convertView!!.findViewById(R.id.listview_billspayforserv)

                childViewHolder!!.et_bill_name!!.setText("").toString()
                childViewHolder!!.et_bill_account!!.setText("").toString()
            }


            return convertView
        }

        override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
            return false
        }

        override fun hasStableIds(): Boolean {
            return false
        }

        inner class GroupViewHolder {

            internal var tv_headername_city_payforser: TextView? = null

        }

        inner class ChildViewHolder {

            internal var et_bill_name: EditText? = null
            internal var et_bill_account: EditText? = null
            internal var tv_submit: TextView? = null
            internal var listview_billspayforserv: ListView? = null
        }

        private fun hasValidCredentials(): Boolean {
            if (TextUtils.isEmpty(name_string))
                childViewHolder!!.et_bill_name!!.setError("Name Required")
            else if (TextUtils.isEmpty(account_string))
                childViewHolder!!.et_bill_account!!.setError("Account Required")
            else if (city_id.equals(""))
                Toast.makeText(context, "Please Select Your City", Toast.LENGTH_SHORT).show()
            else
                return true
            return false
        }
    }


    private fun callGetAllServiceTypesList(service_id: String) {
        my_loader.show()
        getservicelisttdata.clear()
        val apiService = ApiInterface.create()
        val call = apiService.getAllServicesTypes(service_id)
        call.enqueue(object : Callback<GetAllServiceTypesResponse> {
            override fun onResponse(call: Call<GetAllServiceTypesResponse>, response: retrofit2.Response<GetAllServiceTypesResponse>?) {
                my_loader.dismiss()
                Log.w("GETALLSERVICETYPES", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        my_loader.dismiss()
                        val list: Array<GetAllServiceTypesDataResponse> = response.body()!!.data!!
                        spinner_services_array.add("Select Service Type")
                        spinner_services_array_ids.add("0")
                        for (item: GetAllServiceTypesDataResponse in list.iterator()) {
                            getservicelisttdata.add(item)
                            spinner_services_array.add(item.service_type!!)
                            spinner_services_array_ids.add(item.id!!)
                            Log.w("item.id", "*** " + item.service_type + "---" + item.id)
                        }
                        spinner_service_type_id.visibility = View.VISIBLE
                        service_spinner_adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, spinner_services_array)
                        service_spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        spinner_service_type_id.adapter = service_spinner_adapter

                    }

                } else {
                    // Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<GetAllServiceTypesResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun callgetAllService(service_id: String) {
        my_loader.show()
        //listDataHeader.clear()
        val apiService = ApiInterface.create()
        val call = apiService.getAllService(service_id)
        call.enqueue(object : Callback<GetAllServiceResponse> {
            override fun onResponse(call: Call<GetAllServiceResponse>, response: retrofit2.Response<GetAllServiceResponse>?) {
                my_loader.dismiss()
                Log.w("GETALLSERVICETYPES", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        listview_citypayforservice!!.visibility = View.VISIBLE
                        my_loader.dismiss()
                        //val list: Array<GetAllServiceDataResponse> = response.body()!!.data!!
                        listDataHeader = java.util.ArrayList()
                        listDataHeader1 = java.util.ArrayList()
                        listData_paytype = ArrayList()
                        listData_serviceamount = ArrayList()
                        listDataChild = HashMap()
                        val list: Array<GetAllServiceDataResponse>? = response.body()!!.data!!


                        if (list!!.size > 0) {
                            for (j in 0 until list.size) {

                                listDataHeader1.add(list.get(j).service_name.toString())
                                listDataHeader.add(list.get(j).id.toString())
                                listData_paytype.add(list.get(j).pay_type.toString())
                                listData_serviceamount.add(list.get(j).servive_amount.toString())

                                val top = java.util.ArrayList<GetAllServiceDataResponse>()
                                top.add(list.get(j))
                                Log.w("Result_Order_details", list.get(j).id.toString())

                                listDataChild.put(listDataHeader[j], top)

                                selected_service_type = list.get(j).service_type.toString()
                                //selected_service_id = list.get(j).id.toString()
                                Log.e("selected_type", selected_service_type.toString())
                            }
                            Log.e("stdferer", "" + listDataChild.size)
                            listAdapter = ExpListViewAdapterWithCheckbox(activity!!, listDataHeader, listDataChild)

                            // setting list adapter
                            listview_citypayforservice!!.setAdapter(listAdapter)
                            listAdapter.notifyDataSetChanged()


                            val param: ViewGroup.LayoutParams = listview_citypayforservice!!.getLayoutParams()

                            param.height = 1200 * list.size

                            listview_citypayforservice!!.setLayoutParams(param)

                            listview_citypayforservice!!.refreshDrawableState()

                        }

                    } else {
                        listDataHeader1.clear()
                    }

                } else {
                    //listDataHeader1.clear()
                    listview_citypayforservice!!.visibility = View.GONE

                }
            }

            override fun onFailure(call: Call<GetAllServiceResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun callInsertData(name: String, account: String) {
        val apiService = ApiInterface.create()
        my_loader.show()
        Log.w("Result_INSERTDATA", "Result:" + city_id + "---" + selected_service_type.toString() + "---" + final_service_id.toString() + "----" + name + "---" + account + "---" + sessionManager.isId)
        val call = apiService.insertData(city_id, selected_service_type.toString(), final_service_id, name, account, sessionManager.isId)

        call.enqueue(object : Callback<InsertServiceResponse> {
            override fun onResponse(call: Call<InsertServiceResponse>, response: retrofit2.Response<InsertServiceResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_INSERT_RESPONSE", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        Log.w("Result_INSERT_RESPONSE", "Result : " + response.body()!!.status)
                        //Toast.makeText(activity, ""+response.body()!!.result, Toast.LENGTH_SHORT).show()

                        my_loader.dismiss()
                        val list: InsertServiceDataResponse = response.body()!!.data!!
                        val serviceid = list.id
                        childViewHolder!!.et_bill_name!!.setText("").toString()
                        childViewHolder!!.et_bill_account!!.setText("").toString()
                        Log.d("GETSERVICEBYID_BELOw", "" + serviceid.toString())

                        callgetServiceListById(final_service_id)

                    }

                }
            }

            override fun onFailure(call: Call<InsertServiceResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


    private fun callupdateAccountAPI(name: String, account: String) {
        val apiService = ApiInterface.create()
        my_loader.show()
        Log.w("Result_INSERTDATA", "Result:" + city_id + "---" + selected_service_type.toString() + "---" + final_service_id.toString() + "----" + name + "---" + account + "---" + sessionManager.isId)
        val call = apiService.updateAccountAPI(getServiceId_stg, name, account)

        call.enqueue(object : Callback<getServiceDetailsResponse> {
            override fun onResponse(call: Call<getServiceDetailsResponse>, response: retrofit2.Response<getServiceDetailsResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_INSERT_RESPONSE", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        Log.w("Result_INSERT_RESPONSE", "Result : " + response.body()!!.status)
                        //Toast.makeText(activity, ""+response.body()!!.result, Toast.LENGTH_SHORT).show()

                        my_loader.dismiss()
                        val list: getServiceDetailsDataResponse = response.body()!!.data!!
                        val serviceid = list.id
                        childViewHolder!!.et_bill_name!!.setText("").toString()
                        childViewHolder!!.et_bill_account!!.setText("").toString()
                        Log.d("GETSERVICEBYID_BELOw", "" + serviceid.toString())

                        callgetServiceListById(final_service_id)
                    }

                }
            }

            override fun onFailure(call: Call<getServiceDetailsResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

    private fun calldeleteAccountAPI(id: String) {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.deleteAccountAPI(id)

        call.enqueue(object : Callback<getServiceDetailsResponse> {
            override fun onResponse(call: Call<getServiceDetailsResponse>, response: retrofit2.Response<getServiceDetailsResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_INSERT_RESPONSE", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {

                        callgetServiceListById(final_service_id)
                    }

                }
            }

            override fun onFailure(call: Call<getServiceDetailsResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


    private fun callCitySearchList() {
        my_loader.show()
        searchlistdata.clear()
        search_names.clear()
        val apiService = ApiInterface.create()
        val call = apiService.getAllCities(search_string)
        Log.d("City REQUEST", call.toString() + "------" + search_string)
        call.enqueue(object : Callback<SearchCityListResponse> {
            override fun onResponse(call: Call<SearchCityListResponse>, response: retrofit2.Response<SearchCityListResponse>?) {
                my_loader.dismiss()
                Log.w("city Status", "*** " + response!!.body()!!.status)
                if (response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        my_loader.dismiss()
                        val list: Array<SearchCityListDataResponse> = response.body()!!.data!!

                        for (item: SearchCityListDataResponse in list.iterator()) {
                            searchlistdata.add(item)
                            search_names.add(item.city!!)
                            search_ids.add(item.id!!)

                        }
                        //setProductAdapter(searchlistdata)
                        if (searchlistdata.size == 0) {
                            my_loader.dismiss()

                        }
                        search_autocomplete.setAdapter(adapteo)
                    }

                } else {
                    my_loader.dismiss()
                    // Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<SearchCityListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun callStateSearchList() {
        // my_loader.show()
        searchstatelistdata.clear()
        search_names_state.clear()
        val apiService = ApiInterface.create()
        val call = apiService.getAllStates(search_string)
        Log.d("State REQUEST", call.toString() + "------" + search_string)
        call.enqueue(object : Callback<SearchStateListResponse> {
            override fun onResponse(call: Call<SearchStateListResponse>, response: retrofit2.Response<SearchStateListResponse>?) {
                // my_loader.dismiss()
                Log.w("StateStatus", "*** " + response!!.body()!!.status)
                if (response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        my_loader.dismiss()
                        val list: Array<SearchStateListDataResponse> = response.body()!!.data!!

                        for (item: SearchStateListDataResponse in list.iterator()) {
                            searchstatelistdata.add(item)
                            search_names_state.add(item.state!!)
                            search_ids_state.add(item.id!!)
                        }
                        //setProductAdapter(searchlistdata)
                        if (searchstatelistdata.size == 0) {
                            my_loader.dismiss()

                        }
                        search_autocomplete_state.setAdapter(adapteo_state)
                    }

                } else {
                    // my_loader.dismiss()
                    // Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<SearchStateListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }



    private fun callCountySearchList() {
        // my_loader.show()
        searchstatelistdata.clear()
        search_names_county.clear()
        val apiService = ApiInterface.create()
        val call = apiService.getAllCounties(search_string)
        Log.d("State REQUEST", call.toString() + "------" + search_string)
        call.enqueue(object : Callback<SearchCountyListResponse> {
            override fun onResponse(call: Call<SearchCountyListResponse>, response: retrofit2.Response<SearchCountyListResponse>?) {
                // my_loader.dismiss()
                Log.w("StateStatus", "*** " + response!!.body()!!.status)
                if (response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        my_loader.dismiss()
                        val list: Array<SearchCountyListDataResponse> = response.body()!!.data!!

                        for (item: SearchCountyListDataResponse in list.iterator()) {
                            searchcountylistdata.add(item)
                            search_names_county.add(item.county!!)
                            search_ids_county.add(item.id!!)
                        }
                        //setProductAdapter(searchlistdata)
                        if (searchcountylistdata.size == 0) {
                            my_loader.dismiss()

                        }
                        search_autocomplete_county.setAdapter(adapteo_county)
                    }

                } else {
                    // my_loader.dismiss()
                    // Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<SearchCountyListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


}