package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.*
import android.support.v7.widget.Toolbar
import android.text.Html
import android.util.Log
import android.view.*
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.CustomTextViewBold
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.getNewsDetailViewResponse
import com.gov.biil.model.apiresponses.getNewsListResponse
import com.gov.biil.model.apiresponses.newsDetailViewDataResponse
import com.squareup.picasso.Picasso
import model.apiresponses.getAllCommentsReplyDataResponse
import model.apiresponses.getAllCommentsbyNewsIdDataResponse
import model.apiresponses.getAllCommentsbyNewsIdResponse
import model.apiresponses.postCommentResponse
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*

class DashboardPostDetailViewActivity : AppCompatActivity() {

    internal lateinit var sessionManager: SessionManager
    var news_id = ""
    var title_link = ""
    var author_link = ""
    var title_name = ""
    var et_comments_stg = ""
    var et_reply_comments_stg = ""
    lateinit var iv_postimage: ImageView
    lateinit var username_txt_view: CustomTextView
    lateinit var user_author_txt_view: CustomTextView
    lateinit var tv_posted_time: CustomTextView
    lateinit var tv_user_title: CustomTextView
    lateinit var tv_description_main: CustomTextView
    lateinit var tv_likes_count: CustomTextView
    lateinit var tv_share_count: CustomTextView
    lateinit var iv_likes_image: ImageView
    lateinit var iv_profile_image: ImageView
    lateinit var et_comments: EditText
    lateinit var btn_postcomment: ImageView
    lateinit var ll_comments_post: LinearLayout
    internal lateinit var my_loader: Dialog

    internal lateinit var dialog_list2: Dialog

    var newscommentlistArray = java.util.ArrayList<getAllCommentsbyNewsIdDataResponse>()

    lateinit var rv_newscommentlist: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_image_detailview)

        sessionManager = SessionManager(this)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        myloading()
        news_id = intent.getStringExtra("news_id")

        if(!intent.getStringExtra("title_link").equals("")){
            title_link = intent.getStringExtra("title_link")
        }
        if(!intent.getStringExtra("author_link").equals("")){
            author_link = intent.getStringExtra("author_link")
        }

        if(!intent.getStringExtra("title_link").equals("")){
            title_link = intent.getStringExtra("title_link")
        }
        if(!intent.getStringExtra("title_name").equals("")){
            title_name = intent.getStringExtra("title_name")
        }

        Log.d("NEWSLIST", sessionManager.isId + "----" + news_id)

        getNewsDetaiViewAPI()

        iv_postimage = findViewById(R.id.iv_postimage) as ImageView
        username_txt_view = findViewById(R.id.username_txt_view) as CustomTextView
        user_author_txt_view = findViewById(R.id.user_author_txt_view) as CustomTextView
        tv_posted_time = findViewById(R.id.tv_posted_time) as CustomTextView
        tv_user_title = findViewById(R.id.tv_user_title) as CustomTextView
        tv_description_main = findViewById(R.id.tv_description_main) as CustomTextView
        tv_likes_count = findViewById(R.id.tv_likes_count) as CustomTextView
        tv_share_count = findViewById(R.id.tv_share_count) as CustomTextView
        iv_likes_image = findViewById(R.id.iv_likes_image) as ImageView
        iv_profile_image = findViewById(R.id.iv_profile_image) as ImageView
        et_comments = findViewById(R.id.et_comments) as EditText
        btn_postcomment = findViewById(R.id.btn_postcomment) as ImageView
        ll_comments_post = findViewById(R.id.ll_comments_post) as LinearLayout

        rv_newscommentlist = findViewById(R.id.rv_newscommentlist) as RecyclerView
        rv_newscommentlist!!.isNestedScrollingEnabled = false





        tv_user_title.setOnClickListener {

            if(title_link == "" || title_link == null || title_link == "null" ){

            }else{
                val intent = Intent(this, NewsTitleWebActivity::class.java)
                intent.putExtra("title_link", title_link)
                intent.putExtra("title_name", title_name)
                startActivity(intent)
            }

        }


        username_txt_view.setOnClickListener {

            if(author_link == "" || author_link == null || author_link == "null" ){

            }else{
                val intent = Intent(this, NewsTitleWebActivity::class.java)
                intent.putExtra("title_link", author_link)
                intent.putExtra("title_name", title_name)
                startActivity(intent)
            }

        }


        iv_likes_image.setOnClickListener {
            getNewsLikeStatusAPI(sessionManager.isId,news_id)

        }


        btn_postcomment.setOnClickListener {
            et_comments_stg = et_comments.text.toString()
            if(et_comments_stg.isEmpty()){
                Toast.makeText(this@DashboardPostDetailViewActivity, "Please Enter Comment", Toast.LENGTH_SHORT).show()
            }else{

                postCommentAPI(news_id,sessionManager.isId,et_comments_stg)

            }


        }





    }


    private fun getNewsDetaiViewAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getNewsListDetailView(sessionManager.isId, news_id)
        Log.d("NEWSLISTDETAIL", sessionManager.isId +"----"+news_id)
        call.enqueue(object : Callback<getNewsDetailViewResponse> {
            override fun onResponse(call: Call<getNewsDetailViewResponse>, response: retrofit2.Response<getNewsDetailViewResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("NEWSLIST_RESPONSE_STATUS", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.newsdetailview != null) {
                            Log.w("NEWSLIST_RESPONSE_RESULT", "Result : " + response.body()!!.newsdetailview)
                            // setProfileDatatoView(response.body()!!.user_info)
                            val list: newsDetailViewDataResponse? = response.body()!!.newsdetailview!!

                            Picasso.with(this@DashboardPostDetailViewActivity)
                                    .load("https://www.i1gov.com/uploads/news/" + list!!.image)
                                    .into(iv_postimage)

                            username_txt_view.setText(list.posted_by)
                            user_author_txt_view.setText(list.author_profession)
                            tv_user_title.setText(list.title)
                           // tv_description_main.setText(list.news)
                            tv_likes_count.setText(list.likes_count)


                            Picasso.with(this@DashboardPostDetailViewActivity)
                                    .load("https://www.i1gov.com/uploads/news/" + list.posted_by_image)
                                    .error(R.drawable.user_icon)
                                    .into(iv_profile_image)


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                tv_description_main.setText(Html.fromHtml(list.news, Html.FROM_HTML_MODE_COMPACT))
                            } else {
                                tv_description_main.setText(Html.fromHtml(list.news))
                            }


                            /* if (list.like_status.equals("1")) {
                                 iv_likes_image.setImageDrawable(resources.getDrawable(R.drawable.like))
                             } else {
                                 iv_likes_image.setImageDrawable(resources.getDrawable(R.drawable.unlike))
                             }*/


                            val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                            val currentDateandTime = format.format(Date())
                            var d1: Date? = null
                            var d2: Date? = null
                            try {
                                d1 = format.parse(currentDateandTime)
                                d2 = format.parse(list.created_date)
                                //in milliseconds
                                val diff = d1!!.time - d2!!.time
                                val diffSeconds = diff / 1000 % 60
                                val diffMinutes = diff / (60 * 1000) % 60
                                val diffHours = diff / (60 * 60 * 1000) % 24
                                val diffDays = diff / (24 * 60 * 60 * 1000)
                                Log.e("FINALNEWSTIME", "DAYS" + diffDays + "HOURS" + diffHours + "MIN" + diffMinutes + "SEC" + diffSeconds);

                                tv_posted_time.setText(diffHours.toString() + "hrs"+diffMinutes.toString() + "min")

                               /* if(diffHours.toString()!=null){
                                    tv_posted_time.setText(diffHours.toString() + "hrs")
                                }else if(diffMinutes.toString()!=null){
                                    tv_posted_time.setText(diffMinutes.toString() + "min")
                                }else if(diffSeconds.toString()!=null){
                                    tv_posted_time.setText(diffSeconds.toString() + "sec")
                                }*/




                            } catch (e: Exception) {
                                e.printStackTrace()
                            }


                            getAllCommentsByNewsIdAPI()

                            /*if (newsdetaillistArray.size == 0) {
                                rv_newsdetail_list!!.visibility = View.GONE
                            }*/
                        }

                    }

                }
            }

            override fun onFailure(call: Call<getNewsDetailViewResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }

        })
    }


    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


    private fun getNewsLikeStatusAPI(user_id: String, news_id: String) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getNewsLikeStatus(user_id, news_id)
        Log.d("NEWSLIKEPARAMS", sessionManager.isId + "-------" + news_id)
        call.enqueue(object : Callback<getNewsListResponse> {
            override fun onResponse(call: Call<getNewsListResponse>, response: retrofit2.Response<getNewsListResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("NEWSLIKESTATUS_RESPONSE_STATUS", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        //  holder?.iv_like_status.setImageDrawable(context.resources.getDrawable(R.drawable.like))

                        //getNewsLikeStatusAPI(sessionManager.isId,news_id)

                        if(response.body()!!.status.equals("1")){
                            iv_likes_image.setImageDrawable(resources.getDrawable(R.drawable.like))
                        }else {
                            iv_likes_image.setImageDrawable(resources.getDrawable(R.drawable.unlike))
                        }

                        getNewsDetaiViewAPI()


                    } else if (response.body()!!.status.equals("2")) {

                        //getNewsDetaiViewAPI()
                        //getNewsLikeStatusAPI(sessionManager.isId,news_id)

                        getNewsDetaiViewAPI()

                    }

                }
            }

            override fun onFailure(call: Call<getNewsListResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }

        })
    }



    private fun postCommentAPI(news_id: String, user_id: String,comment: String) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.postComment(news_id, user_id,comment)
        Log.d("COMMENTSPARAMS", news_id + "-------" + user_id+"--------"+comment)
        call.enqueue(object : Callback<postCommentResponse> {
            override fun onResponse(call: Call<postCommentResponse>, response: retrofit2.Response<postCommentResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("POSTCOMMENTS_RESPONSE_STATUS", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        getNewsDetaiViewAPI()
                        et_comments.text.clear()
                    } else if (response.body()!!.status.equals("2")) {
                        getNewsDetaiViewAPI()

                    }

                }
            }
            override fun onFailure(call: Call<postCommentResponse>, t: Throwable) {
                Log.w("POSTCOMMENTS_RESPONSE_FAILED", t.toString())
            }

        })
    }




    private fun getAllCommentsByNewsIdAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAllCommentsByNewsId(news_id)
        Log.d("NEWSLIST", sessionManager.isId)
        newscommentlistArray.clear()
        call.enqueue(object : Callback<getAllCommentsbyNewsIdResponse> {
            override fun onResponse(call: Call<getAllCommentsbyNewsIdResponse>, response: retrofit2.Response<getAllCommentsbyNewsIdResponse>?) {

                try {
                    if (response != null) {
                        my_loader.dismiss()

                        if (response.body()!!.status.equals("1")) {
                            Log.w("NEWSLIST_RESPONSE_STATUS", "Result : " + response.body()!!.status)
                            if (response.body()!!.data != null) {
                                Log.w("NEWSLIST_RESPONSE_RESULT", "Result : " + response.body()!!.data)
                                // setProfileDatatoView(response.body()!!.user_info)
                                val list: Array<getAllCommentsbyNewsIdDataResponse>? = response.body()!!.data!!
                                for (item: getAllCommentsbyNewsIdDataResponse in list!!.iterator()) {
                                    newscommentlistArray.add(item)
                                }
                                Log.w("NEWSLISTARRAYSIZE", "Result : " + newscommentlistArray.size)
                                val news_list_adapter = NewsCommentsDetailsRecyclerAdapter(newscommentlistArray, this@DashboardPostDetailViewActivity!!)
                                rv_newscommentlist.setAdapter(news_list_adapter)
                                news_list_adapter.notifyDataSetChanged()
                                rv_newscommentlist.setHasFixedSize(true)
                                rv_newscommentlist.setLayoutManager(LinearLayoutManager(this@DashboardPostDetailViewActivity))
                                rv_newscommentlist.setItemAnimator(DefaultItemAnimator())

                                /* for (i in 0 until list!!.size) {

                                 setNewsListAdapter(newslistArray)
                                 rv_newslist!!.setHasFixedSize(true)
                                 val layoutManager = LinearLayoutManager(context);
                                 rv_newslist!!.setLayoutManager(layoutManager)
                                 rv_newslist!!.setItemAnimator(DefaultItemAnimator())
                                 val news_list_adapter = NewsDetailsRecyclerAdapter(newslistArray, context!!)
                                 rv_newslist!!.setAdapter(news_list_adapter)
                                 news_list_adapter.notifyDataSetChanged()

                             }
 */
                                Log.d("NEWSARRAYSIZE", newscommentlistArray.size.toString())


                                if (newscommentlistArray.size == 0) {
                                    //noservice!!.visibility = View.VISIBLE
                                    rv_newscommentlist!!.visibility = View.GONE
                                }
                            }

                        } else {
                            Toast.makeText(this@DashboardPostDetailViewActivity, "Response Failed", Toast.LENGTH_SHORT).show()
                        }

                    } else {
                        Toast.makeText(this@DashboardPostDetailViewActivity, "Response Failed", Toast.LENGTH_SHORT).show()
                    }

                } catch (e: java.lang.Exception) {
                    Log.w("Result_Profile_Exception", e.toString())
                }
            }

            override fun onFailure(call: Call<getAllCommentsbyNewsIdResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }

        })
    }


    inner class NewsCommentsDetailsRecyclerAdapter(val mServiceList: ArrayList<getAllCommentsbyNewsIdDataResponse>, val context: Context) : RecyclerView.Adapter<ViewHolder2>() {


        internal lateinit var my_loader: Dialog
        internal lateinit var sessionManager: SessionManager
        var comment_id_string = ""

        var arrowdownup = false
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder2 {
            return ViewHolder2(LayoutInflater.from(context).inflate(R.layout.list_item_newscomments_adapter, parent, false))

        }

        override fun onBindViewHolder(holder: ViewHolder2, position: Int) {


            sessionManager = SessionManager(context)
            myloading()


            if(sessionManager.isId.equals(mServiceList.get(position).user_id)){
                holder?.tv_reply.visibility = View.VISIBLE
            }else{
                holder?.tv_reply.visibility = View.GONE
            }

            holder?.tv_user_comment.setText(mServiceList.get(position).comment)
            holder?.tv_user_name.setText(mServiceList.get(position).username)



            val inputFormat = SimpleDateFormat("yyyy-MM-dd hh:mm")
            val outputFormat = SimpleDateFormat("MM-dd-yyyy hh:mm")
            val inputDateStr = mServiceList.get(position).created_at
            val date = inputFormat.parse(inputDateStr)
            val outputDateStr = outputFormat.format(date)


            holder?.tv_user_comment_date.setText(outputDateStr+" min")

            holder?.tv_user_name.setText(mServiceList.get(position).username)


            val list: Array<getAllCommentsReplyDataResponse>? = mServiceList.get(position).reply_data
            Log.e("REPLYDATA_SIZE",""+list!!.size)
            if(list!!.size < 1){
                holder?.tv_admin_comment.visibility = View.GONE
                holder?.tv_admin.visibility = View.GONE
            }else{
                for (item: getAllCommentsReplyDataResponse in list!!.iterator()) {
                    Log.e("MANIBABU",item.created_at)

                   /* if(item.user_type.equals("admin")){
                        holder?.tv_admin.visibility = View.VISIBLE
                    }else {
                        holder?.tv_admin.visibility = View.GONE
                    }*/

                    if(item.user_type.equals("user")){

                        val inputFormat = SimpleDateFormat("yyyy-MM-dd hh:mm")
                        val outputFormat = SimpleDateFormat("MM-dd-yyyy hh:mm")
                        val inputDateStr = item.created_at
                        val date = inputFormat.parse(inputDateStr)
                        val outputDateStr = outputFormat.format(date)


                        val a = LinearLayout(context);
                        val cardview = CardView(context)

                        val params = LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                        params.setMargins(5,5,5,5)
                        cardview.setCardElevation(9F);
                        cardview.setMaxCardElevation(15F);
                        cardview.setContentPadding(15, 15, 15, 15);
                        cardview.setLayoutParams(params);

                        a.setOrientation(LinearLayout.VERTICAL);
                        val textView2 = TextView(context);
                        val textViewTime = TextView(context);
                        textViewTime.setText(outputDateStr+" min")
                        textViewTime.setTextColor(resources.getColor(R.color.timer_inner_bg))
                        textViewTime.setTextSize(12f)

                        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                        layoutParams.gravity = Gravity.LEFT;
                        a.setPadding(10, 10, 10, 10); // (left, top, right, bottom)
                        a.setLayoutParams(layoutParams);
                        textViewTime.setLayoutParams(layoutParams)
                        textView2!!.text = item.comment


                        cardview.addView(textView2)
                        a.addView(cardview)

                        a.addView(textViewTime)

                        holder?.ll_admin_comments.addView(a);

                    }else if(item.user_type.equals("admin")){

                        val inputFormat = SimpleDateFormat("yyyy-MM-dd hh:mm")
                        val outputFormat = SimpleDateFormat("MM-dd-yyyy hh:mm")
                        val inputDateStr = item.created_at
                        val date = inputFormat.parse(inputDateStr)
                        val outputDateStr = outputFormat.format(date)


                        val a = LinearLayout(context);
                        val cardview = CardView(context)

                        val params = LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                        params.setMargins(5,5,5,5)
                        cardview.setCardElevation(9F)
                        cardview.setMaxCardElevation(15F)
                        cardview.setContentPadding(15, 15, 15, 15);
                        cardview.setLayoutParams(params);
                        a.setOrientation(LinearLayout.VERTICAL);
                        val textView2 = TextView(context)
                        val textView3 = TextView(context)
                        val textView_time = TextView(context);
                        textView3.setText("Admin")
                        textView_time.setText(outputDateStr+" min")
                        val tf = Typeface.createFromAsset(context.assets, "fonts/Raleway-Bold.ttf")
                        textView3.setTypeface(tf)
                        textView3.setTextColor(resources.getColor(R.color.black))
                        textView_time.setTextColor(resources.getColor(R.color.timer_inner_bg))
                        textView_time.setTextSize(12f)
                        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                        layoutParams.gravity = Gravity.RIGHT;
                        a.setPadding(10, 10, 10, 10); // (left, top, right, bottom)
                        a.setLayoutParams(layoutParams)
                        textView3.setLayoutParams(layoutParams)
                        textView_time.setLayoutParams(layoutParams)
                        textView2!!.text = item.comment
                        cardview.addView(textView2)
                        a.addView(textView3)
                        a.addView(cardview)
                        a.addView(textView_time)

                        holder?.ll_admin_comments.addView(a)

                    }

                }

            }


            dialog_list2 = Dialog(context)
            dialog_list2.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog_list2.setContentView(R.layout.dialog_news_comment)
            dialog_list2.setCancelable(true)
            val et_replydata_comment = dialog_list2.findViewById(R.id.et_replydata_comment) as EditText
            val btn_replydata_postcomment = dialog_list2.findViewById(R.id.btn_replydata_postcomment) as CustomTextView
            val iv_close_dialog_service = dialog_list2.findViewById(R.id.iv_close_dialog_service) as ImageView
            iv_close_dialog_service.setOnClickListener {
                dialog_list2.dismiss()
            }

            holder?.tv_reply.setOnClickListener {

                dialog_list2.show()

                comment_id_string = mServiceList.get(position).comment_id.toString()


            }

            btn_replydata_postcomment.setOnClickListener {

                et_reply_comments_stg = et_replydata_comment.text.toString()
                if(et_reply_comments_stg.isEmpty()){
                    Toast.makeText(this@DashboardPostDetailViewActivity, "Please Enter Comment", Toast.LENGTH_SHORT).show()
                }else{
                    postReplydataCommentAPI(news_id,sessionManager.isId,et_reply_comments_stg,comment_id_string)
                }



            }


        }

        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return mServiceList.size
        }


        private fun myloading() {
            my_loader = Dialog(context)
            my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
            my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            my_loader.setCancelable(false);
            my_loader.setContentView(R.layout.mkloader_dialog)
        }


    }


    class ViewHolder2(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val tv_user_comment = view.findViewById(R.id.tv_user_comment) as CustomTextView
        val tv_reply = view.findViewById(R.id.tv_reply) as CustomTextView
        val tv_admin_comment = view.findViewById(R.id.tv_admin_comment) as CustomTextView
        val tv_admin = view.findViewById(R.id.tv_admin) as CustomTextViewBold
        val ll_admin_comments = view.findViewById(R.id.ll_admin_comments) as LinearLayout
        val tv_user_name = view.findViewById(R.id.tv_user_name) as CustomTextViewBold
        val tv_user_comment_date = view.findViewById(R.id.tv_user_comment_date) as CustomTextView


    }



    private fun postReplydataCommentAPI(news_id: String, user_id: String,comment: String,comment_id: String) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.replyComment(news_id, user_id,comment,comment_id)
        Log.d("REPLYCOMMENTSPARAMS", news_id + "-------" + user_id+"--------"+comment+"----"+comment_id)
        call.enqueue(object : Callback<postCommentResponse> {
            override fun onResponse(call: Call<postCommentResponse>, response: retrofit2.Response<postCommentResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("REPLY_POSTCOMMENTS_RESPONSE_STATUS", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {

                        dialog_list2.dismiss()
                        getNewsDetaiViewAPI()

                    } else if (response.body()!!.status.equals("2")) {
                        getNewsDetaiViewAPI()

                    }

                }
            }
            override fun onFailure(call: Call<postCommentResponse>, t: Throwable) {
                Log.w("POSTCOMMENTS_RESPONSE_FAILED", t.toString())
            }

        })
    }




}