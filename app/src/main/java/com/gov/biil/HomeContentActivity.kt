package com.gov.biil

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Window
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import android.widget.Toast
import com.gov.a4print.Session.SessionManager


class HomeContentActivity : AppCompatActivity() {


    var uri: Uri? = null
    lateinit var mywebview: WebView
    internal lateinit var my_loader: Dialog
    internal var paylink: String? = ""
    internal lateinit var sessionManager: SessionManager

    var form_screen_stg = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.countryfragment)


        val toolbar = findViewById(R.id.toolbar) as Toolbar
        val toolbar_title = findViewById(R.id.toolbar_title) as TextView




        (this as AppCompatActivity).setSupportActionBar(toolbar)
        (this as AppCompatActivity).supportActionBar!!.setHomeButtonEnabled(true)
        (this as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (this as AppCompatActivity).supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }



        mywebview = findViewById<WebView>(R.id.webview_pricecalc)
        form_screen_stg = intent.getStringExtra("form_screen")

        myloading()
        sessionManager = SessionManager(this)
        Log.w("CITYURL", "" + sessionManager.cityurl)
        Log.w("MANIBABU", "" + sessionManager.cityurl)
        if (form_screen_stg.equals("City")) {
            toolbar_title.setText("City")
            if(sessionManager.cityurl!=""){

                paylink = ""+sessionManager.cityurl

                if (paylink!!.contains("http") || paylink!!.contains("https")) {
                    uri = Uri.parse(paylink)
                } else {
                    uri = Uri.parse("https://" + paylink)
                }

            }else{
                paylink = "https://www.blueisland.org/"

                if (paylink!!.contains("http") || paylink!!.contains("https")) {
                    uri = Uri.parse(paylink)
                } else {
                    uri = Uri.parse("https://" + paylink)
                }

            }
        }else if (form_screen_stg.equals("State")) {
            toolbar_title.setText("State")
            if(sessionManager.stateurl!=""){
                paylink = sessionManager.stateurl
                if (paylink!!.contains("http") || paylink!!.contains("https")) {
                    uri = Uri.parse(paylink)
                } else {
                    uri = Uri.parse("https://" + paylink)
                }
            }else{
                paylink = "https://www.blueisland.org/"

                if (paylink!!.contains("http") || paylink!!.contains("https")) {
                    uri = Uri.parse(paylink)
                } else {
                    uri = Uri.parse("https://" + paylink)
                }

            }
        }else if (form_screen_stg.equals("County")) {
            toolbar_title.setText("County")
            if(sessionManager.countyurl!=""){
                paylink = sessionManager.countyurl
                if (paylink!!.contains("http") || paylink!!.contains("https")) {
                    if(!paylink!!.startsWith("http://")){
                        paylink = "https://"+paylink;
                        Log.e("COUNTYHTTP",paylink)
                        uri = Uri.parse(paylink)
                    }
                    //uri = Uri.parse(paylink)
                } else {
                    Log.e("COUNTYHTTP",paylink)
                    uri = Uri.parse("https://" + paylink)
                }
            }else{
                paylink = "https://www.cookcountyil.gov/"

                if (paylink!!.contains("http") || paylink!!.contains("https")) {
                    uri = Uri.parse(paylink)
                } else {
                    uri = Uri.parse("https://" + paylink)
                }

            }
        }else if (form_screen_stg.equals("Federal")) {
            toolbar_title.setText("Federal")
            paylink = "https://www.usa.gov"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("https://" + paylink)
            }
        }



        /*mywebview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)

                return true
            }
        }*/
        mywebview!!.getSettings().setJavaScriptEnabled(true);
        mywebview!!.setHorizontalScrollBarEnabled(false);
        mywebview!!.getSettings().setDomStorageEnabled(true)
        mywebview!!.getSettings().setUseWideViewPort(true);
        mywebview!!.getSettings().setLoadWithOverviewMode(true);
        mywebview!!.loadUrl(uri.toString())

      /*  mywebview!!.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK && mywebview!!.canGoBack()) {
                    mywebview!!.goBack()
                    return true
                }
                return false
            }
        })*/

        val alertDialog = AlertDialog.Builder(this).create()

        mywebview.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                if (my_loader.isShowing()) {
                    my_loader.dismiss()
                }
            }

            override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                Toast.makeText(this@HomeContentActivity, "Oh no! $description", Toast.LENGTH_SHORT).show()
                alertDialog.setTitle("Error")
                alertDialog.setMessage(description)
                alertDialog.setButton("OK", DialogInterface.OnClickListener { dialog, which -> return@OnClickListener })
                alertDialog.show()
            }
        })




    }

    private fun myloading() {
        my_loader = Dialog(this@HomeContentActivity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(true);
        my_loader.setContentView(R.layout.mkloader_dialog)
        my_loader.show()
    }


    override fun onBackPressed() {
        if (mywebview!!.canGoBack()) {
            mywebview!!.goBack()
        } else {
            super.onBackPressed()

        }
        //super.onBackPressed()
    }
}
