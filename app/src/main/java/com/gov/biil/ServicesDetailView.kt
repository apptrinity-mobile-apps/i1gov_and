package com.gov.biil

import Helper.CustomTextView
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.StripePayServiceDataResponse
import com.gov.biil.model.apiresponses.StripePayServiceResponse
import com.gov.biil.model.apiresponses.getServiceDetailsDataResponse
import com.gov.biil.model.apiresponses.getServiceDetailsResponse
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*


class ServicesDetailView : AppCompatActivity(), AdapterView.OnItemSelectedListener {


    var spinner_list = arrayOf("Select Current or Past Due", "Current", "Past")
    var pay_type = ""
    var id = ""
    var service_id = ""
    var servive_amount = ""
    var service_price_int = 0.00
    var final_price = 0.00

    var address_stg = ""
    var date_stg = ""
    var due_current_past_stg = ""
    var ptype_wallet = ""
    var wallet_amount = ""
    var wallet_after_amount = 0.00
    var get_amount_stg=""
    var send_amount_id=""

    internal lateinit var sessionManager: SessionManager
    lateinit var tv_name_id: CustomTextView
    lateinit var tv_account_id: CustomTextView
    lateinit var et_amount_id: EditText
    lateinit var et_address_id: EditText
    lateinit var et_date_id: EditText
    lateinit var tv_subtotal_id: TextView
    lateinit var tv_service_amount_id: TextView
    lateinit var tv_total_amount_id: TextView
    lateinit var tv_pay_now_id: TextView
    lateinit var cb_wallet_id: RadioButton
    lateinit var tv_wallet_amount_id: TextView
    lateinit var payment_Dialog: Dialog
    lateinit var tv_ok_id: Button
    lateinit var tv_cancel_id: Button
    lateinit var tv_message_id: CustomTextView
    lateinit var tv_total_id: CustomTextView
    var cal = Calendar.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_services_detail_view)
        id = intent.getStringExtra("id")
        service_id = intent.getStringExtra("service_id")
        pay_type = intent.getStringExtra("pay_type")
        servive_amount = intent.getStringExtra("servive_amount")
        sessionManager = SessionManager(this@ServicesDetailView)
        wallet_amount = sessionManager.wallet_balance
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        val spin = findViewById(R.id.spinner) as Spinner
        spin.setOnItemSelectedListener(this@ServicesDetailView)

        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinner_list)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spin.adapter = aa

        myloading()
        getAdminStatusInfo()

        paymentalert()



        callgetServiceDetailsById()


        tv_name_id = findViewById(R.id.tv_name_id)
        tv_account_id = findViewById(R.id.tv_account_id)
        et_amount_id = findViewById(R.id.et_amount_id)
        et_address_id = findViewById(R.id.et_address_id)
        et_date_id = findViewById(R.id.et_date_id)
        tv_subtotal_id = findViewById(R.id.tv_subtotal_id)
        tv_service_amount_id = findViewById(R.id.tv_service_amount_id)
        tv_total_amount_id = findViewById(R.id.tv_total_amount_id)
        tv_pay_now_id = findViewById(R.id.tv_pay_now_id)
        cb_wallet_id = findViewById(R.id.cb_wallet_id)
        tv_wallet_amount_id = findViewById(R.id.tv_wallet_amount_id)
        tv_wallet_amount_id.setText("$"+sessionManager.wallet_balance)
        cb_wallet_id.setOnClickListener {

            if(sessionManager.wallet_balance.equals("0")){
                ptype_wallet = ""
                cb_wallet_id.isChecked=false
            }else if (sessionManager.wallet_balance.equals("0.00")){
                ptype_wallet = ""
                cb_wallet_id.isChecked=false
            }else{
                if (cb_wallet_id.isChecked) {
                    ptype_wallet = "wallet"

                } else {
                    ptype_wallet = ""
                }
            }
        }


        val service_date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)


            val myFormat = "MM/dd/yyyy" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.UK)
            Log.e("dateformat", sdf.toString())
            val date = sdf.format(cal.time)

            et_date_id.setText(date)

        }
        et_date_id.setOnClickListener {


            val datePickerDialog = DatePickerDialog(this, service_date,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));
            // Limiting access to past dates in the step below:
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show()

        }


        if (pay_type.equals("Fixed")) {
            service_price_int = servive_amount.toDouble()
            tv_service_amount_id.setText("$"+service_price_int.toString())

        } else if (pay_type.equals("Percentage")) {
            val ser_Percentage = (servive_amount.toDouble())
            tv_service_amount_id.setText(ser_Percentage.toString() + " %")
        }

        et_amount_id.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                if (!et_amount_id.text.toString().equals("")) {
                    val amount_enter = et_amount_id.text.toString().toDouble()
                    send_amount_id = et_amount_id.text.toString().toDouble().toString()
                    tv_subtotal_id.setText("$"+amount_enter.toString())
                    if (pay_type.equals("Fixed")) {
                        final_price = amount_enter + service_price_int
                        Log.e("final_price", final_price.toString())

                    } else if (pay_type.equals("Percentage")) {
                        service_price_int = amount_enter * (servive_amount.toDouble()) / 100
                        val just_percent_stg = servive_amount.toDouble()

                        final_price = amount_enter + service_price_int
                        Log.e("final_price_per", final_price.toString())

                        tv_service_amount_id.setText("$"+service_price_int.toString() + " ( " + just_percent_stg.toString() + "% )")
                    }
                    tv_total_amount_id.setText(final_price.toString())
                } else {
                    tv_subtotal_id.setText("0.00")
                    if (pay_type.equals("Fixed")) {
                        service_price_int = servive_amount.toDouble()
                        tv_service_amount_id.setText(service_price_int.toString())

                    } else if (pay_type.equals("Percentage")) {
                        val ser_Percentage = (servive_amount.toDouble())
                        tv_service_amount_id.setText(servive_amount + " %")
                    }
                    tv_total_amount_id.setText("0.00")
                }


            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        tv_pay_now_id.setOnClickListener {
            address_stg = et_address_id.text.toString()
            date_stg = et_date_id.text.toString()
            get_amount_stg = et_amount_id.text.toString()


            if(hasValidCredentials()){
                if (ptype_wallet.equals("wallet")) {
                    if (wallet_amount.toDouble()>=final_price){
                        ///service call
                        Log.e("wallet_amount",wallet_amount.toString()+"--"+final_price.toString())

                        payment_Dialog.show()
                        tv_total_id.setText("$"+final_price.toString())

                    }else if(wallet_amount.toDouble()< final_price){

                        val after_stripe_amount=final_price-wallet_amount.toDouble()
                        Log.e("after_stripe_amount",after_stripe_amount.toString()+"--"+final_price.toString())

                        val intent = Intent(this, StripePayment::class.java)
                        intent.putExtra("amount", get_amount_stg)
                        intent.putExtra("from", "services")
                        intent.putExtra("ptype_wallet", ptype_wallet)
                        intent.putExtra("id", id)
                        intent.putExtra("service_id", service_id)
                        intent.putExtra("address_stg", address_stg)
                        intent.putExtra("due_current_past_stg", due_current_past_stg)
                        intent.putExtra("after_amount", after_stripe_amount.toString())
                        startActivity(intent)

                    }

                } else {
                    val intent = Intent(this, StripePayment::class.java)
                    intent.putExtra("amount", get_amount_stg)
                    intent.putExtra("from", "services")
                    intent.putExtra("ptype_wallet", ptype_wallet)
                    intent.putExtra("id", id)
                    intent.putExtra("service_id", service_id)
                    intent.putExtra("address_stg", address_stg)
                    intent.putExtra("due_current_past_stg", due_current_past_stg)
                    intent.putExtra("after_amount", final_price.toString())
                    startActivity(intent)
                }
            }

            //callStripePayServiceAPI()
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

       // Toast.makeText(getApplicationContext(), spinner_list[position], Toast.LENGTH_LONG).show();
        due_current_past_stg = spinner_list[position]
    }

    private fun callgetServiceDetailsById() {
         my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAccountDetailsAPI(id)
        call.enqueue(object : Callback<getServiceDetailsResponse> {
            override fun onResponse(call: Call<getServiceDetailsResponse>, response: retrofit2.Response<getServiceDetailsResponse>?) {
                my_loader.dismiss()
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        //my_loader.dismiss()
                        val list: getServiceDetailsDataResponse = response.body()!!.data!!
                        tv_name_id.setText(list!!.name)
                        tv_account_id.setText(list!!.account)
                    }

                } else {
                    //Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }

            }

            override fun onFailure(call: Call<getServiceDetailsResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun callStripePayServiceAPI() {
         my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.StripePayServiceAPI(id,service_id, sessionManager.isId, final_price.toString(), ptype_wallet, address_stg, "", due_current_past_stg)
        call.enqueue(object : Callback<StripePayServiceResponse> {
            override fun onResponse(call: Call<StripePayServiceResponse>, response: retrofit2.Response<StripePayServiceResponse>?) {
                my_loader.dismiss()
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        val list: StripePayServiceDataResponse = response.body()!!.data!!
                        Toast.makeText(this@ServicesDetailView, "Payment Success", Toast.LENGTH_SHORT).show()

                        /*val intent = Intent(this@ServicesDetailView, MainActivity::class.java)
                        startActivity(intent)*/

                        val intent=Intent(this@ServicesDetailView,PaymetDetails::class.java)
                        intent.putExtra("bill_id",list.id)
                        intent.putExtra("bill_type",list.service_type)
                        startActivity(intent)
                    }

                } else {
                    //Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<StripePayServiceResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(address_stg))
        Toast.makeText(this, "Address Required", Toast.LENGTH_SHORT).show()
        else if (TextUtils.isEmpty(date_stg))
            Toast.makeText(this, "Please Select Date", Toast.LENGTH_SHORT).show()
        else if (get_amount_stg.equals(""))
            Toast.makeText(this, "Please Enter Amount", Toast.LENGTH_SHORT).show()
        else if (due_current_past_stg.equals("Select Current or Past Due"))
            Toast.makeText(this, "Please Select Due", Toast.LENGTH_SHORT).show()
        else
            return true
        return false
    }
    fun paymentalert(){
        payment_Dialog = Dialog(this)
        payment_Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        payment_Dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        payment_Dialog.setCancelable(false);
        payment_Dialog.setContentView(R.layout.paynow_dialog_layout)
        tv_message_id=payment_Dialog.findViewById(R.id.tv_message_id)
        //tv_message_id.setTextColor(resources.getColor(R.color.green))
        //tv_message_id.setText("Success")
        tv_ok_id=payment_Dialog.findViewById(R.id.tv_ok_id)
        tv_cancel_id=payment_Dialog.findViewById(R.id.tv_cancel_id)
        tv_total_id=payment_Dialog.findViewById(R.id.tv_total_id)

        tv_ok_id.setOnClickListener {
            payment_Dialog.dismiss()
            callStripePayServiceAPI()
        }
        tv_cancel_id.setOnClickListener {
            payment_Dialog.dismiss()
        }

    }

    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }
}
