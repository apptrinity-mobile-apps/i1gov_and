package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class UserProfileResponse {
     val user_info: UserProfileInfo? = null

     val result: String? = null

     val status: String? = null
}
