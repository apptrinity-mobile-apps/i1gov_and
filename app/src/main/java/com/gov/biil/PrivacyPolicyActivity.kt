package com.gov.biil

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Window
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.biil.model.ApiInterface
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback

class PrivacyPolicyActivity : AppCompatActivity() {


    internal  var paylink:String?=""
    internal lateinit var id:String
    var mywebview: WebView? = null
    internal lateinit var sessionManager: SessionManager
    lateinit  var uri: Uri

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)
        mywebview = findViewById<WebView>(R.id.webview_privacypolicy)
        myloading()
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        my_loader.show()

        sessionManager = SessionManager(this)

        getAdminStatusInfo()

            paylink = "https://i1gov.com/site/privacy"
            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }



        //paylink = "http://www.blueisland.org/"

        mywebview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)

                return true
            }
        }
        mywebview!!.getSettings().setJavaScriptEnabled(true);
        mywebview!!.setHorizontalScrollBarEnabled(false);
        mywebview!!.loadUrl(uri.toString())

        my_loader.dismiss()

    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


    override fun onBackPressed() {
        if (mywebview!!.canGoBack()) {
            mywebview!!.goBack()
        } else {
            val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)

        }
    }

}
