package model.apiresponses

class StartParkingDataResponse {

    val hour_id: String? = null
    val zone_id: String? = null
    val vehicle_type: String? = null
    val start_date: String? = null
    val end_date: String? = null
    val id: String? = null
    val personal_price: String? = null
    val commercial_price: String? = null
    val hour: String? = null

}
