package com.gov.biil

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.getServiceDetailsDataResponse
import com.gov.biil.model.apiresponses.getServiceDetailsResponse
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback

class MyBillsAccountUpdateActivity : AppCompatActivity() {


    lateinit var tabLayout: TabLayout
    lateinit var viewPager: ViewPager
    var  account_id = ""
    var  account_name = ""
    var  account = ""

    lateinit var et_bill_name : EditText
    lateinit var et_bill_account : EditText
    lateinit var tv_submit : TextView
    internal lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_myaccountupdate)

        sessionManager = SessionManager(this)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        et_bill_name = findViewById<EditText>(R.id.et_bill_name)
        et_bill_account = findViewById<EditText>(R.id.et_bill_account)
        tv_submit = findViewById<EditText>(R.id.tv_submit)

        myloading()




        account_id = intent.getStringExtra("account_id")
        account_name = intent.getStringExtra("account_name")
        account = intent.getStringExtra("account")

        et_bill_name.setText(account_name)
        et_bill_account.setText(account)

        Log.w("ACCOUNTUPDATE", ""+ account_id+"----"+account_name+"----"+account)


        getAdminStatusInfo()

        tv_submit.setOnClickListener {

            callupdateAccountAPI(account_id, et_bill_name.text.toString(),et_bill_account.text.toString())
        }

    }





    private fun callupdateAccountAPI(account_id: String, account_name: String,account: String) {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.updateAccountAPI(account_id, account_name, account)

        call.enqueue(object : Callback<getServiceDetailsResponse> {
            override fun onResponse(call: Call<getServiceDetailsResponse>, response: retrofit2.Response<getServiceDetailsResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_INSERT_RESPONSE", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        Log.w("Result_INSERT_RESPONSE", "Result : " + response.body()!!.status)
                        //Toast.makeText(activity, ""+response.body()!!.result, Toast.LENGTH_SHORT).show()

                        my_loader.dismiss()
                        val list: getServiceDetailsDataResponse = response.body()!!.data!!
                        val serviceid = list.id
                        val intent = Intent(this@MyBillsAccountUpdateActivity, MyBillsActivity::class.java)
                        startActivity(intent)


                    }

                }
            }

            override fun onFailure(call: Call<getServiceDetailsResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


}
