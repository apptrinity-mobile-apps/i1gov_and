package com.gov.biil.model.apiresponses

class getServiceListByIdResponse {

    val data:Array<getServiceListByIdDataResponse>?=null
    val status: String? = null
    val result: String? = null

}
