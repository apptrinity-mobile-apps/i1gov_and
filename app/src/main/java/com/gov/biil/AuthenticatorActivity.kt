package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.DeviceTokenModel
import com.gov.biil.model.apiresponses.ServiceResponse
import retrofit2.Call
import retrofit2.Callback


class AuthenticatorActivity : AppCompatActivity() {
    internal lateinit var et_user_name_id: EditText
    internal var et_pass_id:EditText? = null
    internal lateinit var tv_forgot_pass_id: CustomTextView
    internal lateinit var tv_sign_in_id:CustomTextView
    internal lateinit var tv_forgot_user_name_id:CustomTextView
    internal lateinit var user_name_stg: String
    internal lateinit var password_stg:String
    internal lateinit var device_id:String
    internal lateinit var tv_register:CustomTextView

    internal lateinit var deviceTokenModel: DeviceTokenModel
    internal lateinit var refreshedToken: String
    internal lateinit var authenticator_id: String
    internal lateinit var tv_yes: ImageView
    internal lateinit var tv_no: ImageView
    internal lateinit var tv_username: TextView
    internal lateinit var sessionManager: SessionManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authenticator)
        myloading()
        sessionManager = SessionManager(this)

        authenticator_id = intent.getStringExtra("id")
        Log.d("authenticator_id", authenticator_id)

        tv_yes = findViewById(R.id.tv_yes) as ImageView
        tv_no = findViewById(R.id.tv_no) as ImageView
        tv_username = findViewById(R.id.tv_username) as TextView
        tv_username.setText(sessionManager.isUserName)


        tv_yes.setOnClickListener(View.OnClickListener {
            callAddServiceAPI("YES")
        })

        tv_no.setOnClickListener(View.OnClickListener {
            callAddServiceAPI("NO")

        })



    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun callAddServiceAPI(status: String) {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.mobileAuthUpdate(authenticator_id,status)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<ServiceResponse> {
            override fun onResponse(call: Call<ServiceResponse>, response: retrofit2.Response<ServiceResponse>?) {

                if (response != null) {
                    my_loader.dismiss()
                    if(response.body()!!.status.equals("1")){

                        if(status.equals("NO")){
                            finish()
                        }else{
                            Toast.makeText(applicationContext,"AUTHENTICATOR SUCCESS", Toast.LENGTH_SHORT).show()
                            val intent = Intent(this@AuthenticatorActivity,MainActivity::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                        }


                    }

                }
            }

            override fun onFailure(call: Call<ServiceResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })
    }




    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(user_name_stg))
            et_user_name_id.setError("Username Required")
        else if (TextUtils.isEmpty(password_stg))
            et_pass_id!!.setError("Password Required")
        else if (et_pass_id != null && et_pass_id!!.length() < 6)
            et_pass_id!!.setError("Invalid Password")
        else
            return true
        return false
    }

    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }





}
