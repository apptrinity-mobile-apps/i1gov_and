package com.gov.biil

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.*
import android.util.Log
import android.view.View
import android.view.Window
import android.webkit.WebView
import android.widget.TextView
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.biil.adapter.CityInfoRecyclerAdapter
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.GetAllAttractionsDataResponse
import com.gov.biil.model.apiresponses.GetAllAttractionsResponse
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback



class CityInfoActivity : AppCompatActivity() {

    internal  var paylink:String?=""
    internal lateinit var id:String
    var mywebview: WebView? = null
    internal lateinit var sessionManager: SessionManager
    internal lateinit var rv_city_info_list: RecyclerView
    public var mCityInfo: ArrayList<GetAllAttractionsDataResponse> = ArrayList<GetAllAttractionsDataResponse>()

    private var mCityInfoRecyclerAdapter: CityInfoRecyclerAdapter? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city_info)

        myloading()
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        val toolbar_title = findViewById(R.id.toolbar_title) as TextView
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        my_loader.show()

        sessionManager = SessionManager(this)

        getAdminStatusInfo()

        rv_city_info_list = findViewById(R.id.rv_city_info_list) as RecyclerView
        rv_city_info_list!!.isNestedScrollingEnabled = false

       /* toolbar_title.setOnClickListener {
            val intent = Intent(this, CityInfoDetailViewActivity::class.java)
            startActivity(intent)
        }*/

        Log.e("CITY_INFO",sessionManager.isId)
        callCityInfoList()



    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }




    private fun callCityInfoList() {
        // my_loader.show()
        val apiService = ApiInterface.create()

        val call = apiService.getAllAttractions(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<GetAllAttractionsResponse> {
            override fun onResponse(call: Call<GetAllAttractionsResponse>, response: retrofit2.Response<GetAllAttractionsResponse>?) {
                try {
                    my_loader.dismiss()
                    if (response != null && response.body()!!.status.equals("1")) {
                        if (response.body()!!.data != null) {
                            val list: Array<GetAllAttractionsDataResponse> = response.body()!!.data!!
                            for (item: GetAllAttractionsDataResponse in list.iterator()) {
                                mCityInfo.add(item)
                                setCityInfoAdapter(mCityInfo)



                                // LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(MVFullList.this, LinearLayoutManager.HORIZONTAL, false);
                                //setdataDepartmentAdapter(mServiceInfo)
                                rv_city_info_list!!.setHasFixedSize(true)
                                val gridLayoutManager = StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
                                rv_city_info_list!!.setLayoutManager(gridLayoutManager)
                                rv_city_info_list!!.setItemAnimator(DefaultItemAnimator())
                                val details_adapter = CityInfoRecyclerAdapter(mCityInfo, this@CityInfoActivity!!)
                                rv_city_info_list!!.setAdapter(details_adapter)
                                details_adapter.notifyDataSetChanged()


                            }
                            if (mCityInfo.size == 0) {
                               // noservice!!.visibility = View.VISIBLE
                                rv_city_info_list!!.visibility = View.GONE
                            }

                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<GetAllAttractionsResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun setCityInfoAdapter(mCityinfoList: ArrayList<GetAllAttractionsDataResponse>) {
        mCityInfoRecyclerAdapter = CityInfoRecyclerAdapter(mCityinfoList, this!!)
        rv_city_info_list!!.adapter = mCityInfoRecyclerAdapter
        mCityInfoRecyclerAdapter!!.notifyDataSetChanged()
    }

    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

}
