package com.gov.biil

import Helper.CustomTextView
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.support.annotation.RequiresApi
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.biil.fragment.HomeFragment
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import model.apiresponses.getAdminStatusInfo
import org.jsoup.Jsoup
import retrofit2.Call
import retrofit2.Callback


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var fragmentName: Fragment
    internal lateinit var sessionManager: SessionManager
    internal lateinit var drawer_layout: DrawerLayout
    internal lateinit var tv_addservice: ImageView
    internal lateinit var search_autocomplete: ImageView
    internal lateinit var context: Context
    lateinit var tabLayout: TabLayout
    lateinit var iv_gridview: ImageView
    lateinit var viewPager: ViewPager
    internal lateinit var my_loader: Dialog
    internal lateinit var dialog_list: Dialog
    internal lateinit var tv_nodata_id: CustomTextView
    internal lateinit var dialog_list2: Dialog
    internal lateinit var dialog_list3: Dialog
    internal lateinit var recycler_id: RecyclerView
    var arrowdownup = false
    var park_new_all = true
    public var parkingTimesData: java.util.ArrayList<getAllParkingDataResponse> = java.util.ArrayList<getAllParkingDataResponse>()
    var final_receipt_main_id = ""
    var total_main_balance = ""

    internal var package_name = "com.gov.biil"

    internal lateinit var dialog: Dialog
    internal lateinit var recyclerView_dash_tabs: RecyclerView
    var recycle_tabs_name: Array<String>? = null
    var recycle_tab_items: ArrayList<String>? = null

    companion object {
        val TAG: String = MainActivity::class.java.simpleName
        fun newInstance() = MainActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        context = this
        sessionManager = SessionManager(this)
        setSupportActionBar(toolbar)
        //setTitle("DASH BOARD")
        drawer_layout = findViewById(R.id.drawer_layout) as DrawerLayout
        //  tv_addservice = findViewById(R.id.tv_addservice) as ImageView
        search_autocomplete = findViewById(R.id.search_autocomplete) as ImageView
        recyclerView_dash_tabs = findViewById(R.id.recyclerView_dash_tabs) as RecyclerView
        recycle_tabs_name = resources.getStringArray(R.array.dashboard_tabs)

        recycle_tab_items = ArrayList()

        for (i in 0 until recycle_tabs_name!!.size) {
            recycle_tab_items!!.add(recycle_tabs_name!![i])
        }


        val prefsLayoutManager =
                LinearLayoutManager(this@MainActivity, LinearLayout.HORIZONTAL, false)
        recyclerView_dash_tabs.layoutManager = prefsLayoutManager
        recyclerView_dash_tabs.setHasFixedSize(true)
        val editPrefsAdapter = EditPrefsAdapter(this@MainActivity, recycle_tab_items!!)
        recyclerView_dash_tabs.adapter = editPrefsAdapter
        myloading()

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)

        getAdminStatusInfo()

        web_update()
        if (web_update()) {
            Alert("Update Available", "Do you want to update?")
        }

        toggle.isDrawerIndicatorEnabled = false
        toggle.setHomeAsUpIndicator(R.drawable.nav_menu);
        drawer_layout.addDrawerListener(toggle)

        toggle.syncState()

        toggle.setToolbarNavigationClickListener {
            drawer_layout.openDrawer(GravityCompat.START);
        }

        getSupportActionBar()!!.setDisplayShowTitleEnabled(false);

        nav_view.setNavigationItemSelectedListener(this)
        val header = nav_view.getHeaderView(0)
        val tv_nav_header_name_id = header.findViewById(R.id.username_txt_view) as TextView
        tv_nav_header_name_id.setText(sessionManager.isUserName)
        //pushFragment()


        search_autocomplete.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)

        })

        viewPager = findViewById(R.id.container) as ViewPager
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tabs) as TabLayout
        iv_gridview = findViewById(R.id.iv_gridview) as ImageView
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        getProfileInfo()

        if (android.os.Build.VERSION.SDK_INT > 9) {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
        }




        dialog_list = Dialog(this)
        dialog = Dialog(this)


        val window = dialog_list.getWindow();
        //window.setGravity(Gravity.CENTER_VERTICAL);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);


        /* val lWindowParams =  WindowManager.LayoutParams();
 lWindowParams.copyFrom(dialog_list.getWindow().getAttributes());
 lWindowParams.width = WindowManager.LayoutParams.FILL_PARENT; // this is where the magic happens
 lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
         dialog_list.hide();*/ //I was told to call show first I am not sure if this it to cause layout to happen so that we can override width?


        dialog_list.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list.setContentView(R.layout.dialog_grid_dashboard)
        val iv_payforservicedown = dialog_list.findViewById(R.id.iv_payforservicedown) as LinearLayout
        val iv_paytoparkdown = dialog_list.findViewById(R.id.iv_paytoparkdown) as LinearLayout
        val iv_my_bills = dialog_list.findViewById(R.id.iv_my_bills) as LinearLayout
        val iv_votefordown = dialog_list.findViewById(R.id.iv_votefordown) as LinearLayout
        val iv_emailID = dialog_list.findViewById(R.id.iv_emailID) as LinearLayout
        val iv_platePhone = dialog_list.findViewById(R.id.iv_platePhone) as LinearLayout
        val iv_911 = dialog_list.findViewById(R.id.iv_911) as LinearLayout
        val iv_dashEverything = dialog_list.findViewById(R.id.iv_dashEverything) as LinearLayout
        val iv_censes = dialog_list.findViewById(R.id.iv_censes) as LinearLayout
        val iv_annualReport = dialog_list.findViewById(R.id.iv_annualReport) as LinearLayout
        val iv_city_info = dialog_list.findViewById(R.id.iv_city_info) as LinearLayout
        val iv_alertdown = dialog_list.findViewById(R.id.iv_alertdown) as LinearLayout

        iv_911.setOnClickListener(View.OnClickListener {

            val intent = Intent(this, ComingSoonActivity::class.java)
            intent.putExtra("email", "911")
            startActivity(intent)

        })

        iv_alertdown.setOnClickListener(View.OnClickListener {

            val intent = Intent(this, ComingSoonActivity::class.java)
            intent.putExtra("email", "Alerts")
            startActivity(intent)

        })
        iv_dashEverything.setOnClickListener(View.OnClickListener {

            val intent = Intent(this, ComingSoonActivity::class.java)
            intent.putExtra("email", "everything")
            startActivity(intent)

        })
        iv_censes.setOnClickListener(View.OnClickListener {

            val intent = Intent(this, ComingSoonActivity::class.java)
            intent.putExtra("email", "censes")
            startActivity(intent)

        })
        iv_annualReport.setOnClickListener(View.OnClickListener {

            val intent = Intent(this, ComingSoonActivity::class.java)
            intent.putExtra("email", "annualreport")
            startActivity(intent)

        })

        iv_city_info.setOnClickListener(View.OnClickListener {

            val intent = Intent(this, CityInfoActivity::class.java)
            startActivity(intent)

        })
        iv_platePhone.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, ComingSoonActivity::class.java)
            intent.putExtra("email", "platephone")
            startActivity(intent)

        })
        iv_emailID.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, ComingSoonActivity::class.java)
            intent.putExtra("email", "email")
            startActivity(intent)

        })

        iv_votefordown.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, IVoteForActivity::class.java)
            startActivity(intent)

        })

        iv_my_bills.setOnClickListener {
            val intent = Intent(context, PaymentHistory::class.java)
            startActivity(intent)
        }

        dialog_list.setCancelable(true)


        iv_gridview.setOnClickListener {
            if (!dialog_list.isShowing())
                dialog_list.show()
            // dialog_list.getWindow().setAttributes(lWindowParams);
        }

        dialog_list2 = Dialog(this)
        dialog_list2.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list2.setContentView(R.layout.dialog_payforservices)
        dialog_list2.setCancelable(true)
        val iv_payforserviceaddbillsarrow = dialog_list2.findViewById(R.id.iv_payforserviceaddbillsarrow) as LinearLayout
        val iv_payforservicemybillsarrow = dialog_list2.findViewById(R.id.iv_payforservicemybillsarrow) as LinearLayout
        val iv_close_dialog_service = dialog_list2.findViewById(R.id.iv_close_dialog_service) as ImageView
        iv_close_dialog_service.setOnClickListener {
            dialog_list2.dismiss()
        }
        iv_payforservicedown.setOnClickListener {
            if (!dialog_list2.isShowing())
                dialog_list2.show()
        }

        iv_payforserviceaddbillsarrow.setOnClickListener {
            val intent = Intent(this, PayForServicesActivity::class.java)
            startActivity(intent)
        }
        iv_payforservicemybillsarrow.setOnClickListener {
            val intent = Intent(this, MyBillsActivity::class.java)
            startActivity(intent)
        }


        dialog_list3 = Dialog(this)
        dialog_list3.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list3.setContentView(R.layout.dialog_recycler_view)
        dialog_list3.setCancelable(true)
        val tv_main_parking_id = dialog_list3.findViewById(R.id.tv_main_parking_id) as LinearLayout
        val tv_new_parking_id = dialog_list3.findViewById(R.id.tv_new_parking_id) as LinearLayout
        val iv_dialog_down = dialog_list3.findViewById(R.id.iv_dialog_down) as ImageView
        tv_nodata_id = dialog_list3.findViewById(R.id.tv_nodata_id) as CustomTextView
        recycler_id = dialog_list3.findViewById(R.id.recycler_id) as RecyclerView

        val iv_close_dialog_park = dialog_list3.findViewById(R.id.iv_close_dialog_park) as ImageView
        iv_close_dialog_park.setOnClickListener {
            dialog_list3.dismiss()
        }

        iv_paytoparkdown.setOnClickListener {
            if (!dialog_list3.isShowing())
                dialog_list3.show()
        }
        callgetAllParkingsAPI()
        tv_main_parking_id.setOnClickListener {
            if (park_new_all) {
                park_new_all = false
                recycler_id.visibility = View.VISIBLE
                iv_dialog_down.setImageResource(R.drawable.uparrow_white)
            } else {
                park_new_all = true
                recycler_id.visibility = View.GONE
                iv_dialog_down.setImageResource(R.drawable.downarrow_white)
            }

        }
        tv_new_parking_id.setOnClickListener {
            dialog_list3.dismiss()
            val intent = Intent(this, PaytoParkActivty::class.java)
            startActivity(intent)
        }


    }

    private fun setDifferentColor() {
        for (i in 0 until tabLayout.getChildCount()) {
            //val tv = tabLayout.getChildAt(i) as TextView

            if (i == 0) {
                tabLayout.getChildAt(i).setBackgroundColor(Color.parseColor("#121312"));
                tabLayout.setTabTextColors(ContextCompat.getColorStateList(this, R.color.black));
            }/*else if (i == 1) {
                tabLayout.getChildAt(i).setBackgroundColor(Color.parseColor("#edc421"));
                tabLayout.setTabTextColors(ContextCompat.getColorStateList(this, R.color.green));
            } else {
                tabLayout.getChildAt(i).setBackgroundColor(Color.parseColor("#FC4A86"));
                tabLayout.setTabTextColors(ContextCompat.getColorStateList(this, R.color.color_white));
            }*/
        }
    }

    override fun onBackPressed() {
        /*   val f =fragmentManager.findFragmentById(R.id.content_layout)
            if (f is DashBoardFragment) {
                if(f.isVisible) {
                    (f as DashBoardFragment).closeAPP()
                }
            }else {*/
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            //super.onBackPressed()


            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
        //  }


    }


    override fun onPostResume() {
        super.onPostResume()
        //pushFragment()
    }

    /* override fun onCreateOptionsMenu(menu: Menu): Boolean {
         // Inflate the menu; this adds items to the action bar if it is present.
         menuInflater.inflate(R.menu.main, menu)
         return true
     }*/
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.

        val nv = findViewById<View>(R.id.nav_view) as NavigationView
        val m = nv.getMenu()

        when (item.itemId) {
            R.id.nav_dashboard -> {
                /*   val f =fragmentManager.findFragmentById(R.id.content_layout)
                   if (f is DashBoardFragment) {
                       if (f.isVisible) {
                           removeFragment()
                           pushFragment()
                       }else{
                           pushFragment()
                       }
                   }
                   pushFragment()*/
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
            R.id.nav_reminder -> {

                /*val b = !m.findItem(R.id.nav_reminder).isVisible()

                m.findItem(R.id.nav_addservice).setVisible(b);
                m.findItem(R.id.nav_reminderr).setVisible(b);*/

                val intent = Intent(this, ReminderActivity::class.java)
                startActivity(intent)

            }
            R.id.nav_addservice -> {
                val intent = Intent(this, AddServiceActivity::class.java)
                startActivity(intent)

            }
            R.id.nav_bill_history -> {
                val intent = Intent(this, PaymentHistory::class.java)
                startActivity(intent)

            }
            R.id.nav_profile -> {
                val intent = Intent(this, ProfileInfoActivity::class.java)
                startActivity(intent)

            }
            R.id.nav_changepassword -> {
                val intent = Intent(this, ChangePasswordActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_contactus -> {
                val intent = Intent(this, ContactUsActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_feedback -> {
                val intent = Intent(this, FeedBackActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_terms -> {
                val intent = Intent(this, PrivacyWebActivity::class.java)
                intent.putExtra("privacy","terms")
                startActivity(intent)
            }
            R.id.nav_privacypolicy -> {
                val intent = Intent(this, PrivacyPolicyActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_city -> {
                val intent = Intent(this, CityWebActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_county -> {
                val intent = Intent(this, CountywebActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_state -> {
                val intent = Intent(this, StateWebActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_federal -> {
                val intent = Intent(this, FederalWebActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_logout -> {

                //  alertdialog()
                showChangeLangDialog()
                // sessionManager.logoutUser()

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


    /*   private fun pushFragment() {
           fragmentName = DashBoardFragment()
           val transaction = supportFragmentManager.beginTransaction()
           transaction.replace(R.id.content_layout, fragmentName,"dashboard")
           transaction.addToBackStack(null)
           transaction.commit()
       }*/
    private fun removeFragment() {
        for (fragment in supportFragmentManager.fragments) {
            supportFragmentManager.beginTransaction().remove(fragment).commit()
        }
    }


    fun showChangeLangDialog() {
        val dialogBuilder = AlertDialog.Builder(this)

        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.custom_dialog_library, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()

        val yes_btn = dialogView.findViewById(R.id.custom_dialog_library_ok_button) as Button
        val no_btn = dialogView.findViewById(R.id.custom_dialog_library_cancel_button) as Button

        yes_btn.setOnClickListener(View.OnClickListener {
            sessionManager.logoutUser()

        })
        no_btn.setOnClickListener(View.OnClickListener {
            b.dismiss()

        })
        b.show()

        /*dialogBuilder.setTitle("Log Out")
        dialogBuilder.setMessage("Are sure want to Logout?")
        dialogBuilder.setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, whichButton ->

        })
        dialogBuilder.setNegativeButton("No", DialogInterface.OnClickListener { dialog, whichButton ->

        })*/

    }


    private fun setupTabIcons() {
/*  tabLayout.getTabAt(0)!!.setCustomView(R.layout.tab_selector_home);
tabout.getTabAt(1)!!.setCustomView(R.layout.tab_selector_city)
 tabLayout.getTabAt(2)!!.setCustomView(R.layout.tab_selector_county)
 tabLayout.getTabAt(3)!!.setCustomView(R.layout.tab_selector_state)
 tabLayout.getTabAt(4)!!.setCustomView(R.layout.tab_selector_federal)*/

    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFrag(HomeFragment(), "Home")
        /*adapter.addFrag(CityFragment(), "City")
        adapter.addFrag(CountryFragment(), "County")
        adapter.addFrag(StateFragment(), "State")
        adapter.addFrag(FederalFragment(), "Federal")*/
        viewPager.adapter = adapter

        viewPager.addOnPageChangeListener(
                TabLayout.TabLayoutOnPageChangeListener(tabs))
        for (i in 0 until tabs.tabCount) {
            val view = tabs.getTabAt(i) as TextView
            view.setTextColor(resources.getColor(R.color.gray))
            if (i == 1) {
                val view = tabs.getTabAt(i) as TextView
                view.setTextColor(resources.getColor(R.color.green))
            } else if (i == 2) {
                val view = tabs.getTabAt(i) as TextView
                view.setTextColor(resources.getColor(R.color.yellow_color))
            } else if (i == 3) {
                val view = tabs.getTabAt(i) as TextView
                view.setTextColor(resources.getColor(R.color.red_color))
            }

        }
        tabs.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position

                //setSelectedTabColor()
                // setTabColor(tabhost = );
                //tab.getIcon()!!.setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);

            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })

    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList.get(position)
        }
    }

/* fun  setTabColor(tabhost : TabHost) {

  for (i in 0 until tabhost.getTabWidget().getChildCount())
  tabhost.getTabWidget().getChildAt(i).setBackgroundColor(resources.getColor(R.color.blue)); //unselected

if(tabhost.getCurrentTab()==0)
     tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab()).setBackgroundColor(resources.getColor(R.color.red_color)); //1st tab selected
else
     tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab()).setBackgroundColor(resources.getColor(R.color.gray)); //2nd tab selected
}

*//* lateinit var tabhost: TabHost
private fun setSelectedTabColor() {
 for (i in 0 until tabhost.getTabWidget().getChildCount()) {
     tabhost.getTabWidget().getChildAt(i)
             .setBackgroundColor(Color.WHITE)
 }
 tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab())
         .setBackgroundColor(Color.RED)
}*/


    private fun callgetAllParkingsAPI() {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAllParkings(sessionManager.isId)
        Log.d("GETALLPARKING", call.toString() + sessionManager.isId)
        parkingTimesData.clear()
        call.enqueue(object : Callback<getAllParkingResponse> {
            override fun onResponse(call: Call<getAllParkingResponse>, response: retrofit2.Response<getAllParkingResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        tv_nodata_id.visibility = View.GONE
                        my_loader.dismiss()
                        Log.d("GETALLPARKINGSTATUS", response.body()!!.status)
                        if (response.body()!!.data != null) {
                            val list: Array<getAllParkingDataResponse> = response.body()!!.data!!
                            for (item: getAllParkingDataResponse in list.iterator()) {
                                parkingTimesData.add(item)
                            }

                            val details_adapter = MapDetailsAdapter(parkingTimesData, this@MainActivity!!)
                            recycler_id.setAdapter(details_adapter)
                            details_adapter.notifyDataSetChanged()
                            recycler_id.setHasFixedSize(true)
                            recycler_id.setLayoutManager(LinearLayoutManager(this@MainActivity))
                            recycler_id.setItemAnimator(DefaultItemAnimator())

                        }

                    } else if (response.body()!!.status.equals("2")) {
                        tv_nodata_id.visibility = View.VISIBLE
                    }

                }
            }

            override fun onFailure(call: Call<getAllParkingResponse>, t: Throwable) {
                my_loader.dismiss()
                Log.w("Result_Address_Profile", t.toString())
            }


        })
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_main_parking_list_id = view.findViewById<CustomTextView>(R.id.tv_main_parking_list_id)
        val ll_current_parking_timing = view.findViewById<LinearLayout>(R.id.ll_current_parking_timing)
        // var userSelected = true;
    }

    inner class MapDetailsAdapter(val title: java.util.ArrayList<getAllParkingDataResponse>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_paytopark_timers, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.tv_main_parking_list_id.setText(title.get(position).no_plate)
            holder.ll_current_parking_timing.setOnClickListener {
                final_receipt_main_id = title.get(position).receipt_id!!
                CallgetParkingData()
                dialog_list.dismiss()
            }
        }

        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return title.size
        }
    }


    private fun CallgetParkingData() {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getParkingData(final_receipt_main_id)
        Log.e("final_receipt_LL", final_receipt_main_id)
        call.enqueue(object : Callback<getParkingDataResponse> {
            override fun onResponse(call: Call<getParkingDataResponse>, response: retrofit2.Response<getParkingDataResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        my_loader.dismiss()
                        //  if (response.body()!!.data != null) {
                        val list: parkingDataResponse = response.body()!!.data!!

                        Log.e("final_receipt_id", final_receipt_main_id + "--" + list.remain_hour + "--" + response.body()!!.data!!.remain_min + "--" + list!!.zone_id)
                        val intent = Intent(this@MainActivity, TimerActivity::class.java)
                        intent.putExtra("timer_hrs", list.remain_min)
                        intent.putExtra("receipt_id", list.receipt_id)
                        intent.putExtra("user_id", sessionManager.isId)
                        intent.putExtra("no_plate", "")
                        intent.putExtra("state", "")
                        intent.putExtra("vehicle_type", "")
                        intent.putExtra("zone_rate_id", "")
                        intent.putExtra("lot_rate_id", "")
                        intent.putExtra("parkig_start_date", "")
                        intent.putExtra("parkig_end_date", "")
                        intent.putExtra("zone_id", list!!.zone_id)
                        intent.putExtra("lot_id", list!!.lot_id)
                        intent.putExtra("from", "all_current")
                        intent.putExtra("total_main_balance", total_main_balance)
                        startActivity(intent)

                        // }

                    } else if (response.body()!!.status.equals("2")) {
                        my_loader.dismiss()

                    }

                }
            }

            override fun onFailure(call: Call<getParkingDataResponse>, t: Throwable) {
                my_loader.dismiss()
                Log.w("Result_Address_Profile", t.toString())
            }

        })
    }

    private fun getProfileInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getUserProfile(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UserProfileResponse> {
            override fun onResponse(call: Call<UserProfileResponse>, response: retrofit2.Response<UserProfileResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    // Log.w("Result_Address_Profile","Result : "+response.body()!!.user_info)
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            Log.w("Result_Address_Profile", "Result : " + response.body()!!.user_info)
                            total_main_balance = response.body()!!.user_info!!.wallet_amount!!

                            sessionManager.wallet_update(total_main_balance)
                        }

                    }

                }
            }

            override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


    private fun web_update(): Boolean {
        try {
            val curVersion = this@MainActivity.packageManager.getPackageInfo(package_name, 0).versionName
            println("currentversion-----------$curVersion")
            var newVersion = curVersion
            Log.e("PACKAGENAME", "https://play.google.com/store/apps/details?id=$package_name&hl=en")
            Log.e("CURRENTVERSION", curVersion)
            val document = Jsoup.connect("https://play.google.com/store/apps/details?id=$package_name&hl=en")
                    .timeout(30000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
            /*.select("div[itemprop=softwareVersion]")
                    .first()
                    .ownText();*/

            if (document != null) {
                val element = document!!.getElementsContainingOwnText("Current Version")
                Log.e("element", element.toString())
                for (ele in element) {
                    if (ele.siblingElements() != null) {
                        val sibElemets = ele.siblingElements()
                        for (sibElemet in sibElemets) {
                            newVersion = sibElemet.text()
                        }
                    }
                }
            }

            Log.e("new_Version", newVersion)
            println("Newversion-----------$newVersion")
            return if (value(curVersion) < value(newVersion)) true else false
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }

    }


    private fun value(string: String): Long {
        var string = string
        string = string.trim { it <= ' ' }
        if (string.contains(".")) {
            val index = string.lastIndexOf(".")
            return value(string.substring(0, index)) * 100 + value(string.substring(index + 1))
        } else {
            return java.lang.Long.valueOf(string)
        }
    }


    private fun Alert(title: String, message: String) {
        val builder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.custom_appupdate_dialog, null)
        val alert_title = dialogView.findViewById(R.id.custom_dialog_appupdate_title_textview) as TextView
        val alert_message = dialogView.findViewById(R.id.custom_dialog_appupdate_message_textview) as TextView
        val Bt_action = dialogView.findViewById(R.id.custom_dialog_appupdate_ok_button) as Button
        Bt_action.text = "Update now"
        val Bt_dismiss = dialogView.findViewById(R.id.custom_dialog_appupdate_cancel_button) as Button
        Bt_dismiss.visibility = View.GONE
        builder.setView(dialogView)
        alert_title.text = title
        alert_message.text = message
        builder.setCancelable(false)
        Bt_action.setOnClickListener {
            dialog.dismiss()
            finish()
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$package_name")))
            } catch (anfe: ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$package_name")))
            }
        }
        builder.setOnKeyListener { dialog, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP)
                finish()
            false
        }
        dialog = builder.create()
        dialog.show()
    }


    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


    inner class EditPrefsAdapter(context: Context, list: java.util.ArrayList<String>) :
            RecyclerView.Adapter<EditPrefsAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: java.util.ArrayList<String> = java.util.ArrayList()
        private var selectedIndex = -1

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(
                parent: ViewGroup,
                position: Int
        ): EditPrefsAdapter.ViewHolder {

            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycle_dash_tabs, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {

            return mList.size

        }

        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
        override fun onBindViewHolder(holder: EditPrefsAdapter.ViewHolder, position: Int) {

            val title = mList[position]

            if (position == 0) {
                holder.tv_prefs_name.text = title
                holder.view_id.visibility = View.VISIBLE
                holder.tv_prefs_name.setTextColor(ContextCompat.getColor(context, R.color.blue))
            } else {
                holder.tv_prefs_name.text = title
                holder.view_id.visibility = View.GONE
                holder.tv_prefs_name.setTextColor(ContextCompat.getColor(context, R.color.gray))

            }


            holder.tv_prefs_name.setOnClickListener {
                if(position !=0 ){
                    val intent =Intent(context,HomeContentActivity::class.java)
                    intent.putExtra("form_screen",mList[position])
                    startActivity(intent)
                }
            }


        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            var tv_prefs_name: TextView = view.findViewById(R.id.tv_tab_name)
            var ll_match_prefs: LinearLayout = view.findViewById(R.id.ll_match_prefs)
            var view_id: View = view.findViewById(R.id.view_id)


            @SuppressLint("NewApi")
            fun bind(name: String) {


            }

        }

        fun getItem(position: Int): String? {
            return mList[position]
        }

        fun setSelectedIndex(index: Int) {
            selectedIndex = index
            notifyDataSetChanged()
        }

        fun getSelectedIndex(): Int {
            return selectedIndex
        }

    }


}
