package com.gov.biil.model.apiresponses

/**
 * Created by indo67 on 7/26/2018.
 */
class getBillServicesResponse {

    var service_address: String? = null
    var trans_id: String? = null
    var payment_id: String? = null
    var name: String? = null
    var account: String? = null
    var message: String? = null
    var transaction_id: String? = null
    var amount: String? = null
    var amount_type: String? = null
    var pay_with_wallet: String? = null
    var strip_amount: String? = null
}