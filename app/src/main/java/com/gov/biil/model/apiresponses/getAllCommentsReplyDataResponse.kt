package model.apiresponses

class getAllCommentsReplyDataResponse {

    val id: String? = null
    val comment_id: String? = null
    val user_id: String? = null
    val news_id: String? = null
    val comment: String? = null
    val status: String? = null
    val user_type: String? = null
    val created_at: String? = null

}
