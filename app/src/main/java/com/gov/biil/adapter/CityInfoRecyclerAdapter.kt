package com.gov.biil.adapter

import Helper.CustomTextView
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import com.gov.biil.CityInfoDetailViewActivity
import com.gov.biil.R
import com.gov.biil.model.apiresponses.GetAllAttractionsDataResponse
import com.squareup.picasso.Picasso

class CityInfoRecyclerAdapter(val mCityinfoList: ArrayList<GetAllAttractionsDataResponse>, val context: Context) : RecyclerView.Adapter<ViewHolder1>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder1 {
        return ViewHolder1(LayoutInflater.from(context).inflate(R.layout.city_info_adapter_layout, parent, false))


    }

    override fun onBindViewHolder(holder: ViewHolder1, position: Int) {

        //holder1?.iv_cityinfolist. = mCityinfoList.get(position).image
        holder?.tv_cityinfolist.text = mCityinfoList.get(position).title
        Picasso.with(context)
                .load("https://www.i1gov.com/uploads/adminprofiles/"+mCityinfoList.get(position).image)
                .into(holder?.iv_cityinfolist);

        holder?.ll_cityinfolist.setOnClickListener(View.OnClickListener {

            val intent = Intent(this.context, CityInfoDetailViewActivity::class.java)
            intent.putExtra("city_title", mCityinfoList.get(position).title)
            intent.putExtra("city_image", "https://www.i1gov.com/uploads/adminprofiles/"+mCityinfoList.get(position).image)
            intent.putExtra("city_text", mCityinfoList.get(position).text)
            context.startActivity(intent)

        })

    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return mCityinfoList.size
    }

    // Inflates the item views

}

class ViewHolder1(view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val iv_cityinfolist = view.findViewById(R.id.iv_cityinfolist) as ImageView
    val tv_cityinfolist = view.findViewById(R.id.tv_cityinfolist) as CustomTextView
    val ll_cityinfolist = view.findViewById(R.id.ll_cityinfolist) as RelativeLayout
}
