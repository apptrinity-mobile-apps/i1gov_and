package com.gov.biil.model.apiresponses

class GetAllAttractionsResponse {


    val result: String? = null
    val status: String? = null
    val data: Array<GetAllAttractionsDataResponse>? = null

}
