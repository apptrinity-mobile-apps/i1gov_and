package com.gov.biil.model.apiresponses

class getNewsListResponse {
    val status: String? = null

    val result: String? = null

    val newslist: Array<newsListDataResponse>?=null
}
