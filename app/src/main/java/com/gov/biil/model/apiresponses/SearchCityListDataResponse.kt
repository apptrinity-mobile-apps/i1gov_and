package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class SearchCityListDataResponse {

     val id: String? = null
     val city: String? = null
     val state_code: String? = null

}
