package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class UserProfileInfo {

    val  phone_number: String? = null

    val  password_link: String? = null

    val  ssn: String? = null

    val  state: String? = null

    val  password: String? = null

    val  city: String? = null

    val  id: String? = null
    val  first_name: String? = null
    val  county_url: String? = null
    val  organization: String? = null
    val  city_url: String? = null
    val  create_date: String? = null
    val  gender: String? = null
    val  user_id: String? = null
    val  state_url: String? = null
    val  user_type: String? = null
    val  status: String? = null
    val  middle_name: String? = null
    val  zipcode: String? = null
    val  address: String? = null

    val  county: String? = null

    val  email: String? = null

    val  dob: String? = null

    val  last_name: String? = null

    val  work_phone: String? = null

    val  home_phone: String? = null
    val  email_status: String? = null
    val  mobile_status: String? = null
    val  wallet_amount: String? = null
}
