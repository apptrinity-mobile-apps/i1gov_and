package com.gov.biil.model

import com.gov.biil.model.APIResponse.ChangePasswordResponse
import com.gov.biil.model.APIResponse.RegisterResponse
import com.gov.biil.model.APIResponse.UpdateProfileResponse
import com.gov.biil.model.APIResponse.UserLoginResponse
import com.gov.biil.model.apiresponses.*
import model.apiresponses.getAdminStatusInfo
import model.apiresponses.getAllCommentsbyNewsIdResponse
import model.apiresponses.postCommentResponse
import model.apiresponses.startParkingResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Created by indo67 on 4/13/2018.
 */
public interface ApiInterface {


    @FormUrlEncoded
    @POST("/api/login")
    abstract fun login(@Field("username") username: String,
                       @Field("password") password: String,
                       @Field("mobile_token") mobile_token: String,
                       @Field("device_type") device_type: String
    ): Call<UserLoginResponse>


    @FormUrlEncoded
    @POST("/api/userProfile")
    abstract fun getUserProfile(@Field("user_id") user_id: String): Call<UserProfileResponse>


    @FormUrlEncoded
    @POST("/api/updateProfile")
    abstract fun updateProfile(@Field("user_id") user_id: String,
                               @Field("user_type") user_type: String,
                               @Field("first_name") first_name: String,
                               @Field("middle_name") middle_name: String,
                               @Field("last_name") last_name: String,
                               @Field("dob") dob: String,
                               @Field("gender") gender: String,
                               @Field("ssn") ssn: String,
                               @Field("address") address: String,
                               @Field("city") city: String,
                               @Field("city_url") city_url: String,
                               @Field("county") county: String,
                               @Field("county_url") county_url: String,
                               @Field("state") state: String,
                               @Field("state_url") state_url: String,
                               @Field("zipcode") zipcode: String,
                               @Field("email") email: String,
                               @Field("phone_number") phone_number: String,
                               @Field("home_phone") home_phone: String,
                               @Field("work_phone") work_phone: String,
                               @Field("organization") organization: String): Call<UpdateProfileResponse>


    @FormUrlEncoded
    @POST("/api/saveUserStepTwo")
    abstract fun saveUser(@Field("user_type") user_type: String,
                          @Field("first_name") first_name: String,
                          @Field("middle_name") middle_name: String,
                          @Field("last_name") last_name: String,
                          @Field("dob") dob: String,
                          @Field("gender") gender: String,
                          @Field("ssn") ssn: String,
                          @Field("address") address: String,
                          @Field("city") city: String,
                          @Field("county") county: String,
                          @Field("state") state: String,
                          @Field("zip") zip: String,
                          @Field("aprtment") aprtment: String,
                          @Field("home_phone") home_phone: String,
                          @Field("work_phone") work_phone: String,
                          @Field("city_url") city_url: String,
                          @Field("state_url") state_url: String,
                          @Field("county_url") county_url: String,
                          @Field("organization") organization: String,@Field("user_id") user_id: String, @Field("reg_status") reg_status: String): Call<UpdateProfileResponse>


    @FormUrlEncoded
    @POST("/api/getServicesCount")
    abstract fun getServicesCount(@Field("user_id") user_id: String): Call<UpdateProfileResponse>


    @FormUrlEncoded
    @POST("/api/mobileVerification")
    abstract fun mobileVerification(
                          @Field("cell_phone") cell_phone: String,@Field("email") email: String
                         ): Call<RegistrationStepOneResponse>

      @FormUrlEncoded
    @POST("/api/saveUserStepOne")
    abstract fun saveUserStepOne(@Field("email") email: String,
                          @Field("password") password: String,
                          @Field("cell_phone") cell_phone: String,
                          @Field("mobile_status") mobile_status: String): Call<RegistrationStepOneResponse>

    @FormUrlEncoded
    @POST("/api/userProfile")
    abstract fun userProfile(@Field("user_id") user_id: String): Call<UserProfileResponse>


    @FormUrlEncoded
    @POST("/api/serviceAdd")
    abstract fun serviceAdd(@Field("address") address: String,
                            @Field("apt_unitno") apt_unitno: String,
                            @Field("user_id") user_id: String,
                            @Field("city") city: String,
                            @Field("state") state: String,
                            @Field("zipcode") zipcode: String,
                            @Field("service_link") service_link: String,
                            @Field("due_date") due_date: String,
                            @Field("reminder_date") reminder_date: String,
                            @Field("city_name") city_name: String,
                            @Field("reminder_date_one") reminder_date_one: String,
                            @Field("reminder_date_two") reminder_date_two: String,
                            @Field("service_type") service_type: String): Call<ServiceResponse>

    @FormUrlEncoded
    @POST("/api/serviceUpdate")
    abstract fun serviceUpdate(@Field("address") address: String,
                               @Field("apt_unitno") apt_unitno: String,
                               @Field("id") user_id: String,
                               @Field("city") city: String,
                               @Field("state") state: String,
                               @Field("zipcode") zipcode: String,
                               @Field("service_link") service_link: String,
                               @Field("due_date") due_date: String,
                               @Field("reminder_date") reminder_date: String,
                               @Field("reminder_date_one") reminder_date_one: String,
                               @Field("reminder_date_two") reminder_date_two: String,
                               @Field("city_name") city_name: String,
                               @Field("service_type") service_type: String): Call<ServiceResponse>


    @FormUrlEncoded
    @POST("/api/getAllServicesByUserId")
    abstract fun getAllServicesByUserId(@Field("user_id") user_id: String): Call<AllServicesByUserResponse>


    @FormUrlEncoded
    @POST("/api/getDashboardList")
    abstract fun getDashboardList(@Field("user_id") user_id: String): Call<AllServicesByUserResponse>


    @FormUrlEncoded
    @POST("/api/getServicesById")
    abstract fun getServicesById(@Field("id") id: String): Call<AllServicesByUserResponse>


    @FormUrlEncoded
    @POST("/api/changePassword")
    abstract fun changePassword(@Field("user_id") user_id: String,
                                @Field("old_password") old_password: String,
                                @Field("new_password") new_password: String): Call<ChangePasswordResponse>


    @FormUrlEncoded
    @POST("/api/forgotPassword")
    abstract fun forgotPassword(@Field("email") email: String): Call<ChangePasswordResponse>

    @FormUrlEncoded
    @POST("/api/serviceDelete")
    abstract fun serviceDelete(@Field("id") id: String): Call<ServiceDelete>

    /*-----------------------------*/
    @FormUrlEncoded
    @POST("/Mobile/signup")
    abstract fun registerAccount(@Field("first_name") name: String,
                                 @Field("last_name") company_name: String,
                                 @Field("username") user_name: String,
                                 @Field("email") email: String,
                                 @Field("company_name") address1: String,
                                 @Field("phone_number") address2: String,
                                 @Field("alternate_number") city: String,
                                 @Field("password") passwd: String,
                                 @Field("referred_by") zipcode: String,
                                 @Field("terms_conditions") terms: String): Call<RegisterResponse>


    @FormUrlEncoded
    @POST("/api/mobileAuthUpdate")
    abstract fun mobileAuthUpdate(@Field("id") id: String,
                                  @Field("status") status: String
                                  ): Call<ServiceResponse>

    @FormUrlEncoded
    @POST("/api/usersearch")
    abstract fun usersearch(@Field("search") search: String
    ): Call<SearchListResponse>


    @FormUrlEncoded
    @POST("/api/getAllCities")
    abstract fun getAllCities(@Field("city") city: String
    ): Call<SearchCityListResponse>


    @FormUrlEncoded
    @POST("/api/getAllServicesTypes")
    abstract fun getAllServicesTypes(@Field("id") city: String): Call<GetAllServiceTypesResponse>

    @FormUrlEncoded
    @POST("/api/getAllService")
    abstract fun getAllService(@Field("id") id: String): Call<GetAllServiceResponse>


    @FormUrlEncoded
    @POST("/api/insertData")
    abstract fun insertData(@Field("city_id") city_id: String,
                            @Field("service_type") service_type: String,
                            @Field("service_id") service_id: String,
                            @Field("name") name: String,
                            @Field("account") account: String,
                            @Field("user_id") user_id: String): Call<InsertServiceResponse>



    @GET("api/getStates")
    abstract fun getpaytoparkStates():  Call<getStatesresponse>

    @FormUrlEncoded
    @POST("/api/getParkingZonesAndAddresses")
    abstract fun getParkinzonesandAddress(@Field("state_id") id: String): Call<getParkinZonesadrsResponse>

    @FormUrlEncoded
    @POST("/api/getZonePrics")
    abstract fun getparkingprices(@Field("zone_id") id: String): Call<zonePricesResponse>

    @FormUrlEncoded
    @POST("/api/getaddressesPrics")
    abstract fun getAddressPrices(@Field("lot_id") id: String): Call<addressprices>

    @FormUrlEncoded
    @POST("/api/tcStatus")
    abstract fun posttcStatus(@Field("user_id") user_id: String,
                              @Field("status") status: String): Call<addressprices>

    @FormUrlEncoded
    @POST("/api/confirmreceipt")
    abstract fun postconfirmreceipt(@Field("id") id: String,
                                    @Field("no_plate")no_plate:String ,
                                    @Field("vehicle_type")vehicle_type:String,
                                    @Field("rate_price")rate_price:String,
                                   /* @Field("start_date")start_date:String,
                                    @Field("end_date")end_date:String,*/
                                    @Field("parking_zone_street")parking_zone_street:String,
                                    @Field("parking_lot_street")parking_lot_street:String,
                                    @Field("state")state:String,
                                    @Field("state_code")state_code:String,
                                    @Field("hours")hours:String,
                                    @Field("amount")amount:String): Call<confirmResponse>


    @FormUrlEncoded
    @POST("/api/getAllParkings")
    abstract fun getAllParkings(@Field("user_id") user_id: String): Call<getAllParkingResponse>

    @FormUrlEncoded
    @POST("/api/getServiceListById")
    abstract fun getServiceListById(@Field("user_id") user_id: String,
                                    @Field("service_id") service_id: String): Call<getServiceListByIdResponse>


    @FormUrlEncoded
    @POST("/api/getListOfCategories")
    abstract fun getListOfCategoriesAPI(@Field("id") id: String): Call<getListOfCategoriesResponse>

    @FormUrlEncoded
    @POST("/api/getParkingData")
    abstract fun getParkingData(@Field("receipt_id") receipt_id: String): Call<getParkingDataResponse>

    @FormUrlEncoded
    @POST("/api/updateParkingStatus")
    abstract fun updateParkingStatus(@Field("receipt_id") receipt_id: String): Call<getParkingDataResponse>

    @FormUrlEncoded
    @POST("/api/extendParking")
    abstract fun extendParking(@Field("receipt_id") receipt_id: String,
                              /* @Field("start_date") start_date: String,
                               @Field("end_date") end_date: String,*/
                               @Field("hours") hours: String,
                               @Field("amount")amount:String,
                               @Field("id") id: String): Call<extendParkingResponse>


    @FormUrlEncoded
    @POST("/api/stripePayment")
    abstract fun stripPayment(@Field("stripeToken") stripeToken: String,
                              @Field("name") name: String,
                              @Field("amount") amount: String,
                              @Field("user_id") user_id: String): Call<PaymentResponseAPI>

    @FormUrlEncoded
    @POST("/api/getMyBills")
    abstract fun getMyBills(@Field("user_id") user_id: String): Call<getBillsDataResponse>

    @FormUrlEncoded
    @POST("/api/getMyBillsDetailview")
    abstract fun getBillDetails(@Field("id") bill_id: String): Call<getBillDetailsResponse>

    @FormUrlEncoded
    @POST("/api/getAccountDetails")
    abstract fun getAccountDetailsAPI(@Field("id") id: String): Call<getServiceDetailsResponse>


    @FormUrlEncoded
    @POST("/api/StripePayService")
    abstract fun StripePayServiceAPI(@Field("id") id: String,
                                     @Field("service_id") service_id: String,
                                     @Field("user_id")user_id:String,
                                     @Field("amount")amount:String,
                                     @Field("ptype")ptype:String,
                                     @Field("address")address:String,
                                     @Field("stripeToken")stripeToken:String,
                                     @Field("type")type:String): Call<StripePayServiceResponse>


    @FormUrlEncoded
    @POST("/api/updateAccount")
    abstract fun updateAccountAPI(@Field("id") id: String,@Field("name") name: String,@Field("account") account: String): Call<getServiceDetailsResponse>


    @FormUrlEncoded
    @POST("/api/deleteAccount")
    abstract fun deleteAccountAPI(@Field("id") id: String): Call<getServiceDetailsResponse>

    @FormUrlEncoded
    @POST("/api/votingList")
    abstract fun votingList(@Field("user_id") user_id: String): Call<VotingListResponse>

    @FormUrlEncoded
    @POST("/api/voteCount")
    abstract fun voteCount(@Field("category_id") category_id: String,@Field("sub_category_id") sub_category_id: String,@Field("city_id") city_id: String,@Field("state_id") state_id: String): Call<VotingCountResponse>

    @FormUrlEncoded
    @POST("/api/getAllStates")
    abstract fun getAllStates(@Field("state") state: String
    ): Call<SearchStateListResponse>

    @FormUrlEncoded
    @POST("/api/getcitywiseUsers")
    abstract fun getcitywiseUsers(@Field("city_id") city_id: String,@Field("category_id") category_id: String,@Field("sub_category_id") sub_category_id: String
    ): Call<GetCityWiseUsersResponse>

    @FormUrlEncoded
    @POST("/api/getStatewiseUsers")
    abstract fun getStatewiseUsers(@Field("state_id") state_id: String,@Field("category_id") category_id: String,@Field("sub_category_id") sub_category_id: String
    ): Call<GetStateWiseUsersResponse>

    @FormUrlEncoded
    @POST("/api/vote")
    abstract fun vote(@Field("category_id") category_id: String,@Field("sub_category_id") sub_category_id: String
    ): Call<VoteResponse>

    @FormUrlEncoded
    @POST("/api/saveVote")
    abstract fun saveVote(@Field("user_id") user_id: String,@Field("candidate_id") candidate_id: String,@Field("city_id") city_id: String
                          ,@Field("state_id") state_id: String): Call<SaveVoteResponse>

    @FormUrlEncoded
    @POST("/api/myaccounts")
    abstract fun myaccounts(@Field("user_id") user_id: String): Call<GetAllAccountsResponse>


    @FormUrlEncoded
    @POST("/api/getAllCounties")
    abstract fun getAllCounties(@Field("county") county: String
    ): Call<SearchCountyListResponse>


    @FormUrlEncoded
    @POST("/api/getAllAttractions")
    abstract fun getAllAttractions(@Field("user_id") user_id: String
    ): Call<GetAllAttractionsResponse>


    @FormUrlEncoded
    @POST("/api/newsList")
    abstract fun getNewsList(@Field("user_id") user_id: String): Call<getNewsListResponse>

    @FormUrlEncoded
    @POST("/api/newsDetailView")
    abstract fun getNewsListDetailView(@Field("user_id") user_id: String,@Field("id") id: String): Call<getNewsDetailViewResponse>

    @FormUrlEncoded
    @POST("/api/newsLike")
    abstract fun getNewsLikeStatus(@Field("user_id") user_id: String,@Field("id") id: String): Call<getNewsListResponse>

    @FormUrlEncoded
    @POST("/api/addContact")
    abstract fun postContactComment(@Field("name") name: String,@Field("email") email: String,@Field("comments") comments: String): Call<getNewsListResponse>

    @FormUrlEncoded
    @POST("/api/adminStatus")
    abstract fun getAdminStatus(@Field("user_id") user_id: String): Call<getAdminStatusInfo>

    @FormUrlEncoded
    @POST("/api/startParking")
    abstract fun startParking(@Field("hour_id") hour_id: String,@Field("zone_id") zone_id: String,@Field("vehicle_type") vehicle_type: String,@Field("lot_id") lot_id: String): Call<startParkingResponse>

    @FormUrlEncoded
    @POST("/api/getAllCommentsByNewsId")
    abstract fun getAllCommentsByNewsId(@Field("news_id") news_id: String): Call<getAllCommentsbyNewsIdResponse>

    @FormUrlEncoded
    @POST("/api/postComment")
    abstract fun postComment(@Field("news_id") news_id: String,@Field("user_id") user_id: String,@Field("comment") comment: String): Call<postCommentResponse>

    @FormUrlEncoded
    @POST("/api/replyComment")
    abstract fun replyComment(@Field("news_id") news_id: String,@Field("user_id") user_id: String,@Field("comment") comment: String,@Field("comment_id") comment_id: String): Call<postCommentResponse>


    @GET("/api/getUstabs")
    abstract fun getUstabs(): Call<getUsTabsResponse>

    companion object Factory {
        //val BASE_URL = "http://34.213.239.232"
        val BASE_URL = "https://www.i1gov.com"
        // const val IMAGE_BASE_URL = "https://www.4print.com/uploads/products/"
        //  val ENCRIPTION_KEY ="7d94c049db8081c50400d84cb96ed302"
        //  const val IMAGE_BASE_ORDER_URL="https://www.4print.com/uploads/orders/"

        fun create(): ApiInterface {
            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}