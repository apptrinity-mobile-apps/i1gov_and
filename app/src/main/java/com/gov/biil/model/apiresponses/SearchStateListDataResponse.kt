package com.gov.biil.model.apiresponses

class SearchStateListDataResponse {

    val id: String? = null
    val state: String? = null
    val state_code: String? = null

}
