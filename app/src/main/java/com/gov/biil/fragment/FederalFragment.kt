package com.gov.biil.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.*
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import com.gov.a4print.Session.SessionManager
import com.gov.biil.MainActivity
import com.gov.biil.R




/**
 * A simple [Fragment] subclass.
 */
class FederalFragment : Fragment() {
    lateinit var fragmentName: Fragment
    var mywebview: WebView? = null
    internal lateinit var my_loader: Dialog
    internal lateinit var paylink:String
    internal lateinit var sessionManager: SessionManager
    var uri: Uri? = null


    companion object {
        val TAG: String = FederalFragment::class.java.simpleName
        fun newInstance() = FederalFragment()
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = LayoutInflater.from(activity).inflate(R.layout.countryfragment, container, false)
        mywebview = rootView.findViewById<WebView>(R.id.webview_pricecalc)
        myloading()

        val toolbar = rootView.findViewById(R.id.toolbar) as Toolbar
        val toolbar_title = rootView.findViewById(R.id.toolbar_title) as TextView




        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar!!.setHomeButtonEnabled(true)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        toolbar_title.setText("Federal")

        sessionManager = SessionManager(this.activity!!)


        /*if(sessionManager.countyurl!=""){

            paylink = ""+sessionManager.countyurl

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("https://" + paylink)
            }

        }else{*/
            paylink = "https://www.usa.gov"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }

        //}

        //my_loader.show()

      /*  paylink = "https://www.usa.gov/"
        paylink = "https://www.navy.com/"*/


        mywebview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)

                return true
            }
        }
        mywebview!!.getSettings().setJavaScriptEnabled(true);
        mywebview!!.setHorizontalScrollBarEnabled(false);
        mywebview!!.loadUrl(paylink)

        mywebview!!.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK && mywebview!!.canGoBack()) {
                    mywebview!!.goBack()
                    return true
                }
                return false
            }
        })

        my_loader.dismiss()
        return rootView
    }

    private fun myloading() {
        my_loader = Dialog(activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


    fun onBackPressed() {
        if (mywebview!!.canGoBack()) {
            mywebview!!.goBack()
        } else {
           // activity!!.onBackPressed()

            val intent = Intent(activity,MainActivity::class.java)
            startActivity(intent)

            //activity.super.onBackPressed();
        }
    }

}