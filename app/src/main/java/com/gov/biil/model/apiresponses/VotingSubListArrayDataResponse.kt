package com.gov.biil.model.apiresponses

class VotingSubListArrayDataResponse {

    val id: String? = null
    val category_id: String? = null
    val sub_category_name: String? = null
    val status: String? = null
    val is_vote: String? = null

}
