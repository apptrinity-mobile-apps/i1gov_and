package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.gov.a4printuser.Helper.ConnectionManager
import com.gov.biil.model.APIResponse.ChangePasswordResponse
import com.gov.biil.model.ApiInterface
import retrofit2.Call
import retrofit2.Callback

class ForgotPasswordActivity : AppCompatActivity() {
    internal lateinit var et_email_id: EditText
    internal lateinit var tv_recov_pass_id: TextView
    internal lateinit var tv_privacy: CustomTextView
    internal lateinit var tv_terms:CustomTextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        var drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.login_menu);
        toolbar.setOverflowIcon(drawable)

        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        var rootview = findViewById(R.id.rootview) as RelativeLayout
        toolbar.setNavigationOnClickListener { onBackPressed() }
        myloading()
        tv_recov_pass_id = findViewById(R.id.tv_recov_pass_id) as TextView
        et_email_id = findViewById(R.id.et_email_id) as EditText

        tv_privacy = findViewById(R.id.tv_privacy) as CustomTextView
        tv_terms = findViewById(R.id.tv_terms) as CustomTextView

        tv_recov_pass_id.setOnClickListener(View.OnClickListener {
            Log.e("email_lll", et_email_id.getText().toString())
            if (hasValidCredentials()) {
                if(ConnectionManager.checkConnection(applicationContext)) {
                     callForgotPasswordAPI(et_email_id.getText().toString())
                }
                else{
                    ConnectionManager.snackBarNetworkAlert_Relative(rootview,applicationContext)
                }

            }
        })

        tv_privacy.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@ForgotPasswordActivity, PrivacyWebActivity::class.java)
            intent.putExtra("privacy","privacy")
            startActivity(intent)

        })

        tv_terms.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@ForgotPasswordActivity, PrivacyWebActivity::class.java)
            intent.putExtra("privacy","terms")
            startActivity(intent)

        })
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }



    private fun callForgotPasswordAPI(email: String) {
        val apiService = ApiInterface.create()
        my_loader.dismiss()
        val call = apiService.forgotPassword(email)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<ChangePasswordResponse> {
            override fun onResponse(call: Call<ChangePasswordResponse>, response: retrofit2.Response<ChangePasswordResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    Log.w("Result_Address","Result : "+response.body()!!.status)
                    if(response.body()!!.status.equals("1") && response.body()!!.Message!=null) {
                        Toast.makeText(applicationContext, response.body()!!.Message, Toast.LENGTH_SHORT).show()
                        onBackPressed()
                    }else{
                        Toast.makeText(applicationContext, response.body()!!.result, Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<ChangePasswordResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })
    }
    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(et_email_id.getText().toString()))
            et_email_id.setError("Email Required")
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(et_email_id.getText().toString()).matches())
            et_email_id.setError("Invalid Email")
        else
            return true
        return false
    }

    override
    fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override
    fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        when (id) {
            R.id.faq -> {
                val intent = Intent(this@ForgotPasswordActivity, FaqActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.contact -> {
                val intent = Intent(this@ForgotPasswordActivity, ContactUsActivity::class.java)
                startActivity(intent)
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

}
