package com.gov.biil.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gov.biil.DashBoardFragment
import com.gov.biil.R


/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {
    lateinit var fragmentName: Fragment
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = LayoutInflater.from(this.activity).inflate(R.layout.fragment_home, container, false)

        fragmentName = DashBoardFragment()
        val transaction = fragmentManager!!.beginTransaction()
        transaction.replace(R.id.content_layout, fragmentName,"dashboard")
        transaction.addToBackStack(null)
        transaction.commit()
        return rootView
    }


}