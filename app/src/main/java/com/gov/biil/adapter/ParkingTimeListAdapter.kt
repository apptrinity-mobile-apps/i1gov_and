package com.gov.biil.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.gov.biil.R
import com.gov.biil.TimerActivity
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.getAllParkingDataResponse
import com.gov.biil.model.apiresponses.getParkingDataResponse
import com.gov.biil.model.apiresponses.parkingDataResponse
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*


data class ParkingTimeListAdapter(val mServiceList: ArrayList<getAllParkingDataResponse>, val activity: Activity) : BaseAdapter() {


    internal var parkig_start_date: String? = null
    internal var receipt_id: String? = null
    internal var no_plate: String? = null
    internal var vehicle_type: String? = null
    internal var zone_rate_id: String? = null
    internal var user_id: String? = null
    internal var lot_rate_id: String? = null
    internal var state: String? = null
    internal var parkig_end_date: String? = null
    internal var zone_id: String ? = null
    internal var lot_id: String ? = null
    internal var final_Time: String? = null
    internal var tv_currentparking_time: TextView? = null
    internal var ll_current_parking_timing: LinearLayout? = null


    override fun getItem(position: Int): Any {
        return mServiceList.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return mServiceList.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = View.inflate(activity, R.layout.list_item_paytopark_timers, null)
        tv_currentparking_time = view.findViewById(R.id.tv_currentparking_time) as TextView
        ll_current_parking_timing = view.findViewById(R.id.ll_current_parking_timing) as LinearLayout

        myloading()

        parkig_start_date = mServiceList.get(position).parkig_start_date
        parkig_end_date = mServiceList.get(position).parkig_end_date


        receipt_id = mServiceList.get(position).receipt_id
        no_plate = mServiceList.get(position).no_plate
        vehicle_type = mServiceList.get(position).vehicle_type
        zone_rate_id = mServiceList.get(position).zone_rate_id
        lot_rate_id = mServiceList.get(position).lot_rate_id
        user_id = mServiceList.get(position).user_id
        state = mServiceList.get(position).state
        zone_id = mServiceList.get(position).zone_id!!
        lot_id = mServiceList.get(position).lot_id!!


        printDifference(parkig_start_date, parkig_end_date)


        return view
    }

    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


    fun printDifference(startDate: String?, endDate: String?) {


        val dateStart = startDate
        val dateStop = endDate


        /* val sdf = SimpleDateFormat("hh:mm aa")
         val currentDateandTime = sdf.format(Date())

         val date = sdf.parse(currentDateandTime)
         val calendar = Calendar.getInstance()

         calendar.setTime(date)
         calendar.add(Calendar.HOUR,Calendar.MINUTE)

         Log.e("CURRENTTIME", "" +Calendar.HOUR+""+Calendar.MINUTE);*/

        Log.e("Parking_START", "" + dateStart + "----" + dateStop + "PARKING" + startDate + "------" + endDate);


        //HH converts hour in 24 hours format (0-23), day calculation
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

        var d1: Date? = null
        var d2: Date? = null

        try {
            d1 = format.parse(dateStart)
            d2 = format.parse(dateStop)

            //in milliseconds
            val diff = d2!!.time - d1!!.time

            val diffSeconds = diff / 1000 % 60
            val diffMinutes = diff / (60 * 1000) % 60
            val diffHours = diff / (60 * 60 * 1000) % 24
            val diffDays = diff / (24 * 60 * 60 * 1000)

            Log.e("FINALTIME", "DAYS" + diffDays + "HOURS" + diffHours + "MIN" + diffMinutes + "SEC" + diffSeconds);

            print(diffDays.toString() + " days, ")
            print(diffHours.toString() + " hours, ")
            print(diffMinutes.toString() + " minutes, ")
            print(diffSeconds.toString() + " seconds.")


            tv_currentparking_time!!.setText(diffHours.toString())

            ll_current_parking_timing!!.setOnClickListener(View.OnClickListener {

           /*     val intent = Intent(activity, TimerActivityOld::class.java)
                intent.putExtra("timer_hrs", diffHours.toString())
                intent.putExtra("receipt_id", receipt_id)
                intent.putExtra("user_id", user_id)
                intent.putExtra("no_plate", no_plate)
                intent.putExtra("state", state)
                intent.putExtra("vehicle_type", vehicle_type)
                intent.putExtra("zone_rate_id", zone_rate_id)
                intent.putExtra("lot_rate_id", lot_rate_id)
                intent.putExtra("parkig_start_date", parkig_start_date)
                intent.putExtra("parkig_end_date", parkig_end_date)
                intent.putExtra("zone_id", zone_id)
                intent.putExtra("lot_id", lot_id)
                activity.startActivity(intent)*/

                CallgetParkingData(receipt_id!!)

                Toast.makeText(activity, "", Toast.LENGTH_SHORT).show()

            })


        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun CallgetParkingData(final_receipt_id:String) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getParkingData(final_receipt_id)

        call.enqueue(object : Callback<getParkingDataResponse> {
            override fun onResponse(call: Call<getParkingDataResponse>, response: retrofit2.Response<getParkingDataResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        my_loader.dismiss()
                        if (response.body()!!.data != null) {
                            val list: parkingDataResponse = response.body()!!.data!!

                            Log.e("final_receipt_id",final_receipt_id+"--"+list.remain_hour+"--"+response.body()!!.data!!.remain_min)
                            val intent = Intent(activity, TimerActivity::class.java)
                            intent.putExtra("timer_hrs", list.remain_hour)
                            intent.putExtra("receipt_id", final_receipt_id)
                            intent.putExtra("user_id", user_id)
                            intent.putExtra("no_plate", no_plate)
                            intent.putExtra("state", state)
                            intent.putExtra("vehicle_type", vehicle_type)
                            intent.putExtra("zone_rate_id", zone_rate_id)
                            intent.putExtra("lot_rate_id", lot_rate_id)
                            intent.putExtra("parkig_start_date", parkig_start_date)
                            intent.putExtra("parkig_end_date", parkig_end_date)
                            intent.putExtra("zone_id", list!!.zone_id)
                            intent.putExtra("lot_id", list!!.lot_id)
                            activity.startActivity(intent)

                        }

                    } else if (response.body()!!.status.equals("2")) {
                        my_loader.dismiss()

                    }

                }
            }

            override fun onFailure(call: Call<getParkingDataResponse>, t: Throwable) {
                my_loader.dismiss()
                Log.w("Result_Address_Profile", t.toString())
            }


        })
    }
}
