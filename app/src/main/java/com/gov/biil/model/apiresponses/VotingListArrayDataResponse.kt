package com.gov.biil.model.apiresponses

class VotingListArrayDataResponse {


    val id: String? = null
    val category_name: String? = null
    val status: String? = null
    val created_date: String? = null
    val sub: Array<VotingSubListArrayDataResponse>?=null

}
