package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.UserProfileResponse
import com.gov.biil.model.apiresponses.getBillsDataResponse
import com.gov.biil.model.apiresponses.getBillsListResponse
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat

class PaymentHistory : AppCompatActivity() {

    lateinit var rv_bill_list_id: RecyclerView
    lateinit var tv_total_amount_id: CustomTextView
    lateinit var tv_load_funds_id: CustomTextView
    lateinit var progress_id: ProgressBar
    internal lateinit var sessionManager: SessionManager
    var total_main_balance = ""
    internal lateinit var my_loader: Dialog
    lateinit var billsDataArray: ArrayList<getBillsListResponse>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_history)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        sessionManager = SessionManager(this)
        myloading()
        getAdminStatusInfo()

        rv_bill_list_id = findViewById(R.id.rv_bill_list_id)
        tv_total_amount_id = findViewById(R.id.tv_total_amount_id)
        tv_load_funds_id = findViewById(R.id.tv_load_funds_id)
        progress_id = findViewById(R.id.progress_id)
        tv_load_funds_id.setOnClickListener {
            val intent = Intent(this, FoundsLoad::class.java)
            intent.putExtra("total_main_balance", total_main_balance)
            startActivity(intent)
        }


        ///UserDetails
        getProfileInfo()
        //MyBillsResponse
        CallgetBillDataAPI()
    }

    private fun getProfileInfo() {
        progress_id.visibility = View.VISIBLE
        val apiService = ApiInterface.create()
        val call = apiService.getUserProfile(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UserProfileResponse> {
            override fun onResponse(call: Call<UserProfileResponse>, response: retrofit2.Response<UserProfileResponse>?) {
                if (response != null) {
                    progress_id.visibility = View.GONE
                    // Log.w("Result_Address_Profile","Result : "+response.body()!!.user_info)
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            Log.w("Result_Address_Profile", "Result : " + response.body()!!.user_info)
                            total_main_balance = response.body()!!.user_info!!.wallet_amount!!
                            tv_total_amount_id.setText("$ "+ total_main_balance)
                            sessionManager.wallet_update(total_main_balance)
                        }

                    }

                }
            }

            override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

    private fun CallgetBillDataAPI() {

        val apiService = ApiInterface.create()
        val call = apiService.getMyBills(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        billsDataArray = ArrayList()
        billsDataArray.clear()
        call.enqueue(object : Callback<getBillsDataResponse> {
            override fun onResponse(call: Call<getBillsDataResponse>, response: retrofit2.Response<getBillsDataResponse>?) {
                if (response != null) {
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.data != null) {
                            val list: ArrayList<getBillsListResponse> = response.body()!!.data!!
                            for (item: getBillsListResponse in list.iterator()) {
                                billsDataArray.add(item)
                            }
                            rv_bill_list_id.setHasFixedSize(true)
                            rv_bill_list_id.setLayoutManager(LinearLayoutManager(this@PaymentHistory))
                            rv_bill_list_id.setItemAnimator(DefaultItemAnimator())
                            val details_adapter = BillsAdapter(billsDataArray, this@PaymentHistory)
                            rv_bill_list_id.setAdapter(details_adapter)
                            details_adapter.notifyDataSetChanged()
                        }

                    }

                }
            }

            override fun onFailure(call: Call<getBillsDataResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

    inner class BillsAdapter(val title: java.util.ArrayList<getBillsListResponse>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.payment_history_list_item, parent, false))
        }
        //@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder?.tv_pay_for_type_id.setText(title.get(position).message)
            holder?.tv_tranId_id.setText("#" + title.get(position).transaction_id)




            val inputFormat = SimpleDateFormat("yyyy-MM-dd")
            val outputFormat = SimpleDateFormat("MM-dd-yyyy")
            val inputDateStr = title.get(position).date.toString()
            val date = inputFormat.parse(inputDateStr)
            val outputDateStr = outputFormat.format(date)

            Log.w("MYRECIEPTSDATE", title.get(position).date.toString()+"-------"+outputDateStr +"AMOUNT"+title.get(position).amount)


            holder?.tv_date_time_id.setText(outputDateStr + "  |  " + title.get(position).time)
            if (title.get(position).service_type!!.equals("funds")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder?.img_pay_type_id.setImageDrawable(getDrawable(R.drawable.add_funds))
                }
            } else if (title.get(position).service_type!!.equals("park")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder?.img_pay_type_id.setImageDrawable(getDrawable(R.drawable.parking_ic))
                }
            } else if (title.get(position).service_type!!.equals("service")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder?.img_pay_type_id.setImageDrawable(getDrawable(R.drawable.service))
                }
            }
            if (title.get(position).amount_type!!.equals("debit")) {
                holder?.tv_amount_id.setTextColor(resources.getColor(R.color.red_color))
                holder?.tv_amount_id.setText("- $" + title.get(position).amount)
            } else {
                holder?.tv_amount_id.setTextColor(resources.getColor(R.color.green))
                holder?.tv_amount_id.setText("$" + title.get(position).amount)
            }
            holder?.rl_layout_id.setOnClickListener {

                if(title.get(position).service_type!!.equals("park")){
                    val intent=Intent(this@PaymentHistory,PaymetDetails2Park::class.java)
                    intent.putExtra("bill_id",title.get(position).id)
                    intent.putExtra("bill_type",title.get(position).service_type)
                    startActivity(intent)
                }else{
                    val intent=Intent(this@PaymentHistory,PaymetDetails::class.java)
                    intent.putExtra("bill_id",title.get(position).id)
                    intent.putExtra("bill_type",title.get(position).service_type)
                    startActivity(intent)
                }


            }


        }

        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return title.size
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_pay_for_type_id = view.findViewById<CustomTextView>(R.id.tv_pay_for_type_id)
        val tv_tranId_id = view.findViewById<CustomTextView>(R.id.tv_tranId_id)
        val tv_amount_id = view.findViewById<CustomTextView>(R.id.tv_amount_id)
        val tv_date_time_id = view.findViewById<CustomTextView>(R.id.tv_date_time_id)
        val img_pay_type_id = view.findViewById<ImageView>(R.id.img_pay_type_id)
        val rl_layout_id = view.findViewById<RelativeLayout>(R.id.rl_layout_id)

        // var userSelected = true;

    }

    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }
}
