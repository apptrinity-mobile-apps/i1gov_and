package com.gov.biil

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.ConnectionManager
import com.gov.biil.adapter.ReminderDetailsAdapter
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.AllServicesByUserInfo
import com.gov.biil.model.apiresponses.AllServicesByUserResponse
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback


class ReminderActivity : AppCompatActivity() {
    private var mReminderView: ListView? = null
    public var mServiceInfo: ArrayList<AllServicesByUserInfo> = ArrayList<AllServicesByUserInfo>()
    public var mServiceInfo_temp: ArrayList<AllServicesByUserInfo> = ArrayList<AllServicesByUserInfo>()
    private var mReminderDetailsAdapter: ReminderDetailsAdapter? = null
    private var mProgressBar: ProgressBar? = null
    internal lateinit var sessionManager: SessionManager
    internal lateinit var noservice: TextView
    internal lateinit var tv_city_count: RadioButton
    internal lateinit var tv_county_count: RadioButton
    internal lateinit var tv_state_count: RadioButton
    internal lateinit var tv_federal_count: RadioButton
    internal lateinit var tv_all: RadioButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reminder_layout)
        mReminderView = findViewById(R.id.rv_dash_list) as ListView
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        //val ttitle = findViewById(R.id.toolbar_title) as TextView
        toolbar.visibility = View.VISIBLE
        //ttitle.visibility = View.VISIBLE
        setSupportActionBar(toolbar)
        //setTitle("REMINDER")
        sessionManager = SessionManager(this)
        myloading()
        getAdminStatusInfo()
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        var rootview = findViewById(R.id.rootview) as LinearLayout
        noservice = findViewById(R.id.no_service) as TextView

        tv_city_count = findViewById(R.id.tv_city_count) as RadioButton
        tv_county_count = findViewById(R.id.tv_county_count) as RadioButton
        tv_state_count = findViewById(R.id.tv_state_count) as RadioButton
        tv_federal_count = findViewById(R.id.tv_federal_count) as RadioButton
        tv_all = findViewById(R.id.tv_all) as RadioButton

        //tv_all.setBackgroundResource(R.drawable.border_button_all_sel_rem)
        // tv_all.setTextColor(resources.getColor(R.color.white))


        if (ConnectionManager.checkConnection(this)) {
            callServiceDetails()
        } else {
            Toast.makeText(this, resources.getString(R.string.network_error), Toast.LENGTH_SHORT).show()
        }


        tv_city_count.setOnClickListener {
            Log.w("city_CITYYYYYY", "CITY")
            /*tv_city_count.setBackgroundResource(R.drawable.border_button_city_sel_rem)
            tv_city_count.setTextColor(resources.getColor(R.color.white))

            tv_state_count.setBackgroundResource(R.drawable.border_button_state_rem)
            tv_state_count.setTextColor(resources.getColor(R.color.dashboard_state))
            tv_county_count.setBackgroundResource(R.drawable.border_button_county_rem)
            tv_county_count.setTextColor(resources.getColor(R.color.dashboard_county))
            tv_federal_count.setBackgroundResource(R.drawable.border_button_federal_rem)
            tv_federal_count.setTextColor(resources.getColor(R.color.dashboard_federal))*/

            mServiceInfo_temp.clear()
            noservice!!.visibility = View.GONE
            if (mServiceInfo.size > 0) {
                for (i in mServiceInfo.indices) {
                    if (mServiceInfo.get(i).service_type.equals("City")) {
                        mServiceInfo_temp.add(mServiceInfo.get(i))
                        setProductAdapter(mServiceInfo_temp)
                        Log.w("city_size", "::" + mServiceInfo_temp.size)
                        //  tv_city_count.setText(resources.getString(R.string.city)+ mServiceInfo_temp.size)
                    }
                }
                if (mServiceInfo_temp.size == 0) {
                    setProductAdapter(mServiceInfo_temp)
                    noservice.text = "No Cities Found"
                    noservice!!.visibility = View.VISIBLE
                }

            }
        }


        tv_state_count.setOnClickListener {
            Log.w("city_CITYYYYYY", "CITY")
/* tv_state_count.setBackgroundResource(R.drawable.border_button_state_sel_rem)
 tv_state_count.setTextColor(resources.getColor(R.color.white))



tv_all.setBackgrounesource(R.drawable.border_button_all_rem)
 tv_all.setTextColor(resources.getColor(R.color.dark_color_blue))
 tv_city_count.setBackgroundResource(R.drawable.border_button_city_rem)
 tv_city_count.setTextColor(resources.getColor(R.color.dashboard_city))
 tv_county_count.setBackgroundResource(R.drawable.border_button_county_rem)
 tv_county_count.setTextColor(resources.getColor(R.color.dashboard_county))
 tv_federal_count.setBackgroundResource(R.drawable.border_button_federal_rem)
 tv_federal_count.setTextColor(resources.getColor(R.color.dashboard_federal))*/


            mServiceInfo_temp.clear()
            noservice!!.visibility = View.GONE
            if (mServiceInfo.size > 0) {
                for (i in mServiceInfo.indices) {
                    if (mServiceInfo.get(i).service_type.equals("State")) {

                        mServiceInfo_temp.add(mServiceInfo.get(i))
                        setProductAdapter(mServiceInfo_temp)
                        Log.w("city_size", "::" + mServiceInfo_temp.size)
                        //tv_state_count.setText(resources.getString(R.string.state)+ mServiceInfo_temp.size)
                    }
                }
                if (mServiceInfo_temp.size == 0) {
                    setProductAdapter(mServiceInfo_temp)
                    noservice.text = "No States Found"
                    noservice!!.visibility = View.VISIBLE
                }
            }

        }

        tv_county_count.setOnClickListener {
            Log.w("city_CITYYYYYY", "CITY")
/*tv_county_count.setBackgroundResource(R.drawable.border_button_county_sel_rem)
tv_county_count.setTextColor(resources.getColor(R.color.white))



_all.setBackgroundResource(R.drawable.border_button_all_rem)
tv_all.setTextColor(resources.getColor(R.color.dark_color_blue))
tv_state_count.setBackgroundResource(R.drawable.border_button_state_rem)
tv_state_count.setTextColor(resources.getColor(R.color.dashboard_state))
tv_city_count.setBackgroundResource(R.drawable.border_button_city_rem)
tv_city_count.setTextColor(resources.getColor(R.color.dashboard_city))
tv_federal_count.setBackgroundResource(R.drawable.border_button_federal_rem)
tv_federal_count.setTextColor(resources.getColor(R.color.dashboard_federal))*/

            mServiceInfo_temp.clear()
            noservice!!.visibility = View.GONE
            if (mServiceInfo.size > 0) {
                for (i in mServiceInfo.indices) {
                    if (mServiceInfo.get(i).service_type.equals("County")) {
                        mServiceInfo_temp.add(mServiceInfo.get(i))
                        setProductAdapter(mServiceInfo_temp)
                        Log.w("city_size", "::" + mServiceInfo_temp.size)
                        // tv_county_count.setText(resources.getString(R.string.country)+ mServiceInfo_temp.size)
                    }
                }
                if (mServiceInfo_temp.size == 0) {
                    setProductAdapter(mServiceInfo_temp)
                    noservice.text = "No County Found"
                    noservice!!.visibility = View.VISIBLE
                }
            }
        }

        tv_federal_count.setOnClickListener {
            Log.w("city_CITYYYYYY", "CITY")
/*tv_federal_count.setBackgroundResource(R.drawable.border_button_federal_sel_rem)
tv_federal_count.setTextColor(resources.getColor(R.color.white))


  tv_all.setBackgroundResource(R.drawable.border_button_all_rem)
tv_all.setTextColor(resources.getColor(R.color.dark_color_blue))
tv_state_count.setBackgroundResource(R.drawable.border_button_state_rem)
tv_state_count.setTextColor(resources.getColor(R.color.dashboard_state))
tv_city_count.setBackgroundResource(R.drawable.border_button_city_rem)
tv_city_count.setTextColor(resources.getColor(R.color.dashboard_city))
tv_county_count.setBackgroundResource(R.drawable.border_button_county_rem)
tv_county_count.setTextColor(resources.getColor(R.color.dashboard_county))*/

            mServiceInfo_temp.clear()
            noservice!!.visibility = View.GONE
            if (mServiceInfo.size > 0) {
                for (i in mServiceInfo.indices) {
                    if (mServiceInfo.get(i).service_type.equals("Federal")) {

                        mServiceInfo_temp.add(mServiceInfo.get(i))
                        setProductAdapter(mServiceInfo_temp)
                        Log.w("city_size", "::" + mServiceInfo_temp.size)
                        // tv_federal_count.setText(resources.getString(R.string.federal)+ mServiceInfo_temp.size)
                    }
                }
                if (mServiceInfo_temp.size == 0) {
                    setProductAdapter(mServiceInfo_temp)
                    noservice.text = "No Federal Found"
                    noservice!!.visibility = View.VISIBLE
                }
            }

        }


        tv_all.setOnClickListener {

            /* tv_all.setBackgroundResource(R.drawable.border_button_all_sel_rem)
            tv_all.setTextColor(resources.getColor(R.color.white))

            tv_federal_count.setBackgroundResource(R.drawable.border_button_federal_rem)
            tv_federal_count.setTextColor(resources.getColor(R.color.dashboard_federal))
            tv_state_count.setBackgroundResource(R.drawable.border_button_state_rem)
            tv_state_count.setTextColor(resources.getColor(R.color.dashboard_state))
            tv_city_count.setBackgroundResource(R.drawable.border_button_city_rem)
            tv_city_count.setTextColor(resources.getColor(R.color.dashboard_city))
            tv_county_count.setBackgroundResource(R.drawable.border_button_county_rem)
            tv_county_count.setTextColor(resources.getColor(R.color.dashboard_county))*/

            Log.w("city_CITYYYYYY", "CITY")
            mServiceInfo_temp.clear()
            noservice!!.visibility = View.GONE
            if (mServiceInfo.size > 0) {

                for (i in mServiceInfo.indices) {
                    setProductAdapter(mServiceInfo)
                }
            } else {
                Log.w("city_size_elseInside", "else")
                if (mServiceInfo_temp.size == 0) {
                    setProductAdapter(mServiceInfo_temp)
                    noservice.text = "No data Found"
                    noservice!!.visibility = View.VISIBLE
                }

            }
        }


    }

    private fun callServiceDetails() {

        my_loader.show()
        val apiService = ApiInterface.create()

        val call = apiService.getAllServicesByUserId(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<AllServicesByUserResponse> {
            override fun onResponse(call: Call<AllServicesByUserResponse>, response: retrofit2.Response<AllServicesByUserResponse>?) {

                Log.w("Status", "*** " + response!!.body()!!.status)
                my_loader.dismiss()
                if (response != null && response.body()!!.status.equals("1")) {

                    if (response.body()!!.service_info != null) {
                        val list: Array<AllServicesByUserInfo> = response.body()!!.service_info!!

                        for (item: AllServicesByUserInfo in list.iterator()) {
                            mServiceInfo.add(item)
                            setProductAdapter(mServiceInfo)
                        }
                        if (mServiceInfo.size == 0) {
                            noservice.text = "No Reminders Found"
                            noservice!!.visibility = View.VISIBLE
                            mReminderView!!.visibility = View.GONE
                        }
                    }

                }
            }

            override fun onFailure(call: Call<AllServicesByUserResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    private fun setProductAdapter(mServiceList: ArrayList<AllServicesByUserInfo>) {
        mReminderDetailsAdapter = ReminderDetailsAdapter(mServiceList, this)
        mReminderView!!.adapter = mReminderDetailsAdapter
        mReminderDetailsAdapter!!.notifyDataSetChanged()
    }

    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }
}