package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class GetAllServiceTypesResponse {
     val result: String? = null

     val data: Array<GetAllServiceTypesDataResponse>? = null

     val status: String? = null


}
