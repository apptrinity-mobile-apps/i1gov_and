package com.gov.biil.model.apiresponses

/**
 * Created by indo67 on 7/31/2018.
 */
class getServiceDetailsResponse {

    val data:getServiceDetailsDataResponse?=null
    val status: String? = null
    val result: String? = null
}