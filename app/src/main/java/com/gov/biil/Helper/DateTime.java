package com.gov.biil.Helper;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.text.format.DateFormat;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@SuppressWarnings("NumericOverflow")
@SuppressLint("SimpleDateFormat")
public class DateTime {

    private String dateString = "";
    private String timeString = "";
    private boolean is24Format = false;
    private Date dateTime;
    private String timeZone = "America/Los_Angeles";

    //static value for refernce to get output for desired values
    public static final int YEAR = 1;
    public static final int MNTH = 12;
    public static final int DAYS = 30;
    public static final int HOUR = 24;
    public static final int MINS = 60;
    public static final int SECS = 60000;
    private final int SECS_CONV = 1000;
    private final int MINS_CONV = 60 * SECS_CONV;
    private final int HOUR_CONV = 60 * MINS_CONV;
    private final int DAYS_CONV = 24 * HOUR_CONV;
    private final int YEAR_CONV = 365 * DAYS_CONV;
    private final int MNTH_CONV = 30 * DAYS_CONV;

    /**
     * For Current date
     * user can create by using Date dataType
     */
    public DateTime() {
        this.dateTime = new Date();
        dateString = getDateString("yyyy-MM-dd");
        timeString = getTimeString(true);
    }

    /**
     * For Custom Date
     *
     * @param dateTime user can create by using Date dataType
     */
    public DateTime(Date dateTime) {
        this.dateTime = dateTime;
        dateString = getDateString("yyyy-MM-dd");
        timeString = getTimeString(true);
    }

    /**
     * @param dateString generate only using date
     */
    public DateTime(String dateString) {
        this.dateString = dateString;
        changeDateFormat("yyyy-MM-dd", "");
        if (dateTime == null)
            changeDateFormat("MM-dd-yyyy", "");
    }

    /**
     * @param timeString time
     * @param is24Format time format AM/PM or 24 Hours
     */
    public DateTime(String timeString, boolean is24Format) {
        this.timeString = timeString;
        this.is24Format = is24Format;
        if (is24Format)
            changeDateFormat("", "HH:mm:ss");
        else
            changeDateFormat("", "hh:mm a");
    }

    /**
     * @param dateString date string
     * @param timeString time String
     */
    public DateTime(String dateString, String timeString) {
        this.dateString = dateString;
        this.timeString = timeString;
        changeDateFormat("yyyy-MM-dd", "HH:mm");
    }

    /**
     * @param dateString date string
     * @param timeString time String
     * @param is24Format AM/PM or 24 Hours
     */
    public DateTime(String dateString, String timeString, boolean is24Format) {
        this.dateString = dateString;
        this.timeString = timeString;
        this.is24Format = is24Format;
        if (is24Format)
            changeDateFormat("yyyy-MM-dd", "HH:mm:ss");
        else
            changeDateFormat("yyyy-MM-dd", "hh:mm a");
    }

    /**
     * @return Date of this variable
     */
    public Date getDateTime() {
        return dateTime;
    }

    /**
     * @param toFormat get by desired formats like
     *                 by passing "yyyy-MM-dd" results 2018-12-31
     *                 by passing "MM-dd-yyyy" results 12-31-2018
     *                 by passing "MMM dd, yyyy" results Dec 31, 2018
     * @return returns desired format value of date
     */
    public String getDateString(String toFormat) {
        SimpleDateFormat outputFormat = new SimpleDateFormat(toFormat);//"MM-dd-yyyy"
        return outputFormat.format(dateTime);
    }

    public String getDateString() {
        return this.dateString;
    }

    /**
     * @param is24Format AM/PM or 24 Hours
     * @return returns desired time format of Hours and Minutes
     */
    public String getTimeString(boolean is24Format) {
        if (is24Format) {
            SimpleDateFormat outputFormat = new SimpleDateFormat("HH:mm:ss");
            return outputFormat.format(dateTime);
        } else {
            SimpleDateFormat outputFormat = new SimpleDateFormat("hh:mm a");
            return outputFormat.format(dateTime);
        }
    }

    public String getTimeString() {
        return timeString;
    }

    /**
     * @param dateString Date String
     * @param fromFormat Time String
     * @return Date variable using date and time strings
     */
    private Date stringToDate(String dateString, String fromFormat) {
        Date returnDate = null;
        SimpleDateFormat inputFormat = new SimpleDateFormat(fromFormat);

        try {
            returnDate = inputFormat.parse(dateString);
        } catch (Exception ignore) {
            Log.e("DateTime", "stringToDate: ", ignore);
        }
        return returnDate;
    }

    /**
     * To Change Date Formats and returns Date
     *
     * @param datePattern date formats
     * @param timePattern time formats
     * @return returns Date and assigning to parent value dateTime
     */
    @SuppressWarnings("UnusedReturnValue")
    public Date changeDateFormat(@NonNull String datePattern, @NonNull String timePattern) {
        if (!dateString.isEmpty() && !timeString.isEmpty() && !datePattern.isEmpty() && !timePattern.isEmpty())
            dateTime = stringToDate(dateString + " " + timeString, datePattern + " " + timePattern);
        else if (!dateString.isEmpty() && !datePattern.isEmpty())
            dateTime = stringToDate(dateString, datePattern);
        else if (!timeString.isEmpty() && !timePattern.isEmpty())
            dateTime = stringToDate(timeString, timePattern);
        return dateTime;
    }

    /**
     * @param timeZone It is a time zone id like
     *                 "America/Chicago"
     *                 "America/Los_Angeles"
     */
    public void setTimeZone(String timeZone) {
        this.timeZone = (timeZone);
    }

    /**
     * Converts TimeZone for a particular timeZone set to "DateTime"
     * Note: Before calling convertServerTimeZone() need to set time zone id by using @setTimeZone("America/Los_Angeles")
     */
    public void convertServerTimeZone() {
//        TimeZone tz = TimeZone.getTimeZone("America/Chicago"); //"America/Los_Angeles"
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

        String result = destFormat.format(dateTime);
        dateTime = stringToDate(result, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * Converts TimeZone for a particular timeZone set to "DateTime"
     * Note: Before calling convertLocalTimeZone() need to set default time zone, if Changes by using @setTimeZone("America/Los_Angeles")
     */
    public void convertLocalTimeZone() { //"America/Los_Angeles"
        try {
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
            cal.setTimeInMillis(dateTime.getTime());
            String date = DateFormat.format("yyyy-MM-dd HH:mm:ss", cal.getTimeInMillis()).toString();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone(timeZone));
            Date value = formatter.parse(date);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateFormatter.setTimeZone(TimeZone.getDefault());
            date = dateFormatter.format(value);
            dateTime = stringToDate(date, "yyyy-MM-dd HH:mm:ss");


        } catch (Exception e) {
            Log.e("DateTime", "convertLocalTimeZone: ", e);
        }
    }

    /**
     * @param startDate First value, it might be Present Date
     * @param endDate   Second Value, it might be Future/Past Date
     * @param diffType  Type can be Seconds, Minutes, Hours, Days, Months(30 days), Years(365 days)
     * @return If it returns positive(+Ve) value, second date is Future. If it returns negative(-Ve) value, second date is Past
     */
    public long compareDateTime(Date startDate, Date endDate, int diffType) {
        long startLong = startDate.getTime();
        long endLong = endDate.getTime();

        long diff = endLong - startLong;
        switch (diffType) {
            case 1: // years
                diff = diff / YEAR_CONV;
                break;
            case 12: // months
                diff = diff / MNTH_CONV;
                break;
            case 30: // days
                diff = diff / DAYS_CONV;
                break;
            case 24: //hours
                diff = diff / HOUR_CONV;
                break;
            case 60: // minutes
                diff = diff / MINS_CONV;
                break;
            case 60000: //seconds
                diff = diff / SECS_CONV;
                break;
        }
        return diff;
    }

}
