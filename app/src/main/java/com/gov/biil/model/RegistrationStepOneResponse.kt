package com.gov.biil.model

import com.gov.biil.model.apiresponses.RegisterUserInfo

class RegistrationStepOneResponse {

    val status: String? = null
    val result: String? = null
    val otp: String? = null
    val usetr_info: RegisterUserInfo? = null

}
