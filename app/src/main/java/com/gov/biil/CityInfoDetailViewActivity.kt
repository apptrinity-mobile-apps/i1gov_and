package com.gov.biil

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Window
import android.webkit.WebView
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.biil.model.ApiInterface
import com.squareup.picasso.Picasso
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback

class CityInfoDetailViewActivity : AppCompatActivity() {

    internal  var paylink:String?=""
    internal lateinit var id:String
    var mywebview: WebView? = null
    internal lateinit var sessionManager: SessionManager
    internal lateinit var rv_city_info_list: RecyclerView
    internal lateinit var collapsing_toolbar: CollapsingToolbarLayout
    internal lateinit var iv_detailview_image: ImageView
    internal lateinit var iv_panorama_icon: ImageView
    internal lateinit var tv_description: TextView

   // private var mCityInfoRecyclerAdapter: CityInfoRecyclerAdapter? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city_info_detailview)

        myloading()
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        sessionManager = SessionManager(this)

        getAdminStatusInfo()

       // my_loader.show()

         var city_title = intent.getStringExtra("city_title")
         var city_image = intent.getStringExtra("city_image")
         var city_text = intent.getStringExtra("city_text")


        Log.e("CITYINFODETAIL",city_title+"------"+city_image+"--------"+city_text)


        collapsing_toolbar = findViewById(R.id.collapsing_toolbar) as CollapsingToolbarLayout
        iv_detailview_image = findViewById(R.id.iv_detailview_image) as ImageView
        tv_description = findViewById(R.id.tv_description) as TextView
        iv_panorama_icon = findViewById(R.id.iv_panorama_icon) as ImageView




        Picasso.with(this)
                .load(city_image)
                .into(iv_detailview_image)
        tv_description.setText(city_text)
        toolbar.setTitle(city_title)


        iv_panorama_icon.setOnClickListener {

            val intent = Intent(this, PanoramaActivity::class.java)
            intent.putExtra("city_image", city_image)
            startActivity(intent)
        }

        val mAppBarLayout = findViewById<View>(R.id.app_bar) as AppBarLayout
        mAppBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            internal var isShow = false
            internal var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true
                    //showOption(R.id.action_info)
                } else if (isShow) {
                    isShow = false
                   // hideOption(R.id.action_info)
                }
            }
        })




    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }




   /* private fun callCityInfoList() {
        // my_loader.show()
        val apiService = ApiInterface.create()

        val call = apiService.getAllServicesByUserId(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<AllServicesByUserResponse> {
            override fun onResponse(call: Call<AllServicesByUserResponse>, response: retrofit2.Response<AllServicesByUserResponse>?) {
                try {
                    my_loader.dismiss()
                    if (response != null && response.body()!!.status.equals("1")) {
                        if (response.body()!!.service_info != null) {
                            val list: Array<AllServicesByUserInfo> = response.body()!!.service_info!!
                            for (item: AllServicesByUserInfo in list.iterator()) {
                                mServiceInfo.add(item)
                                setProductAdapter(mServiceInfo)

                                //setdataDepartmentAdapter(mServiceInfo)
                                rv_city_info_list!!.setHasFixedSize(true)
                                val layoutManager = LinearLayoutManager(this@CityInfoDetailViewActivity);
                                rv_city_info_list!!.setLayoutManager(layoutManager)
                                rv_city_info_list!!.setItemAnimator(DefaultItemAnimator())
                                val details_adapter = ServiceDetailsRecyclerAdapter(mServiceInfo, context!!)
                                rv_city_info_list!!.setAdapter(details_adapter)
                                details_adapter.notifyDataSetChanged()


                            }
                            if (mServiceInfo.size == 0) {
                                noservice!!.visibility = View.VISIBLE
                                rv_city_info_list!!.visibility = View.GONE
                            }

                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<AllServicesByUserResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun setProductAdapter(mServiceList: ArrayList<AllServicesByUserInfo>) {
        mCityInfoRecyclerAdapter = CityInfoRecyclerAdapter(mServiceList, this!!)
        rv_city_info_list!!.adapter = mCityInfoRecyclerAdapter
        mCityInfoRecyclerAdapter!!.notifyDataSetChanged()
    }*/



    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

}
