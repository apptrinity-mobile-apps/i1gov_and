package com.gov.biil

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Window
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import com.gov.a4print.Session.SessionManager

class PrivacyWebActivity : AppCompatActivity() {

    internal  var paylink:String?=""
    internal  var privacy_url:String?=""
    internal  var terms_url:String?=""
    internal lateinit var id:String
    var mywebview: WebView? = null
    internal lateinit var sessionManager: SessionManager
    lateinit  var uri: Uri

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_web)
        mywebview = findViewById<WebView>(R.id.webview_pricecalc)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        val toolbar_title = findViewById(R.id.toolbar_title) as TextView
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        myloading()


        privacy_url = intent.getStringExtra("privacy")


        if(privacy_url.equals("privacy")){
            toolbar_title.setText("Privacy Policy")
            paylink = "https://i1gov.com/site/privacy"

        }else if(privacy_url.equals("terms")){
            toolbar_title.setText("Terms")
            paylink = "https://i1gov.com/site/terms"

        }






           // paylink = sessionManager.stateurl
            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }


        mywebview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }
        mywebview!!.getSettings().setJavaScriptEnabled(true);
        mywebview!!.setHorizontalScrollBarEnabled(false);
        mywebview!!.loadUrl(uri.toString())

    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    override fun onBackPressed() {
        if (mywebview!!.canGoBack()) {
            mywebview!!.goBack()
        } else {
            val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)

        }
    }
}
