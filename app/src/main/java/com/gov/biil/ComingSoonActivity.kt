package com.gov.biil

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.TextView

class ComingSoonActivity : AppCompatActivity() {


    var coming_soon: String =""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comingsoon)

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        val toolbar_title = findViewById(R.id.toolbar_title) as TextView

        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        coming_soon = intent.getStringExtra("email")

        if(coming_soon.equals("email")){
            toolbar_title.setText("Email-Id")
        }else if(coming_soon.equals("platephone")){
            toolbar_title.setText("Plate Phone")
        }else if(coming_soon.equals("911")){
            toolbar_title.setText("911")
        }else if(coming_soon.equals("everything")){
            toolbar_title.setText("E-everything")
        }else if(coming_soon.equals("censes")){
            toolbar_title.setText("Censes")
        }else if(coming_soon.equals("annualreport")){
            toolbar_title.setText("SSN Annual Report")
        }else if(coming_soon.equals("Alerts")){
            toolbar_title.setText("Alerts")
        }

    }
}
