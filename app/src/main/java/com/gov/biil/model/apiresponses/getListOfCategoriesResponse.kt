package com.gov.biil.model.apiresponses

/**
 * Created by indo67 on 8/1/2018.
 */
class getListOfCategoriesResponse {

    var status:String?=null
    var result:String?=null
    var data:Array<getListOfCategoriesDataResponse>?=null

}