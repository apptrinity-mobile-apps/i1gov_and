package com.gov.biil

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.BundleDataInfo
import com.gov.a4printuser.Helper.ConnectionManager
import com.gov.biil.model.APIResponse.UpdateProfileResponse
import com.gov.biil.model.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*



class RegisterActivity : AppCompatActivity() {


    var list_items_list: ListView? = null
    var dialog_list: Dialog? = null
    internal lateinit var sessionManager: SessionManager



    internal lateinit var usertype_st: String
    internal lateinit var gender_st: String
    internal lateinit var user_id: String
    internal lateinit var email: String
    internal lateinit var phone: String


    internal lateinit var et_first_id: EditText
    internal lateinit var reg_middlename: EditText
    internal lateinit var reg_lastanme: EditText
    internal lateinit var reg_dob: EditText
    internal lateinit var reg_ssn: EditText
    internal lateinit var reg_address: EditText
    internal lateinit var reg_city: EditText
    internal lateinit var reg_city_lnk: EditText
    internal lateinit var reg_country: EditText
    internal lateinit var reg_apartmentno: EditText
    internal lateinit var reg_country_lnk: TextView
    internal lateinit var reg_state: TextView


    internal lateinit var reg_state_lnk: EditText
    internal lateinit var reg_zipcode: EditText
    internal lateinit var reg_cellphone: EditText
    internal lateinit var reg_homephone: TextView
    internal lateinit var reg_workphone: TextView

    internal lateinit var reg_email: EditText
    internal lateinit var reg_email_confrm: EditText
    internal lateinit var reg_password: EditText
    internal lateinit var reg_organization: TextView
    internal lateinit var tv_header_organization: TextView

    internal lateinit var terms_check: CheckBox
    internal lateinit var create_account: Button


    internal lateinit var reg_random: EditText
    internal lateinit var reg_random_edit: EditText

    internal lateinit var citizen: RadioButton
    internal lateinit var bussiness: RadioButton
    internal lateinit var government: RadioButton
    internal lateinit var myRadioGroup: RadioGroup

    internal lateinit var rb_female: RadioButton
    internal lateinit var rb_male: RadioButton
    var cal = Calendar.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        sessionManager = SessionManager(this)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        user_id = intent.getStringExtra("user_id")
        email = intent.getStringExtra("email")
        phone = intent.getStringExtra("phone")

        var rootview = findViewById(R.id.rootview) as LinearLayout
        dialog_list = Dialog(this)
        dialog_list!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list!!.setContentView(R.layout.dialog_view)
        dialog_list!!.setCancelable(true)

        list_items_list = dialog_list!!.findViewById(R.id.list_languages)


        citizen= findViewById(R.id.citizen) as RadioButton
        bussiness= findViewById(R.id.bussiness) as RadioButton
        government= findViewById(R.id.government) as RadioButton
        myRadioGroup= findViewById(R.id.myRadioGroup) as RadioGroup

        rb_male= findViewById(R.id.male) as RadioButton
        rb_female= findViewById(R.id.female) as RadioButton


        et_first_id = findViewById(R.id.reg_firstname) as EditText
        reg_middlename = findViewById(R.id.reg_middlename) as EditText
        reg_lastanme = findViewById(R.id.reg_lastanme) as EditText
        reg_dob = findViewById(R.id.reg_dob) as EditText
        reg_ssn = findViewById(R.id.reg_ssn) as EditText
        reg_address = findViewById(R.id.reg_address) as EditText
        reg_city = findViewById(R.id.reg_city) as EditText
        reg_city_lnk = findViewById(R.id.reg_city_lnk) as EditText
        reg_country = findViewById(R.id.reg_country) as EditText
        reg_country_lnk = findViewById(R.id.reg_country_lnk) as EditText
        reg_apartmentno = findViewById(R.id.reg_apartmentno) as EditText
        reg_state = findViewById(R.id.reg_state) as EditText
        reg_state_lnk = findViewById(R.id.reg_state_lnk) as EditText
        reg_zipcode = findViewById(R.id.reg_zipcode) as EditText
        reg_cellphone = findViewById(R.id.reg_cellphone) as EditText
        reg_homephone = findViewById(R.id.reg_homephone) as EditText
        reg_workphone = findViewById(R.id.reg_workphone) as EditText
        reg_email = findViewById(R.id.reg_email) as EditText
        reg_password = findViewById(R.id.reg_password) as EditText
        reg_organization = findViewById(R.id.reg_organization) as EditText
        terms_check = findViewById(R.id.terms) as CheckBox
        create_account = findViewById(R.id.reg_create_account) as Button

        reg_random = findViewById(R.id.reg_random) as EditText
        reg_random_edit = findViewById(R.id.reg_random_edit) as EditText
        tv_header_organization = findViewById(R.id.tv_header_organization) as TextView

        reg_email.setText(email)
        reg_cellphone.setText(phone)



        myloading()
        val rand = Random()
        val ran = rand.nextInt(10000)+1000
        Log.d("RANDOM_NUMBER", ""+rand.nextInt(10000)+1000)
        reg_random.setText(ran.toString())
        usertype_st=""
        gender_st = "Male"
        citizen.setOnClickListener(View.OnClickListener {
            reg_organization.visibility=View.GONE
            usertype_st = citizen.text.toString()
        })
        bussiness.setOnClickListener(View.OnClickListener {
            reg_organization.visibility=View.VISIBLE
            tv_header_organization.visibility=View.VISIBLE
            usertype_st = bussiness.text.toString()})
        government.setOnClickListener(View.OnClickListener {
            reg_organization.visibility=View.VISIBLE
            tv_header_organization.visibility=View.VISIBLE
            usertype_st = government.text.toString()
        })
        rb_male.setOnClickListener(View.OnClickListener {
            gender_st=rb_male.text.toString()
        })
        rb_female.setOnClickListener(View.OnClickListener {
            gender_st=rb_female.text.toString()
        })
        terms_check.setOnClickListener(View.OnClickListener {
            if(terms_check.isChecked) {
                create_account.isEnabled = true
                create_account.setBackgroundColor(resources.getColor(R.color.blue))
            } else {
                create_account.isEnabled = false
                create_account.setBackgroundColor(resources.getColor(R.color.gray))
            }
        })
        reg_dob.setOnClickListener {

            val calendar = Calendar.getInstance()
            val mYear = calendar.get(Calendar.YEAR)
            val mMonth = calendar.get(Calendar.MONTH)
            val mDay = calendar.get(Calendar.DAY_OF_MONTH)

            val dpDialog = DatePickerDialog(this, myDateListener, mYear, mMonth, mDay)
            dpDialog.show()
            dpDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());



        }

        reg_state.setOnClickListener(View.OnClickListener {
            if (!dialog_list!!.isShowing)
                dialog_list!!.show()
            val profile_array_adapter = ArrayAdapter<String>(this, R.layout.simple_spinner_item, BundleDataInfo.STATES_LIST)
            list_items_list!!.adapter = profile_array_adapter
            list_items_list!!.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_state = list_items_list!!.getItemAtPosition(i).toString()
                reg_state.setText(selected_state)
                dialog_list!!.dismiss()
                // selected_profile_id = state_code_array.get(stringProfileArrayList.indexOf(selected_profile_name!!))
                Log.e("selected_name_id_name", selected_state)
                reg_state_lnk.setText(BundleDataInfo.STATES_URLLIST.get(i))


            }
        })




        create_account.setOnClickListener(View.OnClickListener {


            if (hasValidCredentials()) {
                if (true) {
                    if (ConnectionManager.checkConnection(applicationContext)) {
                        if(reg_random.text.toString() == reg_random_edit.text.toString()){
                            callRegisterAPI()
                        }else {
                            Toast.makeText(applicationContext, "Enter correct captcha", Toast.LENGTH_SHORT).show()
                        }

                    } else {
                        ConnectionManager.snackBarNetworkAlert(rootview, applicationContext)
                    }
                } else {
                    Toast.makeText(applicationContext, "Please select terms and conditions", Toast.LENGTH_SHORT).show()
                }
            }
        })



    }


    private fun callRegisterAPI() {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.saveUser(usertype_st, et_first_id.text.toString(), reg_middlename.text.toString(), reg_lastanme.text.toString(), reg_dob.text.toString(), gender_st, reg_ssn.text.toString(),reg_address.text.toString(),reg_city.text.toString(), reg_country.text.toString(),reg_state.text.toString(), reg_zipcode.text.toString(),reg_apartmentno.text.toString(),reg_homephone.text.toString(),reg_workphone.text.toString(),reg_city_lnk.text.toString(),reg_state_lnk.text.toString(),reg_country_lnk.text.toString(),reg_organization.text.toString(),user_id,"1")

        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UpdateProfileResponse> {
            override fun onResponse(call: Call<UpdateProfileResponse>, response: retrofit2.Response<UpdateProfileResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        Toast.makeText(applicationContext, "Registration Success", Toast.LENGTH_SHORT).show()
                        val intent = Intent(this@RegisterActivity, LoginActivity::class.java)
                        finish()
                        startActivity(intent)

                    }
                    else{
                        Toast.makeText(applicationContext,response.body()!!.result.toString(),Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<UpdateProfileResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }
    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(et_first_id.text.toString())) {
            et_first_id.setError("Enter first name")
        } else if (TextUtils.isEmpty(reg_lastanme.text.toString())) {
            reg_lastanme.setError("Enter last name")
        }  else if (TextUtils.isEmpty(reg_ssn.text.toString())) {
            reg_ssn.setError("Enter ssn number ")
        } else if (TextUtils.isEmpty(reg_dob.text.toString())) {
            reg_dob.setError("Enter date of birth")
        }  else if (TextUtils.isEmpty(reg_address.text.toString())) {
            reg_address.setError("Enter Resident address")
        }  else if (TextUtils.isEmpty(reg_city.text.toString())) {
            reg_city.setError("Enter city")
        }else if (TextUtils.isEmpty(reg_country.text.toString())) {
            reg_country.setError("Enter county")
        } else if (TextUtils.isEmpty(reg_state.text.toString())) {
            reg_state.setError("Enter State")
        } else if (TextUtils.isEmpty(reg_zipcode.text.toString())) {
            reg_zipcode.setError("Enter Zipcode")
        }   else if (TextUtils.isEmpty(reg_random.text.toString())) {
            reg_random.setError("Enter Captcha")
        }else
            return true
        return false
    }


    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


    private val myDateListener = DatePickerDialog.OnDateSetListener { arg0, year, monthOfYear, dayOfMonth -> Log.e("onDateSet()", "arg0 = [$arg0], year = [$year], monthOfYear = [$monthOfYear], dayOfMonth = [$dayOfMonth]")

        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        var dateofmonth = dayOfMonth.toString()
        var monthofyear = monthOfYear
        var yearstg = year.toString()
        //getAge(year, monthOfYear, dayOfMonth)
        val myFormat = "MM/dd/yyyy" // mention the format you need

        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        Log.e("dateformat", sdf.toString())
        val date = sdf.format(cal.time)
        reg_dob.setText(date)

    }


}
