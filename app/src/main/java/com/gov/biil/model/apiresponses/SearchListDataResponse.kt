package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class SearchListDataResponse {

     val id: String? = null
     val user_id: String? = null
     val user_type: String? = null
     val first_name: String? = null
     val middle_name: String? = null
     val last_name: String? = null
     val dob: String? = null
     val gender: String? = null
     val ssn: String? = null
     val address: String? = null
     val city: String? = null
     val city_url: String? = null
     val county: String? = null
     val county_url: String? = null
     val state: String? = null
     val state_url: String? = null
     val zipcode: String? = null
     val email: String? = null
     val password: String? = null
     val phone_number: String? = null
     val home_phone: String? = null
     val work_phone: String? = null
     val status: String? = null
     val create_date: String? = null
     val organization: String? = null
     val password_link: String? = null
     val mobile_token: String? = null
     val device_type: String? = null
     val mobile_auth: String? = null



}
