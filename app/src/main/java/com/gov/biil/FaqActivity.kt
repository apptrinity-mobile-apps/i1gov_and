package com.gov.biil

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem

class FaqActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_faq)

        val toolbar = findViewById(R.id.toolbar) as Toolbar

        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        var drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.login_menu);
        toolbar.setOverflowIcon(drawable)
    }

    override
    fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override
    fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        when (id) {
            R.id.faq -> {
                val intent = Intent(this@FaqActivity, FaqActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.feedback -> {
                val intent = Intent(this@FaqActivity, FeedBackActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.contact -> {
                val intent = Intent(this@FaqActivity, ContactUsActivity::class.java)
                startActivity(intent)
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }
}
