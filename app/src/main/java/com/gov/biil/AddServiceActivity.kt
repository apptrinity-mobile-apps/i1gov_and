package com.gov.biil


import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.BundleDataInfo
import com.gov.a4printuser.Helper.ConnectionManager
import com.gov.biil.Helper.*
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.ServiceResponse
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*



class AddServiceActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener  {


    internal lateinit var et_cityname: EditText
    internal lateinit var et_address: EditText
    internal lateinit var et_unitnumber: EditText
    internal lateinit var et_city: EditText
    internal lateinit var et_state: EditText
    internal lateinit var et_zipcode: EditText
    internal lateinit var et_servicelink: EditText
    internal lateinit var et_duedate: EditText
    internal lateinit var et_reminderdate1: EditText
    internal lateinit var et_reminderdate2: EditText
    internal lateinit var et_reminderdate3: EditText
    internal lateinit var et_servicetype: EditText
    internal lateinit var addservice: TextView
    internal lateinit var tv_cityname: TextView
    internal lateinit var et_country: EditText

    internal lateinit var ll_reminderdate1: LinearLayout
    internal lateinit var ll_reminderdate2: LinearLayout
    internal lateinit var ll_reminderdate3: LinearLayout

    var list_items_list: ListView? = null
    var dialog_list: Dialog? = null

    internal lateinit var rd_city: RadioButton
    internal lateinit var rd_country: RadioButton
    internal lateinit var rd_state: RadioButton
    internal lateinit var rd_federal: RadioButton
    internal lateinit var btn_plus: Button
    internal lateinit var btn_reminder_plus: ImageView
    internal lateinit var btn_reminder_minus2: ImageView
    internal lateinit var btn_reminder_minus3: ImageView

    internal lateinit var serviceType_st:String
    internal lateinit var sessionManager: SessionManager
    var cal = Calendar.getInstance()
    var parentLayout: LinearLayout? = null


    private val KEY_POSITION = "keyPosition"

    private var navPosition: BottomNavigationPosition = BottomNavigationPosition.HOME

    private lateinit var toolbar: Toolbar

    private lateinit var bottomNavigation: BottomNavigationView

    var screen_position:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addservice)

        sessionManager = SessionManager(this)
        myloading()
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        getAdminStatusInfo()

        et_cityname = findViewById(R.id.et_cityname) as EditText
        tv_cityname = findViewById(R.id.tv_cityname) as TextView
        et_address = findViewById(R.id.et_address) as EditText
        et_unitnumber = findViewById(R.id.et_unitnumber) as EditText
        et_city = findViewById(R.id.et_city) as EditText
        et_country = findViewById(R.id.et_country) as EditText
        et_state = findViewById(R.id.et_state) as EditText
        et_zipcode = findViewById(R.id.et_zipcode) as EditText
        et_servicelink = findViewById(R.id.et_servicelink) as EditText
        et_duedate = findViewById(R.id.et_duedate) as EditText
        et_reminderdate1 = findViewById(R.id.et_reminderdate1) as EditText
        et_reminderdate2 = findViewById(R.id.et_reminderdate2) as EditText
        et_reminderdate3 = findViewById(R.id.et_reminderdate3) as EditText

        ll_reminderdate1 = findViewById(R.id.ll_reminderdate1) as LinearLayout
        ll_reminderdate2 = findViewById(R.id.ll_reminderdate2) as LinearLayout
        ll_reminderdate3 = findViewById(R.id.ll_reminderdate3) as LinearLayout

        btn_reminder_plus= findViewById(R.id.btn_reminder_plus) as ImageView
        btn_reminder_minus2= findViewById(R.id.btn_reminder_minus2) as ImageView
        btn_reminder_minus3= findViewById(R.id.btn_reminder_minus3) as ImageView

        rd_city= findViewById(R.id.City) as RadioButton
        rd_country= findViewById(R.id.Country) as RadioButton
        rd_state= findViewById(R.id.State) as RadioButton
        rd_federal= findViewById(R.id.Federal) as RadioButton
        btn_plus= findViewById(R.id.btn_plus) as Button

        btn_plus= findViewById(R.id.btn_plus) as Button
        parentLayout = findViewById(R.id.ll_edittextlayout) as LinearLayout
        sessionManager = SessionManager(this)
        dialog_list = Dialog(this)
        dialog_list!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list!!.setContentView(R.layout.dialog_view)
        dialog_list!!.setCancelable(true)

        list_items_list = dialog_list!!.findViewById(R.id.list_languages)



        bottomNavigation = findViewById(R.id.bottom_navigation)
        setupBottomNavigation()
        initFragment(savedInstanceState)

        /* if(screen_position.equals("result_screen")){
             navPosition = BottomNavigationPosition.ADJEST
         }else{*/
       // navPosition = BottomNavigationPosition.HOME






        var rootview = findViewById(R.id.rootview) as RelativeLayout
        addservice = findViewById(R.id.addservice_id) as TextView

        addservice.setOnClickListener(View.OnClickListener {view ->
            //textOut.getTag()

//workingcode dynamic textview
          /*  for (i in 0 until ed.size) {

                Log.d("Value ", "Val " + ed[i]!!.getText().toString())
            }*/

            if(hasValidCredentials()){
                if(ConnectionManager.checkConnection(applicationContext)) {
                    callAddServiceAPI()
                }else{
                    ConnectionManager.snackBarNetworkAlert_Relative(rootview,applicationContext)
                }
            }

        })
        serviceType_st=rd_city.text.toString()
        rd_city.setOnClickListener(View.OnClickListener {
            et_cityname.setHint("City Name")
            et_address.setHint("Address")
            tv_cityname.setText("City Name")
            serviceType_st=rd_city.text.toString()
        })
        rd_country.setOnClickListener(View.OnClickListener {
            et_cityname.setHint("County Name")
            et_address.setHint("Address")
            tv_cityname.setText("County Name")
            serviceType_st=rd_country.text.toString()
        })
        rd_state.setOnClickListener(View.OnClickListener {
            et_cityname.setHint("State Name")
            et_address.setHint("Address")
            tv_cityname.setText("State Name")
            serviceType_st=rd_state.text.toString()
        })
        rd_federal.setOnClickListener(View.OnClickListener {
            et_cityname.setHint("Department Name")
            et_address.setHint("Department Address")
            tv_cityname.setText("Department Name")
            serviceType_st=rd_federal.text.toString()
        })
        et_duedate.setOnClickListener {
           /* DatePickerDialog(this@AddServiceActivity, duedatelistener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()*/

            val datePickerDialog = DatePickerDialog(this, duedatelistener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));
            // Limiting access to past dates in the step below:
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show()
        }
        et_reminderdate1.setOnClickListener {
            /*DatePickerDialog(this@AddServiceActivity, reminderdatelistener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()*/

            val datePickerDialog = DatePickerDialog(this, reminderdatelistener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));
            // Limiting access to past dates in the step below:
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show()


        }


        et_state.setOnClickListener(View.OnClickListener {
            if (!dialog_list!!.isShowing)
                dialog_list!!.show()
            val profile_array_adapter = ArrayAdapter<String>(this, R.layout.simple_spinner_item, BundleDataInfo.STATES_LIST)
            list_items_list!!.adapter = profile_array_adapter
            list_items_list!!.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_state = list_items_list!!.getItemAtPosition(i).toString()
                et_state.setText(selected_state)
                dialog_list!!.dismiss()
                // selected_profile_id = state_code_array.get(stringProfileArrayList.indexOf(selected_profile_name!!))
                Log.e("selected_name_id_name", selected_state)


            }
        })


        et_reminderdate2.setOnClickListener {
           /* DatePickerDialog(this@AddServiceActivity, reminderdatelistener2,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()*/

            val datePickerDialog = DatePickerDialog(this, reminderdatelistener2,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));
            // Limiting access to past dates in the step below:
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show()

        }
        et_reminderdate3.setOnClickListener {
            /*DatePickerDialog(this@AddServiceActivity, reminderdatelistener3,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()*/

            val datePickerDialog = DatePickerDialog(this, reminderdatelistener3,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));
            // Limiting access to past dates in the step below:
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show()

        }


        /*btn_plus.setOnClickListener(View.OnClickListener {view->
            createEditTextView3(view)
           // createEditTextView2(view)

        })*/


        btn_reminder_plus.setOnClickListener(View.OnClickListener {view->

            if(et_reminderdate1.text.toString().equals("")){
                Toast.makeText(applicationContext,"Please set Reminder date",Toast.LENGTH_SHORT).show()
            }else{
                ll_reminderdate2.visibility= View.VISIBLE
                if(et_reminderdate2.text.toString().equals("")){
                    Toast.makeText(applicationContext,"Please set Reminder date",Toast.LENGTH_SHORT).show()
                }else{
                    ll_reminderdate3.visibility= View.VISIBLE
                }
            }


        })

        btn_reminder_minus2.setOnClickListener(View.OnClickListener {view->
            ll_reminderdate2.visibility = View.GONE
            et_reminderdate2.text.clear()

        })
        btn_reminder_minus3.setOnClickListener(View.OnClickListener {view->

            ll_reminderdate3.visibility = View.GONE
            et_reminderdate3.text.clear()


        })


    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun callAddServiceAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.serviceAdd(et_address.text.toString(),et_unitnumber.text.toString(),sessionManager.isId,et_city.text.toString(),et_state.text.toString(),et_zipcode.text.toString(),et_servicelink.text.toString()
        ,et_duedate.text.toString(),et_reminderdate1.text.toString(),et_cityname.text.toString(),et_reminderdate2.text.toString(),et_reminderdate3.text.toString(),serviceType_st)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<ServiceResponse> {
            override fun onResponse(call: Call<ServiceResponse>, response: retrofit2.Response<ServiceResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if(response.body()!!.status.equals("1")){
                        Toast.makeText(applicationContext,"Service added Successfully",Toast.LENGTH_SHORT).show()
                        val intent = Intent(this@AddServiceActivity,MainActivity::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                    }

                }
            }

            override fun onFailure(call: Call<ServiceResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })
    }

    private fun hasValidCredentials(): Boolean {

        if (TextUtils.isEmpty(et_cityname.text.toString()))
            et_cityname.setError("City name Required")
        else if (TextUtils.isEmpty(et_address.text.toString()))
            et_address!!.setError("Address Required")
        else if (TextUtils.isEmpty(et_city.text.toString()))
            et_city!!.setError("City Required")
        else if (TextUtils.isEmpty(et_state.text.toString()))
            et_state!!.setError("State Required")
        else if (TextUtils.isEmpty(et_zipcode.text.toString()))
            et_zipcode!!.setError("zipcode required")
        else if (TextUtils.isEmpty(et_servicelink.text.toString()))
            et_servicelink!!.setError("Service link Required")
        else if (TextUtils.isEmpty(et_duedate.text.toString()))
            et_duedate!!.setError("Due date Required")
        else if (TextUtils.isEmpty(et_reminderdate1.text.toString()))
            et_reminderdate1!!.setError("Reminder Required")
        else
            return true
        return false
    }
    val duedatelistener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        var dateofmonth = dayOfMonth.toString()
        var monthofyear = monthOfYear
        var yearstg = year.toString()
        //getAge(year, monthOfYear, dayOfMonth)
        val myFormat = "MM/dd/yyyy" // mention the format you need

        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        Log.e("dateformat", sdf.toString())
        val date = sdf.format(cal.time)

        et_duedate.setText(date)

    }
    val reminderdatelistener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->




      //  monthOfYear.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)



        var dateofmonth = dayOfMonth.toString()
        var monthofyear = monthOfYear
        var yearstg = year.toString()
        //getAge(year, monthOfYear, dayOfMonth)
        val myFormat = "MM/dd/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        Log.e("dateformat", sdf.toString())
        val date = sdf.format(cal.time)

        et_reminderdate1.setText(date)

    }

    val reminderdatelistener2 = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

        //  monthOfYear.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        var dateofmonth = dayOfMonth.toString()
        var monthofyear = monthOfYear
        var yearstg = year.toString()
        //getAge(year, monthOfYear, dayOfMonth)
        val myFormat = "MM/dd/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        Log.e("dateformat", sdf.toString())
        val date = sdf.format(cal.time)

        et_reminderdate2.setText(date)

    }

    val reminderdatelistener3 = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

        //  monthOfYear.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        var dateofmonth = dayOfMonth.toString()
        var monthofyear = monthOfYear
        var yearstg = year.toString()
        //getAge(year, monthOfYear, dayOfMonth)
        val myFormat = "MM/dd/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        Log.e("dateformat", sdf.toString())
        val date = sdf.format(cal.time)

        et_reminderdate3.setText(date)

    }



    private var hint = 0
    private var tetxvalue = ""
    private var tetxvalue2 = ""
    private var tetxvalue3 = ""
    lateinit var textOut:EditText
    @SuppressLint("ServiceCast")
    protected fun createEditTextView(view: View) {
        val layoutInflater = baseContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val addView = layoutInflater.inflate(R.layout.row, null);
        textOut = addView.findViewById(R.id.textout) as EditText
        textOut.setHint("Reminder:"+hint)

        hint++
        val buttonRemove = addView.findViewById(R.id.remove) as TextView

        buttonRemove.setOnClickListener(View.OnClickListener {
            //((LinearLayout)addView.getParent()).removeView(addView);
            parentLayout!!.removeView(addView)
        })

        parentLayout!!.addView(addView)
        textOut.setTag(view.id,textOut.text.toString())
        tetxvalue= view.id.toString()
        tetxvalue2= textOut.id.toString()
        tetxvalue3= addView.id.toString()


    }

    /*lateinit var ed : EditText
    val  allEds =  ArrayList<EditText>();
    protected fun createEditTextView2(view: View) {




        for ( i in 0 until 10) {

            ed =  EditText(this);
            allEds.add(ed);
            ed.setTag(view.id,ed.text);
            ed.setLayoutParams(LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            parentLayout!!.addView(ed);
        }

    }*/
    val ed = arrayOfNulls<EditText>(10)
    protected fun createEditTextView3(view: View) {

        for (i in 0 until 10) {

            ed[i] = EditText(this)

            ed[i]!!.setId(i)
            ed[i]!!.setLayoutParams(LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT))
            parentLayout!!.addView(ed[i])
        }


    }

    override fun onSaveInstanceState(outState: Bundle?) {
        // Store the current navigation position.
        //outState?.putInt(KEY_POSITION, navPosition.id)
        /* if(screen_position.equals("result_screen")){
             outState?.putInt(KEY_POSITION, 2131230874)
         }else{*/
        outState?.putInt(KEY_POSITION, navPosition.id)
        //}
        super.onSaveInstanceState(outState)
    }

    private fun restoreSaveInstanceState(savedInstanceState: Bundle?) {
        // Restore the current navigation position.
        savedInstanceState?.also {
            //val id = it.getInt(KEY_POSITION, BottomNavigationPosition.DOSING.id)

            /*if(screen_position.equals("result_screen")){
                val id = it.getInt(KEY_POSITION, BottomNavigationPosition.ADJEST.id)
                navPosition = findNavigationPositionById(id)
            }else{*/
            val id = it.getInt(KEY_POSITION, BottomNavigationPosition.HOME.id)
            navPosition = findNavigationPositionById(id)
            //}

        }
    }

    private fun setupBottomNavigation() {
        bottomNavigation.disableShiftMode() // Extension function
        bottomNavigation.active(navPosition.position)   // Extension function
        bottomNavigation.setOnNavigationItemSelectedListener(this)
    }

    private fun initFragment(savedInstanceState: Bundle?) {

        if(screen_position.equals("result_screen")){
            savedInstanceState ?: switchFragment(BottomNavigationPosition.COUNTY)
            bottomNavigation.menu.findItem(R.id.county).setChecked(true);
        }else{
          //  savedInstanceState ?: switchFragment(BottomNavigationPosition.HOME)

        }
    }

    /**
     * Immediately execute transactions with FragmentManager#executePendingTransactions.
     */
    private fun switchFragment(navPosition: BottomNavigationPosition): Boolean {
        val fragment = supportFragmentManager.findFragment(navPosition)
        if (fragment.isAdded) return false
        detachFragment()
        attachFragment(fragment, navPosition.getTag())
        supportFragmentManager.executePendingTransactions()
        return true
    }

    private fun FragmentManager.findFragment(position: BottomNavigationPosition): Fragment {
        return findFragmentByTag(position.getTag()) ?: position.createFragment()
    }



    private fun detachFragment() {
        supportFragmentManager.findFragmentById(R.id.container)?.also {
            supportFragmentManager.beginTransaction().detach(it).commit()
        }
    }

    private fun attachFragment(fragment: Fragment, tag: String) {
        if (fragment.isDetached) {
            supportFragmentManager.beginTransaction().attach(fragment).commit()
        } else {
            supportFragmentManager.beginTransaction().add(R.id.container, fragment, tag).commit()
        }
        // Set a transition animation for this transaction.
        supportFragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit()
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        //navPosition = findNavigationPositionById(item.itemId)
        Log.e("BottomNavigationHelper", "Unable to get shift mode field")
        // Log.e("KEY_POSITION_lll",KEY_POSITION+"---"+navPosition.id)
        //2131230874

        /*  if(screen_position.equals("result_screen")){
              navPosition = findNavigationPositionById(2131230874)
          }else{*/
        navPosition = findNavigationPositionById(item.itemId)
        Log.e("KEY_POSITION_lll",KEY_POSITION+"---"+navPosition.id+"--"+item.itemId)
        //}
        return switchFragment(navPosition)
    }


    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


}

