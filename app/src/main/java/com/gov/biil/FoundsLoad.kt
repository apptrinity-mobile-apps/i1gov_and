package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Window
import android.widget.EditText
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.CustomTextViewBold
import com.gov.biil.model.ApiInterface
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback

class FoundsLoad : AppCompatActivity() {
    lateinit var et_amount_id: EditText
    lateinit var tv_add_amount_id: CustomTextViewBold
    lateinit var tv_balance_id: CustomTextView
    var total_main_balance = ""
    internal lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_founds_load)
        myloading()
        sessionManager = SessionManager(this)
        getAdminStatusInfo()

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        et_amount_id = findViewById(R.id.et_amount_id)
        tv_add_amount_id = findViewById(R.id.tv_add_amount_id)
        tv_balance_id = findViewById(R.id.tv_balance_id)
        total_main_balance=intent.getStringExtra("total_main_balance")
        tv_balance_id.setText("Available Balance : $ "+total_main_balance)
        tv_add_amount_id.setOnClickListener {
            val amount = et_amount_id.text.toString().trim()
            if (amount.equals("")) {
                Toast.makeText(this@FoundsLoad, "Please Enter Amount", Toast.LENGTH_SHORT).show()
            } else if (amount.toInt()>=50) {
                val intent = Intent(this, StripePayment::class.java)
                intent.putExtra("amount", amount)
                intent.putExtra("from", "load_funds")
                startActivity(intent)
            }else{
                Toast.makeText(this@FoundsLoad, "Minimum Amount $50", Toast.LENGTH_SHORT).show()
            }
        }
    }



    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

}
