package com.gov.biil

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.InputType
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.*
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*




class PaytoParkActivty : AppCompatActivity() {
    lateinit var dialog: Dialog
    lateinit var dialog2: Dialog
    lateinit var img_close_id: ImageView
    lateinit var img_down: ImageView
    lateinit var down_arrow_current: ImageView
    lateinit var iv_currentparking: CardView
    lateinit var cv_currentparking: CardView
    lateinit var iv_newparking: CardView
    lateinit var lv_currentparking_listview: ListView
    lateinit var btn_accept_id: Button
    lateinit var tv_nodata: TextView
    lateinit var tv_start_park_id: TextView
    lateinit var tv_vehicle_type_id: TextView
    lateinit var et_vehicle_no_id: EditText
    lateinit var tv_balance_id: TextView
    lateinit var et_vehicle_state_id: EditText
    lateinit var et_vehicle_stateID_id: EditText
    lateinit var rb_personal_id: RadioButton
    lateinit var rb_commercial_id: RadioButton
    lateinit var et_parkzone_no_id: EditText
    lateinit var et_parkaddress_id: EditText
    lateinit var et_street_no_id: EditText

    lateinit var tv_time_and_price_id: TextView

    lateinit var dialog_list_zone: Dialog
    lateinit var dialog_list: Dialog
    lateinit var dialog_list_price: Dialog
    lateinit var dialog_list_address: Dialog
    lateinit var list_items_list: ListView
    lateinit var list_items_list_zone: ListView
    lateinit var list_items_list_price: ListView
    lateinit var list_items_list_address: ListView
    lateinit var ll_load_funds_id: LinearLayout
    var vehicle_type = "Personal"
    var final_receipt_main_id = ""

    var stateslistArray = ArrayList<stateslistDataResponse>()
    var zonelistArray = ArrayList<parkzones>()
    var addressListArray = ArrayList<parkinAddress>()

    var priceslistArray = ArrayList<priceslist>()
    var addresspricesArray = ArrayList<lotaddresspriceslist>()
    var mSateId = ""
    var mZoneId = ""
    var mAddresId = ""
    var vehicle_number = ""
    var vehicle_state = ""
    var vehicle_state_id = ""
    var vehicle_zone = ""
    var vehicle_address = ""
    var vehicle_time = ""
    var vehicle_time_only = ""
    var vehicle_price = ""
    var vehicle_price_id = ""
    var total_main_balance = ""
    public var parkingTimesData: ArrayList<getAllParkingDataResponse> = ArrayList<getAllParkingDataResponse>()
    private var mParkingTimeListAdapter: ParkingTimeListAdapter? = null

    var arrowdownup = false
    internal lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payto_park_activty)
        sessionManager = SessionManager(this)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        myloading()

        getAdminStatusInfo()
        getStatesAPI()

        getProfileInfo()
        dialog2 = Dialog(this);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog2.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog2.setCancelable(false)
        dialog2.getWindow().setGravity(Gravity.BOTTOM)
        dialog2.setContentView(R.layout.dialog_list_paytopark)
        //dialog2.show()

        ll_load_funds_id = findViewById(R.id.ll_load_funds_id)
        ll_load_funds_id.setOnClickListener {
            val intent = Intent(this, FoundsLoad::class.java)
            intent.putExtra("total_main_balance", total_main_balance)
            startActivity(intent)
        }
        iv_currentparking = dialog2.findViewById(R.id.iv_currentparking)
        cv_currentparking = dialog2.findViewById(R.id.cv_currentparking)
        iv_newparking = dialog2.findViewById(R.id.iv_newparking)
        lv_currentparking_listview = dialog2.findViewById(R.id.lv_currentparking_listview)
        down_arrow_current = dialog2.findViewById(R.id.down_arrow_current)
        tv_nodata = dialog2.findViewById(R.id.tv_nodata)
        img_down = dialog2.findViewById(R.id.img_down)


        iv_currentparking.setOnClickListener(View.OnClickListener {
            Log.d("arrow", "" + arrowdownup)
            if (arrowdownup == false) {
                Log.d("arrow1", "" + arrowdownup)
                img_down!!.visibility = View.VISIBLE
                down_arrow_current.setImageResource(R.drawable.up_arrow)
                arrowdownup = true
                cv_currentparking!!.visibility = View.VISIBLE
                lv_currentparking_listview!!.visibility = View.VISIBLE


            } else {
                Log.d("arrow2", "" + arrowdownup)
                cv_currentparking!!.visibility = View.GONE
                lv_currentparking_listview!!.visibility = View.GONE
                img_down!!.visibility = View.GONE
                down_arrow_current.setImageResource(R.drawable.down_arrow)
                arrowdownup = false
            }

        })


        dialog = Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCancelable(false)
        dialog.getWindow().setGravity(Gravity.BOTTOM)
        dialog.setContentView(R.layout.accept_dialog_layout)


        img_close_id = dialog.findViewById(R.id.img_close_id)
        btn_accept_id = dialog.findViewById(R.id.btn_accept_id)
        img_close_id.setOnClickListener {
            onBackPressed()
        }
        Log.e("sessionManager_terms", sessionManager.acceptId)



        iv_newparking.setOnClickListener(View.OnClickListener {
            Log.d("arrow", "" + arrowdownup)
            dialog2.dismiss()
            if (sessionManager.acceptId.equals("0")) {
                dialog.show()
            } else {
                dialog.dismiss()
            }
            btn_accept_id.setOnClickListener {
                calltcStatusAPI()
            }


        })





        tv_start_park_id = findViewById(R.id.tv_start_park_id)
        tv_balance_id = findViewById(R.id.tv_balance_id)
        et_vehicle_no_id = findViewById(R.id.et_vehicle_no_id)
        et_vehicle_state_id = findViewById(R.id.et_vehicle_state_id)
        et_vehicle_stateID_id = findViewById(R.id.et_vehicle_stateID_id)
        rb_personal_id = findViewById(R.id.rb_personal_id)
        rb_commercial_id = findViewById(R.id.rb_commercial_id)
        et_parkzone_no_id = findViewById(R.id.et_parkzone_no_id)
        et_parkaddress_id = findViewById(R.id.et_parkaddress_id)
        tv_vehicle_type_id = findViewById(R.id.tv_vehicle_type_id)
        et_street_no_id = findViewById(R.id.et_street_no_id)
        tv_time_and_price_id = findViewById(R.id.tv_time_and_price_id)

        if(!sessionManager.no_plate_stg.equals("")){
            et_vehicle_no_id.setText(sessionManager.no_plate_stg)
        }


        et_vehicle_no_id.addTextChangedListener(object : TextWatcher {
            var len = 0
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                sessionManager. editor.putString(SessionManager.SESSION_NO_PLATE, s.toString())
                sessionManager. editor.commit()

                val str = et_vehicle_no_id.getText().toString()
                if (str.length == 0){
                    if (str.length == 3 && len < str.length) {//len check for backspace
                        et_vehicle_no_id.append(" ")

                    }
                    et_vehicle_no_id.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES)

                }



            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                val str = et_vehicle_no_id.getText().toString()
                len = str.length

            }

            override fun afterTextChanged(s: Editable) {

                val str = et_vehicle_no_id.getText().toString()

                if(str.length ==1){
                    if (str.length == 3 && len < str.length) {//len check for backspace
                        et_vehicle_no_id.append(" ")

                    }
                    et_vehicle_no_id.setInputType(InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED)

                }



            }
        })

        tv_start_park_id.setOnClickListener {
            vehicle_number = et_vehicle_no_id.text.toString()
            vehicle_state = et_vehicle_state_id.text.toString()
            vehicle_state_id = et_vehicle_stateID_id.text.toString()
            vehicle_zone = et_parkzone_no_id.text.toString()
            vehicle_address = et_parkaddress_id.text.toString()
            vehicle_time = tv_time_and_price_id.text.toString()


            Log.e("Statename",vehicle_state+"----"+vehicle_state_id)

            if (hasValidCredentials()) {
                val intent = Intent(this, ReceiptActivity::class.java)
                intent.putExtra("vehicle_no", vehicle_number)
                intent.putExtra("vehicle_state", vehicle_state)
                intent.putExtra("vehicle_stateid", vehicle_state_id)
                intent.putExtra("vehicle_zone", vehicle_zone)
                intent.putExtra("vehicle_address", vehicle_address)
                intent.putExtra("vehicle_time", vehicle_time)
                intent.putExtra("vehicle_time_only", vehicle_time_only)
                intent.putExtra("vehicle_price", vehicle_price)
                intent.putExtra("vehicle_price_id", vehicle_price_id)
                intent.putExtra("vehicle_type", vehicle_type)
                intent.putExtra("zone_id", mZoneId)
                intent.putExtra("lot_rateid", mAddresId)
                intent.putExtra("total_main_balance", total_main_balance)
                startActivity(intent)
            }

        }

        rb_personal_id.setOnClickListener {
            rb_personal_id.isChecked
            rb_commercial_id.isChecked = false
            vehicle_type = "Personal"
            tv_vehicle_type_id.text = "Personal Vehicle Rates"
        }
        rb_commercial_id.setOnClickListener {
            rb_commercial_id.isChecked
            rb_personal_id.isChecked = false
            vehicle_type = "Commercial"
            tv_vehicle_type_id.text = "Commercial Vehicle Rates"
        }


        dialog_list = Dialog(this)
        dialog_list.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list.setContentView(R.layout.dialog_view)
        dialog_list.setCancelable(true)
        list_items_list = dialog_list.findViewById(R.id.list_languages) as ListView

        et_vehicle_state_id.setOnClickListener {
            if (!dialog_list.isShowing())
                dialog_list.show()
        }

        dialog_list_zone = Dialog(this)
        dialog_list_zone.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_zone.setContentView(R.layout.dialog_view)
        dialog_list_zone.setCancelable(true)
        list_items_list_zone = dialog_list_zone.findViewById(R.id.list_languages) as ListView
        et_parkzone_no_id.setOnClickListener {
            if (TextUtils.isEmpty(et_vehicle_state_id.text.toString())) {
                // et_vehicle_state_id.setError("State Required")
                Toast.makeText(this@PaytoParkActivty, "State Required ", Toast.LENGTH_SHORT).show()
            } else {
                if (!dialog_list_zone.isShowing())
                    dialog_list_zone.show()
            }

        }

        dialog_list_price = Dialog(this)
        dialog_list_price.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_price.setContentView(R.layout.dialog_view)
        dialog_list_price.setCancelable(true)
        list_items_list_price = dialog_list_price.findViewById(R.id.list_languages) as ListView
        tv_time_and_price_id.setOnClickListener {
            if (TextUtils.isEmpty(et_parkzone_no_id.text.toString()) && TextUtils.isEmpty(et_parkaddress_id.text.toString()))
                Toast.makeText(this@PaytoParkActivty, "Select Parking Zone Number or Parking Lot Address ", Toast.LENGTH_SHORT).show()
            else {
                if (!dialog_list_price.isShowing())
                    dialog_list_price.show()
            }

        }

        dialog_list_address = Dialog(this)
        dialog_list_address.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_address.setContentView(R.layout.dialog_view)
        dialog_list_address.setCancelable(true)
        list_items_list_address = dialog_list_address.findViewById(R.id.list_languages) as ListView
        et_parkaddress_id.setOnClickListener {
            if (TextUtils.isEmpty(et_vehicle_state_id.text.toString())) {
                //et_vehicle_state_id.setError("State Required")
                Toast.makeText(this@PaytoParkActivty, "State Required ", Toast.LENGTH_SHORT).show()
            } else {
                if (!dialog_list_address.isShowing())
                    dialog_list_address.show()
            }

        }
    }


    private fun getStatesAPI() {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getpaytoparkStates()
        Log.d("REQUEST", call.toString() + "")
        stateslistArray.clear()
        call.enqueue(object : Callback<getStatesresponse> {
            override fun onResponse(call: Call<getStatesresponse>, response: retrofit2.Response<getStatesresponse>?) {
                if (response != null) {
                    // my_loader.dismiss()
                    Log.w("Result_Address_Profile", "Result : " + response.body()!!.statesList)
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.statesList != null) {
                            Log.w("Result_Address_Profile", "Result : " + response.body()!!.statesList)
                            // setProfileDatatoView(response.body()!!.user_info)
                            val list: Array<stateslistDataResponse>? = response.body()!!.statesList!!
                            for (i in 0 until list!!.size) {
                                Log.w("states_date", list.get(i).state.toString())
                                stateslistArray!!.add(list.get(i))

                                if(list.get(i).state.equals("Illinois")){
                                    mSateId = list.get(i).id.toString()
                                    et_vehicle_state_id.setText(list.get(i).state)
                                    Log.e("STATE_ID",mSateId)
                                    calltoZonesAPI()


                                }
                                if(list.get(i).state_code.equals("IL")) {
                                    et_vehicle_stateID_id.setText(list.get(i).state_code)
                                }

                            }



                            val adapter_state = Dataadapter(this@PaytoParkActivty, stateslistArray)
                            list_items_list.adapter = adapter_state
                        }

                    }

                }
            }

            override fun onFailure(call: Call<getStatesresponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }


        })
    }

    private fun calltoZonesAPI() {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getParkinzonesandAddress(mSateId)
        Log.d("REQUEST", call.toString() + "")
        zonelistArray.clear()
        addressListArray.clear()
        call.enqueue(object : Callback<getParkinZonesadrsResponse> {
            override fun onResponse(call: Call<getParkinZonesadrsResponse>, response: retrofit2.Response<getParkinZonesadrsResponse>?) {
                if (response != null) {
                    // my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.parkingList!!.parkingZones != null && response.body()!!.parkingList!!.lotAddresses != null) {
                            // setProfileDatatoView(response.body()!!.user_info)
                            val list: Array<parkzones>? = response.body()!!.parkingList!!.parkingZones!!
                            val list_address: Array<parkinAddress>? = response.body()!!.parkingList!!.lotAddresses!!
                            for (i in 0 until list!!.size) {
                                Log.w("states_date", list.get(i).zone_number.toString())
                                zonelistArray!!.add(list.get(i))

                            }
                            for (j in 0 until list_address!!.size) {
                                Log.w("states_date", list_address.get(j).lot_address.toString())
                                addressListArray!!.add(list_address.get(j))

                            }
                            val adapter_zone = ZonesAdapter(this@PaytoParkActivty, zonelistArray)
                            list_items_list_zone.adapter = adapter_zone

                            val adapter_address = AddressAdapter(this@PaytoParkActivty, addressListArray)
                            list_items_list_address.adapter = adapter_address

                        }

                    }
                }
            }

            override fun onFailure(call: Call<getParkinZonesadrsResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }


        })
    }

    private fun callPricesAPI() {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getparkingprices(mZoneId)
        Log.d("REQUEST", call.toString() + "")
        priceslistArray.clear()
        call.enqueue(object : Callback<zonePricesResponse> {
            override fun onResponse(call: Call<zonePricesResponse>, response: retrofit2.Response<zonePricesResponse>?) {
                if (response != null) {
                    // my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.zoneprises != null) {
                            // setProfileDatatoView(response.body()!!.user_info)
                            val list: Array<priceslist>? = response.body()!!.zoneprises!!
                            for (i in 0 until list!!.size) {
                                Log.w("states_date", list.get(i).zone_number.toString())
                                priceslistArray!!.add(list.get(i))


                            }
                            val adapter_zone = PricesAdapter(this@PaytoParkActivty, priceslistArray)
                            list_items_list_price.adapter = adapter_zone
                            adapter_zone.notifyDataSetChanged()
                        }

                    }

                }
            }

            override fun onFailure(call: Call<zonePricesResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

    private fun callAddressPricesAPI() {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAddressPrices(mAddresId)
        Log.d("REQUEST", call.toString() + "")
        addresspricesArray.clear()
        addresspricesArray = ArrayList<lotaddresspriceslist>()
        call.enqueue(object : Callback<addressprices> {
            override fun onResponse(call: Call<addressprices>, response: retrofit2.Response<addressprices>?) {
                if (response != null) {
                    // my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.zoneprises != null) {
                            // setProfileDatatoView(response.body()!!.user_info)
                            val list: Array<lotaddresspriceslist>? = response.body()!!.zoneprises!!
                            for (i in 0 until list!!.size) {
                                Log.w("states_date_id", list.get(i).lot_address.toString())
                                addresspricesArray!!.add(list.get(i))

                            }
                            val adapter_zone = AddressPricesAdapter(this@PaytoParkActivty, addresspricesArray)
                            list_items_list_price.adapter = adapter_zone
                            adapter_zone.notifyDataSetChanged()
                        }

                    } else if (response.body()!!.status.equals("2")) {
                        addresspricesArray.clear()
                        priceslistArray.clear()
                    }

                }
            }

            override fun onFailure(call: Call<addressprices>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }


        })
    }

    private fun calltcStatusAPI() {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.posttcStatus(sessionManager.isId, "1")
        Log.d("REQUEST", call.toString() + "")

        call.enqueue(object : Callback<addressprices> {
            override fun onResponse(call: Call<addressprices>, response: retrofit2.Response<addressprices>?) {
                if (response != null) {
                    // my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        dialog.dismiss()
                        sessionManager.editor.putString(SessionManager.SESSION_TERMS, "1")
                        sessionManager.editor.commit()
                        Log.w("save_or_not", sessionManager.acceptId)
                    }

                }
            }

            override fun onFailure(call: Call<addressprices>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }


        })
    }

    private fun getProfileInfo() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getUserProfile(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UserProfileResponse> {
            override fun onResponse(call: Call<UserProfileResponse>, response: retrofit2.Response<UserProfileResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    // Log.w("Result_Address_Profile","Result : "+response.body()!!.user_info)
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            Log.w("Result_Address_Profile", "Result : " + response.body()!!.user_info)
                            total_main_balance = response.body()!!.user_info!!.wallet_amount!!
                            Log.w("total_main_balance", "Result : " +total_main_balance)
                            tv_balance_id.setText("$ " + total_main_balance)
                        }

                    }

                }
            }

            override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

    inner class Dataadapter(context: Context, items: ArrayList<stateslistDataResponse>) : ArrayAdapter<stateslistDataResponse>(context, 0, items) {

        internal var inflater: LayoutInflater

        init {
            inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var v = convertView
            val cell = getItem(position)

            v = inflater.inflate(R.layout.states_list_item, null)
            val name = v!!.findViewById(R.id.tv_list_id) as TextView


            name.setText(cell.state)
            name.setOnClickListener {
                mSateId = cell.id.toString()
                dialog_list.dismiss()
                et_vehicle_state_id.setText(cell.state)
                et_vehicle_stateID_id.setText(cell.state_code)
                Log.e("mJobType_id_lll", mSateId)

                calltoZonesAPI()
            }

            return v
        }
    }

    inner class ZonesAdapter(context: Context, items: ArrayList<parkzones>) : ArrayAdapter<parkzones>(context, 0, items) {

        internal var inflater: LayoutInflater

        init {
            inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var v = convertView
            val cell = getItem(position)

            v = inflater.inflate(R.layout.states_list_item, null)
            val name = v!!.findViewById(R.id.tv_list_id) as TextView

            name.setText(cell.zone_number)
            name.setOnClickListener {
                mZoneId = cell.id.toString()
                dialog_list_zone.dismiss()
                et_parkzone_no_id.setText(cell.zone_number)
                et_parkaddress_id.setText("")
                tv_time_and_price_id.setText("Select Time & Rate")
                Log.e("mJobType_id_lll", mZoneId)
                mAddresId = ""

                callPricesAPI()
            }

            return v
        }
    }

    inner class AddressAdapter(context: Context, items: ArrayList<parkinAddress>) : ArrayAdapter<parkinAddress>(context, 0, items) {

        internal var inflater: LayoutInflater

        init {
            inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var v = convertView
            val cell = getItem(position)

            v = inflater.inflate(R.layout.states_list_item, null)
            val name = v!!.findViewById(R.id.tv_list_id) as TextView

            name.setText(cell.lot_address)
            name.setOnClickListener {
                mAddresId = cell.id.toString()
                dialog_list_address.dismiss()
                et_parkaddress_id.setText(cell.lot_address)
                et_parkzone_no_id.setText("")
                tv_time_and_price_id.setText("Select Time & Rate")
                Log.e("mJobType_id_adress", mAddresId)
                mZoneId = ""

                callAddressPricesAPI()
            }

            return v
        }
    }

    inner class PricesAdapter(context: Context, items: ArrayList<priceslist>) : ArrayAdapter<priceslist>(context, 0, items) {

        internal var inflater: LayoutInflater

        init {
            inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var v = convertView
            val cell = getItem(position)

            v = inflater.inflate(R.layout.prices_list_item, null)
            val name = v!!.findViewById(R.id.tv_prices_id) as TextView
            val ch_hrs_id = v!!.findViewById(R.id.ch_hrs_id) as TextView

            if (cell.hour.equals("1")) {
                ch_hrs_id.setText(cell.hour + " hr")
            } else {
                ch_hrs_id.setText(cell.hour + " hrs")
            }
            if (vehicle_type.equals("Personal")) {
                name.setText(cell.personal_price)

                ch_hrs_id.setOnClickListener {
                    addresspricesArray.clear()
                    dialog_list_price.dismiss()
                    vehicle_price_id = cell.id.toString()
                    vehicle_price = cell.personal_price.toString()
                    if (cell.hour.equals("1")) {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id.setText(cell.hour + " hr" /*+ " & $ " + cell.personal_price*/)
                    } else {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id.setText(cell.hour + " hrs" /*+ " & $ " + cell.personal_price*/)
                    }
                }
            } else {
                name.setText(cell.commercial_price)
                ch_hrs_id.setOnClickListener {
                    addresspricesArray.clear()
                    dialog_list_price.dismiss()
                    vehicle_price_id = cell.id.toString()
                    vehicle_price = cell.commercial_price.toString()
                    if (cell.hour.equals("1")) {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id.setText(cell.hour + " hr" /*+ " & $ " + cell.commercial_price*/)
                    } else {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id.setText(cell.hour + " hrs" /*+ " & $ " + cell.commercial_price*/)
                    }
                }
            }



            return v
        }
    }

    inner class AddressPricesAdapter(context: Context, items: ArrayList<lotaddresspriceslist>) : ArrayAdapter<lotaddresspriceslist>(context, 0, items) {

        internal var inflater: LayoutInflater

        init {
            inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var v = convertView
            val cell = getItem(position)

            v = inflater.inflate(R.layout.prices_list_item, null)
            val name = v!!.findViewById(R.id.tv_prices_id) as TextView
            val ch_hrs_id = v!!.findViewById(R.id.ch_hrs_id) as TextView

            if (cell.hour.equals("1")) {
                ch_hrs_id.setText(cell.hour + " hr")
            } else {
                ch_hrs_id.setText(cell.hour + " hrs")
            }
            if (vehicle_type.equals("Personal")) {
                name.setText(cell.personal_price)

                ch_hrs_id.setOnClickListener {
                    priceslistArray.clear()
                    dialog_list_price.dismiss()
                    vehicle_price_id = cell.id.toString()
                    vehicle_price = cell.personal_price.toString()
                    if (cell.hour.equals("1")) {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id.setText(cell.hour + " hr" /*+ " & $ " + cell.personal_price*/)
                    } else {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id.setText(cell.hour + " hrs" /*+ " & $ " + cell.personal_price*/)
                    }
                }
            } else {
                name.setText(cell.commercial_price)

                ch_hrs_id.setOnClickListener {
                    priceslistArray.clear()
                    dialog_list_price.dismiss()
                    vehicle_price_id = cell.id.toString()
                    vehicle_price = cell.commercial_price.toString()
                    if (cell.hour.equals("1")) {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id.setText(cell.hour + " hr" /*+ " & $ " + cell.commercial_price*/)
                    } else {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id.setText(cell.hour + " hrs" /*+ " & $ " + cell.commercial_price*/)
                    }
                }
            }



            return v
        }
    }

    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(vehicle_number))
            et_vehicle_no_id.setError("vehicle Number Required")
        else if (TextUtils.isEmpty(vehicle_state))
            et_vehicle_state_id.setError("State Required")
        else if (TextUtils.isEmpty(vehicle_state_id))
            et_vehicle_stateID_id.setError("vehicle State Id Required")
        else if (TextUtils.isEmpty(vehicle_zone) && TextUtils.isEmpty(vehicle_address))
            Toast.makeText(this@PaytoParkActivty, "Select Parking Zone Number or Parking Lot Address ", Toast.LENGTH_SHORT).show()
        else if (vehicle_time.equals("Select Time & Rate"))
            tv_time_and_price_id.setError("Time and rate Required")
        else
            return true
        return false
    }


    private fun setProductAdapter(parkingTimesData: ArrayList<getAllParkingDataResponse>) {
        mParkingTimeListAdapter = ParkingTimeListAdapter(parkingTimesData, this)
        lv_currentparking_listview!!.adapter = mParkingTimeListAdapter
        mParkingTimeListAdapter!!.notifyDataSetChanged()
    }


    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    inner class ParkingTimeListAdapter(val mServiceList: ArrayList<getAllParkingDataResponse>, val activity: Activity) : BaseAdapter() {


        internal var parkig_start_date: String? = null
        internal var receipt_id: String? = null
        internal var no_plate: String? = null
        internal var vehicle_type: String? = null
        internal var zone_rate_id: String? = null
        internal var user_id: String? = null
        internal var lot_rate_id: String? = null
        internal var state: String? = null
        internal var parkig_end_date: String? = null
        internal var zone_id: String? = null
        internal var lot_id: String? = null
        internal var final_Time: String? = null
        internal var tv_currentparking_time: TextView? = null
        internal var ll_current_parking_timing: LinearLayout? = null


        override fun getItem(position: Int): Any {
            return mServiceList.get(position)
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return mServiceList.size
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view: View = View.inflate(activity, R.layout.list_item_paytopark_timers, null)
            tv_currentparking_time = view.findViewById(R.id.tv_currentparking_time) as TextView
            ll_current_parking_timing = view.findViewById(R.id.ll_current_parking_timing) as LinearLayout

            myloading()

            parkig_start_date = mServiceList.get(position).parkig_start_date
            parkig_end_date = mServiceList.get(position).parkig_end_date


            receipt_id = mServiceList.get(position).receipt_id
            no_plate = mServiceList.get(position).no_plate
            vehicle_type = mServiceList.get(position).vehicle_type
            zone_rate_id = mServiceList.get(position).zone_rate_id
            lot_rate_id = mServiceList.get(position).lot_rate_id
            user_id = mServiceList.get(position).user_id
            state = mServiceList.get(position).state
            zone_id = mServiceList.get(position).zone_id!!
            lot_id = mServiceList.get(position).lot_id!!


            printDifference(parkig_start_date, parkig_end_date)


            return view
        }

        internal lateinit var my_loader: Dialog
        private fun myloading() {
            my_loader = Dialog(activity)
            my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
            my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            my_loader.setCancelable(false);
            my_loader.setContentView(R.layout.mkloader_dialog)
        }


        fun printDifference(startDate: String?, endDate: String?) {


            val dateStart = startDate
            val dateStop = endDate


            /* val sdf = SimpleDateFormat("hh:mm aa")
             val currentDateandTime = sdf.format(Date())

             val date = sdf.parse(currentDateandTime)
             val calendar = Calendar.getInstance()

             calendar.setTime(date)
             calendar.add(Calendar.HOUR,Calendar.MINUTE)

             Log.e("CURRENTTIME", "" +Calendar.HOUR+""+Calendar.MINUTE);*/

            Log.e("Parking_START", "" + dateStart + "----" + dateStop + "PARKING" + startDate + "------" + endDate);


            //HH converts hour in 24 hours format (0-23), day calculation
            val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

            var d1: Date? = null
            var d2: Date? = null

            try {
                d1 = format.parse(dateStart)
                d2 = format.parse(dateStop)

                //in milliseconds
                val diff = d2!!.time - d1!!.time

                val diffSeconds = diff / 1000 % 60
                val diffMinutes = diff / (60 * 1000) % 60
                val diffHours = diff / (60 * 60 * 1000) % 24
                val diffDays = diff / (24 * 60 * 60 * 1000)

                Log.e("FINALTIME", "DAYS" + diffDays + "HOURS" + diffHours + "MIN" + diffMinutes + "SEC" + diffSeconds);

                print(diffDays.toString() + " days, ")
                print(diffHours.toString() + " hours, ")
                print(diffMinutes.toString() + " minutes, ")
                print(diffSeconds.toString() + " seconds.")


                tv_currentparking_time!!.setText(diffHours.toString())


                ll_current_parking_timing!!.setOnClickListener {

                }


            } catch (e: Exception) {
                e.printStackTrace()
            }

        }


    }

    override fun onBackPressed() {
        val intent = Intent(this@PaytoParkActivty, MainActivity::class.java)
        startActivity(intent)
    }

    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


}
