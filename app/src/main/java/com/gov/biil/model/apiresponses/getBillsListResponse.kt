package com.gov.biil.model.apiresponses

/**
 * Created by indo67 on 7/26/2018.
 */
class getBillsListResponse {
    var id:String?=null
    var service_id:String?=null
    var user_id:String?=null
    var service_type:String?=null
    var message:String?=null
    var transaction_id:String?=null
    var amount:String?=null
    var amount_type:String?=null
    var time:String?=null
    var date:String?=null
    var created_at:String?=null
}