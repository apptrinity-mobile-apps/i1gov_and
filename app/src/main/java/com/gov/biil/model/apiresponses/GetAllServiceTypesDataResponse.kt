package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class GetAllServiceTypesDataResponse {

     val id: String? = null
     val status: String? = null
     val create_date: String? = null
     val service_type: String? = null




}
