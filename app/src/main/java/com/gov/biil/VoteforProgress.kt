package com.gov.biil

import Helper.CustomTextView
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.VotingCountDataResponse
import com.gov.biil.model.apiresponses.VotingCountResponse
import com.gov.biil.model.apiresponses.VotingVoteCountDataResponse
import com.squareup.picasso.Picasso
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback
import java.util.*







class VoteforProgress : AppCompatActivity() {


    lateinit var rv_voted_list: RecyclerView
    lateinit var  tv_city_name: CustomTextView
    internal lateinit var sessionManager: SessionManager
    var sub_category_id = ""
    var category_id = ""
    var sub_category_name = ""
    var city_id = ""
    var state_id = ""
    var city_name = ""
    var state_name = ""
    public var votecountdataresponse: ArrayList<VotingVoteCountDataResponse> = ArrayList<VotingVoteCountDataResponse>()

    internal lateinit var my_loader: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_votefor_progress)


        val toolbar = findViewById(R.id.toolbar) as Toolbar
        tv_city_name = findViewById(R.id.tv_city_name) as CustomTextView
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        sessionManager = SessionManager(this)

        myloading()


        getAdminStatusInfo()
        rv_voted_list = findViewById(R.id.rv_voted_list)


        category_id = intent.getStringExtra("category_id")
        sub_category_id = intent.getStringExtra("sub_category_id")
        sub_category_name = intent.getStringExtra("sub_category_name")
        city_id = intent.getStringExtra("city_id")
        Log.d("PROGRESSCITY_ID_EMPTY", city_id+"-----"+state_id)
        try {
            /*if(intent.getStringExtra("city_id").equals("") || intent.getStringExtra("city_id").equals("null")){
                city_id =""
                Log.d("PROGRESSCITY_ID_EMPTY", city_id+"-----"+state_id)
            }else{
                city_id = intent.getStringExtra("city_id")
                Log.d("PROGRESSCITY_ID", city_id+"-----"+state_id)
            }*/
            if(intent.getStringExtra("state_id").equals("") || intent.getStringExtra("state_id").equals("null")){
                state_id =""
            }else{
                state_id = intent.getStringExtra("state_id")
            }
        } catch (e: NullPointerException) {
           // city_id =""
           // state_id =""
        }


       /* if(intent.getStringExtra("city_id") != "" || intent.getStringExtra("city_id") != "null" || intent.getStringExtra("city_id") != "0"){
            city_id = intent.getStringExtra("city_id")
        }else{
            city_id =""
        }
        if(intent.getStringExtra("state_id")!= "" || intent.getStringExtra("state_id")!= "null" || intent.getStringExtra("state_id")!= "0"){
            state_id = intent.getStringExtra("state_id")
        }else{
            state_id =""
        }*/

        Log.d("VoteCount", "" + category_id + "-----" + sub_category_id +"-----"+city_id+"-----"+state_id)
        callVoteCountAPI(category_id, sub_category_id,city_id,state_id)


    }


    private fun callVoteCountAPI(category_id: String, sub_category_id: String, city_id: String, state_id: String) {

        //my_loader.show()
        votecountdataresponse.clear()
        val apiService = ApiInterface.create()
        val call = apiService.voteCount(category_id, sub_category_id,city_id,state_id)
        Log.d("VoteForListingAPI", category_id + "------" + sub_category_id+"-----"+city_id+"-----"+state_id)
        call.enqueue(object : Callback<VotingCountResponse> {
            override fun onResponse(call: Call<VotingCountResponse>, response: retrofit2.Response<VotingCountResponse>?) {
                if (response != null) {
                    //my_loader.dismiss()
                    Log.w("Result_VoteForListing", response.body().toString())
                    if (response.body()!!.status.equals("1") && response.body()!!.data != null) {

                        val list: VotingCountDataResponse? = response.body()!!.data!!

                        val totalcount = list!!.total.toString()
                        val city_name = list!!.city.toString()
                        val state_name = list!!.state.toString()

                        if(!city_name.equals("")){
                            if(city_name.equals("null")){
                                tv_city_name.setText(sub_category_name)
                            }else{
                                tv_city_name.setText(city_name +" - "+ sub_category_name)
                            }


                        }else if(!state_name.equals("")){
                            if(state_name.equals("null")){
                                tv_city_name.setText(sub_category_name)
                            }else {
                                tv_city_name.setText(state_name + " - " + sub_category_name)
                            }
                        }

                        val voting_list_array: Array<VotingVoteCountDataResponse>? = list!!.votecount

                        for (item: VotingVoteCountDataResponse in voting_list_array!!.iterator()) {
                            votecountdataresponse.add(item)
                        }
                        if (voting_list_array!!.size > 0) {
                           /* for (j in 0 until voting_list_array.size) {

                                val vote_count = voting_list_array.get(j).count

                                Log.w("VOTECOUNT", vote_count)




                            }*/

                            rv_voted_list!!.setHasFixedSize(true)
                            val layoutManager = LinearLayoutManager(this@VoteforProgress);
                            rv_voted_list!!.setLayoutManager(layoutManager)
                            rv_voted_list!!.setItemAnimator(DefaultItemAnimator())
                            val details_adapter = VoteCountAdapter(votecountdataresponse,totalcount,this@VoteforProgress!!)
                            rv_voted_list!!.setAdapter(details_adapter)
                            details_adapter.notifyDataSetChanged()

                        }

                        if (voting_list_array.size == 0) {
                            // no_service!!.text = "No Data Found"
                            // no_service!!.visibility = View.VISIBLE
                        }

                    } else if (response.body()!!.status!!.equals("2")) {
                        // no_service!!.text = "No Data Found"
                        // no_service!!.visibility = View.VISIBLE
                    }

                }
            }

            override fun onFailure(call: Call<VotingCountResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
      //  val tv_title_id = view.findViewById<TextView>(R.id.tv_title_id)
        val prog_main_id = view.findViewById<ProgressBar>(R.id.prog_main_id)
        val tv_votecount = view.findViewById<TextView>(R.id.tv_votecount)
        val tv_name_votes = view.findViewById<CustomTextView>(R.id.tv_name_votes)
        val imglist_icon = view.findViewById<ImageView>(R.id.imglist_icon)
        val tv_partyname = view.findViewById<CustomTextView>(R.id.tv_partyname)


    }

    class VoteCountAdapter(val title: ArrayList<VotingVoteCountDataResponse>,val totalcount: String,val context: Context) : RecyclerView.Adapter<ViewHolder>() {

        lateinit var tv_title_id: TextView
        lateinit var tv_partyname: CustomTextView
        private lateinit var prog_main_id: ProgressBar
        lateinit var tv_votecount: TextView
        lateinit var tv_name_votes: CustomTextView
        private val coloredItems = ArrayList<Int>()
       // val colors = intArrayOf(0x700170c1, 0x707367f0,0x70d939cd,0x70f6416c,0x70e96d71)
        private val colors = intArrayOf(Color.parseColor("#0170c1"), Color.parseColor("#7367f0"), Color.parseColor("#d939cd"), Color.parseColor("#f6416c"), Color.parseColor("#e96d71"), Color.parseColor("#28c76f"), Color.parseColor("#e63939"))
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.progress_list_item, parent, false))
        }

        @SuppressLint("NewApi", "ResourceAsColor")
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {


            val test = title.get(position).party_name
            val first = test!![0]


            val value = Integer.parseInt(title.get(position).count)
            val total_count = totalcount.toInt()
            val final = value * 100 / total_count

            Log.w("COUNTVALUE", final.toString())
            holder?.prog_main_id.progress = final

            //holder?.prog_main_id.setProgressTintList(ColorStateList.valueOf(Color.RED));



           // holder?.tv_title_id.text = first.toString()
            holder?.tv_votecount.text = title.get(position).count
            holder?.tv_name_votes.text = title.get(position).candidate_name
            holder?.tv_partyname.setText(title.get(position).party_name)

            val party_name = title.get(position).party_name
            val ind_party_name = title.get(position).ind_party_name
            Log.d("PARTYNAME", party_name+"-----"+ind_party_name)

            Picasso.with(context)
                    .load("https://www.i1gov.com/uploads/party/"+title.get(position).party_logo)
                    .into(holder?.imglist_icon)

           /* if(party_name!!.equals("Republican Party")){

                holder?.imglist_icon.setImageDrawable(context.resources.getDrawable(R.drawable.trump))

            }else if(party_name!!.equals("Democratic Party")){
                holder?.imglist_icon.setImageDrawable(context.resources.getDrawable(R.drawable.democratic))
            }*/




       /*     if(position %2 == 1)
            {
                holder?.tv_title_id.setBackgroundColor(Color.parseColor("#7367f0"));
                holder?.tv_name_votes.setTextColor(Color.parseColor("#7367f0"));
                holder?.tv_votecount.setTextColor(Color.parseColor("#7367f0"));
                //  holder.imageView.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
            else
            {
                  holder.tv_title_id.setBackgroundColor(Color.parseColor("#d939cd"));
                  holder.tv_name_votes.setTextColor(Color.parseColor("#d939cd"));
                  holder.tv_votecount.setTextColor(Color.parseColor("#d939cd"));
            }*/

           /* if (coloredItems.contains(position)) {
                holder?.tv_title_id.setBackgroundColor(Color.CYAN);
            } else {
                holder?.tv_title_id.setBackgroundColor(Color.BLACK); //or whatever was original
            }*/

            val colorPos = position % colors.size
           // holder?.tv_title_id.setBackgroundColor(colors[colorPos])
            holder?.prog_main_id.setIndeterminate(false);
            holder?.prog_main_id.getIndeterminateDrawable().setColorFilter(colors[colorPos], android.graphics.PorterDuff.Mode.MULTIPLY);
           // holder?.prog_main_id.getProgressDrawable().setColorFilter(colors[colorPos], PorterDuff.Mode.SRC_IN)
            holder?.prog_main_id.progress = final






        }

        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return title.size
        }
    }


    override fun onBackPressed() {
        val intent = Intent(this@VoteforProgress, IVoteForActivity::class.java)
        startActivity(intent)

    }


    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


}




