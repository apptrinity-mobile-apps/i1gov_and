package com.gov.biil

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.widget.ImageView
import com.lespinside.simplepanorama.view.SphericalView
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL


class PanoramaActivity : AppCompatActivity() {

    private var sphericalView: SphericalView? = null
    private var imageview_test: ImageView? = null
    lateinit var  bitmaps:Bitmap
    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actuvity_panorama)

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        val city_image = intent.getStringExtra("city_image")
        Log.d("IMAGE_PANORAMA", city_image)

        sphericalView = findViewById<View>(R.id.spherical_view) as SphericalView
        imageview_test = findViewById<View>(R.id.imageview_test) as ImageView

        getBitmapFromURL(city_image)

       /* bitmaps = PLUtils.getBitmap(this,city_image)
        sphericalView!!.setPanorama(bitmaps, true)*/



       // changePanorama(0)

    }

    override fun onResume() {
        super.onResume()
        sphericalView!!.onResume()
    }

    override fun onPause() {
        super.onPause()
        sphericalView!!.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        sphericalView!!.onDestroy()
    }

    private fun changePanorama(index: Int) {
        sphericalView!!.setPanorama(bitmaps, true)
    }


    fun getBitmapFromURL(src: String): Bitmap? {
        try {
            val url = URL(src)
            val connection = url.openConnection() as HttpURLConnection
            connection.setDoInput(true)
            connection.connect()
            val input = connection.getInputStream()

         //  imageview_test!!.setImageResource(input)
           // bitmaps = PLUtils.getBitmap(this,BitmapFactory.decodeStream(input).toString())
            sphericalView!!.setPanorama(BitmapFactory.decodeStream(input), true)

            Log.e("BITMAAP",""+BitmapFactory.decodeStream(input))

            return BitmapFactory.decodeStream(input)
        } catch (e: IOException) {
            // Log exception
            return null
        }

    }

}