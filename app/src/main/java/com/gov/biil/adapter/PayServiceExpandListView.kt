package com.gov.biil.adapter

import Helper.CustomTextView
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.biil.Helper.Helper
import com.gov.biil.R
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.*
import retrofit2.Call
import retrofit2.Callback
import java.util.*

/**
 * Created by indo67 on 7/28/2018.
 */


/**
 * A simple [Fragment] subclass.
 */
class PayServiceExpandListView : Fragment() {
    lateinit var fragmentName: Fragment
    var uri: Uri? = null
    var city_id: String=""
    lateinit var search_autocomplete: EditText


    lateinit var listview_searchcity: ListView
    lateinit var listview_getallservice: RecyclerView
    internal lateinit var my_loader: Dialog
    internal var paylink: String? = ""
    internal var selected_service_type: String? = ""
    internal var selected_service_id: String? = ""
    internal var selected_service_id1: String? = ""
    internal lateinit var sessionManager: SessionManager
    private var listview_citypayforservice: ExpandableListView? = null
    private var no_service: TextView? = null
    lateinit var listAdapter: ExpListViewAdapterWithCheckbox
    lateinit var listDataHeader: ArrayList<String>
    lateinit var listDataHeader1: ArrayList<String>
    lateinit var listDataChild: HashMap<String, ArrayList<GetAllServiceDataResponse>>
    lateinit var list: List<String>
    private var lastExpandedPosition = -1

    lateinit var radioButton: RadioButton
    lateinit var radiobtn_allservices: RadioGroup

    var timer: Timer? = null

    internal lateinit var search_string: String
    internal lateinit var name_string: String
    internal lateinit var account_string: String
    private var mSearchListAdapter: SearchCityListAdapter? = null
    private var mSeviceListByIdListAdapter: ServiceListByIdListAdapter? = null
    private var mGetAllServicetypesListAdapter: GetAllServiceListAdapter? = null
    public var searchlistdata: ArrayList<SearchCityListDataResponse> = ArrayList<SearchCityListDataResponse>()
    public var serviceListdata: ArrayList<getServiceListByIdDataResponse> = ArrayList<getServiceListByIdDataResponse>()
    public var getservicelisttdata: ArrayList<GetAllServiceTypesDataResponse> = ArrayList<GetAllServiceTypesDataResponse>()
    var search_temp = false

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = LayoutInflater.from(activity!!).inflate(R.layout.city_payforservice_fragment, container, false)
        search_autocomplete = rootView.findViewById<EditText>(R.id.search_autocomplete)
        listview_searchcity = rootView.findViewById<ListView>(R.id.listview_searchcity)
        listview_getallservice = rootView.findViewById<RecyclerView>(R.id.listview_getallservice)
        listview_citypayforservice = rootView!!.findViewById(R.id.listview_citypayforservice) as ExpandableListView
        radiobtn_allservices = rootView!!.findViewById(R.id.radiobtn_allservices) as RadioGroup
        myloading()
        sessionManager = SessionManager(this.activity!!)


        my_loader.dismiss()

        callGetAllServiceTypesList()


        search_autocomplete.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                // TODO Auto-generated method stub

                search_string = search_autocomplete.text.toString().trim()

                if (search_string.equals("")) {
                    search_temp = false
                    searchlistdata.clear()
//                    mSearchListAdapter!!.notifyDataSetChanged()
                    listview_searchcity.visibility = View.INVISIBLE
                } else {

                    if (search_string.length > 2) { //just checks that there is something. You may want to check that length is greater than or equal to 3

                        if(search_temp == false){
                            callCitySearchList()
                            listview_searchcity.visibility = View.VISIBLE
                        }

                        //do what you need with it
                    }
                }


            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })

        // Listview Group click listener
        listview_citypayforservice!!.setOnGroupClickListener { parent, v, groupPosition, id ->

            false
        }

        // Listview Group expanded listener
        listview_citypayforservice!!.setOnGroupExpandListener { groupPosition ->

            if (lastExpandedPosition != -1
                    && groupPosition != lastExpandedPosition) {
                listview_citypayforservice!!.collapseGroup(lastExpandedPosition);
            }
            lastExpandedPosition = groupPosition;

        }

        // Listview Group collasped listener
        listview_citypayforservice!!.setOnGroupCollapseListener { groupPosition ->

          callgetServiceListById(listDataHeader.get(groupPosition))

        }

        // Listview on child click listener
        listview_citypayforservice!!.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->


            false
        }







        return rootView
    }


    private fun myloading() {
        my_loader = Dialog(activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private var childViewHolder: ExpListViewAdapterWithCheckbox.ChildViewHolder? = null
    inner class ExpListViewAdapterWithCheckbox
    (
            private val mContext: Context,

            private val mListDataGroup: ArrayList<String>,
            private val mListDataChild: HashMap<String, ArrayList<GetAllServiceDataResponse>>) : BaseExpandableListAdapter() {

        private val mChildCheckStates: HashMap<Int, BooleanArray>
        private val mGroupCheckStates: HashMap<Int, BooleanArray>

        //private var childViewHolder: ChildViewHolder? = null
        private var groupViewHolder: GroupViewHolder? = null
        private var groupText: String? = null
        private var childText: String? = null

        init {

            mChildCheckStates = HashMap()
            mGroupCheckStates = HashMap()
        }

        fun getNumberOfCheckedItemsInGroup(mGroupPosition: Int): Int {
            val getChecked = mChildCheckStates[mGroupPosition]
            var count = 0
            if (getChecked != null) {

                for (j in getChecked.indices) {
                    if (getChecked[j] == true) count++
                }
            }
            return count
        }

        fun getNumberOfGroupCheckedItems(mGroupPosition: Int): Int {
            val getChecked = mGroupCheckStates[mGroupPosition]
            var count = 0
            if (getChecked != null) {
                for (j in getChecked.indices) {
                    if (getChecked[j] == true) count++
                }
            }
            return count
        }

        override fun getGroupCount(): Int {
            return mListDataGroup.size
        }

        override fun getGroup(groupPosition: Int): String {
            return mListDataGroup[groupPosition]
        }

        override fun getGroupId(groupPosition: Int): Long {
            return groupPosition.toLong()
        }


        override fun getGroupView(groupPosition: Int, isExpanded: Boolean,
                                  convertView: View?, parent: ViewGroup): View {
            var convertView = convertView

            groupText = getGroup(groupPosition)

            if (convertView == null) {

                val inflater = mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.list_item_payforservicecityheader, null)

                // Initialize the GroupViewHolder defined at the bottom of this document
                groupViewHolder = GroupViewHolder()

                groupViewHolder!!.tv_headername_city_payforser = convertView!!.findViewById(R.id.tv_headername_city_payforser) as CustomTextView
                groupViewHolder!!.ll_headername = convertView!!.findViewById(R.id.ll_headername) as LinearLayout
                // groupViewHolder!!.mbg_layout!!.setBackgroundColor(resources.getColor(R.color.tab2_bg))

                groupViewHolder!!.ll_headername!!.setSelected(isExpanded);

                /*if (isExpanded) {
                    groupViewHolder!!.img_layout!!.setImageResource(R.drawable.down_arrow)
                } else {
                    groupViewHolder!!.img_layout!!.setImageResource(R.drawable.right_arrow)
                }*/



                convertView.tag = groupViewHolder
            } else {

                groupViewHolder = convertView.tag as GroupViewHolder
            }

            //groupViewHolder!!.mGroupText!!.text = groupText
            groupViewHolder!!.tv_headername_city_payforser!!.text = listDataHeader1.get(groupPosition)

            groupViewHolder!!.tv_headername_city_payforser!!.setOnClickListener {

            }



            return convertView
        }

        override fun getChildrenCount(groupPosition: Int): Int {
            return mListDataChild[mListDataGroup[groupPosition]]!!.size
        }

        override fun getChild(groupPosition: Int, childPosition: Int): GetAllServiceDataResponse {
            return mListDataChild[mListDataGroup[groupPosition]]!!.get(childPosition)
        }

        override fun getChildId(groupPosition: Int, childPosition: Int): Long {
            return childPosition.toLong()
        }

        override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView


            val mGroupPosition = groupPosition

            //childText = getChild(groupPosition, childPosition)
            Log.e("mListDataChild", "" + mListDataChild.size)

            // val list: Array<JobsListDataResponse>? = mListDataChild[mListDataGroup[groupPosition]]
            // list =  mListDataChild[mListDataGroup[groupPosition]] as List<*> as List<JobsListDataResponse>
            // Log.e("asasas",""+ mListDataChild[mListDataGroup[groupPosition]]!!.get(childPosition).job_id);
            serviceListdata.clear()


            if (convertView == null) {

                val inflater = this.mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.list_item_citypayforservchilditem, null)

                childViewHolder = ChildViewHolder()
                childViewHolder!!.et_bill_name = convertView!!.findViewById(R.id.et_bill_name)
                childViewHolder!!.et_bill_account = convertView!!.findViewById(R.id.et_bill_account)
                childViewHolder!!.tv_submit = convertView!!.findViewById(R.id.tv_submit)
                childViewHolder!!.listview_billspayforserv = convertView!!.findViewById(R.id.listview_billspayforserv)



                childViewHolder!!.tv_submit!!.setOnClickListener(View.OnClickListener {

                    name_string = childViewHolder!!.et_bill_name!!.text.toString()
                    account_string = childViewHolder!!.et_bill_account!!.text.toString()

                    Log.e("CHILD_ITEM_NAMES", "" + name_string+"------"+account_string)

                    if(hasValidCredentials()){
                        callInsertData(name_string,account_string)
                    }

                })



                convertView.setTag(R.layout.list_item_citypayforservchilditem, childViewHolder)



            } else {

                childViewHolder = convertView
                        .getTag(R.layout.list_item_citypayforservchilditem) as ChildViewHolder

            }

            //  childViewHolder!!.tv_customer!!.text =  listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).custmer

            // childViewHolder!!.mCheckBox!!.setOnCheckedChangeListener(null)

            return convertView
        }

        override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
            return false
        }

        override fun hasStableIds(): Boolean {
            return false
        }

        inner class GroupViewHolder {

            internal var tv_headername_city_payforser: TextView? = null
            internal var ll_headername: LinearLayout? = null
            internal var img_layout: ImageView? = null
            //internal var mGroupCheckbox: CheckBox? = null
        }

        inner class ChildViewHolder {

            internal var et_bill_name: EditText? = null
            internal var et_bill_account: EditText? = null
            internal var tv_submit: TextView? = null
            internal var listview_billspayforserv: ListView? = null


        }


    }


    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(name_string))
            childViewHolder!!.et_bill_name!!.setError("Name Required")
        else if (TextUtils.isEmpty(account_string))
            childViewHolder!!.et_bill_account!!.setError("Account Required")

        else
            return true
        return false
    }

    private fun callGetAllServiceTypesList() {
        my_loader.show()
        getservicelisttdata.clear()
        val apiService = ApiInterface.create()
        val call = apiService.getAllServicesTypes("")
        call.enqueue(object : Callback<GetAllServiceTypesResponse> {
            override fun onResponse(call: Call<GetAllServiceTypesResponse>, response: retrofit2.Response<GetAllServiceTypesResponse>?) {
                my_loader.dismiss()
                Log.w("GETALLSERVICETYPES", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        my_loader.dismiss()
                        val list: Array<GetAllServiceTypesDataResponse> = response.body()!!.data!!
                        for (item: GetAllServiceTypesDataResponse in list.iterator()) {
                            getservicelisttdata.add(item)
                        }

                        var rprms: RadioGroup.LayoutParams
                        for (i in 0 until getservicelisttdata.size) {
                            radioButton = RadioButton(context)
                            radioButton.text = getservicelisttdata.get(i).service_type
                            selected_service_id = getservicelisttdata.get(i).id
                            rprms = RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT)
                            radioButton.setPadding(10, 5, 25, 0)




                            if (Build.VERSION.SDK_INT >= 21) {
                                val colorStateList = ColorStateList(
                                        arrayOf(
                                                intArrayOf(-android.R.attr.state_enabled), //disabled
                                                intArrayOf(android.R.attr.state_enabled) //enabled
                                        ),
                                        intArrayOf(
                                                Color.BLUE //disabled
                                                , Color.BLUE //enabled
                                        )
                                )
                                radioButton.setButtonTintList(colorStateList)//set the color tint list
                                radioButton.setOnClickListener(View.OnClickListener {

                                    Toast.makeText(context, getservicelisttdata.get(i).service_type.toString(), Toast.LENGTH_SHORT).show()
                                })
                            }

                            radiobtn_allservices.addView(radioButton, rprms)
                            radiobtn_allservices.setPadding(10, 5, 30, 0)


                        }

                        radiobtn_allservices.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { radioGroup, i ->


                            /*Toast.makeText(context," On checked change : "+getservicelisttdata.get(i-1).service_type.toString()+"with minus"+getservicelisttdata.get(i-1).id.toString(),
                                    Toast.LENGTH_SHORT).show()*/

                            //  selected_service_id = getservicelisttdata.get(i-1).id.toString()

                            callgetAllService(getservicelisttdata.get(i-1).id.toString())

                        })

                        //  setgetAllServiceListAdapter(getservicelisttdata)

                        /*listview_getallservice.setHasFixedSize(true)
                        listview_getallservice.setLayoutManager(LinearLayoutManager(activity))
                        listview_getallservice.setItemAnimator(DefaultItemAnimator())

                        val details_adapter = MapDetailsAdapter(getservicelisttdata, activity)
                        listview_getallservice.setAdapter(details_adapter)
                        details_adapter.notifyDataSetChanged()*/

                        if (getservicelisttdata.size == 0) {

                        }
                    }

                } else {
                    Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<GetAllServiceTypesResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }



    private fun callgetAllService(service_id: String) {
        my_loader.show()
       // listDataHeader.clear()
        val apiService = ApiInterface.create()
        val call = apiService.getAllService(service_id)
        call.enqueue(object : Callback<GetAllServiceResponse> {
            override fun onResponse(call: Call<GetAllServiceResponse>, response: retrofit2.Response<GetAllServiceResponse>?) {
                my_loader.dismiss()
                Log.w("GETALLSERVICETYPES", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        listview_citypayforservice!!.visibility = View.VISIBLE
                        my_loader.dismiss()
                        //val list: Array<GetAllServiceDataResponse> = response.body()!!.data!!
                        listDataHeader = ArrayList()
                        listDataHeader1 = ArrayList()
                        listDataChild = HashMap()
                        val list: Array<GetAllServiceDataResponse>? = response.body()!!.data!!


                        if(list!!.size>0){
                            for (j in 0 until list.size) {

                                listDataHeader1.add(list.get(j).service_name.toString())
                                listDataHeader.add(list.get(j).id.toString())

                                val top = ArrayList<GetAllServiceDataResponse>()
                                top.add(list.get(j))
                                Log.w("Result_Order_details", list.get(j).id.toString())

                                listDataChild.put(listDataHeader[j],top)

                                selected_service_type = list.get(j).service_type.toString()
                                // selected_service_id = list.get(j).id.toString()


                            }
                            Log.e("stdferer",""+listDataChild.size)
                            listAdapter = ExpListViewAdapterWithCheckbox(activity!!, listDataHeader, listDataChild)

                            // setting list adapter
                            listview_citypayforservice!!.setAdapter(listAdapter)
                            listAdapter.notifyDataSetChanged()



                            val param: ViewGroup.LayoutParams = listview_citypayforservice!!.getLayoutParams()

                            param.height = 800*list.size

                            listview_citypayforservice!!.setLayoutParams(param)

                            listview_citypayforservice!!.refreshDrawableState()

                        }

                        //  setgetAllServiceListAdapter(getservicelisttdata)

                        /*listview_getallservice.setHasFixedSize(true)
                        listview_getallservice.setLayoutManager(LinearLayoutManager(activity))
                        listview_getallservice.setItemAnimator(DefaultItemAnimator())

                        val details_adapter = MapDetailsAdapter(getservicelisttdata, activity)
                        listview_getallservice.setAdapter(details_adapter)
                        details_adapter.notifyDataSetChanged()*/

                        /*if (getservicelisttdata.size == 0) {

                        }*/
                    }else{
                        listDataHeader1.clear()
                    }

                } else {
                    //listDataHeader1.clear()
                    listview_citypayforservice!!.visibility = View.GONE
                    // listAdapter.notifyDataSetChanged()
                    Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<GetAllServiceResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    /*  private fun setgetAllServiceListAdapter(getservicelisttdata: ArrayList<GetAllServiceTypesDataResponse>) {
          mGetAllServicetypesListAdapter = GetAllServiceListAdapter(getservicelisttdata, activity)
          listview_getallservice!!.adapter = mGetAllServicetypesListAdapter
          mGetAllServicetypesListAdapter!!.notifyDataSetChanged()
      }*/


    private fun callInsertData(name: String, account: String) {
        val apiService = ApiInterface.create()
        my_loader.show()
        Log.w("Result_INSERTDATA","Result:"+city_id+"---"+selected_service_type.toString()+"---"+selected_service_id.toString()+"----"+name+"---"+account+"---"+sessionManager.isId)
        val call = apiService.insertData(city_id,selected_service_type.toString(),selected_service_id.toString(),name,account,sessionManager.isId)

        call.enqueue(object : Callback<InsertServiceResponse> {
            override fun onResponse(call: Call<InsertServiceResponse>, response: retrofit2.Response<InsertServiceResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_INSERT_RESPONSE","Result : "+response.body()!!.status)
                    if(response.body()!!.status.equals("1")) {
                        Log.w("Result_INSERT_RESPONSE","Result : "+response.body()!!.status)
                        Toast.makeText(activity, ""+response.body()!!.result, Toast.LENGTH_SHORT).show()

                        my_loader.dismiss()
                        val list: InsertServiceDataResponse = response.body()!!.data!!
                        val serviceid = list.id

                        Log.d("GETSERVICEBYID_BELOw", ""+serviceid.toString())
                        // serviceListdata.clear()
                        //callgetServiceListById(selected_service_type)
                        // storeLoginData(response.body()!!.user_info)
                        /*val intent = Intent(this@ProfileInfoEditActivity, ProfileInfoActivity::class.java)
                        finish()
                        startActivity(intent)*/
                    }

                }
            }

            override fun onFailure(call: Call<InsertServiceResponse>, t: Throwable) {
                Log.w("Result_Address_Profile",t.toString())
            }
        })
    }




    private fun callCitySearchList() {
        my_loader.show()
        searchlistdata.clear()
        val apiService = ApiInterface.create()
        val call = apiService.getAllCities(search_string)
        Log.d("REQUEST", call.toString() + "------" + search_string)
        call.enqueue(object : Callback<SearchCityListResponse> {
            override fun onResponse(call: Call<SearchCityListResponse>, response: retrofit2.Response<SearchCityListResponse>?) {
                my_loader.dismiss()
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        my_loader.dismiss()
                        val list: Array<SearchCityListDataResponse> = response.body()!!.data!!

                        for (item: SearchCityListDataResponse in list.iterator()) {
                            searchlistdata.add(item)

                        }
                        setProductAdapter(searchlistdata)
                        if (searchlistdata.size == 0) {
                            my_loader.dismiss()

                        }
                    }

                } else {
                    my_loader.dismiss()
                    Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<SearchCityListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    private fun setProductAdapter(searchlistdata: ArrayList<SearchCityListDataResponse>) {
        mSearchListAdapter = SearchCityListAdapter(searchlistdata, activity!!)
        listview_searchcity!!.adapter = mSearchListAdapter
        Helper.getListViewSize(listview_searchcity)
        mSearchListAdapter!!.notifyDataSetChanged()
    }






    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val radiobtn_allservices = view.findViewById<RadioGroup>(R.id.radiobtn_allservices)

        var userSelected = true;

    }

    class MapDetailsAdapter(val title: ArrayList<GetAllServiceTypesDataResponse>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

        lateinit var radioButton: RadioButton
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.listitem_getallservicetypes, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            var rprms: RadioGroup.LayoutParams
            for (i in 0 until title.size) {
                radioButton = RadioButton(context)
                radioButton.text = title.get(i).service_type
                rprms = RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT)
                radioButton.setPadding(10, 5, 25, 0)

                if (Build.VERSION.SDK_INT >= 21) {
                    val colorStateList = ColorStateList(
                            arrayOf(
                                    intArrayOf(-android.R.attr.state_enabled), //disabled
                                    intArrayOf(android.R.attr.state_enabled) //enabled
                            ),
                            intArrayOf(
                                    Color.BLUE //disabled
                                    , Color.BLUE //enabled
                            )
                    )
                    radioButton.setButtonTintList(colorStateList)//set the color tint list


                    radioButton.setOnClickListener(View.OnClickListener {

                        Toast.makeText(context, title.get(i).service_type.toString(), Toast.LENGTH_SHORT).show()

                    })


                }

                holder?.radiobtn_allservices.addView(radioButton, rprms)
                holder?.radiobtn_allservices.setPadding(10, 5, 30, 0)

            }


            holder?.radiobtn_allservices!!.setOnCheckedChangeListener(
                    RadioGroup.OnCheckedChangeListener { group, checkedId ->
                        //radioButton =  (checkedId) as RadioButton
                        /* val selectedRadioButtonID = holder?.radiobtn_allservices.getCheckedRadioButtonId()

                         val radio: RadioButton = findViewById(radio_group.checkedRadioButtonId)

                          if (selectedRadioButtonID != -1) {


                          }*/

                        Toast.makeText(context,title.get(position).service_type,
                                Toast.LENGTH_SHORT).show()
                    })


        }

        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return title.size
        }
    }


    inner class SearchCityListAdapter(val mServiceList: ArrayList<SearchCityListDataResponse>, val activity: Activity) : BaseAdapter() {


        override fun getItem(position: Int): Any {

            return mServiceList.get(position)
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return mServiceList.size
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view: View = View.inflate(activity, R.layout.city_search_list, null)
            val tv_city_name = view.findViewById(R.id.tv_city_name) as CustomTextView

            myloading()
            tv_city_name!!.text = mServiceList.get(position).city

            tv_city_name.setOnClickListener {
                search_temp = true
                search_autocomplete!!.setText(mServiceList.get(position).city.toString())
                city_id = mServiceList.get(position).id.toString()
                Log.w("Result_INSERT_RESPONSE","Result : "+city_id)
                /*searchlistdata.clear()*/
                listview_searchcity.visibility = View.INVISIBLE
            }

            return view
        }

        internal lateinit var my_loader: Dialog
        private fun myloading() {
            my_loader = Dialog(activity)
            my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
            my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            my_loader.setCancelable(false);
            my_loader.setContentView(R.layout.mkloader_dialog)
        }


    }



    private fun callgetServiceListById(serviceid: String?) {
        my_loader.show()
        serviceListdata.clear()
        val apiService = ApiInterface.create()
        val call = apiService.getServiceListById(sessionManager.isId,serviceid.toString())
        Log.d("CALLGETSERVICEBYID",  sessionManager.isId+"------------"+serviceid)
        call.enqueue(object : Callback<getServiceListByIdResponse> {
            override fun onResponse(call: Call<getServiceListByIdResponse>, response: retrofit2.Response<getServiceListByIdResponse>?) {
                my_loader.dismiss()
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        my_loader.dismiss()
                        val list: Array<getServiceListByIdDataResponse> = response.body()!!.data!!
                        for (item: getServiceListByIdDataResponse in list.iterator()) {
                            serviceListdata.add(item)

                        }
                        setAdapterServiceListById(serviceListdata)
                        if (serviceListdata.size == 0) {

                        }
                    }

                } else {
                    Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<getServiceListByIdResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    private fun setAdapterServiceListById(serviceListdata: ArrayList<getServiceListByIdDataResponse>) {
        mSeviceListByIdListAdapter = ServiceListByIdListAdapter(serviceListdata, activity!!)
        childViewHolder!!.listview_billspayforserv!!.adapter = mSeviceListByIdListAdapter
        Helper.getListViewSize(childViewHolder!!.listview_billspayforserv)
        mSeviceListByIdListAdapter!!.notifyDataSetChanged()
    }





    inner class ServiceListByIdListAdapter(val mServiceList: ArrayList<getServiceListByIdDataResponse>, val activity: Activity) : BaseAdapter() {


        override fun getItem(position: Int): Any {

            return mServiceList.get(position)
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return mServiceList.size
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view: View = View.inflate(activity, R.layout.list_item_billpayforservice, null)
            val tv_name_list = view.findViewById(R.id.tv_name_list) as CustomTextView
            val tv_account_list = view.findViewById(R.id.tv_account_list) as CustomTextView

            myloading()
            tv_name_list!!.text = mServiceList.get(position).name
            tv_account_list!!.text = mServiceList.get(position).account

            /*tv_city_name.setOnClickListener {
                search_temp = true
                search_autocomplete!!.setText(mServiceList.get(position).city.toString())
                city_id = mServiceList.get(position).id.toString()
                Log.w("Result_INSERT_RESPONSE","Result : "+city_id)
                *//*searchlistdata.clear()*//*
                listview_searchcity.visibility = View.INVISIBLE
            }*/

            return view
        }

        internal lateinit var my_loader: Dialog
        private fun myloading() {
            my_loader = Dialog(activity)
            my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
            my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            my_loader.setCancelable(false);
            my_loader.setContentView(R.layout.mkloader_dialog)
        }


    }










}