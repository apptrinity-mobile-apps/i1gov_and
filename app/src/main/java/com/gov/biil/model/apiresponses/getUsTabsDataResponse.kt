package com.gov.biil.model.apiresponses

class getUsTabsDataResponse {

    val tab_id: String? = null
    val title: String? = null
    val status: String? = null
    val url: String? = null
    val created_date: String? = null

}
