package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.CustomTextViewBold
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.getBillDetailsResponse
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat


class PaymetDetails : AppCompatActivity() {
    var bill_id = ""
    var bill_type = ""
    lateinit var tv_pay_for_type_id: CustomTextViewBold
    lateinit var img_pay_type_id: ImageView
    lateinit var dashboard_home: ImageView
    lateinit var tv_reciept_id: CustomTextView
    lateinit var tv_no_plate_id: CustomTextView
    lateinit var tv_vehicle_type_id: CustomTextView
    lateinit var tv_start_date_id: CustomTextView
    lateinit var tv_end_date_id: CustomTextView
    lateinit var tv_zone_id: CustomTextView
    lateinit var tv_amount_id: CustomTextView
    lateinit var tv_hours_id: CustomTextView
    lateinit var tv_send_mail_id: CustomTextView
    lateinit var tv_sripe_amount_id: CustomTextView
    lateinit var tv_wallet_amount_id: CustomTextView

    lateinit var tv_reciept_title_id: CustomTextView
    lateinit var tv_no_plate_title_id: CustomTextView
    lateinit var tv_vehicle_type_title_id: CustomTextView
    lateinit var tv_start_date_title_id: CustomTextView
    lateinit var tv_end_date_title_id: CustomTextView
    lateinit var tv_zone_title_id: CustomTextView
    lateinit var tv_amount_title_id: CustomTextView
    lateinit var tv_hours_title_id: CustomTextView


    lateinit var ll_start_date_id: LinearLayout
    lateinit var ll_end_date_id: LinearLayout
    lateinit var ll_zone_no_id: LinearLayout
    lateinit var ll_hours_id: LinearLayout
    lateinit var ll_stripe_id: LinearLayout
    lateinit var ll_wallet_id: LinearLayout
    internal lateinit var sessionManager: SessionManager
    internal lateinit var my_loader: Dialog

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paymet_details)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        dashboard_home = findViewById(R.id.dashboard_home) as ImageView
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        sessionManager = SessionManager(this)
        myloading()
        getAdminStatusInfo()

        bill_id = intent.getStringExtra("bill_id")
        bill_type = intent.getStringExtra("bill_type")
        getBillDetailsInfo()
        tv_pay_for_type_id = findViewById(R.id.tv_pay_for_type_id)
        img_pay_type_id = findViewById(R.id.img_pay_type_id)
        tv_reciept_id = findViewById(R.id.tv_reciept_id)
        tv_no_plate_id = findViewById(R.id.tv_no_plate_id)
        tv_vehicle_type_id = findViewById(R.id.tv_vehicle_type_id)
        tv_start_date_id = findViewById(R.id.tv_start_date_id)
        tv_end_date_id = findViewById(R.id.tv_end_date_id)
        tv_zone_id = findViewById(R.id.tv_zone_id)
        tv_amount_id = findViewById(R.id.tv_amount_id)
        tv_hours_id = findViewById(R.id.tv_hours_id)
        tv_send_mail_id = findViewById(R.id.tv_send_mail_id)
        tv_sripe_amount_id = findViewById(R.id.tv_sripe_amount_id)
        tv_wallet_amount_id = findViewById(R.id.tv_wallet_amount_id)


        tv_reciept_title_id = findViewById(R.id.tv_reciept_title_id)
        tv_no_plate_title_id = findViewById(R.id.tv_no_plate_title_id)
        tv_vehicle_type_title_id = findViewById(R.id.tv_vehicle_type_title_id)
        tv_start_date_title_id = findViewById(R.id.tv_start_date_title_id)
        tv_end_date_title_id = findViewById(R.id.tv_end_date_title_id)
        tv_zone_title_id = findViewById(R.id.tv_zone_title_id)
        tv_amount_title_id = findViewById(R.id.tv_amount_title_id)
        tv_hours_title_id = findViewById(R.id.tv_hours_title_id)

        ll_start_date_id = findViewById(R.id.ll_start_date_id)
        ll_end_date_id = findViewById(R.id.ll_end_date_id)
        ll_zone_no_id = findViewById(R.id.ll_zone_no_id)
        ll_hours_id = findViewById(R.id.ll_hours_id)
        ll_stripe_id = findViewById(R.id.ll_stripe_id)
        ll_wallet_id = findViewById(R.id.ll_wallet_id)

        if (bill_type.equals("funds")) {
            img_pay_type_id.setImageDrawable(getDrawable(R.drawable.add_money_full_ic))
        } else if (bill_type.equals("park")) {
            img_pay_type_id.setImageDrawable(getDrawable(R.drawable.parking_full_ic))
        } else if (bill_type.equals("service")) {
            img_pay_type_id.setImageDrawable(getDrawable(R.drawable.service_full_ic))
        }

        dashboard_home.setOnClickListener {
            val intent = Intent(this@PaymetDetails, MainActivity::class.java)
            startActivity(intent)
        }

    }

    private fun getBillDetailsInfo() {

        val apiService = ApiInterface.create()
        val call = apiService.getBillDetails(bill_id)
        Log.d("REQUEST", bill_id + "")
        call.enqueue(object : Callback<getBillDetailsResponse> {
            override fun onResponse(call: Call<getBillDetailsResponse>, response: retrofit2.Response<getBillDetailsResponse>?) {
                if (response != null) {

                    // Log.w("Result_Address_Profile","Result : "+response.body()!!.user_info)
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.data != null) {


                            if (bill_type.equals("funds")) {
                                Log.e("main_details", response.body()!!.data!!.funds!!.amount.toString())

                                tv_pay_for_type_id.setText(response.body()!!.data!!.funds!!.message.toString())

                                tv_reciept_id.setText(response.body()!!.data!!.funds!!.transaction_id.toString())
                                tv_no_plate_title_id.setText("Date")


                                val inputFormat = SimpleDateFormat("yyyy-MM-dd")
                                val outputFormat = SimpleDateFormat("MM-dd-yyyy")
                                val inputDateStr = response.body()!!.data!!.funds!!.date.toString()
                                val date = inputFormat.parse(inputDateStr)
                                val outputDateStr = outputFormat.format(date)

                                tv_no_plate_id.setText(outputDateStr)
                                tv_vehicle_type_title_id.setText("Amount has been")
                                tv_vehicle_type_id.setText(response.body()!!.data!!.funds!!.amount_type.toString())
                                tv_amount_id.setText("$ " + response.body()!!.data!!.funds!!.amount.toString())

                                ll_start_date_id.visibility = View.GONE
                                ll_end_date_id.visibility = View.GONE
                                ll_zone_no_id.visibility = View.GONE
                                ll_hours_id.visibility = View.GONE

                            } /*else if (bill_type.equals("park")) {

                                val inputEndFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                                val outputEndFormat = SimpleDateFormat("MM-dd-yyyy HH:mm:ss")
                                val inputEndDateStr = response.body()!!.data!!.park!!.parkig_end_date
                                val dateend = inputEndFormat.parse(inputEndDateStr)
                                val outputEndDateStr = outputEndFormat.format(dateend)

                                val inputStartFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                                val outputStartFormat = SimpleDateFormat("MM-dd-yyyy HH:mm:ss")
                                val inputStartDateStr = response.body()!!.data!!.park!!.parkig_start_date
                                val datestart = inputStartFormat.parse(inputStartDateStr)
                                val outputStartDateStr = outputStartFormat.format(datestart)
                                Log.e("PARKING_ENDDATE", response.body()!!.data!!.park!!.parkig_end_date+"------"+outputEndDateStr)






                                Log.e("main_details", response.body()!!.data!!.park!!.amount.toString())
                                tv_pay_for_type_id.setText(response.body()!!.data!!.park!!.message.toString())
                                tv_reciept_id.setText(response.body()!!.data!!.park!!.transaction_id.toString())
                                tv_no_plate_id.setText(response.body()!!.data!!.park!!.no_plate.toString())
                                tv_vehicle_type_id.setText(response.body()!!.data!!.park!!.vehicle_type.toString())
                                tv_start_date_id.setText(outputStartDateStr.toString())
                                tv_end_date_id.setText(outputEndDateStr.toString())
                                tv_zone_id.setText(response.body()!!.data!!.park!!.zone_rate_id.toString())
                                tv_amount_id.setText("$ " + response.body()!!.data!!.park!!.amount.toString())
                                // tv_hours_id.setText(response.body()!!.data!!.park!!..toString())


                                val end_date = SimpleDateFormat("MM-dd-yyyy HH:mm:ss").parse(response.body()!!.data!!.park!!.parkig_end_date)
                                val start_time = SimpleDateFormat("MM-dd-yyyy HH:mm:ss").parse(response.body()!!.data!!.park!!.parkig_start_date)

                                val diff = end_date.time - start_time.time
                                val numOfDays = (diff / (1000 * 60 * 60 * 24)).toInt()
                                val hours = (diff / (1000 * 60 * 60)).toInt()
                                val minutes = (diff / (1000 * 60)).toInt()
                                val seconds = (diff / 1000).toInt()
                                tv_hours_id.setText(hours.toString())


                            }*/ else if (bill_type.equals("service")) {
                                Log.e("main_details", response.body()!!.data!!.service!!.amount.toString())

                                tv_pay_for_type_id.setText(response.body()!!.data!!.service!!.message.toString())

                                tv_reciept_id.setText(response.body()!!.data!!.service!!.transaction_id.toString())
                                tv_no_plate_title_id.setText("Name")
                                tv_no_plate_id.setText(response.body()!!.data!!.service!!.name.toString())
                                tv_vehicle_type_title_id.setText("Service Address")
                                tv_vehicle_type_id.setText(response.body()!!.data!!.service!!.service_address.toString())
                                tv_start_date_title_id.setText("Account")
                                tv_start_date_id.setText(response.body()!!.data!!.service!!.account.toString())
                                tv_amount_id.setText("$ " + response.body()!!.data!!.service!!.amount.toString())

                                tv_wallet_amount_id.setText("$ " + response.body()!!.data!!.service!!.pay_with_wallet.toString())
                                tv_sripe_amount_id.setText("$ " + response.body()!!.data!!.service!!.strip_amount.toString())


                                ll_stripe_id.visibility = View.VISIBLE
                                ll_wallet_id.visibility = View.VISIBLE
                                ll_end_date_id.visibility = View.GONE
                                ll_zone_no_id.visibility = View.GONE
                                ll_hours_id.visibility = View.GONE

                            }



                        }

                    }else if (response.body()!!.status.equals("2")){

                    }

                }
            }

            override fun onFailure(call: Call<getBillDetailsResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


}
