package com.gov.biil.adapter

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.RadioButton
import android.widget.RadioGroup
import com.gov.biil.R
import com.gov.biil.model.apiresponses.GetAllServiceTypesDataResponse




data class GetAllServiceListAdapter(val mServiceList: ArrayList<GetAllServiceTypesDataResponse>, val activity: Activity) : BaseAdapter() {

    /*init {
        this.mServiceList2 = mServiceList
        setDatatoRadioGroup(mServiceList2)
    }*/

    private lateinit var radiobtn_allservices: RadioGroup
    var temp = false
    override fun getItem(position: Int): Any {
        return mServiceList.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return 1
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view: View = View.inflate(activity, R.layout.listitem_getallservicetypes, null)
        radiobtn_allservices = view.findViewById(R.id.radiobtn_allservices) as RadioGroup

        //val radiobtn_allservices = view.findViewById(R.id.radiobtn_allservices) as RadioButton

        /* radiobtn_allservices.setOrientation(LinearLayout.HORIZONTAL);
         val rbn = RadioButton(activity)
         val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f)
         rbn.layoutParams = params
         radiobtn_allservices.addView(rbn)*/

        //rbn.text = mServiceList.get(position).service_type
        //if (temp == false) {
           // setDatatoRadioGroup(mServiceList)
       // }

        setDatatoRadioGroup(mServiceList)

        return view
    }


    private fun setDatatoRadioGroup(mUPSList: ArrayList<GetAllServiceTypesDataResponse>) {

        temp = true
        radiobtn_allservices.removeAllViews()
        var rprms: RadioGroup.LayoutParams
        for (i in 0 until mUPSList.size) {
            val radioButton = RadioButton(activity)
            radioButton.text = mUPSList.get(i).service_type
            rprms = RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT)
            radioButton.setPadding(10,5,25,0)

            /*if (Build.VERSION.SDK_INT >= 21) {
                val colorStateList = ColorStateList(
                        arrayOf(
                                intArrayOf(-android.R.attr.state_enabled), //disabled
                                intArrayOf(android.R.attr.state_enabled) //enabled
                        ),
                        intArrayOf(

                                Color.BLUE //disabled
                                , Color.BLUE //enabled
                        )
                )
                radioButton.setButtonTintList(colorStateList)//set the color tint list
                radioButton.invalidate() //could not be necessary
            }*/

            radiobtn_allservices.addView(radioButton, rprms)
            radiobtn_allservices.setPadding(10,5,30,0)


        }
    }



}
