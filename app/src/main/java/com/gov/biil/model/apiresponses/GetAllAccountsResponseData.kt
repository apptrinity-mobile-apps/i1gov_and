package com.gov.biil.model.apiresponses

class GetAllAccountsResponseData {

    val city: Array<GetAllAccountsCityResponseData>?=null
    val state: Array<GetAllAccountsStateResponseData>?=null
    val federal: Array<GetAllAccountsFederalResponseData>?=null
    val county: Array<GetAllAccountsCountyResponseData>?=null
    val user_id: String? = null

}
