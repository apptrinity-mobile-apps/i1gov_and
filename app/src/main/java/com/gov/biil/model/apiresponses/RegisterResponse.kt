package com.gov.biil.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class RegisterResponse {
     val message: String? = null

     val result: String? = null

     val status: String? = null

}
