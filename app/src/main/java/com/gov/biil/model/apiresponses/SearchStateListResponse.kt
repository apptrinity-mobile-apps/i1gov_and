package com.gov.biil.model.apiresponses

class SearchStateListResponse {

    val result: String? = null

    val data: Array<SearchStateListDataResponse>? = null

    val status: String? = null

}
