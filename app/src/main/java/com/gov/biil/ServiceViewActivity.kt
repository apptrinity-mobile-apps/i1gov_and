package com.gov.biil

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import com.gov.a4printuser.Helper.BundleDataInfo
import com.gov.a4printuser.Helper.ConnectionManager
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.AllServicesByUserInfo
import com.gov.biil.model.apiresponses.AllServicesByUserResponse
import retrofit2.Call
import retrofit2.Callback

class ServiceViewActivity : AppCompatActivity() {
    var mServiceInfo: ArrayList<AllServicesByUserInfo> = ArrayList<AllServicesByUserInfo>()

    internal lateinit var ss_id:TextView
    internal lateinit var sd_duedate:TextView
    internal lateinit var sd_reminderdate:TextView
    internal lateinit var sd_reminderdate2:TextView
    internal lateinit var sd_reminderdate3:TextView
    internal lateinit var sd_servicetype:TextView
    internal lateinit var sd_service_name:TextView
    internal lateinit var sd_service_unitno:TextView
    internal lateinit var sd_servicelink:TextView

    internal lateinit var tv_address:TextView
    internal lateinit var tv_city:TextView
    internal lateinit var tv_state:TextView
    internal lateinit var tv_zipcode:TextView

    internal lateinit var id:String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_view)

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        ss_id = findViewById(R.id.sd_id) as TextView
        sd_duedate = findViewById(R.id.sd_duedate) as TextView
        sd_reminderdate = findViewById(R.id.sd_reminderdate) as TextView
        sd_reminderdate2 = findViewById(R.id.sd_reminderdate2) as TextView
        sd_reminderdate3 = findViewById(R.id.sd_reminderdate3) as TextView
        sd_servicetype = findViewById(R.id.sd_servicetype) as TextView
        sd_service_name = findViewById(R.id.sd_service_name) as TextView
        sd_service_unitno = findViewById(R.id.sd_service_unitno) as TextView
        sd_servicelink = findViewById(R.id.sd_servicelink) as TextView

        tv_address = findViewById(R.id.tv_address) as TextView
        tv_city = findViewById(R.id.tv_city) as TextView
        tv_state = findViewById(R.id.tv_state) as TextView
        tv_zipcode = findViewById(R.id.tv_zipcode) as TextView

        val rootview = findViewById(R.id.rootview) as LinearLayout
        myloading()
        if(intent != null && intent.getStringExtra(BundleDataInfo.SERVICE_VIEW_ID)!= null){
            id = intent.getStringExtra(BundleDataInfo.SERVICE_VIEW_ID)
            if(ConnectionManager.checkConnection(applicationContext)) {
                callServiceViewAPI()
            }else{
                ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
            }

        }






    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun callServiceViewAPI() {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.getServicesById(id)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<AllServicesByUserResponse> {
            override fun onResponse(call: Call<AllServicesByUserResponse>, response: retrofit2.Response<AllServicesByUserResponse>?) {
                my_loader.dismiss()
                Log.w("Status","*** "+response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if(response.body()!!.service_info!= null) {
                        val list: Array<AllServicesByUserInfo> = response.body()!!.service_info!!

                        for (item: AllServicesByUserInfo in list.iterator()) {
                            mServiceInfo.add(item)
                            setServicetoView(mServiceInfo)
                        }
                    }

                }
            }

            override fun onFailure(call: Call<AllServicesByUserResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })
    }

    private fun setServicetoView(mServiceInfo: ArrayList<AllServicesByUserInfo>) {
        ss_id.text= mServiceInfo.get(0).id
        sd_duedate.text= mServiceInfo.get(0).due_date
        if(mServiceInfo.get(0).reminder_date_one.equals("")){
            sd_reminderdate2.visibility = View.GONE
        }else{
            sd_reminderdate2.visibility = View.VISIBLE
            sd_reminderdate2.text= mServiceInfo.get(0).reminder_date_one
        }
        if(mServiceInfo.get(0).reminder_date_two.equals("")){
            sd_reminderdate3.visibility = View.GONE
        }else{
            sd_reminderdate3.visibility = View.VISIBLE
            sd_reminderdate3.text= mServiceInfo.get(0).reminder_date_two
        }
        sd_reminderdate.text= mServiceInfo.get(0).remider_date
        sd_reminderdate2.text= mServiceInfo.get(0).reminder_date_one
        sd_reminderdate3.text= mServiceInfo.get(0).reminder_date_two
        sd_servicetype.text= mServiceInfo.get(0).service_type
        sd_service_name.text= mServiceInfo.get(0).city_name
        sd_service_unitno.text= mServiceInfo.get(0).apt_unit_no
        sd_servicelink.text= mServiceInfo.get(0).service_link

        tv_address.text = mServiceInfo.get(0).address
        tv_city.text = mServiceInfo.get(0).city
        tv_state.text = mServiceInfo.get(0).state
        tv_zipcode.text = mServiceInfo.get(0).zip_code

    }
}
