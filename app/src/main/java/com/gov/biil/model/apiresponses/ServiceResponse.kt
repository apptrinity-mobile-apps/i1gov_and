package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class ServiceResponse {
    val user_info: Service_Info? = null

    val result: String? = null

    val status: String? = null
}
