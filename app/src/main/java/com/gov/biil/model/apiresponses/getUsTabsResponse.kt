package com.gov.biil.model.apiresponses

class getUsTabsResponse {

    val data: Array<getUsTabsDataResponse>?=null

    val result: String? = null

    val status: String? = null

}
