package com.gov.biil.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.webkit.WebView
import android.webkit.WebViewClient
import com.gov.a4print.Session.SessionManager
import com.gov.biil.R


/**
 * A simple [Fragment] subclass.
 */
class CountyPayForServiceFragment : Fragment() {
    lateinit var fragmentName: Fragment
    var mywebview: WebView? = null
    internal lateinit var my_loader: Dialog
    internal  var paylink:String?=""
    var uri: Uri? = null
    internal lateinit var sessionManager: SessionManager

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = LayoutInflater.from(activity).inflate(R.layout.countryfragment, container, false)
        mywebview = rootView.findViewById<WebView>(R.id.webview_pricecalc)
        myloading()


        //my_loader.show()

       // paylink = "https://www.cookcountyil.gov/"

        sessionManager = SessionManager(this.activity!!)

        Log.w("COUNTY",""+sessionManager.countyurl)

        if(sessionManager.countyurl!=""){
            paylink = sessionManager.countyurl
            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }
        }else{
            paylink = "https://www.cookcountyil.gov/"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }

        }



        mywebview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)

                return true
            }
        }
        mywebview!!.getSettings().setJavaScriptEnabled(true);
        mywebview!!.setHorizontalScrollBarEnabled(false);
        mywebview!!.loadUrl(uri.toString())

        my_loader.dismiss()
        return rootView
    }

    private fun myloading() {
        my_loader = Dialog(activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }
}