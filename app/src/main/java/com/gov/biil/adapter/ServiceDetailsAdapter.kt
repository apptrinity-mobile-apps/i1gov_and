package com.gov.biil.adapter

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.gov.a4printuser.Helper.BundleDataInfo
import com.gov.a4printuser.Helper.ConnectionManager
import com.gov.biil.R
import com.gov.biil.ServiceEditActivity
import com.gov.biil.ServiceViewActivity
import com.gov.biil.model.apiresponses.AllServicesByUserInfo


data class ServiceDetailsAdapter(val mServiceList: ArrayList<AllServicesByUserInfo>, val activity: Activity) : BaseAdapter() {


    override fun getItem(position: Int): Any {
        return mServiceList.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return mServiceList.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = View.inflate(activity, R.layout.dashboard_details_adapter_layout, null)
        val rootview = view.findViewById(R.id.rootview) as LinearLayout
        val sd_id = view.findViewById(R.id.sd_id) as TextView
        val sd_duedate = view.findViewById(R.id.sd_duedate) as TextView
        val sd_reminderdate = view.findViewById(R.id.sd_reminderdate) as TextView
        val sd_servicetype = view.findViewById(R.id.sd_servicetype) as TextView
        val sd_service_name = view.findViewById(R.id.sd_service_name) as TextView
        val sd_service_unitno = view.findViewById(R.id.sd_service_unitno) as TextView
        val sd_servicelink = view.findViewById(R.id.sd_servicelink) as TextView
        val view_line = view.findViewById(R.id.view_line) as View


        val tv_address = view.findViewById(R.id.tv_address) as TextView
        val tv_city = view.findViewById(R.id.tv_city) as TextView
        val tv_state = view.findViewById(R.id.tv_state) as TextView
        val tv_zipcode = view.findViewById(R.id.tv_zipcode) as TextView

        val view_serivce = view.findViewById(R.id.btn_view) as Button
        val edit_service = view.findViewById(R.id.btn_edit) as Button
        val pay_service = view.findViewById(R.id.btn_pay) as Button



        sd_id.text = mServiceList.get(position).id
        sd_duedate.text = mServiceList.get(position).due_date
        sd_reminderdate.text = mServiceList.get(position).remider_date
        sd_servicetype.text = mServiceList.get(position).service_type
        sd_service_name.text = mServiceList.get(position).city_name
        tv_address.text = mServiceList.get(position).address
        tv_city.text = mServiceList.get(position).city
        tv_state.text = mServiceList.get(position).state
        tv_zipcode.text = mServiceList.get(position).zip_code

        sd_service_unitno.text = mServiceList.get(position).apt_unit_no
        sd_servicelink.text = mServiceList.get(position).service_link


        if(mServiceList.get(position).service_type.equals("City")){
            view_line.setBackgroundColor(activity.resources.getColor(R.color.dashboard_city))
        }else if(mServiceList.get(position).service_type.equals("State")){
            view_line.setBackgroundColor(activity.resources.getColor(R.color.dashboard_state))
        }else if(mServiceList.get(position).service_type.equals("County")){
            view_line.setBackgroundColor(activity.resources.getColor(R.color.dashboard_county))
        }else if(mServiceList.get(position).service_type.equals("Federal")){
            view_line.setBackgroundColor(activity.resources.getColor(R.color.dashboard_federal))
        }

        view_serivce.setOnClickListener(View.OnClickListener {

            if (ConnectionManager.checkConnection(activity)) {
                val intent = Intent(this.activity, ServiceViewActivity::class.java)
                intent.putExtra(BundleDataInfo.SERVICE_VIEW_ID, mServiceList.get(position).id)
                activity.startActivity(intent)
            } else {
                ConnectionManager.snackBarNetworkAlert(rootview, activity)
            }

        })
        edit_service.setOnClickListener(View.OnClickListener {
            if (ConnectionManager.checkConnection(activity)) {
                val intent = Intent(this.activity, ServiceEditActivity::class.java)
                intent.putExtra(BundleDataInfo.SERVICE_VIEW_ID, mServiceList.get(position).id)
                intent.putExtra(BundleDataInfo.SERVICE_VIEW_TYPE, mServiceList.get(position).service_type)
                activity.startActivity(intent)
            } else {
                ConnectionManager.snackBarNetworkAlert(rootview, activity)
            }

        })
        pay_service.setOnClickListener(View.OnClickListener {
            if (ConnectionManager.checkConnection(activity)) {
                val uri: Uri
                if (mServiceList.get(position).service_link != null) {
                    if (mServiceList.get(position).service_link!!.contains("http") || mServiceList.get(position).service_link!!.contains("https")) {
                        uri = Uri.parse(mServiceList.get(position).service_link)
                    } else {
                        uri = Uri.parse("http://" + mServiceList.get(position).service_link)
                    }
                    val intent = Intent(Intent.ACTION_VIEW, uri)
                    activity.startActivity(intent)

                }
            } else {
                ConnectionManager.snackBarNetworkAlert(rootview, activity)
            }

        })

        return view
    }

}
