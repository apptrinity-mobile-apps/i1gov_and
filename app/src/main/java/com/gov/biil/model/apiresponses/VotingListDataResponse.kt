package com.gov.biil.model.apiresponses

class VotingListDataResponse {

    val user_id: String? = null
    val result: String? = null
    val list: Array<VotingListArrayDataResponse>?=null

}
