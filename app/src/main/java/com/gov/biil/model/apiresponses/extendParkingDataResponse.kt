package com.gov.biil.model.apiresponses

class extendParkingDataResponse {

    val id: String? = null
    val receipt_id: String? = null
    val hours: String? = null
    val start_date: String? = null
    val end_date: String? = null
    val remain_min: String? = null
    val remain_hour: String? = null
    val total_min: String? = null
    val total_hours: String? = null
    val zone_id: String? = null
    val lot_id: String? = null

}
