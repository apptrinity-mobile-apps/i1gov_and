package com.gov.biil

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.BundleDataInfo
import com.gov.a4printuser.Helper.ConnectionManager
import com.gov.biil.model.APIResponse.UpdateProfileResponse
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.Service_Info
import com.gov.biil.model.apiresponses.UserInfo
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*


class ProfileInfoEditActivity : AppCompatActivity() {

    public var mProfileData: ArrayList<UserInfo> = ArrayList<UserInfo>()
    internal lateinit var sessionManager: SessionManager

    internal lateinit var userid: EditText
    internal lateinit var fullname: EditText
    internal lateinit var dob: EditText
    internal lateinit var gender: EditText
    internal lateinit var ssn: EditText
    internal lateinit var address: EditText
    internal lateinit var city: EditText
    internal lateinit var cityurl_tv: EditText
    internal lateinit var country: EditText
    internal lateinit var countryurl_tv: EditText
    internal lateinit var state: EditText
    internal lateinit var stateurl_tv: EditText
    internal lateinit var email_tv: EditText
    internal lateinit var zipcode_tv: EditText
    internal lateinit var cellphone: EditText
    internal lateinit var citizen: RadioButton
    internal lateinit var bussiness: RadioButton
    internal lateinit var government: RadioButton
    internal lateinit var myRadioGroup: RadioGroup

    internal lateinit var organization_et: EditText
    internal lateinit var row_organization_et: LinearLayout



    var userid_st: String?=null
    var fullname_st: String?=null
    var dob_st: String?=null
    var gender_st: String?=null
    var ssn_st: String?=null
    var address_st: String?=null
    var city_st: String?=null
    var cityurl_tv_st: String?=null
    var country_st: String?=null
    var countryurl_tv_st: String?=null
    var state_st: String?=null
    var stateurl_tv_st: String?=null
    var email_tv_st: String?=null
    var zipcode_tv_st: String?=null
    var cellphone_st: String?=null
    var usertype_st: String?=null
    var organization_st: String?=null

    var dialog_list: Dialog? = null
    var list_items_list: ListView? = null
    internal lateinit var rb_female: RadioButton
    internal lateinit var rb_male: RadioButton

    internal lateinit var update_profile: Button
    var cal = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profileinfo_edit)
        sessionManager = SessionManager(this)


        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        var rootview = findViewById(R.id.rootview) as LinearLayout
        dialog_list = Dialog(this)
        dialog_list!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list!!.setContentView(R.layout.dialog_view)
        dialog_list!!.setCancelable(true)

        list_items_list = dialog_list!!.findViewById(R.id.list_languages)
        myloading()

        getAdminStatusInfo()

        userid= findViewById(R.id.userid) as EditText
        fullname= findViewById(R.id.fullname) as EditText
        dob= findViewById(R.id.dob) as EditText
       // gender= findViewById(R.id.gender) as EditText
        ssn= findViewById(R.id.ssn) as EditText
        address= findViewById(R.id.address) as EditText
        city= findViewById(R.id.city_tv) as EditText
        cityurl_tv= findViewById(R.id.cityurl_tv) as EditText
        country= findViewById(R.id.country_tv) as EditText
        countryurl_tv= findViewById(R.id.countryurl_tv) as EditText
        state= findViewById(R.id.state_tv) as EditText
        stateurl_tv= findViewById(R.id.stateurl_tv) as EditText
        email_tv= findViewById(R.id.email_tv) as EditText
        zipcode_tv= findViewById(R.id.zipcode_tv) as EditText
        cellphone= findViewById(R.id.cellphone) as EditText


        citizen= findViewById(R.id.citizen) as RadioButton
        bussiness= findViewById(R.id.bussiness) as RadioButton
        government= findViewById(R.id.government) as RadioButton
        myRadioGroup= findViewById(R.id.myRadioGroup) as RadioGroup
        rb_male= findViewById(R.id.male) as RadioButton
        rb_female= findViewById(R.id.female) as RadioButton

        update_profile= findViewById(R.id.btn_update_profile) as Button

        organization_et= findViewById(R.id.organization_et) as EditText
        row_organization_et= findViewById(R.id.row_organization_et) as LinearLayout

        rb_male.setOnClickListener(View.OnClickListener {
            gender_st=rb_male.text.toString()
        })
        rb_female.setOnClickListener(View.OnClickListener {
            gender_st=rb_female.text.toString()
        })

        update_profile.setOnClickListener(View.OnClickListener {
            if(ConnectionManager.checkConnection(applicationContext)) {
                updateProfileInfo()
            }
            else{
                ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
            }


        })

        readBundleData(intent)
        dob.setOnClickListener {
           /* DatePickerDialog(this@ProfileInfoEditActivity, dateselection,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()*/

            val datePickerDialog = DatePickerDialog(this, dateselection,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));
            // Limiting access to past dates in the step below:
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show()
        }

        state.setOnClickListener(View.OnClickListener {
            if (!dialog_list!!.isShowing)
                dialog_list!!.show()
            val profile_array_adapter = ArrayAdapter<String>(this, R.layout.simple_spinner_item, BundleDataInfo.STATES_LIST)
            list_items_list!!.adapter = profile_array_adapter
            list_items_list!!.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_state = list_items_list!!.getItemAtPosition(i).toString()
                state.setText(selected_state)
                dialog_list!!.dismiss()
                // selected_profile_id = state_code_array.get(stringProfileArrayList.indexOf(selected_profile_name!!))
                Log.e("selected_name_id_name", selected_state)
                stateurl_tv.setText(BundleDataInfo.STATES_URLLIST.get(i))


            }
        })

        citizen.setOnClickListener(View.OnClickListener {
            row_organization_et.visibility=View.GONE
            usertype_st = citizen.text.toString()
        })
        bussiness.setOnClickListener(View.OnClickListener {
            row_organization_et.visibility=View.VISIBLE
            usertype_st = bussiness.text.toString()})
        government.setOnClickListener(View.OnClickListener {
            row_organization_et.visibility=View.VISIBLE
            usertype_st = government.text.toString()
        })

    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }
    private fun readBundleData(intent: Intent) {
        if(intent != null && intent.getStringExtra(BundleDataInfo.PROFILE_USERID)!= null ) {

            userid_st = intent.getStringExtra(BundleDataInfo.PROFILE_USERID)
            fullname_st = intent.getStringExtra(BundleDataInfo.PROFILE_FULLNAME)
            dob_st = intent.getStringExtra(BundleDataInfo.PROFILE_DOB)
            gender_st = intent.getStringExtra(BundleDataInfo.PROFILE_GENDER)
            ssn_st = intent.getStringExtra(BundleDataInfo.PROFILE_SSN)
            address_st = intent.getStringExtra(BundleDataInfo.PROFILE_ADDRESS)
            city_st = intent.getStringExtra(BundleDataInfo.PROFILE_CITY)
            cityurl_tv_st = intent.getStringExtra(BundleDataInfo.PROFILE_CITYURL)
            state_st = intent.getStringExtra(BundleDataInfo.PROFILE_STATE)
            stateurl_tv_st = intent.getStringExtra(BundleDataInfo.PROFILE_STATEURL)
            country_st = intent.getStringExtra(BundleDataInfo.PROFILE_COUNTRY)
            countryurl_tv_st = intent.getStringExtra(BundleDataInfo.PROFILE_COUNTRYURL)
            email_tv_st = intent.getStringExtra(BundleDataInfo.PROFILE_EMAIL)
            zipcode_tv_st = intent.getStringExtra(BundleDataInfo.PROFILE_ZIPCODE)
            cellphone_st = intent.getStringExtra(BundleDataInfo.PROFILE_CELLPHONE)
            usertype_st = intent.getStringExtra(BundleDataInfo.PROFILE_USERTYPE)
            if(intent.getStringExtra(BundleDataInfo.PROFILE_ORGANIZATION)!= null) {
                organization_st = intent.getStringExtra(BundleDataInfo.PROFILE_ORGANIZATION)
                row_organization_et.visibility = View.VISIBLE
                organization_et.setText(organization_st)
            }else{
                organization_st = ""
                row_organization_et.visibility = View.GONE
            }

            try {
                userid.setText(userid_st)
                fullname.setText(fullname_st)
                dob.setText(dob_st)
                //gender.setText(gender_st)
                if(gender_st.equals("Female")){
                    gender_st=rb_female.text.toString()
                    rb_female.isChecked=true
                    rb_male.isChecked=false
                }else{
                    gender_st=rb_male.text.toString()
                    rb_female.isChecked=false
                    rb_male.isChecked=true
                }
                ssn.setText(ssn_st)
                address.setText(address_st)
                city.setText(city_st)
                cityurl_tv.setText(cityurl_tv_st)
                country.setText(country_st)
                countryurl_tv.setText(countryurl_tv_st)
                state.setText(state_st)
                stateurl_tv.setText(stateurl_tv_st)
                email_tv.setText(email_tv_st)
                zipcode_tv.setText(zipcode_tv_st)
                cellphone.setText(cellphone_st)

                if(usertype_st!= null){
                    if(usertype_st.equals("Citizen")){
                        citizen.isChecked=true
                        row_organization_et.visibility=View.GONE
                    }else if(usertype_st.equals("Business")){
                        bussiness.isChecked=true
                        row_organization_et.visibility=View.VISIBLE
                    }else if(usertype_st.equals("Government")){
                        government.isChecked=true
                        row_organization_et.visibility=View.VISIBLE
                    }
                }else{
                    usertype_st = citizen.text.toString()
                }
                /*if(organization.text1!= null)
                    organization_st = organization.text.toString()
                else
                    organization_st = ""*/


            }catch (e:Exception){}

        }

    }

    private fun updateProfileInfo() {
        val apiService = ApiInterface.create()
        my_loader.show()
        val name = fullname.text.toString()!!.split(" ")
        val call = apiService.updateProfile(userid.text.toString(),usertype_st.toString(),name[0],"",name[1],dob.text.toString(),gender_st.toString(),
                ssn.text.toString(),address.text.toString(),city.text.toString(),cityurl_tv.text.toString(),country.text.toString(),countryurl_tv.text.toString(),
                state.text.toString(),stateurl_tv.text.toString(),zipcode_tv.text.toString(),email_tv.text.toString(),cellphone.text.toString(),
                "","",organization_et.text.toString())

        Log.d("REQUEST_CALLLLL", call.toString() + ""+cellphone.text.toString())
        call.enqueue(object : Callback<UpdateProfileResponse> {
            override fun onResponse(call: Call<UpdateProfileResponse>, response: retrofit2.Response<UpdateProfileResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_Address_Profile","Result : "+response.body()!!.status)
                    if(response.body()!!.status.equals("1")) {

                        storeLoginData(response.body()!!.user_info)
                        val intent = Intent(this@ProfileInfoEditActivity,ProfileInfoActivity::class.java)
                        finish()
                        startActivity(intent)
                    }

                }
            }

            override fun onFailure(call: Call<UpdateProfileResponse>, t: Throwable) {
                Log.w("Result_Address_Profile",t.toString())
            }
        })
    }

    private fun storeLoginData(user_info: Service_Info?)  {
        if(user_info !=null) {
            try {
                sessionManager.storeUpdateProfileInfoSession(user_info)
            }
            catch (e: Exception) {
                Log.w("exception",""+e.printStackTrace())
            }

        }

    }



    val dateselection = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        var dateofmonth = dayOfMonth.toString()
        var monthofyear = monthOfYear
        var yearstg = year.toString()
        //getAge(year, monthOfYear, dayOfMonth)
        val myFormat = "MM/dd/yyyy" // mention the format you need

        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        Log.e("dateformat", sdf.toString())
        val date = sdf.format(cal.time)
        dob.setText(date)

    }



    override fun onBackPressed() {
        super.onBackPressed()
    }


    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


}
