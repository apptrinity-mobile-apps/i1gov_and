package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Countdashboard {

    val city: String? = null
    val state: String? = null
    val federal: String? = null
    val county: String? = null

}
