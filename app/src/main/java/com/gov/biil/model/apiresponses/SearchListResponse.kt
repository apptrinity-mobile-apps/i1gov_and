package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class SearchListResponse {
     val result: String? = null

     val data: Array<SearchListDataResponse>? = null

     val status: String? = null


}
