package com.gov.biil.model.apiresponses

class SearchCountyListResponse {

    val result: String? = null

    val data: Array<SearchCountyListDataResponse>? = null

    val status: String? = null



}
