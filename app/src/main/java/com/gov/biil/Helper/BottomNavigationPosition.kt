package com.gov.biil.Helper

import android.support.v4.app.Fragment
import com.gov.biil.DashBoardFragment
import com.gov.biil.MainActivity
import com.gov.biil.R
import com.gov.biil.fragment.CityFragment
import com.gov.biil.fragment.CountryFragment
import com.gov.biil.fragment.FederalFragment
import com.gov.biil.fragment.StateFragment

enum class BottomNavigationPosition(val position: Int, val id: Int) {
    HOME(0, R.id.home),
    CITY(1, R.id.city),
    COUNTY(2, R.id.county),
    STATE(3, R.id.state),
    FEDERAL(4, R.id.federal);
}

fun findNavigationPositionById(id: Int): BottomNavigationPosition = when (id) {
    BottomNavigationPosition.HOME.id -> BottomNavigationPosition.HOME
    BottomNavigationPosition.CITY.id -> BottomNavigationPosition.CITY
    BottomNavigationPosition.COUNTY.id -> BottomNavigationPosition.COUNTY
    BottomNavigationPosition.STATE.id -> BottomNavigationPosition.STATE
    BottomNavigationPosition.FEDERAL.id -> BottomNavigationPosition.FEDERAL
    else -> BottomNavigationPosition.HOME
}

fun BottomNavigationPosition.createFragment(): Fragment = when (this) {
    BottomNavigationPosition.HOME -> DashBoardFragment.newInstance()
    BottomNavigationPosition.CITY -> CityFragment.newInstance()
    BottomNavigationPosition.COUNTY -> CountryFragment.newInstance()
    BottomNavigationPosition.STATE -> StateFragment.newInstance()
    BottomNavigationPosition.FEDERAL -> FederalFragment.newInstance()

}





fun BottomNavigationPosition.getTag(): String = when (this) {
    BottomNavigationPosition.HOME -> MainActivity.TAG
    BottomNavigationPosition.CITY -> CityFragment.TAG
    BottomNavigationPosition.COUNTY -> CountryFragment.TAG
    BottomNavigationPosition.STATE -> StateFragment.TAG
    BottomNavigationPosition.FEDERAL -> FederalFragment.TAG
}

