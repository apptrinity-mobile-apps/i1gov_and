package com.gov.biil.model.apiresponses

class GetStateWiseUsersResponse {

    val result: String? = null

    val data: GetStateWiseUsersDataResponse? = null

    val status: String? = null

}
