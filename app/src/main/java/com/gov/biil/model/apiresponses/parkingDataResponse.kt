package com.gov.biil.model.apiresponses

/**
 * Created by indo67 on 7/21/2018.
 */
class parkingDataResponse {
    var remain_min:String?=null
    var remain_hour:String?=null
    var total_min:String?=null
    var total_hours:String?=null
    var zone_id:String?=null
    var lot_id:String?=null
    var receipt_id:String?=null
}