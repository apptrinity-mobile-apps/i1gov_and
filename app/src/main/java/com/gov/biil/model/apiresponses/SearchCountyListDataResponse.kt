package com.gov.biil.model.apiresponses

class SearchCountyListDataResponse {

    val id: String? = null
    val city: String? = null
    val state_code: String? = null
    val state_id: String? = null
    val county: String? = null
    val latitude: String? = null
    val longitude: String? = null

}
