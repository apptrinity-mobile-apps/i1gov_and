package com.gov.biil.model.apiresponses

/**
 * Created by indo67 on 7/7/2018.
 */
class stateslistDataResponse {

    val id: String? = null
    val state: String? = null
    val state_code: String? = null
}