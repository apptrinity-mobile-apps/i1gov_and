package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.CustomTextViewBold
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.*
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class TimerActivity : AppCompatActivity(), View.OnClickListener {

    private var timeCountInMilliSeconds = (1 * 60000).toLong()

    private enum class TimerStatus {
        STARTED,
        STOPPED
    }

    private var timerStatus = TimerStatus.STOPPED
    private var progressBarCircle: ProgressBar? = null
    private var editTextMinute: EditText? = null
    private var textViewTime: TextView? = null
    private var tv_time_and_price_id: TextView? = null
    private var imageViewReset: ImageView? = null
    private var tv_stoptimer: TextView? = null
    private var tv_extend_timer: TextView? = null
    private var cv_vehicle_rates: CardView? = null
    private var tv_confirm: CustomTextViewBold? = null
    private var countDownTimer: CountDownTimer? = null
    var timer_hrs = ""
    var timer_final = 0
    var receipt_id = ""
    var user_id = ""
    var no_plate = ""
    var state = ""
    var vehicle_type = ""
    var zone_rate_id = ""
    var lot_rate_id = ""
    var parkig_start_date = ""
    var parkig_end_date = ""
    var vehicle_price_id = ""
    var vehicle_price = ""
    var vehicle_time = ""
    var vehicle_time_only = ""
    var final_end_time = ""
    var from = ""
    var total_main_balance = ""


    var priceslistArray = ArrayList<priceslist>()
    var addresspricesArray = ArrayList<lotaddresspriceslist>()

    lateinit var stop_Dialog: Dialog
    lateinit var tv_ok_id: CustomTextView
    lateinit var tv_cancel_id: CustomTextView
    lateinit var tv_message_id: CustomTextView

    lateinit var dialog_list_price: Dialog
    lateinit var dialog_list_address: Dialog
    lateinit var list_items_list: ListView
    lateinit var list_items_list_zone: ListView
    lateinit var list_items_list_price: ListView
    lateinit var list_items_list_address: ListView

    lateinit var sessionManager: SessionManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timer)

        // method call to initialize the views
        initViews()
        // method call to initialize the listeners
        initListeners()


        timer_hrs = intent.getStringExtra("timer_hrs")
        receipt_id = intent.getStringExtra("receipt_id")
        user_id = intent.getStringExtra("user_id")
        no_plate = intent.getStringExtra("no_plate")
        state = intent.getStringExtra("state")
        vehicle_type = intent.getStringExtra("vehicle_type")
        zone_rate_id = intent.getStringExtra("zone_id")
        lot_rate_id = intent.getStringExtra("lot_id")
        parkig_start_date = intent.getStringExtra("parkig_start_date")
        total_main_balance = intent.getStringExtra("total_main_balance")




        if(intent.getStringExtra("parkig_end_date").equals("")){
            parkig_end_date = ""
            Log.e("PRKINGENDDATE0", parkig_end_date)
        }else{
            parkig_end_date = intent.getStringExtra("parkig_end_date")
            Log.e("PRKINGENDDATE1", parkig_end_date)
        }

        from = intent.getStringExtra("from")

        Log.d("REQUEST", timer_hrs + "---" + receipt_id + "---" + user_id + "---" + no_plate + "---" + state + "---" + vehicle_type + "---" + zone_rate_id + "---" + lot_rate_id + "---" + parkig_start_date + "---" + parkig_end_date)

        if (!zone_rate_id.equals("0")) {
            callPricesAPI()
        } else if (!lot_rate_id.equals("0")) {
            callAddressPricesAPI()
        }
        if (from.equals("all_current")) {
            timer_final = timer_hrs.toInt()
        } else {
            timer_final = timer_hrs.toInt() * 60
        }

        startStop()


        dialog_list_price = Dialog(this)
        dialog_list_price.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_price.setContentView(R.layout.dialog_view)
        dialog_list_price.setCancelable(true)
        list_items_list_price = dialog_list_price.findViewById(R.id.list_languages) as ListView
        tv_time_and_price_id!!.setOnClickListener {
            if (!dialog_list_price.isShowing())
                dialog_list_price.show()
        }



        tv_confirm!!.setOnClickListener {
            if (tv_time_and_price_id!!.text.toString().equals("Select Time & Rate")) {
                tv_time_and_price_id!!.setError("Time and rate Required")
            } else {
                calltoExtendConfirmAPI()
            }


        }

        Stopalert()
    }

    /**
     * method to initialize the views
     */
    private fun initViews() {
        progressBarCircle = findViewById(R.id.progressBarCircle) as ProgressBar
        editTextMinute = findViewById(R.id.editTextMinute) as EditText
        textViewTime = findViewById(R.id.textViewTime) as TextView
        imageViewReset = findViewById(R.id.imageViewReset) as ImageView
        tv_stoptimer = findViewById(R.id.tv_stoptimer) as TextView
        tv_extend_timer = findViewById(R.id.tv_extend_timer) as TextView
        cv_vehicle_rates = findViewById(R.id.cv_vehicle_rates) as CardView
        tv_confirm = findViewById(R.id.tv_confirm) as CustomTextViewBold

        tv_time_and_price_id = findViewById(R.id.tv_time_and_price_id)


        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        sessionManager = SessionManager(this)
        myloading()
        getAdminStatusInfo()

    }

    /**
     * method to initialize the click listeners
     */
    private fun initListeners() {
        imageViewReset!!.setOnClickListener(this)
        tv_stoptimer!!.setOnClickListener{

            stop_Dialog.show()
        }
        tv_extend_timer!!.setOnClickListener(this)
    }

    /**
     * implemented method to listen clicks
     *
     * @param view
     */
    override fun onClick(view: View) {
        when (view.id) {
        //R.id.imageViewReset -> reset()
        //  R.id.tv_stoptimer -> startStop()
            R.id.tv_extend_timer -> cv_vehicle_rates!!.visibility = View.VISIBLE
        }
    }

    /**
     * method to reset count down timer
     */
    private fun reset() {
        // stopCountDownTimer()
        startCountDownTimer()
    }




    /**
     * method to start and stop count down timer
     */
    private fun startStop() {
        if (timerStatus == TimerStatus.STOPPED) {

            // call to initialize the timer values
            setTimerValues()
            // call to initialize the progress bar values
            setProgressBarValues()
            // showing the reset icon
            imageViewReset!!.visibility = View.GONE
            // changing play icon to stop icon
            // tv_stoptimer.setImageResource(R.drawable.icon_stop);
            // making edit text not editable
            editTextMinute!!.isEnabled = false
            // changing the timer status to started
            timerStatus = TimerStatus.STARTED
            // call to start the count down timer
            startCountDownTimer()

        } else {

            // hiding the reset icon
            imageViewReset!!.visibility = View.GONE
            // changing stop icon to start icon
            //  tv_stoptimer.setImageResource(R.drawable.icon_start);
            // making edit text editable
            editTextMinute!!.isEnabled = true
            // changing the timer status to stopped
            timerStatus = TimerStatus.STARTED
            // stopCountDownTimer()

        }

    }

    /**
     * method to initialize the values for count down timer
     */
    private fun setTimerValues() {
        var time = 0
        if (!timer_final.toString().isEmpty()) {
            // fetching value from edit text and type cast to integer
            time = Integer.parseInt(timer_final.toString().trim { it <= ' ' })

        } else {
            // toast message to fill edit text
            //  Toast.makeText(applicationContext, getString(R.string.message_minutes), Toast.LENGTH_LONG).show()
        }
        // assigning values after converting to milliseconds
        timeCountInMilliSeconds = (time * 60 * 1000).toLong()
    }

    /**
     * method to start count down timer
     */
    private fun startCountDownTimer() {

        countDownTimer = object : CountDownTimer(timeCountInMilliSeconds, 1000) {
            override fun onTick(millisUntilFinished: Long) {

                textViewTime!!.text = hmsTimeFormatter(millisUntilFinished)

                progressBarCircle!!.progress = (millisUntilFinished / 1000).toInt()

            }

            override fun onFinish() {

                textViewTime!!.text = hmsTimeFormatter(timeCountInMilliSeconds)
                // call to initialize the progress bar values
                setProgressBarValues()
                // hiding the reset icon
                imageViewReset!!.visibility = View.GONE
                // changing stop icon to start icon
                // tv_stoptimer.setImageResource(R.drawable.icon_start);
                // making edit text editable
                editTextMinute!!.isEnabled = true
                // changing the timer status to stopped
                timerStatus = TimerStatus.STOPPED
            }

        }.start()
        countDownTimer!!.start()
    }

    /**
     * method to stop count down timer
     */
    private fun stopCountDownTimer() {
        countDownTimer!!.cancel()
    }

    /**
     * method to set circular progress bar values
     */
    private fun setProgressBarValues() {

        progressBarCircle!!.max = timeCountInMilliSeconds.toInt() / 1000
        progressBarCircle!!.progress = timeCountInMilliSeconds.toInt() / 1000
    }


    /**
     * method to convert millisecond to time format
     *
     * @param milliSeconds
     * @return HH:mm:ss time formatted string
     */
    private fun hmsTimeFormatter(milliSeconds: Long): String {

        val hms = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(milliSeconds),
                TimeUnit.MILLISECONDS.toMinutes(milliSeconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliSeconds)),
                TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds)))

        return hms
    }


    private fun callPricesAPI() {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getparkingprices(zone_rate_id)
        Log.d("CALLPRICES", zone_rate_id)
        priceslistArray.clear()
        call.enqueue(object : Callback<zonePricesResponse> {
            override fun onResponse(call: Call<zonePricesResponse>, response: retrofit2.Response<zonePricesResponse>?) {
                if (response != null) {
                    // my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.zoneprises != null) {
                            // setProfileDatatoView(response.body()!!.user_info)
                            val list: Array<priceslist>? = response.body()!!.zoneprises!!
                            for (i in 0 until list!!.size) {
                                Log.w("states_date", list.get(i).zone_number.toString())
                                priceslistArray!!.add(list.get(i))


                            }
                            val adapter_zone = PricesAdapter(this@TimerActivity, priceslistArray)
                            list_items_list_price.adapter = adapter_zone
                            adapter_zone.notifyDataSetChanged()
                        }

                    } else if (response.body()!!.status.equals("2")) {
                        Toast.makeText(applicationContext, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<zonePricesResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

    private fun callAddressPricesAPI() {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAddressPrices(lot_rate_id)
        Log.d("REQUEST", call.toString() + "")
        addresspricesArray.clear()
        addresspricesArray = ArrayList<lotaddresspriceslist>()
        call.enqueue(object : Callback<addressprices> {
            override fun onResponse(call: Call<addressprices>, response: retrofit2.Response<addressprices>?) {
                if (response != null) {
                    // my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.zoneprises != null) {
                            // setProfileDatatoView(response.body()!!.user_info)
                            val list: Array<lotaddresspriceslist>? = response.body()!!.zoneprises!!
                            for (i in 0 until list!!.size) {
                                Log.w("states_date_id", list.get(i).lot_address.toString())
                                addresspricesArray!!.add(list.get(i))

                            }
                            val adapter_zone = AddressPricesAdapter(this@TimerActivity, addresspricesArray)
                            list_items_list_price.adapter = adapter_zone
                            adapter_zone.notifyDataSetChanged()
                        }

                    } else if (response.body()!!.status.equals("2")) {
                        addresspricesArray.clear()
                        priceslistArray.clear()
                    }

                }
            }

            override fun onFailure(call: Call<addressprices>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }


        })
    }


    inner class PricesAdapter(context: Context, items: ArrayList<priceslist>) : ArrayAdapter<priceslist>(context, 0, items) {

        internal var inflater: LayoutInflater

        init {
            inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var v = convertView
            val cell = getItem(position)

            v = inflater.inflate(R.layout.prices_list_item, null)
            val name = v!!.findViewById(R.id.tv_prices_id) as TextView
            val ch_hrs_id = v!!.findViewById(R.id.ch_hrs_id) as TextView

            if (cell.hour.equals("1")) {
                ch_hrs_id.setText(cell.hour + " hr")
            } else {
                ch_hrs_id.setText(cell.hour + " hrs")
            }
            if (vehicle_type.equals("Personal")) {
                name.setText(cell.personal_price)

                ch_hrs_id.setOnClickListener {
                    addresspricesArray.clear()
                    dialog_list_price.dismiss()
                    vehicle_price_id = cell.id.toString()
                    vehicle_price = cell.personal_price.toString()
                    if (cell.hour.equals("1")) {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id!!.setText(cell.hour + " hr" + " & $ " + cell.personal_price)
                    } else {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id!!.setText(cell.hour + " hrs" + " & $ " + cell.personal_price)
                    }

                    val dateandtimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    val presentDateTime = dateandtimeFormat.format(Date())

                   /* Log.e("PRKINGENDDATEIF", parkig_end_date)
                    val date_time = dateandtimeFormat.parse(parkig_end_date)
                    val new_calendar = Calendar.getInstance()

                    new_calendar.setTime(date_time)
                    new_calendar.add(Calendar.HOUR, cell.hour!!.toInt())
                    System.out.println("Time here now" + dateandtimeFormat.format(new_calendar.getTime()))
                    final_end_time = dateandtimeFormat.format(new_calendar.getTime())*/

                }
            } else {
                name.setText(cell.commercial_price)
                ch_hrs_id.setOnClickListener {
                    addresspricesArray.clear()
                    dialog_list_price.dismiss()
                    vehicle_price_id = cell.id.toString()
                    vehicle_price = cell.commercial_price.toString()
                    if (cell.hour.equals("1")) {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id!!.setText(cell.hour + " hr" + " & $ " + cell.commercial_price)
                    } else {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id!!.setText(cell.hour + " hrs" + " & $ " + cell.commercial_price)
                    }

                    val dateandtimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    val presentDateTime = dateandtimeFormat.format(Date())

                   /* Log.e("PRKINGENDDATEELSE", parkig_end_date)
                    val date_time = dateandtimeFormat.parse(parkig_end_date)
                    val new_calendar = Calendar.getInstance()

                    new_calendar.setTime(date_time)
                    new_calendar.add(Calendar.HOUR, cell.hour!!.toInt())
                    System.out.println("Time here now" + dateandtimeFormat.format(new_calendar.getTime()))
                    final_end_time = dateandtimeFormat.format(new_calendar.getTime())*/
                }
            }



            return v
        }
    }

    inner class AddressPricesAdapter(context: Context, items: ArrayList<lotaddresspriceslist>) : ArrayAdapter<lotaddresspriceslist>(context, 0, items) {

        internal var inflater: LayoutInflater

        init {
            inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var v = convertView
            val cell = getItem(position)

            v = inflater.inflate(R.layout.prices_list_item, null)
            val name = v!!.findViewById(R.id.tv_prices_id) as TextView
            val ch_hrs_id = v!!.findViewById(R.id.ch_hrs_id) as TextView

            if (cell.hour.equals("1")) {
                ch_hrs_id.setText(cell.hour + " hr")
            } else {
                ch_hrs_id.setText(cell.hour + " hrs")
            }
            if (vehicle_type.equals("Personal")) {
                name.setText(cell.personal_price)

                ch_hrs_id.setOnClickListener {
                    priceslistArray.clear()
                    dialog_list_price.dismiss()
                    vehicle_price_id = cell.id.toString()
                    vehicle_price = cell.personal_price.toString()
                    if (cell.hour.equals("1")) {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id!!.setText(cell.hour + " hr" + " & $ " + cell.personal_price)
                    } else {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id!!.setText(cell.hour + " hrs" + " & $ " + cell.personal_price)
                    }

                    val dateandtimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    val presentDateTime = dateandtimeFormat.format(Date())

                  /*  val date_time = dateandtimeFormat.parse(parkig_end_date)
                    val new_calendar = Calendar.getInstance()

                    new_calendar.setTime(date_time)
                    new_calendar.add(Calendar.HOUR, cell.hour!!.toInt())
                    System.out.println("Time here now" + dateandtimeFormat.format(new_calendar.getTime()))
                    final_end_time = dateandtimeFormat.format(new_calendar.getTime())*/
                }
            } else {
                name.setText(cell.commercial_price)

                ch_hrs_id.setOnClickListener {
                    priceslistArray.clear()
                    dialog_list_price.dismiss()
                    vehicle_price_id = cell.id.toString()
                    vehicle_price = cell.commercial_price.toString()
                    if (cell.hour.equals("1")) {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id!!.setText(cell.hour + " hr" + " & $ " + cell.commercial_price)
                    } else {
                        vehicle_time_only = cell.hour!!
                        tv_time_and_price_id!!.setText(cell.hour + " hrs" + " & $ " + cell.commercial_price)
                    }

                    val dateandtimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    val presentDateTime = dateandtimeFormat.format(Date())

                   /* val date_time = dateandtimeFormat.parse(parkig_end_date)
                    val new_calendar = Calendar.getInstance()

                    new_calendar.setTime(date_time)
                    new_calendar.add(Calendar.HOUR, cell.hour!!.toInt())
                    System.out.println("Time here now" + dateandtimeFormat.format(new_calendar.getTime()))
                    final_end_time = dateandtimeFormat.format(new_calendar.getTime())*/
                }
            }



            return v
        }
    }


    private fun calltoExtendConfirmAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.extendParking(receipt_id = receipt_id, /*start_date = parkig_end_date, end_date = final_end_time,*/ hours = vehicle_time_only,amount = vehicle_price,id =user_id )
        Log.d("EXTENDCONFIRMAPI", receipt_id + "----" + parkig_end_date + "-----" + final_end_time + "-----" + vehicle_time_only)
        call.enqueue(object : Callback<extendParkingResponse> {
            override fun onResponse(call: Call<extendParkingResponse>, response: retrofit2.Response<extendParkingResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        Log.e("Result", response.body()!!.result.toString())
                        val list: extendParkingDataResponse? = response.body()!!.data!!
                        timer_final = 0
                        timer_final = list!!.remain_min!!.toInt()

                        if (!timer_final.equals(0)) {
                            countDownTimer!!.cancel()
                            setTimerValues()
                            setProgressBarValues()
                            startCountDownTimer()
                            Toast.makeText(this@TimerActivity,"Time added Successfully",Toast.LENGTH_SHORT).show()
                           /* val intent = Intent(this@TimerActivity, MainActivity::class.java)
                            startActivity(intent)*/
                        }

                    } else if (response.body()!!.status.equals("2")) {
                        my_loader.dismiss()
                        Toast.makeText(this@TimerActivity,response.body()!!.result.toString(),Toast.LENGTH_SHORT).show()
                    }else if(response.body()!!.status.equals("3")){

                    }
                }
            }

            override fun onFailure(call: Call<extendParkingResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
                my_loader.dismiss()
            }


        })
    }

    private fun calltoupdateParkingStatusAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.updateParkingStatus(receipt_id = receipt_id)
        Log.d("EXTENDCONFIRMAPI", receipt_id + "----" + parkig_end_date + "-----" + final_end_time + "-----" + vehicle_time_only)
        call.enqueue(object : Callback<getParkingDataResponse> {
            override fun onResponse(call: Call<getParkingDataResponse>, response: retrofit2.Response<getParkingDataResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        Toast.makeText(this@TimerActivity,"Your Parking has been Ended",Toast.LENGTH_SHORT).show()
                        val intent = Intent(this@TimerActivity, MainActivity::class.java)
                        startActivity(intent)
                        finish()
                        stop_Dialog.dismiss()
                    } else if (response.body()!!.status.equals("2")) {
                        Toast.makeText(this@TimerActivity,response.body()!!.result.toString(),Toast.LENGTH_SHORT).show()
                        my_loader.dismiss()
                        tv_ok_id.setText("Load Funds")
                        tv_message_id.setText("Insufficient funds")
                        stop_Dialog.show()
                    }
                }
            }

            override fun onFailure(call: Call<getParkingDataResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
                my_loader.dismiss()
            }


        })
    }

    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    fun Stopalert(){
        stop_Dialog = Dialog(this)
        stop_Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        stop_Dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        stop_Dialog.setCancelable(false);
        stop_Dialog.setContentView(R.layout.stop_dialog_alert)
        tv_message_id=stop_Dialog.findViewById<CustomTextView>(R.id.tv_message_id)
        tv_ok_id=stop_Dialog.findViewById(R.id.tv_ok_id)
        tv_cancel_id=stop_Dialog.findViewById(R.id.tv_cancel_id)
        tv_cancel_id.setOnClickListener {
            stop_Dialog.dismiss()
        }
        tv_ok_id.setOnClickListener {

            if(tv_ok_id.text.toString().equals("Load Funds")){
                val intent = Intent(this, FoundsLoad::class.java)
                intent.putExtra("total_main_balance",total_main_balance)
                startActivity(intent)
                stop_Dialog.dismiss()
            }else{
                calltoupdateParkingStatusAPI()
                stop_Dialog.dismiss()
            }


        }

    }


    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

}
