package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.BundleDataInfo
import com.gov.a4printuser.Helper.ConnectionManager
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.UserInfo
import com.gov.biil.model.apiresponses.UserProfileInfo
import com.gov.biil.model.apiresponses.UserProfileResponse
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback


class ProfileInfoActivity : AppCompatActivity() {

    public var mProfileData: ArrayList<UserInfo> = ArrayList<UserInfo>()
    internal lateinit var sessionManager: SessionManager

    internal lateinit var userid: CustomTextView
    internal lateinit var fullname: CustomTextView
    internal lateinit var dob: CustomTextView
    internal lateinit var gender: CustomTextView
    internal lateinit var ssn: CustomTextView
    internal lateinit var address: CustomTextView
    internal lateinit var city: CustomTextView
    internal lateinit var cityurl_tv: CustomTextView
    internal lateinit var country: CustomTextView
    internal lateinit var countryurl_tv: CustomTextView
    internal lateinit var tv_change_pwd: TextView
    internal lateinit var state: CustomTextView
    internal lateinit var stateurl_tv: CustomTextView
    internal lateinit var email_tv: CustomTextView
    internal lateinit var zipcode_tv: CustomTextView
    internal lateinit var cellphone: CustomTextView
    internal lateinit var citizen: RadioButton
    internal lateinit var bussiness: RadioButton
    internal lateinit var government: RadioButton
    internal lateinit var myRadioGroup: RadioGroup

    internal lateinit var organizaiton: CustomTextView
    internal lateinit var row_organizaiton: LinearLayout

    internal lateinit var edit_profile: Button

    internal lateinit var usertype: String
    internal lateinit var my_loader: Dialog
    internal lateinit var ssn_number: String
    internal lateinit var ssn_value: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        sessionManager = SessionManager(this)
        myloading()

        getAdminStatusInfo()

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        tv_change_pwd = findViewById(R.id.tv_change_pwd) as TextView
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        var rootview = findViewById(R.id.rootview) as LinearLayout

        userid= findViewById(R.id.userid) as CustomTextView
        fullname= findViewById(R.id.fullname) as CustomTextView
        dob= findViewById(R.id.dob) as CustomTextView
        gender= findViewById(R.id.gender) as CustomTextView
        ssn= findViewById(R.id.ssn) as CustomTextView
        address= findViewById(R.id.address) as CustomTextView
        city= findViewById(R.id.city_tv) as CustomTextView
        cityurl_tv= findViewById(R.id.cityurl_tv) as CustomTextView
        country= findViewById(R.id.country_tv) as CustomTextView
        countryurl_tv= findViewById(R.id.countryurl_tv) as CustomTextView
        state= findViewById(R.id.state_tv) as CustomTextView
        stateurl_tv= findViewById(R.id.stateurl_tv) as CustomTextView
        email_tv= findViewById(R.id.email_tv) as CustomTextView
        zipcode_tv= findViewById(R.id.zipcode_tv) as CustomTextView
        cellphone= findViewById(R.id.cellphone) as CustomTextView

        citizen= findViewById(R.id.citizen) as RadioButton
        bussiness= findViewById(R.id.bussiness) as RadioButton
        government= findViewById(R.id.government) as RadioButton
        myRadioGroup= findViewById(R.id.myRadioGroup) as RadioGroup

        organizaiton= findViewById(R.id.organization) as CustomTextView
        row_organizaiton= findViewById(R.id.row_organization) as LinearLayout

        edit_profile= findViewById(R.id.btn_edit_profile) as Button


        if(ConnectionManager.checkConnection(applicationContext)) {
            getProfileInfo()
        }else{
            ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
        }
        edit_profile.setOnClickListener(View.OnClickListener {
            val intent = Intent(this,ProfileInfoEditActivity::class.java)
            finish()
            putBundleData(intent)
            startActivity(intent)

        })

        tv_change_pwd.setOnClickListener(View.OnClickListener {
            val intent = Intent(this,ChangePasswordActivity::class.java)
            startActivity(intent)

        })

    }

    private fun putBundleData(intent: Intent) {
        if(ssn_number == null){ssn_number = ""}
        intent.putExtra(BundleDataInfo.PROFILE_USERID,userid.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_FULLNAME,fullname.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_DOB,dob.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_GENDER,gender.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_SSN,ssn_number)
        intent.putExtra(BundleDataInfo.PROFILE_ADDRESS,address.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_CITY,city.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_CITYURL,cityurl_tv.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_STATE,state.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_STATEURL,stateurl_tv.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_COUNTRY,country.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_COUNTRYURL,countryurl_tv.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_EMAIL,email_tv.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_ZIPCODE,zipcode_tv.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_EMAIL,email_tv.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_CELLPHONE,cellphone.text.toString())
        intent.putExtra(BundleDataInfo.PROFILE_USERTYPE,usertype)
        intent.putExtra(BundleDataInfo.PROFILE_ORGANIZATION,organizaiton.text.toString())

    }
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun getProfileInfo() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getUserProfile(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UserProfileResponse> {
            override fun onResponse(call: Call<UserProfileResponse>, response: retrofit2.Response<UserProfileResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                   // Log.w("Result_Address_Profile","Result : "+response.body()!!.user_info)
                    if(response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            Log.w("Result_Address_Profile","Result : "+response.body()!!.user_info)
                            setProfileDatatoView(response.body()!!.user_info)
                        }

                    }

                }
            }

            override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                Log.w("Result_Address_Profile",t.toString())
            }
        })
    }

    private fun setProfileDatatoView(user_info: UserProfileInfo?) {
        userid.setText(user_info!!.id)
        if(user_info.last_name.equals("null")){
            fullname.setText(user_info!!.first_name)
        }
        if(user_info!!.first_name.equals("null")){
            fullname.setText(user_info!!.last_name)
        }


        dob.setText(""+user_info!!.dob)
        gender.setText(user_info!!.gender)
        try{
            ssn_value = user_info!!.ssn.toString()
            ssn_number =  user_info!!.ssn.toString()
            /*if(ssn_value != null){
                ssn_value.substring(6)
            }*/
            ssn.setText("***-**-"+ssn_value)
        }catch (e:Exception){

        }
        address.setText(user_info!!.address)
        city.setText(user_info!!.city)
        cityurl_tv.setText(user_info!!.city_url)
        country.setText(user_info!!.county)
        countryurl_tv.setText(user_info!!.county_url)
        state.setText(user_info!!.state)
        stateurl_tv.setText(user_info!!.state_url)
        email_tv.setText(user_info!!.email)
        zipcode_tv.setText(""+user_info!!.zipcode)
        cellphone.setText(""+user_info!!.phone_number)
        usertype=""
        if(user_info!= null && user_info!!.user_type!=null){
            if(user_info!!.user_type.equals("Citizen")){
                citizen.isChecked=true
                citizen.isEnabled=false
                bussiness.isEnabled=false
                government.isEnabled=false
                usertype = user_info!!.user_type.toString()
                row_organizaiton.visibility = View.GONE

            }else if(user_info!!.user_type.equals("Business")){
                citizen.isEnabled=false
                bussiness.isEnabled=false
                government.isEnabled=false
                usertype = user_info!!.user_type.toString()
                bussiness.isChecked=true
                row_organizaiton.visibility = View.VISIBLE
                organizaiton.setText(user_info!!.organization)

            }else if(user_info!!.user_type.equals("Government")){
                citizen.isEnabled=false
                bussiness.isEnabled=false
                government.isEnabled=false
                usertype = user_info!!.user_type.toString()
                government.isChecked=true

                row_organizaiton.visibility = View.VISIBLE
                organizaiton.setText(user_info!!.organization)
            }else{
                usertype=""
                citizen.isEnabled=false
                bussiness.isEnabled=false
                government.isEnabled=false
                row_organizaiton.visibility = View.GONE
            }
        }
    }


    override fun onBackPressed() {
        super.onBackPressed()
    }


    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }



}
