package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class AllServicesByUserInfo {
     val id: String? = null

     val service_link: String? = null

     val city_name: String? = null

     val zip_code: String? = null

     val county: String? = null

     val address: String? = null

     val service_type: String? = null

     val state: String? = null

     val apt_unit_no: String? = null

     val due_date: String? = null

     val user_id: String? = null

     val remider_date: String? = null

     val city: String? = null

     val reminder_date_one: String? = null

     val reminder_date_two: String? = null





     val title: String? = null
     val display_type: String? = null
     val category_id: String? = null
     val title_link: String? = null
     val news: String? = null
     val image: String? = null
     val posted_by: String? = null
     val posted_by_image: String? = null
     val author_profession: String? = null
     val author_link: String? = null
     val pay_link: String? = null
     val status: String? = null
     val sort_id: String? = null
     val created_date: String? = null
     val name: String? = null
     val created_at: String? = null
     val likes_count: String? = null
     var like_status: String? = null






}
