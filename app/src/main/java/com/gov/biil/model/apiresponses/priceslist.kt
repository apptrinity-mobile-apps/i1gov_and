package com.gov.biil.model.apiresponses

/**
 * Created by indo67 on 7/7/2018.
 */
class priceslist {

    var id:String?=null
    var zone_id:String?=null
    var personal_price:String?=null
    var commercial_price:String?=null
    var hour_id:String?=null
    var zone_number:String?=null
    var no_vehicles:String?=null
    var hour:String?=null
}