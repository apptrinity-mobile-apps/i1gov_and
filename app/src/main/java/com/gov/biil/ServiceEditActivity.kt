package com.gov.biil

import Helper.CustomTextView
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.BundleDataInfo
import com.gov.a4printuser.Helper.ConnectionManager
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.AllServicesByUserInfo
import com.gov.biil.model.apiresponses.AllServicesByUserResponse
import com.gov.biil.model.apiresponses.ServiceResponse
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*

class ServiceEditActivity : AppCompatActivity() {

    public var mServiceInfo: ArrayList<AllServicesByUserInfo> = ArrayList<AllServicesByUserInfo>()

    internal lateinit var ss_id:EditText
    internal lateinit var sd_duedate:EditText
   // internal lateinit var sd_reminderdate:EditText
    internal lateinit var sd_city:EditText
    internal lateinit var sd_service_name:EditText
    internal lateinit var sd_service_unitno:EditText
    internal lateinit var sd_servicelink:EditText
    internal lateinit var sd_address:EditText
    internal lateinit var sd_zipcode:EditText
    internal lateinit var sd_state:EditText

    internal lateinit var id:String

    internal lateinit var rd_city: RadioButton
    internal lateinit var rd_country: RadioButton
    internal lateinit var rd_state: RadioButton
    internal lateinit var rd_federal: RadioButton

    internal lateinit var sd_service_update: CustomTextView
    var cal = Calendar.getInstance()
    internal lateinit var serviceType_st:String

    var dialog_list: Dialog? = null
    var list_items_list: ListView? = null

    internal lateinit var btn_reminder_plus: ImageView
    internal lateinit var btn_reminder_minus2: ImageView
    internal lateinit var btn_reminder_minus3: ImageView

    internal lateinit var et_reminderdate1: EditText
    internal lateinit var et_reminderdate2: EditText
    internal lateinit var et_reminderdate3: EditText

    internal lateinit var ll_reminderdate1: LinearLayout
    internal lateinit var ll_reminderdate2: LinearLayout
    internal lateinit var ll_reminderdate3: LinearLayout
    internal lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_view)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        dialog_list = Dialog(this)
        dialog_list!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list!!.setContentView(R.layout.dialog_view)
        dialog_list!!.setCancelable(true)

        list_items_list = dialog_list!!.findViewById(R.id.list_languages)
        myloading()
        sessionManager = SessionManager(this)
        getAdminStatusInfo()

        ss_id = findViewById(R.id.sd_id) as EditText
        sd_duedate = findViewById(R.id.sd_duedate) as EditText
       // sd_reminderdate = findViewById(R.id.sd_reminderdate) as EditText
        sd_city = findViewById(R.id.sd_city) as EditText
        sd_service_name = findViewById(R.id.sd_service_name) as EditText
        sd_service_unitno = findViewById(R.id.sd_service_unitno) as EditText
        sd_servicelink = findViewById(R.id.sd_servicelink) as EditText
        sd_address = findViewById(R.id.sd_address) as EditText
        sd_zipcode = findViewById(R.id.sd_zipcode) as EditText
        sd_state = findViewById(R.id.sd_state) as EditText


        et_reminderdate1 = findViewById(R.id.et_reminderdate1) as EditText
        et_reminderdate2 = findViewById(R.id.et_reminderdate2) as EditText
        et_reminderdate3 = findViewById(R.id.et_reminderdate3) as EditText

        ll_reminderdate1 = findViewById(R.id.ll_reminderdate1) as LinearLayout
        ll_reminderdate2 = findViewById(R.id.ll_reminderdate2) as LinearLayout
        ll_reminderdate3 = findViewById(R.id.ll_reminderdate3) as LinearLayout

        btn_reminder_plus= findViewById(R.id.btn_reminder_plus) as ImageView
        btn_reminder_minus2= findViewById(R.id.btn_reminder_minus2) as ImageView
        btn_reminder_minus3= findViewById(R.id.btn_reminder_minus3) as ImageView


        sd_service_update = findViewById(R.id.service_edit_update) as CustomTextView

        val rootview = findViewById(R.id.rootview) as LinearLayout

        rd_city= findViewById(R.id.City) as RadioButton
        rd_country= findViewById(R.id.Country) as RadioButton
        rd_state= findViewById(R.id.State) as RadioButton
        rd_federal= findViewById(R.id.Federal) as RadioButton

        if(intent != null && intent.getStringExtra(BundleDataInfo.SERVICE_VIEW_ID)!= null){
            id = intent.getStringExtra(BundleDataInfo.SERVICE_VIEW_ID)
            if (intent.getStringExtra(BundleDataInfo.SERVICE_VIEW_TYPE)!= null) {
                serviceType_st = intent.getStringExtra(BundleDataInfo.SERVICE_VIEW_TYPE)
                if(serviceType_st.equals("City")){
                    rd_city.isChecked=true
                    rd_country.isChecked=false
                    rd_state.isChecked=false
                    rd_federal.isChecked=false
                }else if(serviceType_st.equals("County")){
                    rd_city.isChecked=false
                    rd_country.isChecked=true
                    rd_state.isChecked=false
                    rd_federal.isChecked=false
                }else if(serviceType_st.equals("State")){
                    rd_city.isChecked=false
                    rd_country.isChecked=false
                    rd_state.isChecked=true
                    rd_federal.isChecked=false
                }else if(serviceType_st.equals("Federal")){
                    rd_city.isChecked=false
                    rd_country.isChecked=false
                    rd_state.isChecked=false
                    rd_federal.isChecked=true
                }
            }else{
                serviceType_st = ""
            }

            if(ConnectionManager.checkConnection(applicationContext)) {
                callServiceViewAPI()
            }else{
                ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
            }

        }
        sd_service_update.setOnClickListener(View.OnClickListener {
            if(hasValidCredentials()){
                if(ConnectionManager.checkConnection(applicationContext)) {
                    callUpdateServiceAPI()
                }else{
                    ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
                }
            }
        })
        rd_city.setOnClickListener(View.OnClickListener {
            sd_city.setHint("City Name")
            sd_address.setHint("Address")
            serviceType_st=rd_city.text.toString()
        })
        rd_country.setOnClickListener(View.OnClickListener {
            sd_city.setHint("County Name")
            sd_address.setHint("Address")
            serviceType_st=rd_country.text.toString()
        })
        rd_state.setOnClickListener(View.OnClickListener {
            sd_city.setHint("State Name")
            sd_address.setHint("Address")
            serviceType_st=rd_state.text.toString()
        })
        rd_federal.setOnClickListener(View.OnClickListener {
            sd_city.setHint("Department Name")
            sd_address.setHint("Department Address")
            serviceType_st=rd_federal.text.toString()
        })
        sd_duedate.setOnClickListener {
            /*DatePickerDialog(this@ServiceEditActivity, duedatelistener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()*/

            val datePickerDialog = DatePickerDialog(this, duedatelistener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));
            // Limiting access to past dates in the step below:
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show()
        }
        et_reminderdate1.setOnClickListener {
            /*DatePickerDialog(this@ServiceEditActivity, reminderdatelistener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()*/

            val datePickerDialog = DatePickerDialog(this, reminderdatelistener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));
            // Limiting access to past dates in the step below:
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show()
        }

        et_reminderdate2.setOnClickListener {
           /* DatePickerDialog(this@ServiceEditActivity, reminderdatelistener2,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()*/

            val datePickerDialog = DatePickerDialog(this, reminderdatelistener2,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));
            // Limiting access to past dates in the step below:
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show()
        }

        et_reminderdate3.setOnClickListener {
            /*DatePickerDialog(this@ServiceEditActivity, reminderdatelistener3,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()*/

            val datePickerDialog = DatePickerDialog(this, reminderdatelistener3,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));
            // Limiting access to past dates in the step below:
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show()
        }


        btn_reminder_plus.setOnClickListener(View.OnClickListener {view->

            if(et_reminderdate1.text.toString().equals("")){
                Toast.makeText(applicationContext,"Please set Reminder date",Toast.LENGTH_SHORT).show()
            }else{
                ll_reminderdate2.visibility= View.VISIBLE
                if(et_reminderdate2.text.toString().equals("")){
                    Toast.makeText(applicationContext,"Please set Reminder date",Toast.LENGTH_SHORT).show()
                }else{
                    ll_reminderdate3.visibility= View.VISIBLE
                }
            }


        })

        btn_reminder_minus2.setOnClickListener(View.OnClickListener {view->
            ll_reminderdate2.visibility = View.GONE
            et_reminderdate2.text.clear()

        })
        btn_reminder_minus3.setOnClickListener(View.OnClickListener {view->

            ll_reminderdate3.visibility = View.GONE
            et_reminderdate3.text.clear()


        })



        sd_state.setOnClickListener(View.OnClickListener {
            if (!dialog_list!!.isShowing)
                dialog_list!!.show()
            val profile_array_adapter = ArrayAdapter<String>(this, R.layout.simple_spinner_item, BundleDataInfo.STATES_LIST)
            list_items_list!!.adapter = profile_array_adapter
            list_items_list!!.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_state = list_items_list!!.getItemAtPosition(i).toString()
                sd_state.setText(selected_state)
                dialog_list!!.dismiss()



            }
        })



    }

    private fun callServiceViewAPI() {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.getServicesById(id)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<AllServicesByUserResponse> {
            override fun onResponse(call: Call<AllServicesByUserResponse>, response: retrofit2.Response<AllServicesByUserResponse>?) {
                my_loader.dismiss()
                Log.w("Status","*** "+response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if(response.body()!!.service_info!= null) {
                        val list: Array<AllServicesByUserInfo> = response.body()!!.service_info!!

                        for (item: AllServicesByUserInfo in list.iterator()) {
                            mServiceInfo.add(item)
                            setServicetoView(mServiceInfo)
                        }
                    }

                }
            }

            override fun onFailure(call: Call<AllServicesByUserResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }
    private fun setServicetoView(mServiceInfo: ArrayList<AllServicesByUserInfo>) {
        ss_id.setText(mServiceInfo.get(0).id)
        sd_duedate.setText(mServiceInfo.get(0).due_date)
        et_reminderdate1.setText(mServiceInfo.get(0).remider_date)
        et_reminderdate2.setText(mServiceInfo.get(0).reminder_date_one)
        et_reminderdate3.setText(mServiceInfo.get(0).reminder_date_two)
        sd_service_name.setText(mServiceInfo.get(0).city_name)
        sd_service_unitno.setText(mServiceInfo.get(0).apt_unit_no)
        sd_servicelink.setText(mServiceInfo.get(0).service_link)
        sd_address.setText(mServiceInfo.get(0).address)
        sd_zipcode.setText(mServiceInfo.get(0).zip_code)
        sd_city.setText(mServiceInfo.get(0).city)
        sd_state.setText(mServiceInfo.get(0).state)

    }
    private fun hasValidCredentials(): Boolean {

        if (TextUtils.isEmpty(sd_service_name.text.toString()))
            sd_service_name.setError("City name Required")
        else if (TextUtils.isEmpty(sd_address.text.toString()))
            sd_address!!.setError("Address Required")
        else if (TextUtils.isEmpty(sd_service_unitno.text.toString()))
            sd_service_unitno!!.setError("Unit number Password")
        else if (TextUtils.isEmpty(sd_service_name.text.toString()))
            sd_service_name!!.setError("Name Required")
        else if (TextUtils.isEmpty(sd_zipcode.text.toString()))
            sd_zipcode!!.setError("zipcode Required")
        else if (TextUtils.isEmpty(sd_servicelink.text.toString()))
            sd_servicelink!!.setError("Service link Required")
        else if (TextUtils.isEmpty(sd_duedate.text.toString()))
            sd_duedate!!.setError("Due date Required")
        else if (TextUtils.isEmpty(et_reminderdate1.text.toString()))
            et_reminderdate1!!.setError("Reminder Required")
        else if (TextUtils.isEmpty(sd_city.text.toString()))
            sd_city!!.setError("Due date Required")
        else if (TextUtils.isEmpty(sd_state.text.toString()))
            sd_state!!.setError("Reminder Required")
        else
            return true
        return false
    }
    private fun callUpdateServiceAPI() {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.serviceUpdate(sd_address.text.toString(),sd_service_unitno.text.toString(),id,sd_city.text.toString(),sd_state.text.toString(),sd_zipcode.text.toString(),
                sd_servicelink.text.toString()
                ,sd_duedate.text.toString(),et_reminderdate1.text.toString(),et_reminderdate2.text.toString(),et_reminderdate3.text.toString(),sd_city.text.toString(),serviceType_st)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<ServiceResponse> {
            override fun onResponse(call: Call<ServiceResponse>, response: retrofit2.Response<ServiceResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if(response.body()!!.status.equals("1")){
                        Toast.makeText(applicationContext,"Service Updated Successfully", Toast.LENGTH_SHORT).show()
                        val intent = Intent(this@ServiceEditActivity,MainActivity::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)

                    }

                }
            }

            override fun onFailure(call: Call<ServiceResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })
    }
    val duedatelistener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        var dateofmonth = dayOfMonth.toString()
        var monthofyear = monthOfYear
        var yearstg = year.toString()
        //getAge(year, monthOfYear, dayOfMonth)
        val myFormat = "MM/dd/yyyy" // mention the format you need

        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        Log.e("dateformat", sdf.toString())
        val date = sdf.format(cal.time)

        sd_duedate.setText(date)

    }
    val reminderdatelistener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        var dateofmonth = dayOfMonth.toString()
        var monthofyear = monthOfYear
        var yearstg = year.toString()
        //getAge(year, monthOfYear, dayOfMonth)
        val myFormat = "MM/dd/yyyy" // mention the format you need

        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        Log.e("dateformat", sdf.toString())
        val date = sdf.format(cal.time)

        et_reminderdate1.setText(date)

    }

    val reminderdatelistener2 = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

        //  monthOfYear.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        var dateofmonth = dayOfMonth.toString()
        var monthofyear = monthOfYear
        var yearstg = year.toString()
        //getAge(year, monthOfYear, dayOfMonth)
        val myFormat = "MM/dd/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        Log.e("dateformat", sdf.toString())
        val date = sdf.format(cal.time)

        et_reminderdate2.setText(date)

    }

    val reminderdatelistener3 = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

        //  monthOfYear.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        var dateofmonth = dayOfMonth.toString()
        var monthofyear = monthOfYear
        var yearstg = year.toString()
        //getAge(year, monthOfYear, dayOfMonth)
        val myFormat = "MM/dd/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        Log.e("dateformat", sdf.toString())
        val date = sdf.format(cal.time)

        et_reminderdate3.setText(date)

    }


    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

}
