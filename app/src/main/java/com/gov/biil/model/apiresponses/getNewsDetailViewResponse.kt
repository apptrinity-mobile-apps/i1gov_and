package com.gov.biil.model.apiresponses

class getNewsDetailViewResponse {

    val status: String? = null

    val result: String? = null

    val newsdetailview: newsDetailViewDataResponse? = null

}
