package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.BaseExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.RelativeLayout
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.CustomTextViewBold
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.VotingListArrayDataResponse
import com.gov.biil.model.apiresponses.VotingListDataResponse
import com.gov.biil.model.apiresponses.VotingListResponse
import com.gov.biil.model.apiresponses.VotingSubListArrayDataResponse
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback
import java.util.HashMap
import kotlin.collections.ArrayList

class IVoteForActivity : AppCompatActivity() {


    private var ivf_expandlistview: ExpandableListView? = null
    private var lastExpandedPosition = -1
    lateinit var listAdapter: ExpListViewAdapterWithCheckbox
    lateinit var listDataHeader: ArrayList<String>
    lateinit var listDataHeader1: ArrayList<String>
    lateinit var listDataChild: HashMap<String, Array<VotingSubListArrayDataResponse>>
    lateinit var list: List<VotingListArrayDataResponse>
    internal lateinit var sessionManager: SessionManager
    internal lateinit var my_loader: Dialog

    lateinit var list_votes: HashMap<String,Array<String>>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ivote_for)


        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

myloading()
        sessionManager = SessionManager(this!!)

        getAdminStatusInfo()

        callVoteForListingAPI()

        ivf_expandlistview = findViewById(R.id.ivf_expandlistview) as ExpandableListView


        ivf_expandlistview!!.setOnGroupClickListener { parent, v, groupPosition, id ->


            false
        }

        // Listview Group expanded listener
        ivf_expandlistview!!.setOnGroupExpandListener { groupPosition ->


            if (lastExpandedPosition != -1
                    && groupPosition != lastExpandedPosition) {
                ivf_expandlistview!!.collapseGroup(lastExpandedPosition);
            }
            lastExpandedPosition = groupPosition;


        }

        // Listview Group collasped listener
        ivf_expandlistview!!.setOnGroupCollapseListener { groupPosition ->



        }

        // Listview on child click listener
        ivf_expandlistview!!.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
            // TODO Auto-generated method stub


            false
        }

    }


    private fun callVoteForListingAPI() {

        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.votingList(sessionManager.isId)
        Log.d("VoteForListingAPIREQUEST", sessionManager.isId)
        call.enqueue(object : Callback<VotingListResponse> {
            override fun onResponse(call: Call<VotingListResponse>, response: retrofit2.Response<VotingListResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_VoteForListingAPI", response.body().toString())
                    if (response.body()!!.status!!.equals("1") && response.body()!!.data != null) {
                        listDataHeader = ArrayList()
                        listDataHeader1 = ArrayList()
                        listDataChild = HashMap()
                        list_votes = HashMap()
                        val list: VotingListDataResponse? = response.body()!!.data!!
                        val voting_list_array: Array<VotingListArrayDataResponse>? = list!!.list

                        if (voting_list_array!!.size > 0) {
                            for (j in 0 until voting_list_array.size) {

                                listDataHeader1.add(voting_list_array.get(j).category_name.toString())
                                listDataHeader.add(voting_list_array.get(j).id.toString())

                                val sublist:Array<VotingSubListArrayDataResponse> = voting_list_array.get(j).sub!!
                              /* val top = ArrayList<VotingSubListArrayDataResponse>()
                                top.add(sublist)
                                Log.w("VoteForListingAPI_ARRAY", voting_list_array.get(j).toString()+""+sublist!!.size)*/

                                listDataChild.put(listDataHeader[j], sublist)
                            }
                            Log.e("stdferer", "" + listDataChild.size)
                            listAdapter = ExpListViewAdapterWithCheckbox(this@IVoteForActivity!!, listDataHeader, listDataChild)

                            // setting list adapter
                            ivf_expandlistview!!.setAdapter(listAdapter)
                            listAdapter.notifyDataSetChanged()
                        }

                        if (voting_list_array.size == 0) {
                            // no_service!!.text = "No Data Found"
                            // no_service!!.visibility = View.VISIBLE
                            my_loader.dismiss()
                        }

                    } else if (response.body()!!.status.equals("2")) {
                        // no_service!!.text = "No Data Found"
                        // no_service!!.visibility = View.VISIBLE
                        my_loader.dismiss()
                    }

                }
            }

            override fun onFailure(call: Call<VotingListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
                my_loader.dismiss()
            }
        })
    }


    private var childViewHolder: ExpListViewAdapterWithCheckbox.ChildViewHolder? = null

    inner class ExpListViewAdapterWithCheckbox/*  Here's the constructor we'll use to pass in our calling
         *  activity's context, group items, and child items
        */
    (// Define activity context
            private val mContext: Context, // ArrayList that is what each key in the above
            // hashmap points to
            private val mListDataGroup: ArrayList<String>,
            private val mListDataChild: HashMap<String, Array<VotingSubListArrayDataResponse>>) : BaseExpandableListAdapter() {

        // Hashmap for keeping track of our checkbox check states
        private val mChildCheckStates: HashMap<Int, BooleanArray>
        private val mGroupCheckStates: HashMap<Int, BooleanArray>

        private var groupViewHolder: GroupViewHolder? = null

        private var groupText: String? = null
        private var childText: String? = null
        private var str_width_height: String? = null

        init {

            // Initialize our hashmap containing our check states here
            mChildCheckStates = HashMap()
            mGroupCheckStates = HashMap()
        }

        fun getNumberOfCheckedItemsInGroup(mGroupPosition: Int): Int {
            val getChecked = mChildCheckStates[mGroupPosition]
            var count = 0
            if (getChecked != null) {

                for (j in getChecked.indices) {
                    if (getChecked[j] == true) count++
                }
            }
            return count
        }

        fun getNumberOfGroupCheckedItems(mGroupPosition: Int): Int {
            val getChecked = mGroupCheckStates[mGroupPosition]
            var count = 0
            if (getChecked != null) {
                for (j in getChecked.indices) {
                    if (getChecked[j] == true) count++
                }
            }
            return count
        }

        override fun getGroupCount(): Int {
            return mListDataGroup.size
        }


        override fun getGroup(groupPosition: Int): String {
            return mListDataGroup[groupPosition]
        }

        override fun getGroupId(groupPosition: Int): Long {
            return groupPosition.toLong()
        }


        override fun getGroupView(groupPosition: Int, isExpanded: Boolean,
                                  convertView: View?, parent: ViewGroup): View {
            var convertView = convertView


            //  I passed a text string into an activity holding a getter/setter
            //  which I passed in through "ExpListGroupItems".
            //  Here is where I call the getter to get that text
            groupText = getGroup(groupPosition)

            if (convertView == null) {

                val inflater = mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.list_item_ivoteforheader, null)

                // Initialize the GroupViewHolder defined at the bottom of this document

                groupViewHolder = GroupViewHolder()

                groupViewHolder!!.tv_headername = convertView!!.findViewById(R.id.tv_headername) as CustomTextViewBold
                groupViewHolder!!.tv_headername!!.setSelected(isExpanded)


                /*if (isExpanded) {
                    groupViewHolder!!.img_layout!!.setImageResource(R.drawable.down_arrow)
                } else {
                    groupViewHolder!!.img_layout!!.setImageResource(R.drawable.right_arrow)
                }*/


                convertView.tag = groupViewHolder
            } else {

                groupViewHolder = convertView.tag as GroupViewHolder
            }

            //groupViewHolder!!.mGroupText!!.text = groupText
            groupViewHolder!!.tv_headername!!.text = listDataHeader1.get(groupPosition)



            return convertView
        }

        override fun getChildrenCount(groupPosition: Int): Int {
            return mListDataChild[mListDataGroup[groupPosition]]!!.size
        }

        /*
         * This defaults to "public object getChild" if you auto import the methods
         * I've always make a point to change it from "object" to whatever item
         * I passed through the constructor
        */
        override fun getChild(groupPosition: Int, childPosition: Int): VotingSubListArrayDataResponse {
            return mListDataChild[mListDataGroup[groupPosition]]!!.get(childPosition)
        }

        override fun getChildId(groupPosition: Int, childPosition: Int): Long {
            return childPosition.toLong()
        }

        override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView


            if (convertView == null) {

                val inflater = this.mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.list_item_ivoteforchilditem, null)

                childViewHolder = ChildViewHolder()
                childViewHolder!!.rl_ivotefor_childitem = convertView!!.findViewById(R.id.rl_ivotefor_childitem)
                childViewHolder!!.tv_listchilditem = convertView!!.findViewById(R.id.tv_listchilditem)


               // val sublist = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).id!!
                //Log.d("SUBLIST", ""+listDataHeader.get(groupPosition))
                Log.d("SUBLIST", ""+listDataChild.get(listDataHeader.get(groupPosition))!!.size)

                for (j in 0 until listDataChild.get(listDataHeader.get(groupPosition))!!.size) {
                    childViewHolder!!.tv_listchilditem!!.text = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).sub_category_name

                    Log.d("SUBNAME", ""+listDataChild.get(listDataHeader.get(groupPosition))!!.get(j).sub_category_name)


                    /*val is_Vote_status =  listDataChild.get(listDataHeader.get(groupPosition))!!.get(j)!!.is_vote
                    Log.d("VOTESTATUSFOR", ""+is_Vote_status)*/



                }


                childViewHolder!!.rl_ivotefor_childitem!!.setOnClickListener {

                    val is_Vote_status = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).is_vote
                    Log.d("VOTESTATUS", "" + is_Vote_status)

                    /* if(is_Vote_status.equals("1")){
                         val category_id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id
                         val id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).id
                         val intent = Intent(this@IVoteForActivity, VoteforProgress::class.java)
                         intent.putExtra("sub_category_id",id)
                         intent.putExtra("category_id",category_id)
                         startActivity(intent)

                     }else if(is_Vote_status.equals("0")){*/

                    // listDataChild.get(listDataHeader.get(childPosition).get(groupPosition).)
                    Log.d("GROUPPOSITION", "" + mListDataGroup.size)

                    if(listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id!!.equals("2")){

                        val category_id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id
                        val sub_category_name = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).sub_category_name
                        val id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).id
                        val intent = Intent(this@IVoteForActivity, VoteforCitySearch::class.java)
                        intent.putExtra("sub_category_id",id)
                        intent.putExtra("category_id",category_id)
                        intent.putExtra("sub_category_name",sub_category_name)
                        intent.putExtra("statelist","statelist")
                        intent.putExtra("citylist","")
                        startActivity(intent)
                    }else if(listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id!!.equals("3")) {


                        val category_id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id
                        val id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).id
                        val sub_category_name = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).sub_category_name
                        val intent = Intent(this@IVoteForActivity, VoteforCitySearch::class.java)
                        intent.putExtra("sub_category_id", id)
                        intent.putExtra("category_id", category_id)
                        intent.putExtra("sub_category_name",sub_category_name)
                        intent.putExtra("citylist","citylist")
                        intent.putExtra("statelist","")
                        startActivity(intent)

                    }else if(listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id!!.equals("1")){

                        val category_id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id
                        val id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).id
                        val sub_category_name = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).sub_category_name
                        val intent = Intent(this@IVoteForActivity, VoteforCitySearch::class.java)
                        intent.putExtra("sub_category_id", id)
                        intent.putExtra("category_id", category_id)
                        intent.putExtra("citylist","")
                        intent.putExtra("statelist","")
                        intent.putExtra("sub_category_name",sub_category_name)
                        intent.putExtra("usa","usa")
                        startActivity(intent)

                    }else if(listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id!!.equals("4")){

                        val category_id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id
                        val id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).id
                        val sub_category_name = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).sub_category_name
                        val intent = Intent(this@IVoteForActivity, VoteforCitySearch::class.java)
                        intent.putExtra("sub_category_id", id)
                        intent.putExtra("category_id", category_id)
                        intent.putExtra("citylist","")
                        intent.putExtra("statelist","")
                        intent.putExtra("sub_category_name",sub_category_name)
                        intent.putExtra("county","county")
                        startActivity(intent)

                    }

                    //}



                }



                convertView.setTag(R.layout.list_item_ivoteforchilditem, childViewHolder)

            } else {

                childViewHolder = convertView
                        .getTag(R.layout.list_item_ivoteforchilditem) as ChildViewHolder


                childViewHolder = ChildViewHolder()
                childViewHolder!!.tv_listchilditem = convertView!!.findViewById(R.id.tv_listchilditem)
                childViewHolder!!.rl_ivotefor_childitem = convertView!!.findViewById(R.id.rl_ivotefor_childitem)

                Log.d("SUBLIST", ""+listDataChild.get(listDataHeader.get(groupPosition))!!.size)

                for (j in 0 until listDataChild.get(listDataHeader.get(groupPosition))!!.size) {
                    childViewHolder!!.tv_listchilditem!!.text = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).sub_category_name

                    Log.d("SUBNAME", ""+listDataChild.get(listDataHeader.get(groupPosition))!!.get(j).sub_category_name)

                }


                childViewHolder!!.rl_ivotefor_childitem!!.setOnClickListener(View.OnClickListener {

                    val is_Vote_status = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).is_vote
                    Log.d("VOTESTATUS", "" + is_Vote_status)

                   /* if(is_Vote_status.equals("1")){
                        val category_id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id
                        val id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).id
                        val intent = Intent(this@IVoteForActivity, VoteforProgress::class.java)
                        intent.putExtra("sub_category_id",id)
                        intent.putExtra("category_id",category_id)
                        startActivity(intent)

                    }else if(is_Vote_status.equals("0")){*/

                       // listDataChild.get(listDataHeader.get(childPosition).get(groupPosition).)
                        Log.d("GROUPPOSITION", "" + mListDataGroup.size)

                        if(listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id.equals("2")){

                            val category_id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id
                            val id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).id
                            val intent = Intent(this@IVoteForActivity, VoteforCitySearch::class.java)
                            intent.putExtra("sub_category_id",id)
                            intent.putExtra("category_id",category_id)
                            intent.putExtra("statelist","statelist")
                            intent.putExtra("citylist","")
                            startActivity(intent)
                        }else if(listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id.equals("3")) {


                            val category_id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id
                            val id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).id
                            val intent = Intent(this@IVoteForActivity, VoteforCitySearch::class.java)
                            intent.putExtra("sub_category_id", id)
                            intent.putExtra("category_id", category_id)
                            intent.putExtra("citylist","citylist")
                            intent.putExtra("statelist","")
                            startActivity(intent)

                        }else if(listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id.equals("1")){

                            val category_id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id
                            val id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).id
                            val intent = Intent(this@IVoteForActivity, VoteforCitySearch::class.java)
                            intent.putExtra("sub_category_id", id)
                            intent.putExtra("category_id", category_id)
                            intent.putExtra("citylist","")
                            intent.putExtra("statelist","")
                            intent.putExtra("usa","usa")
                            startActivity(intent)

                        }else if(listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id.equals("4")){

                            val category_id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).category_id
                            val id = listDataChild.get(listDataHeader.get(groupPosition))!!.get(childPosition).id
                            val intent = Intent(this@IVoteForActivity, VoteforCitySearch::class.java)
                            intent.putExtra("sub_category_id", id)
                            intent.putExtra("category_id", category_id)
                            intent.putExtra("citylist","")
                            intent.putExtra("statelist","")
                            intent.putExtra("county","county")
                            startActivity(intent)

                        }

                    //}

                })


            }


            return convertView
        }

        override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
            return false
        }

        override fun hasStableIds(): Boolean {
            return false
        }

        inner class GroupViewHolder {

            internal var tv_headername: CustomTextViewBold? = null

        }

        inner class ChildViewHolder {

            internal var tv_listchilditem: CustomTextView? = null
            internal var rl_ivotefor_childitem: RelativeLayout? = null

        }


    }


    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


    override fun onBackPressed() {

            val intent = Intent(this@IVoteForActivity, MainActivity::class.java)
            startActivity(intent)

        }


    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


}
