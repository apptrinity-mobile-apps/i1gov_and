package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.Window
import android.widget.EditText
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.getNewsListResponse
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback

class ContactUsActivity : AppCompatActivity() {


    internal lateinit var sessionManager: SessionManager
    internal lateinit var et_user_name: EditText
    internal lateinit var et_email: EditText
    internal lateinit var et_comments: EditText
    internal lateinit var tv_submit: CustomTextView
    internal lateinit var my_loader: Dialog

    internal lateinit var user_name_stg: String
    internal lateinit var email_stg: String
    internal lateinit var comments_stg: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)


        et_user_name = findViewById(R.id.et_user_name) as EditText
        et_email = findViewById(R.id.et_email) as EditText
        et_comments = findViewById(R.id.et_comments) as EditText
        tv_submit = findViewById(R.id.tv_submit) as CustomTextView


        val toolbar = findViewById(R.id.toolbar) as Toolbar

        myloading()

        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }


        sessionManager = SessionManager(this)
        if (sessionManager.isLoggedIn) {
            et_user_name.setText(sessionManager.isUserName)
            et_email.setText(sessionManager.isEmail)
            getAdminStatusInfo()

        }else{
            /*val home_intent = Intent(this@ContactUsActivity, ContactUsActivity::class.java)
            finish()
            startActivity(home_intent)*/
        }

        tv_submit.setOnClickListener {

            if (sessionManager.isLoggedIn) {
                et_user_name.setText(sessionManager.isUserName)
                et_email.setText(sessionManager.isEmail)
                comments_stg = et_comments.getText().toString()
                if(hasValidCredentials2()){
                    callContactCommentsAPI(sessionManager.isUserName,sessionManager.isEmail,comments_stg)
                }


            }else {
                user_name_stg = et_user_name.getText().toString()
                email_stg = et_email.getText().toString()
                comments_stg = et_comments.getText().toString()

                if (hasValidCredentials()) {
                    callContactCommentsAPI(user_name_stg,email_stg,comments_stg)
                }



            }


        }


    }



    private fun callContactCommentsAPI(name: String, email: String, comments: String) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.postContactComment(name, email,comments)
        Log.d("ContactComments", name + "------" + email+"------"+comments)
        call.enqueue(object : Callback<getNewsListResponse> {
            override fun onResponse(call: Call<getNewsListResponse>, response: retrofit2.Response<getNewsListResponse>?) {
                if (response != null && response.body()!!.status!!.equals("1")) {
                    my_loader.dismiss()
                    Toast.makeText(this@ContactUsActivity, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()

                }else{
                    Toast.makeText(this@ContactUsActivity, response!!.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<getNewsListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }




    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(user_name_stg))
            et_user_name.setError("Name Required")
        else if (TextUtils.isEmpty(email_stg))
            et_email.setError("Email Required")
        else if (TextUtils.isEmpty(comments_stg))
            et_comments.setError("Comments Required")
        else
            return true
        return false
    }

    private fun hasValidCredentials2(): Boolean {
        if (TextUtils.isEmpty(comments_stg))
            et_comments.setError("Comments Required")
        else
            return true
        return false
    }

    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


}
