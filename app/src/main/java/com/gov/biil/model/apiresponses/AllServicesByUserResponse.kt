package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class AllServicesByUserResponse {

  val result: String? = null

  val service_info: Array<AllServicesByUserInfo>? = null

  val newslist: Array<AllServicesByUserInfo>? = null

  val status: String? = null

  val count: Countdashboard? = null

}