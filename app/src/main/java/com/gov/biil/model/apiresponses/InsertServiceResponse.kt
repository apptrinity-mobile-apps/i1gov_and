package com.gov.biil.model.apiresponses

class InsertServiceResponse {

    val result: String? = null

    val data: InsertServiceDataResponse? = null

    val status: String? = null

}
