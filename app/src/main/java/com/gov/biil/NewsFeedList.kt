package com.gov.biil

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Window
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import android.widget.Toast


class NewsFeedList : AppCompatActivity() {


    var uri: Uri? = null
    lateinit var mywebview: WebView
    internal lateinit var my_loader: Dialog
    internal var paylink: String? = ""

    var form_screen_stg = ""
    var form_screen_url = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.countryfragment)


        Log.e("TABS","CITY")


        val toolbar = findViewById(R.id.toolbar) as Toolbar
        val toolbar_title = findViewById(R.id.toolbar_title) as TextView

        (this as AppCompatActivity).setSupportActionBar(toolbar)
        (this as AppCompatActivity).supportActionBar!!.setHomeButtonEnabled(true)
        (this as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (this as AppCompatActivity).supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }


        mywebview = findViewById<WebView>(R.id.webview_pricecalc)
        form_screen_stg = intent.getStringExtra("form_screen")

        if(intent.getStringExtra("url") != null){
            form_screen_url = intent.getStringExtra("url")
        }

        if(intent.getStringExtra("pay_link") != null){
            form_screen_url = intent.getStringExtra("pay_link")
        }

        myloading()
        toolbar_title.setText(form_screen_stg)
        paylink = form_screen_url
        if (paylink!!.contains("http") || paylink!!.contains("https")) {
            uri = Uri.parse(paylink)
        } else {
            uri = Uri.parse("http://" + paylink)
        }

        /*else if (form_screen_stg.equals("Us Marines")) {
            toolbar_title.setText("Us Marines")
            paylink = "https://www.marines.com"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }
        }else if (form_screen_stg.equals("Us Army")) {
            toolbar_title.setText("Us Army")
            paylink = "https://www.army.mil"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }
        }else if (form_screen_stg.equals("FBI")) {
            toolbar_title.setText("FBI")
            paylink = "https://www.fbi.gov"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }
        }else if (form_screen_stg.equals("HHS")) {
            toolbar_title.setText("HHS")
            paylink = "https://www.hhs.gov"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }
        }else if (form_screen_stg.equals("HUD")) {
            toolbar_title.setText("HUD")
            paylink = "https://www.hud.gov"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }
        }else if (form_screen_stg.equals("IEA")) {
            toolbar_title.setText("IEA")
            paylink = "https://www.iea.org"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }
        }else if (form_screen_stg.equals("Us National Guard")) {
            toolbar_title.setText("Us National Guard")
            paylink = "https://www.nationalguard.com"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }
        }else if (form_screen_stg.equals("Us Vetrans")) {
            toolbar_title.setText("Us Vetrans")
            paylink = "https://www.va.gov"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }
        }else if (form_screen_stg.equals("United States Marshals")) {
            toolbar_title.setText("United States Marshals")
            paylink = "https://www.usmarshals.gov"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }
        }else if (form_screen_stg.equals("Senior Citizens Administration")) {
            toolbar_title.setText("Senior Citizens Administration")
            paylink = "https://www.usa.gov"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }
        }else if (form_screen_stg.equals("Gun Violence")) {
            toolbar_title.setText("Gun Violence")
            paylink = "https://www.gunviolencearchive.org"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }
        }else if (form_screen_stg.equals("Disabled Kids")) {
            toolbar_title.setText("Disabled Kids")
            paylink = "https://www.ssa.gov/disability"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("http://" + paylink)
            }
        }else */if (form_screen_stg.equals("city_of_water")) {
            toolbar_title.setText("City of Chicago")
            paylink = "https://www.chicago.gov"

            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                uri = Uri.parse(paylink)
            } else {
                uri = Uri.parse("https://" + paylink)
            }
        }



        mywebview!!.getSettings().setJavaScriptEnabled(true)
        mywebview!!.setHorizontalScrollBarEnabled(false)
        mywebview!!.getSettings().setDomStorageEnabled(true)
        mywebview!!.getSettings().setUseWideViewPort(true);
        mywebview!!.getSettings().setLoadWithOverviewMode(true);
        mywebview!!.loadUrl(uri.toString())


        val alertDialog = AlertDialog.Builder(this).create()

        mywebview.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                if (my_loader.isShowing()) {
                    my_loader.dismiss()
                }
            }

            override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                Toast.makeText(this@NewsFeedList, "Oh no! $description", Toast.LENGTH_SHORT).show()
                alertDialog.setTitle("Error")
                alertDialog.setMessage(description)
                alertDialog.setButton("OK", DialogInterface.OnClickListener { dialog, which -> return@OnClickListener })
                alertDialog.show()
            }
        })


    }

    private fun myloading() {
        my_loader = Dialog(this@NewsFeedList)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(true);
        my_loader.setContentView(R.layout.mkloader_dialog)
        my_loader.show()
    }


    override fun onBackPressed() {
        if (mywebview!!.canGoBack()) {
            mywebview!!.goBack()
        } else {
            super.onBackPressed()

        }
    }
}
