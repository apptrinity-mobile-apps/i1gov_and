package com.gov.biil

import android.annotation.SuppressLint
import android.app.Dialog
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Window
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import com.gov.a4print.Session.SessionManager

class NewsTitleWebActivity : AppCompatActivity() {

    internal  var title_link_url:String?=""
    internal  var title_link_name:String?=""
    internal lateinit var id:String
    var mywebview: WebView? = null
    internal lateinit var sessionManager: SessionManager
    lateinit  var uri: Uri

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city_web)
        mywebview = findViewById<WebView>(R.id.webview_pricecalc)
        myloading()
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        val toolbar_title = findViewById(R.id.toolbar_title) as TextView
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        my_loader.show()

        sessionManager = SessionManager(this)


        if(intent.getStringExtra("title_link")!=null){
            title_link_url = intent.getStringExtra("title_link")
            Log.d("NEWSWEBVIEW", title_link_url)
        }
        if(intent.getStringExtra("title_name")!=null){
            title_link_name = intent.getStringExtra("title_name")
            Log.d("NEWSWEBVIEW_NAME", title_link_name)
            toolbar_title.setText(title_link_name)
        }





        if(title_link_url!=""){
            title_link_url = title_link_url
            if (title_link_url!!.contains("http") || title_link_url!!.contains("https")) {
                uri = Uri.parse(title_link_url)
            } else {
                uri = Uri.parse("http://" + title_link_url)
            }
        }else{
            title_link_url = ""

            if (title_link_url!!.contains("http") || title_link_url!!.contains("https")) {
                uri = Uri.parse(title_link_url)
            } else {
                uri = Uri.parse("http://" + title_link_url)
            }

        }


        //paylink = "http://www.blueisland.org/"

        mywebview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)

                return true
            }
        }
        mywebview!!.getSettings().setJavaScriptEnabled(true);
        mywebview!!.setHorizontalScrollBarEnabled(false);
        mywebview!!.loadUrl(uri.toString())

        my_loader.dismiss()

    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


    override fun onBackPressed() {
        if (mywebview!!.canGoBack()) {
            mywebview!!.goBack()
        } else {
            super.onBackPressed()
        }
    }



}
