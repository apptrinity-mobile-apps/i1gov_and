package com.gov.biil.model.apiresponses

class VotingCountDataResponse {


    val total: String? = null
    val city: String? = null
    val state: String? = null
    val votecount: Array<VotingVoteCountDataResponse>?=null

}
