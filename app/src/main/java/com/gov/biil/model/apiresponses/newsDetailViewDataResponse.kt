package com.gov.biil.model.apiresponses

class newsDetailViewDataResponse {

    val id: String? = null
    val title: String? = null
    val news: String? = null
    val image: String? = null
    val posted_by: String? = null
    val status: String? = null
    val created_date: String? = null
    val author_profession: String? = null
    val likes_count: String? = null
    val like_status: String? = null
    val posted_by_image: String? = null

}
