package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.CustomTextViewBold
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.getBillDetailsResponse
import com.gov.biil.model.apiresponses.getBillParkingDetailsList
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat


class PaymetDetails2Park : AppCompatActivity() {
    var bill_id = ""
    var bill_type = ""
    lateinit var img_pay_type_id: ImageView
    lateinit var dashboard_home: ImageView
    lateinit var tv_reciept_id: CustomTextView
    lateinit var tv_no_plate_id: CustomTextView
    lateinit var tv_vehicle_type_id: CustomTextView
    lateinit var tv_start_date_id: CustomTextView
    lateinit var tv_end_date_id: CustomTextView
    lateinit var tv_zone_id: CustomTextView
    lateinit var tv_amount_id: CustomTextView
    lateinit var tv_hours_id: CustomTextView
    lateinit var tv_send_mail_id: CustomTextView
    lateinit var tv_sripe_amount_id: CustomTextView
    lateinit var tv_wallet_amount_id: CustomTextView

    lateinit var tv_reciept_title_id: CustomTextView
    lateinit var tv_no_plate_title_id: CustomTextView
    lateinit var tv_vehicle_type_title_id: CustomTextView
    lateinit var tv_start_date_title_id: CustomTextView
    lateinit var tv_end_date_title_id: CustomTextView
    lateinit var tv_zone_title_id: CustomTextView
    lateinit var tv_amount_title_id: CustomTextView
    lateinit var tv_hours_title_id: CustomTextView


    lateinit var ll_start_date_id: LinearLayout
    lateinit var ll_end_date_id: LinearLayout
    lateinit var ll_zone_no_id: LinearLayout
    lateinit var ll_hours_id: LinearLayout
    lateinit var ll_stripe_id: LinearLayout
    lateinit var ll_wallet_id: LinearLayout
    internal lateinit var sessionManager: SessionManager
    internal lateinit var my_loader: Dialog
    lateinit var rv_bill_detail_list: RecyclerView

    lateinit var billsDataArray: ArrayList<getBillParkingDetailsList>

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paymet_details2park)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        dashboard_home = findViewById(R.id.dashboard_home) as ImageView
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

         img_pay_type_id = findViewById(R.id.img_pay_type_id)

        sessionManager = SessionManager(this)
        myloading()
        getAdminStatusInfo()

        bill_id = intent.getStringExtra("bill_id")
        bill_type = intent.getStringExtra("bill_type")
        getBillDetailsInfo()

        rv_bill_detail_list = findViewById(R.id.rv_bill_detail_list)

        if (bill_type.equals("park")) {
            img_pay_type_id.setImageDrawable(getDrawable(R.drawable.parking_full_ic))
        }

        dashboard_home.setOnClickListener {
            val intent = Intent(this@PaymetDetails2Park, MainActivity::class.java)
            startActivity(intent)
        }

    }

    private fun getBillDetailsInfo() {

        val apiService = ApiInterface.create()
        val call = apiService.getBillDetails(bill_id)
        Log.d("REQUEST", bill_id + "")
        billsDataArray = ArrayList()
        billsDataArray.clear()
        call.enqueue(object : Callback<getBillDetailsResponse> {
            override fun onResponse(call: Call<getBillDetailsResponse>, response: retrofit2.Response<getBillDetailsResponse>?) {
                if (response != null) {

                    // Log.w("Result_Address_Profile","Result : "+response.body()!!.user_info)
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.data != null) {

                            if (response.body()!!.data != null) {

                                val list: Array<getBillParkingDetailsList> = response.body()!!.data!!.park!!
                                for (item: getBillParkingDetailsList in list.iterator()) {
                                    billsDataArray.add(item)
                                }

                                rv_bill_detail_list.setHasFixedSize(true)
                                rv_bill_detail_list.setLayoutManager(LinearLayoutManager(this@PaymetDetails2Park))
                                rv_bill_detail_list.setItemAnimator(DefaultItemAnimator())
                                val details_adapter = BillDetailAdapter(billsDataArray, this@PaymetDetails2Park)
                                rv_bill_detail_list.setAdapter(details_adapter)
                                details_adapter.notifyDataSetChanged()
                            }


                        }

                    }else if (response.body()!!.status.equals("2")){

                    }

                }
            }

            override fun onFailure(call: Call<getBillDetailsResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    inner class BillDetailAdapter(val title: java.util.ArrayList<getBillParkingDetailsList>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.listitem_paymet_details2park, parent, false))
        }
        //@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {



                val inputEndFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val outputEndFormat = SimpleDateFormat("MM-dd-yyyy HH:mm:ss")
                val inputEndDateStr = title.get(position).end_date
                val dateend = inputEndFormat.parse(inputEndDateStr)
                val outputEndDateStr = outputEndFormat.format(dateend)

                val inputStartFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val outputStartFormat = SimpleDateFormat("MM-dd-yyyy HH:mm:ss")
                val inputStartDateStr = title.get(position).start_date
                val datestart = inputStartFormat.parse(inputStartDateStr)
                val outputStartDateStr = outputStartFormat.format(datestart)
                Log.e("PARKING_ENDDATE", title.get(position).end_date+"------"+outputEndDateStr)




                Log.e("main_details", title.get(position).amount.toString())
                holder?.tv_pay_for_type_id.setText(title.get(0).message.toString())
            holder?.tv_reciept_id.setText(title.get(position).receipt_id.toString())
            holder?.tv_no_plate_id.setText(title.get(position).no_plate.toString())
            holder?.tv_vehicle_type_id.setText(title.get(position).vehicle_type.toString())
            holder?.tv_start_date_id.setText(outputStartDateStr.toString())
            holder?.tv_end_date_id.setText(outputEndDateStr.toString())
            holder?.tv_zone_id.setText(title.get(position).zone_rate_id.toString())
            holder?.tv_amount_id.setText("$ " + title.get(position).amount.toString())
                // tv_hours_id.setText(response.body()!!.data!!.park!!..toString())


                val end_date = SimpleDateFormat("MM-dd-yyyy HH:mm:ss").parse(title.get(position).end_date)
                val start_time = SimpleDateFormat("MM-dd-yyyy HH:mm:ss").parse(title.get(position).start_date)

                val diff = end_date.time - start_time.time
                val numOfDays = (diff / (1000 * 60 * 60 * 24)).toInt()
                val hours = (diff / (1000 * 60 * 60)).toInt()
                val minutes = (diff / (1000 * 60)).toInt()
                val seconds = (diff / 1000).toInt()
            holder?.tv_hours_id.setText(title.get(position).hour)


        }

        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return title.size
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val tv_pay_for_type_id = view.findViewById<CustomTextViewBold>(R.id.tv_pay_for_type_id)

        val  tv_reciept_id = view.findViewById<CustomTextView>(R.id.tv_reciept_id)
        val  tv_no_plate_id = view.findViewById<CustomTextView>(R.id.tv_no_plate_id)
        val  tv_vehicle_type_id = view.findViewById<CustomTextView>(R.id.tv_vehicle_type_id)
        val  tv_start_date_id =view.findViewById<CustomTextView>(R.id.tv_start_date_id)
        val  tv_end_date_id = view.findViewById<CustomTextView>(R.id.tv_end_date_id)
        val tv_zone_id = view.findViewById<CustomTextView>(R.id.tv_zone_id)
        val tv_amount_id = view.findViewById<CustomTextView>(R.id.tv_amount_id)
        val tv_hours_id = view.findViewById<CustomTextView>(R.id.tv_hours_id)
        val tv_send_mail_id = view.findViewById<CustomTextView>(R.id.tv_send_mail_id)
        val tv_sripe_amount_id = view.findViewById<CustomTextView>(R.id.tv_sripe_amount_id)
        val tv_wallet_amount_id = view.findViewById<CustomTextView>(R.id.tv_wallet_amount_id)


        val tv_reciept_title_id = view.findViewById<CustomTextView>(R.id.tv_reciept_title_id)
        val tv_no_plate_title_id = view.findViewById<CustomTextView>(R.id.tv_no_plate_title_id)
        val tv_vehicle_type_title_id = view.findViewById<CustomTextView>(R.id.tv_vehicle_type_title_id)
        val tv_start_date_title_id = view.findViewById<CustomTextView>(R.id.tv_start_date_title_id)
        val tv_end_date_title_id = view.findViewById<CustomTextView>(R.id.tv_end_date_title_id)
        val tv_zone_title_id = view.findViewById<CustomTextView>(R.id.tv_zone_title_id)
        val tv_amount_title_id = view.findViewById<CustomTextView>(R.id.tv_amount_title_id)
        val tv_hours_title_id = view.findViewById<CustomTextView>(R.id.tv_hours_title_id)

        val ll_start_date_id = view.findViewById<LinearLayout>(R.id.ll_start_date_id)
        val ll_end_date_id = view.findViewById<LinearLayout>(R.id.ll_end_date_id)
        val ll_zone_no_id = view.findViewById<LinearLayout>(R.id.ll_zone_no_id)
        val ll_hours_id = view.findViewById<LinearLayout>(R.id.ll_hours_id)
        val ll_stripe_id = view.findViewById<LinearLayout>(R.id.ll_stripe_id)
        val ll_wallet_id = view.findViewById<LinearLayout>(R.id.ll_wallet_id)



        // var userSelected = true;

    }


}
