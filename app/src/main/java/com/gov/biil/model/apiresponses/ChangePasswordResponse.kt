package com.gov.biil.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class ChangePasswordResponse {
     val result: String? = null

     val Message: String? = null

     val status: String? = null

}
