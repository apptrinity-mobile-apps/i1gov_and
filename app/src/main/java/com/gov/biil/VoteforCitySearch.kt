package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.*
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.*
import com.squareup.picasso.Picasso
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback
import java.util.*

class VoteforCitySearch : AppCompatActivity() {

    internal lateinit var my_loader: Dialog
    lateinit var rv_voted_list: RecyclerView
    lateinit var rv_citysearchlist: RecyclerView
    lateinit var cv_cardview: CardView
    lateinit var ll_search_city_state: LinearLayout
    lateinit var tv_submit: TextView
    lateinit var no_service: TextView
    internal lateinit var sessionManager: SessionManager
    public var votecountdataresponse: ArrayList<VotingVoteCountDataResponse> = ArrayList<VotingVoteCountDataResponse>()
    public var citywiseuserlistdataresponse: ArrayList<CityWiseUsersListDataResponse> = ArrayList<CityWiseUsersListDataResponse>()
    public var statewiseuserlistdataresponse: ArrayList<StateWiseUsersListDataResponse> = ArrayList<StateWiseUsersListDataResponse>()
    public var savevotelistresponse: ArrayList<SaveVoteListDataResponse> = ArrayList<SaveVoteListDataResponse>()
    public var searchcitylistdata: java.util.ArrayList<SearchCityListDataResponse> = java.util.ArrayList<SearchCityListDataResponse>()
    public var searchstatelistdata: java.util.ArrayList<SearchStateListDataResponse> = java.util.ArrayList<SearchStateListDataResponse>()
    lateinit var search_autocomplete: AutoCompleteTextView
    lateinit var search_autocomplete_state: AutoCompleteTextView
    lateinit var search_names: ArrayList<String>
    lateinit var search_names_state: ArrayList<String>
    lateinit var search_ids: ArrayList<String>
    lateinit var search_ids_state: ArrayList<String>
    lateinit var adapteo: ArrayAdapter<String>
    lateinit var adapteo_state: ArrayAdapter<String>
    var search_string: String = ""
    var aPosition: Int = 0
    var aPosition_state: Int = 0

    var city_id: String = ""
    var state_id: String = ""
    var sub_category_id = ""
    var sub_category_name = ""
    var category_id = ""
    var citylist = ""
    var statelist = ""
    var usa = ""
    var county = ""
    var candidate_id_stg = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voteforsearch)


        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        sessionManager = SessionManager(this)

        myloading()

        getAdminStatusInfo()


        rv_citysearchlist = findViewById(R.id.rv_citysearchlist)
        ll_search_city_state = findViewById(R.id.ll_search_city_state)
        cv_cardview = findViewById(R.id.cv_cardview)
        tv_submit = findViewById(R.id.tv_submit)
        no_service = findViewById(R.id.no_service)



        if (intent.getStringExtra("category_id") !== null) {
            category_id = intent.getStringExtra("category_id")
        }
        if (intent.getStringExtra("sub_category_id") !== null) {
            sub_category_id = intent.getStringExtra("sub_category_id")
        }
        if (intent.getStringExtra("sub_category_name") !== null) {
            sub_category_name = intent.getStringExtra("sub_category_name")
        }




        if (intent.getStringExtra("citylist") !== null) {
            citylist = intent.getStringExtra("citylist")
            cv_cardview.visibility = View.GONE
        }
        if (intent.getStringExtra("statelist") != null) {
            statelist = intent.getStringExtra("statelist")
            cv_cardview.visibility = View.GONE
            Log.e("statelist", statelist)
        }
        if (intent.getStringExtra("usa") != null) {
            usa = intent.getStringExtra("usa")
            cv_cardview.visibility = View.VISIBLE

        }
        if (intent.getStringExtra("county") != null) {
            county = intent.getStringExtra("county")
            cv_cardview.visibility = View.VISIBLE

        }

        search_names = ArrayList()
        search_names_state = ArrayList()
        search_ids = ArrayList()
        search_ids_state = ArrayList()
        search_autocomplete = findViewById<AutoCompleteTextView>(R.id.search_autocomplete)
        search_autocomplete_state = findViewById<AutoCompleteTextView>(R.id.search_autocomplete_state)





        // callStateSearchList()
        adapteo = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, search_names)
        adapteo_state = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, search_names_state)


        if (citylist.equals("citylist")) {
            search_autocomplete_state.visibility = View.GONE
            callCitySearchList()
        } else if (statelist.equals("statelist")) {
            search_autocomplete.visibility = View.GONE
            search_autocomplete_state.visibility = View.VISIBLE
            callStateSearchList()
        }

        search_autocomplete.setOnItemClickListener { parent, view, position, id ->

            aPosition = search_names.indexOf(adapteo.getItem(position).toString())
            if(!search_ids.get(position).equals("")){
                city_id = search_ids.get(aPosition)
            }else{
                city_id =""
            }

            if (city_id != null) {
                Log.d("VoteCount", "" + category_id + "-----" + sub_category_id)
                callCityWiseUsersAPI(city_id, category_id, sub_category_id)
            }

            /*if (state_id != null) {
                Log.d("VoteCount", "" + category_id + "-----" + sub_category_id)
                callStateWiseUsersAPI(state_id, category_id, sub_category_id)
            }*/


        }


        search_autocomplete_state.setOnItemClickListener { parent, view, position, id ->

            aPosition_state = search_names_state.indexOf(adapteo_state.getItem(position).toString())

            if(!search_ids_state.get(aPosition).equals("")){
                state_id = search_ids_state.get(aPosition_state)
            }else{
                state_id =""
            }

            if (state_id != null) {
                Log.d("VoteCount", "state_id"+state_id+"----" + category_id + "-----" + sub_category_id)
                callStateWiseUsersAPI(state_id, category_id, sub_category_id)
            }


        }


        search_autocomplete.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                search_string = search_autocomplete.text.toString().trim()

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        search_autocomplete_state.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                search_string = search_autocomplete_state.text.toString().trim()

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {

            }
        })


        if (usa.equals("usa")) {

            Log.d("USA", category_id+"-----"+sub_category_id)
            ll_search_city_state.visibility = View.GONE
            callVoteUsersAPI(category_id,sub_category_id)
        }

        if (county.equals("county")) {
            Log.d("COUNTY", category_id+"-----"+sub_category_id)
            ll_search_city_state.visibility = View.GONE
            callVoteUsersAPI(category_id,sub_category_id)
        }



        tv_submit.setOnClickListener {

            Log.d("SAVEVOTE", candidate_id_stg)
            if(!city_id.equals("")){
                state_id = ""
                Log.d("SAVEVOTE_CITY", candidate_id_stg+"-------"+city_id+"-----"+state_id)
                callsaveVoteAPI(sessionManager.isId, candidate_id_stg,city_id,state_id)

            }else if(!state_id.equals("")){
                city_id = ""
                Log.d("SAVEVOTE_STATE", candidate_id_stg+"-------"+city_id+"-----"+state_id)
                callsaveVoteAPI(sessionManager.isId, candidate_id_stg,city_id,state_id)
            }else{
                callsaveVoteAPI(sessionManager.isId, candidate_id_stg,"","")
            }




        }


        tv_submit.visibility = View.GONE

    }

    private fun callCitySearchList() {
        my_loader.show()
        searchcitylistdata.clear()
        search_names.clear()
        val apiService = ApiInterface.create()
        val call = apiService.getAllCities(search_string)
        Log.d("REQUEST", call.toString() + "------" + search_string)
        call.enqueue(object : Callback<SearchCityListResponse> {
            override fun onResponse(call: Call<SearchCityListResponse>, response: retrofit2.Response<SearchCityListResponse>?) {
                 my_loader.dismiss()
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response.body()!!.status!!.equals("1")) {
                    if (response.body()!!.data != null) {
                        my_loader.dismiss()
                        val list: Array<SearchCityListDataResponse> = response.body()!!.data!!

                        for (item: SearchCityListDataResponse in list.iterator()) {
                            searchcitylistdata.add(item)
                            search_names.add(item.city!!)
                            search_ids.add(item.id!!)

                        }
                        //setProductAdapter(searchcitylistdata)
                        if (searchcitylistdata.size == 0) {
                            //my_loader.dismiss()

                        }
                        search_autocomplete.setAdapter(adapteo)
                    }

                } else {
                    my_loader.dismiss()
                    // Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<SearchCityListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun callStateSearchList() {
        // my_loader.show()
        searchstatelistdata.clear()
        search_names_state.clear()
        val apiService = ApiInterface.create()
        val call = apiService.getAllStates(search_string)
        Log.d("REQUEST", call.toString() + "------" + search_string)
        call.enqueue(object : Callback<SearchStateListResponse> {
            override fun onResponse(call: Call<SearchStateListResponse>, response: retrofit2.Response<SearchStateListResponse>?) {
                // my_loader.dismiss()
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response.body()!!.status!!.equals("1")) {
                    if (response.body()!!.data != null) {
                        my_loader.dismiss()
                        val list: Array<SearchStateListDataResponse> = response.body()!!.data!!

                        for (item: SearchStateListDataResponse in list.iterator()) {
                            searchstatelistdata.add(item)
                            search_names_state.add(item.state!!)
                            search_ids_state.add(item.id!!)
                        }
                        //setProductAdapter(searchlistdata)
                        if (searchstatelistdata.size == 0) {
                            my_loader.dismiss()

                        }
                        search_autocomplete_state.setAdapter(adapteo_state)
                    }

                } else {
                    // my_loader.dismiss()
                    // Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<SearchStateListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun callCityWiseUsersAPI(city_id: String, category_id: String, sub_category_id: String) {
        //my_loader.show()
        citywiseuserlistdataresponse.clear()
        val apiService = ApiInterface.create()
        tv_submit.visibility = View.VISIBLE
        val call = apiService.getcitywiseUsers(city_id, category_id, sub_category_id)
        Log.d("VoteForListingAPI", city_id + "------" + category_id + "------" + sub_category_id)
        call.enqueue(object : Callback<GetCityWiseUsersResponse> {
            override fun onResponse(call: Call<GetCityWiseUsersResponse>, response: retrofit2.Response<GetCityWiseUsersResponse>?) {
                if (response != null) {
                    //my_loader.dismiss()
                    Log.w("Result_VoteForListing", response.body().toString())
                    if (response.body()!!.status!!.equals("1") && response.body()!!.data != null) {

                        val list: GetCityWiseUsersDataResponse? = response.body()!!.data!!
                        val voting_list_array: Array<CityWiseUsersListDataResponse>? = list!!.candidates

                        if (!voting_list_array!!.isEmpty()) {

                            for (item: CityWiseUsersListDataResponse in voting_list_array!!.iterator()) {
                                citywiseuserlistdataresponse.add(item)
                            }
                            cv_cardview.visibility = View.VISIBLE
                            rv_citysearchlist!!.setHasFixedSize(true)
                            val layoutManager = LinearLayoutManager(this@VoteforCitySearch);
                            rv_citysearchlist!!.setLayoutManager(layoutManager)
                            rv_citysearchlist!!.setItemAnimator(DefaultItemAnimator())
                            val details_adapter = CitywiseUserAdapter(citywiseuserlistdataresponse, this@VoteforCitySearch!!)
                            rv_citysearchlist!!.setAdapter(details_adapter)
                            details_adapter.notifyDataSetChanged()

                            no_service.visibility= View.GONE

                        }else{
                            tv_submit.visibility = View.GONE
                            no_service.visibility= View.VISIBLE
                            no_service.setText("No Candidates Found")
                        }


                    } else if (response.body()!!.status!!.equals("2")) {
                        tv_submit.visibility = View.GONE
                        no_service.visibility= View.VISIBLE
                        no_service.setText("No Candidates Found")
                        // no_service!!.text = "No Data Found"
                        // no_service!!.visibility = View.VISIBLE
                    }

                }
            }

            override fun onFailure(call: Call<GetCityWiseUsersResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val radiobutton_id = view.findViewById<RadioButton>(R.id.radio_list)
        val tv_candidate_name = view.findViewById<TextView>(R.id.txtlist_name)
        val imglist_icon = view.findViewById<ImageView>(R.id.imglist_icon)
        val tv_partyname = view.findViewById<CustomTextView>(R.id.tv_partyname)


    }

    inner class CitywiseUserAdapter(val title: ArrayList<CityWiseUsersListDataResponse>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {


        var selected: RadioButton? = null
        lateinit var tv_partyName: CustomTextView

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.city_wise_user_list, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            holder?.tv_candidate_name.text = title.get(position).candidate_name
            holder?.tv_partyname.setText(title.get(position).party_name)
            Log.d("RADIOGROUP", title.get(position).candidate_name)
            holder?.radiobutton_id.setOnCheckedChangeListener { radioGroup, i ->
                Log.d("RADIOGROUP", i.toString())

            }

            Picasso.with(this@VoteforCitySearch)
                    .load("https://www.i1gov.com/uploads/party/"+title.get(position).party_logo)
                    .into(holder?.imglist_icon)

            holder?.radiobutton_id.setOnClickListener {
                if (selected != null) {
                    selected!!.isChecked = false
                }

                holder?.radiobutton_id.isChecked = true
                candidate_id_stg = title.get(position).id.toString()
                city_id = title.get(position).city_id.toString()
                selected = holder?.radiobutton_id
            }

        }

        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return title.size
        }
    }


    private fun callStateWiseUsersAPI(state_id: String, category_id: String, sub_category_id: String) {

        //my_loader.show()
        statewiseuserlistdataresponse.clear()
        val apiService = ApiInterface.create()
        tv_submit.visibility = View.VISIBLE
        val call = apiService.getStatewiseUsers(state_id, category_id, sub_category_id)
        Log.d("VoteForSTATEWISE", state_id + "------" + category_id + "------" + sub_category_id)
        call.enqueue(object : Callback<GetStateWiseUsersResponse> {
            override fun onResponse(call: Call<GetStateWiseUsersResponse>, response: retrofit2.Response<GetStateWiseUsersResponse>?) {
                if (response != null) {
                    //my_loader.dismiss()
                    Log.w("Result_VoteForListing", response.body().toString())
                    if (response.body()!!.status.equals("1") && response.body()!!.data != null) {

                        tv_submit.visibility = View.VISIBLE
                        val list: GetStateWiseUsersDataResponse? = response.body()!!.data!!
                        val voting_list_array: Array<StateWiseUsersListDataResponse>? = list!!.candidates

                        if (!voting_list_array!!.isEmpty()) {
                            tv_submit.visibility = View.VISIBLE
                            for (item: StateWiseUsersListDataResponse in voting_list_array!!.iterator()) {
                                statewiseuserlistdataresponse.add(item)
                            }
                            cv_cardview.visibility = View.VISIBLE
                            rv_citysearchlist!!.setHasFixedSize(true)
                            val layoutManager = LinearLayoutManager(this@VoteforCitySearch);
                            rv_citysearchlist!!.setLayoutManager(layoutManager)
                            rv_citysearchlist!!.setItemAnimator(DefaultItemAnimator())
                            val details_adapter = StatewiseUserAdapter(statewiseuserlistdataresponse, this@VoteforCitySearch!!)
                            rv_citysearchlist!!.setAdapter(details_adapter)
                            details_adapter.notifyDataSetChanged()

                        }else{
                            tv_submit.visibility = View.GONE
                            no_service.visibility= View.VISIBLE
                            no_service.setText("No Candidates Found")
                        }


                    } else if (response.body()!!.status.equals("2")) {
                        tv_submit.visibility = View.GONE
                        no_service.visibility= View.VISIBLE
                        no_service.setText("No Candidates Found")
                    }

                }
            }

            override fun onFailure(call: Call<GetStateWiseUsersResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    class ViewHolder1(view: View) : RecyclerView.ViewHolder(view) {
        val radiobutton_id = view.findViewById<RadioButton>(R.id.radio_list)
        val tv_candidate_name = view.findViewById<TextView>(R.id.txtlist_name)
        val imglist_icon = view.findViewById<ImageView>(R.id.imglist_icon)
        val tv_partyname = view.findViewById<CustomTextView>(R.id.tv_partyname)



    }

    inner class StatewiseUserAdapter(val title: ArrayList<StateWiseUsersListDataResponse>, val context: Context) : RecyclerView.Adapter<ViewHolder1>() {

        lateinit var tv_partyName: CustomTextView
        lateinit var radiobutton_id: RadioButton
        lateinit var tv_candidate_name: TextView
        lateinit var myRadioCityGroup: RadioGroup

        private var mSelectedRB: RadioButton? = null
        private var mSelectedPosition = -1
        lateinit var ll_part_name_id: LinearLayout

        var selected: RadioButton? = null


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder1 {
            return ViewHolder1(LayoutInflater.from(context).inflate(R.layout.state_wise_user_list, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder1, position: Int) {

            holder?.tv_candidate_name.text = title.get(position).candidate_name
            holder?.tv_partyname.setText(title.get(position).party_name)

            holder?.radiobutton_id.setOnCheckedChangeListener { radioGroup, i ->
                Log.d("RADIOGROUP", i.toString())

            }

            Picasso.with(this@VoteforCitySearch)
                    .load("https://www.i1gov.com/uploads/party/"+title.get(position).party_logo)
                    .into(holder?.imglist_icon)

            holder?.radiobutton_id.setOnClickListener {
                if (selected != null) {
                    selected!!.isChecked = false
                }

                holder?.radiobutton_id.isChecked = true
                candidate_id_stg = title.get(position).id.toString()
                selected = holder?.radiobutton_id
            }

        }

        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return title.size
        }
    }


    private fun callsaveVoteAPI(user_id: String, candidate_id: String,city_id: String,state_id: String) {
        //my_loader.show()
        savevotelistresponse.clear()
        val apiService = ApiInterface.create()
        val call = apiService.saveVote(user_id, candidate_id,city_id,state_id)

        call.enqueue(object : Callback<SaveVoteResponse> {
            override fun onResponse(call: Call<SaveVoteResponse>, response: retrofit2.Response<SaveVoteResponse>?) {
                if (response != null) {
                    //my_loader.dismiss()
                    Log.w("Result_SAVEVote", response.body()!!.status+"---"+user_id+"---"+candidate_id+"---"+state_id+"------"+city_id)
                    if (response.body()!!.status!!.equals("1") && response.body()!!.data != null) {


                        val intent = Intent(this@VoteforCitySearch, VoteforProgress::class.java)
                        Log.w("VOTECITY_ID", city_id)
                        intent.putExtra("sub_category_id", sub_category_id)
                        intent.putExtra("category_id", category_id)
                        intent.putExtra("city_id", city_id)
                        intent.putExtra("sub_category_name", sub_category_name)
                        intent.putExtra("state_id", state_id)
                        startActivity(intent)


                        val list: SaveVoteDataResponse? = response.body()!!.data!!
                        val voting_list_array: Array<SaveVoteListDataResponse>? = list!!.votecount

                        /*for (item: SaveVoteListDataResponse in voting_list_array!!.iterator()) {
                            val candidatename = item.candidate_name
                            val party_name = item.party_name
                            val count = item.count
                            val category_id = item.category_id
                            val sub_category_id = item.sub_category_id
                            val city_id = list.city
                            val state_id = list.state


                        }*/


                    } else if (response.body()!!.status!!.equals("2")) {
                        // no_service!!.text = "No Data Found"
                        // no_service!!.visibility = View.VISIBLE
                    }

                }
            }

            override fun onFailure(call: Call<SaveVoteResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    private fun callVoteUsersAPI(category_id: String, sub_category_id: String) {
        //my_loader.show()
        citywiseuserlistdataresponse.clear()
        val apiService = ApiInterface.create()
        val call = apiService.vote(category_id, sub_category_id)
        Log.d("VoteForUSERSAPI", category_id + "------" + sub_category_id)
        call.enqueue(object : Callback<VoteResponse> {
            override fun onResponse(call: Call<VoteResponse>, response: retrofit2.Response<VoteResponse>?) {
                if (response != null) {
                    //my_loader.dismiss()
                    Log.w("Result_VoteForListing", response.body().toString())
                    if (response.body()!!.status.equals("1") && response.body()!!.data != null) {

                        val list: GetCityWiseUsersDataResponse? = response.body()!!.data!!
                        val voting_list_array: Array<CityWiseUsersListDataResponse>? = list!!.canidates

                        if (!voting_list_array!!.isEmpty()) {
                            tv_submit.visibility = View.VISIBLE

                            for (item: CityWiseUsersListDataResponse in voting_list_array!!.iterator()) {
                                citywiseuserlistdataresponse.add(item)
                            }
                            cv_cardview.visibility = View.VISIBLE
                            rv_citysearchlist!!.setHasFixedSize(true)
                            val layoutManager = LinearLayoutManager(this@VoteforCitySearch);
                            rv_citysearchlist!!.setLayoutManager(layoutManager)
                            rv_citysearchlist!!.setItemAnimator(DefaultItemAnimator())
                            val details_adapter = VoteUserAdapter(citywiseuserlistdataresponse, this@VoteforCitySearch!!)
                            rv_citysearchlist!!.setAdapter(details_adapter)
                            details_adapter.notifyDataSetChanged()

                        }else{
                            tv_submit.visibility = View.GONE
                            no_service.visibility= View.VISIBLE
                            no_service.setText("No Candidates Found")
                        }

                    } else if (response.body()!!.status.equals("2")) {
                        tv_submit.visibility = View.GONE
                        no_service.visibility= View.VISIBLE
                        no_service.setText("No Candidates Found")
                    }

                }
            }

            override fun onFailure(call: Call<VoteResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    class ViewHolder2(view: View) : RecyclerView.ViewHolder(view) {

        val radiobutton_id = view.findViewById<RadioButton>(R.id.radio_list)
        val tv_candidate_name = view.findViewById<TextView>(R.id.txtlist_name)
        val imglist_icon = view.findViewById<ImageView>(R.id.imglist_icon)
        val tv_partyname = view.findViewById<CustomTextView>(R.id.tv_partyname)



    }

    inner class VoteUserAdapter(val title: ArrayList<CityWiseUsersListDataResponse>, val context: Context) : RecyclerView.Adapter<ViewHolder2>() {

        lateinit var tv_partyname: CustomTextView
        lateinit var radiobutton_id: RadioButton
        lateinit var myRadioCityGroup: RadioGroup
        lateinit var tv_candidate_name: TextView
        private var mSelectedRB: RadioButton? = null
        private var mSelectedPosition = -1
        lateinit var ll_part_name_id: LinearLayout
        var selected: RadioButton? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder2 {
            return ViewHolder2(LayoutInflater.from(context).inflate(R.layout.ivotefor_listitem, parent, false))


        }

        override fun onBindViewHolder(holder: ViewHolder2, position: Int) {


            val party_name = title.get(position).party_name
            val ind_party_name = title.get(position).ind_party_name
            Log.d("PARTYNAME", party_name+"-----"+ind_party_name)

            /*if(party_name!!.equals("Republican Party")){

                holder?.imglist_icon.setImageDrawable(resources.getDrawable(R.drawable.trump))

            }else if(party_name!!.equals("Democratic Party")){
                holder?.imglist_icon.setImageDrawable(resources.getDrawable(R.drawable.democratic))
            }*/

            Picasso.with(this@VoteforCitySearch)
                    .load("https://www.i1gov.com/uploads/party/"+title.get(position).party_logo)
                    .into(holder?.imglist_icon)

             holder?.tv_candidate_name.text = title.get(position).candidate_name
            holder?.tv_partyname.setText(title.get(position).party_name)

             holder?.radiobutton_id.setOnCheckedChangeListener { radioGroup, i ->
                 Log.d("RADIOGROUP", i.toString())

             }

            holder?.radiobutton_id.setOnClickListener {
                if (selected != null) {
                    selected!!.isChecked = false
                }

                holder?.radiobutton_id.isChecked = true
                candidate_id_stg = title.get(position).id.toString()
                selected = holder?.radiobutton_id
            }

        }

        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return title.size
        }
    }


    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


}
