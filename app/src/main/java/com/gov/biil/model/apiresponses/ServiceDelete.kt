package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class ServiceDelete {
    val result: String? = null

    val Message: String? = null

    val status: String? = null
}
