package com.gov.biil.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.webkit.WebView
import android.webkit.WebViewClient
import com.gov.biil.R


/**
 * A simple [Fragment] subclass.
 */
class FederalPayForServiceFragment : Fragment() {
    lateinit var fragmentName: Fragment
    var mywebview: WebView? = null
    internal lateinit var my_loader: Dialog
    internal lateinit var paylink:String

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = LayoutInflater.from(activity).inflate(R.layout.countryfragment, container, false)
        mywebview = rootView.findViewById<WebView>(R.id.webview_pricecalc)
        myloading()


        //my_loader.show()

        paylink = "https://www.usa.gov/"


        mywebview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)

                return true
            }
        }
        mywebview!!.getSettings().setJavaScriptEnabled(true);
        mywebview!!.setHorizontalScrollBarEnabled(false);
        mywebview!!.loadUrl(paylink)

        my_loader.dismiss()
        return rootView
    }

    private fun myloading() {
        my_loader = Dialog(activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


}