package com.gov.biil.model.apiresponses

/**
 * Created by indo67 on 7/9/2018.
 */
class lotaddresspriceslist {
    var id:String?=null
    var lot_id:String?=null
    var personal_price:String?=null
    var commercial_price:String?=null
    var hour_id:String?=null
    var lot_address:String?=null
    var no_vehicles:String?=null
    var hour:String?=null
}