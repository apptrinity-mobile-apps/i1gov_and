package com.gov.a4print.Session

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.gov.biil.LaunchingActivity
import com.gov.biil.LoginActivity
import com.gov.biil.model.apiresponses.RegisterUserInfo
import com.gov.biil.model.apiresponses.Service_Info
import com.gov.biil.model.apiresponses.UserInfo
import java.util.*


/**
 * Created by indobytes21 on 2/9/2017.
 */

class SessionManager// Constructor
(// Context
        internal var _context: Context) {
    internal var pref: SharedPreferences
    // Editor for Shared preferences
    internal var editor: SharedPreferences.Editor
    // Shared pref mode
    internal var PRIVATE_MODE = 0
    val isLoggedIn: Boolean
        get() = pref.getBoolean(IS_LOGIN, false)
    val isUserId :String
        get() = pref.getString(SESSION_USERID,"")
    val isUserName :String
        get() = pref.getString(SESSION_FIRSTNAME,"User Name")
    val isId :String
        get() = pref.getString(SESSION_ID,"User Name")

    val isEmail :String
        get() = pref.getString(SESSION_EMAIL,"Email")

    val cityurl :String
        get() = pref.getString(SESSION_CITYURL,"")

    val stateurl :String
        get() = pref.getString(SESSION_STATEURL,"")

    val countyurl :String
        get() = pref.getString(SESSION_COUNTRYURL,"")

    val acceptId :String
        get() = pref.getString(SESSION_TERMS,null)

    val wallet_balance :String
        get() = pref.getString(SESSION_WALLET_BAL,null)

    val no_plate_stg :String
        get() = pref.getString(SESSION_NO_PLATE,"")

    val admin_status :String
        get() = pref.getString(SESSION_ADMIN_STATUS,"Admin Status")

    // user name
    // user email id
    //user.put(PROFILE_WEB_ID, pref.getString(PROFILE_WEB_ID, null));
    // return user
    val userDetails: HashMap<String, String>
        get() {
            val user = HashMap<String, String>()
            user[PROFILE_ID] = pref.getString(PROFILE_ID, null)
            user[PROFILE_EMAIL] = pref.getString(PROFILE_EMAIL, null)
            user[PROFILE_USERNAME] = pref.getString(PROFILE_USERNAME, null)
            user[PROFILE_USER_FIRST_NAME] = pref.getString(PROFILE_USER_FIRST_NAME, null)
            user[PROFILE_LAST_NAME] = pref.getString(PROFILE_LAST_NAME, null)
            user[PROFILE_COMPANY] = pref.getString(PROFILE_COMPANY, null)
            user[PROFILE_PHONE_NUMBER] = pref.getString(PROFILE_PHONE_NUMBER, null)
            user[PROFILE_ALTERNATE_NUMBER] = pref.getString(PROFILE_ALTERNATE_NUMBER, null)
            user[PROFILE_CUSTOMER_SERVICE] = pref.getString(PROFILE_CUSTOMER_SERVICE, null)
            user[PROFILE_REFFERD_BY] = pref.getString(PROFILE_REFFERD_BY, null)
            return user
        }

    init {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    fun createLoginSession(userid: String, first_name: String, last_name: String, username: String, email: String, company: String, phone_number: String, alter_phone: String, cust_service: String, ref_by: String) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true)
        // Storing name in pref
        editor.putString(PROFILE_ID, userid)
        editor.putString(PROFILE_EMAIL, email)
        editor.putString(PROFILE_USERNAME, username)
        editor.putString(PROFILE_USER_FIRST_NAME, first_name)
        editor.putString(PROFILE_LAST_NAME, last_name)
        editor.putString(PROFILE_COMPANY, company)
        editor.putString(PROFILE_PHONE_NUMBER, phone_number)
        editor.putString(PROFILE_ALTERNATE_NUMBER, alter_phone)
        editor.putString(PROFILE_CUSTOMER_SERVICE, cust_service)
        editor.putString(PROFILE_REFFERD_BY, ref_by)
        // commit changes
        editor.commit()
    }
    fun wallet_update(amount_update:String){

        editor.putString(SESSION_WALLET_BAL, amount_update)
        editor.commit()
    }
    fun loginid(userid: String){
        editor.putBoolean(IS_LOGIN, true)
        editor.putString(PROFILE_ID, userid)
        // commit changes
        editor.commit()
    }

    fun checkLogin() {
        // Check login status
        if (!this.isLoggedIn) {
            // user is not logged in redirect him to Login Activity
            val i = Intent(_context, LoginActivity::class.java)
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            // Add new Flag to start new Activity
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            // Staring Login Activity
            _context.startActivity(i)
        }
    }

    fun logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear()
        editor.commit()
        // After logout redirect user to Loing Activity
        val i = Intent(_context, LaunchingActivity::class.java)
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        // Add new Flag to start new Activity
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        // Staring Login Activity
        _context.startActivity(i)
    }

    fun storeList(set: HashSet<String>) {
        editor.putStringSet(STATES_LIST, set)
        editor.commit()

    }

    fun storeLoginInfoSession(user_info: UserInfo) {
        editor.putBoolean(IS_LOGIN, true)
        editor.putString(SESSION_PHONENUMBER,user_info.phone_number)
        editor.putString(SESSION_PASSWROD_LINK,user_info.password_link)
        editor.putString(SESSION_SSN,user_info.ssn)
        editor.putString(SESSION_STATE,user_info.state)
        editor.putString(SESSION_PASSWORD,user_info.password)
        editor.putString(SESSION_CITY,user_info.city)
        editor.putString(SESSION_FIRSTNAME,user_info.first_name)
        editor.putString(SESSION_COUNTRYURL,user_info.county_url)
        editor.putString(SESSION_ORGANIZATION,user_info.organization)
        editor.putString(SESSION_CITYURL,user_info.city_url)
        editor.putString(SESSION_CREATEDATE,user_info.create_date)
        editor.putString(SESSION_GENDER,user_info.gender)
        editor.putString(SESSION_USERID,user_info.user_id)
        editor.putString(SESSION_STATEURL,user_info.state_url)
        editor.putString(SESSION_USERTYPE,user_info.user_type)
        editor.putString(SESSION_STATUS,user_info.status)
        editor.putString(SESSION_MIDDLENAME,user_info.middle_name)
        editor.putString(SESSION_ZIPCODE,user_info.zipcode)
        editor.putString(SESSION_ADDRESS,user_info.address)
        editor.putString(SESSION_COUNTRY,user_info.county)
        editor.putString(SESSION_EMAIL,user_info.email)
        editor.putString(SESSION_DOB,user_info.dob)
        editor.putString(SESSION_LASTNAME,user_info.last_name)
        editor.putString(SESSION_WORKPHONE,user_info.work_phone)
        editor.putString(SESSION_HOMEPHONE,user_info.home_phone)
        editor.putString(SESSION_ID,user_info.id)
        editor.putString(SESSION_ADMIN_STATUS,user_info.admin_status)
        editor.putString(SESSION_TERMS,user_info.tc)
        editor.commit()
    }

    fun storeUpdateProfileInfoSession(user_info: Service_Info) {
        editor.putBoolean(IS_LOGIN, true)
        editor.putString(SESSION_STATE,user_info.state)
        editor.putString(SESSION_CITY,user_info.city)
        editor.putString(SESSION_COUNTRYURL,user_info.county_url)
        editor.putString(SESSION_CITYURL,user_info.city_url)
        editor.putString(SESSION_USERID,user_info.user_id)
        editor.putString(SESSION_STATEURL,user_info.state_url)
        editor.putString(SESSION_ADDRESS,user_info.address)
        editor.putString(SESSION_COUNTRY,user_info.county)
        editor.putString(SESSION_ID,user_info.id)
        editor.commit()
    }

    fun storeLoginInfoSession(user_info: RegisterUserInfo) {
        editor.putBoolean(IS_LOGIN, true)
        editor.putString(SESSION_PHONENUMBER,user_info.phone_number)
        editor.putString(SESSION_PASSWROD_LINK,user_info.password_link)
        editor.putString(SESSION_SSN,user_info.ssn)
        editor.putString(SESSION_STATE,user_info.state)
        editor.putString(SESSION_PASSWORD,user_info.password)
        editor.putString(SESSION_CITY,user_info.city)
        editor.putString(SESSION_FIRSTNAME,user_info.first_name)
        editor.putString(SESSION_COUNTRYURL,user_info.county_url)
        editor.putString(SESSION_ORGANIZATION,user_info.organization)
        editor.putString(SESSION_CITYURL,user_info.city_url)
        editor.putString(SESSION_CREATEDATE,user_info.create_date)
        editor.putString(SESSION_GENDER,user_info.gender)
        editor.putString(SESSION_USERID,user_info.user_id)
        editor.putString(SESSION_STATEURL,user_info.state_url)
        editor.putString(SESSION_USERTYPE,user_info.user_type)
        editor.putString(SESSION_STATUS,user_info.status)
        editor.putString(SESSION_MIDDLENAME,user_info.middle_name)
        editor.putString(SESSION_ZIPCODE,user_info.zipcode)
        editor.putString(SESSION_ADDRESS,user_info.address)
        editor.putString(SESSION_COUNTRY,user_info.county)
        editor.putString(SESSION_EMAIL,user_info.email)
        editor.putString(SESSION_DOB,user_info.dob)
        editor.putString(SESSION_LASTNAME,user_info.last_name)
        editor.putString(SESSION_WORKPHONE,user_info.work_phone)
        editor.putString(SESSION_HOMEPHONE,user_info.home_phone)
        editor.putString(SESSION_ID,user_info.id)
        editor.putString(SESSION_TERMS,user_info.tc)
        editor.commit()
    }






    companion object {

        var SESSION_PHONENUMBER = "phone"
        var SESSION_PASSWROD_LINK = "passlink"
        var SESSION_SSN = "ssn"
        var SESSION_STATE = "state"
        var SESSION_PASSWORD = "passwd"
        var SESSION_CITY = "city"
        var SESSION_ID = "sesionid"
        var SESSION_FIRSTNAME = "firstname"
        var SESSION_COUNTRYURL = "countryurl"
        var SESSION_ORGANIZATION = "org"
        var SESSION_CITYURL = "cityurl"
        var SESSION_CREATEDATE = "createdate"
        var SESSION_GENDER = "gender"
        var SESSION_USERID = "userid"
        var SESSION_STATEURL = "stateurl"
        var SESSION_USERTYPE = "usertype"
        var SESSION_STATUS = "status"
        var SESSION_MIDDLENAME = "middname"
        var SESSION_ZIPCODE = "zip"
        var SESSION_ADDRESS = "addr"
        var SESSION_COUNTRY = "country"
        var SESSION_EMAIL = "email"
        var SESSION_DOB = "dob"
        var SESSION_LASTNAME = "lastnmae"
        var SESSION_WORKPHONE = "workphone"
        var SESSION_HOMEPHONE = "homephone"
        var SESSION_TERMS = "terms"
        var SESSION_WALLET_BAL = "balance_amount"

        var PROFILE_ID = "userId"
        var PROFILE_EMAIL = "email"
        var PROFILE_USERNAME = "username"
        var PROFILE_USER_FIRST_NAME = "first_name"
        var PROFILE_LAST_NAME = "last_name"
        var PROFILE_COMPANY = "company"
        var PROFILE_PHONE_NUMBER = "phone_number"
        var PROFILE_ALTERNATE_NUMBER = "alternate_no"
        var PROFILE_CUSTOMER_SERVICE = "service"
        var PROFILE_REFFERD_BY = "refferd_by"
        var IS_LOGIN = "IsLoggedIn"
        var PREF_NAME = "4print"

        var STATES_LIST = "states"
        var SESSION_NO_PLATE = "no_plate_stg"
        var SESSION_ADMIN_STATUS = "admin_status"

    }
}
