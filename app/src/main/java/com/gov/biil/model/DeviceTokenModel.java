package com.gov.biil.model;

/**
 * Created by INDOBYTES11 on 8/16/2016.
 */


/**
 * Created by INDOBYTES11 on 7/11/2016.
 */
public class DeviceTokenModel {
    private static DeviceTokenModel instance;

    // Global variable
    private String devicetoken;




    // Restrict the constructor from being instantiated
    private DeviceTokenModel(){}

    public void setDevicetoken(String d){
        this.devicetoken=d;
    }
    public String getDevicetoken(){
        return this.devicetoken;
    }





    public static synchronized DeviceTokenModel getInstance(){
        if(instance==null){
            instance=new DeviceTokenModel();
        }
        return instance;
    }
}