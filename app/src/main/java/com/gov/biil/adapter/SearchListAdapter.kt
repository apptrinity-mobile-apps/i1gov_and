package com.gov.biil.adapter

import android.app.Activity
import android.app.Dialog
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.BaseAdapter
import android.widget.TextView
import com.gov.biil.R
import com.gov.biil.model.apiresponses.SearchListDataResponse


data class SearchListAdapter(val mServiceList: ArrayList<SearchListDataResponse>, val activity: Activity) : BaseAdapter() {


    override fun getItem(position: Int): Any {
        return mServiceList.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return mServiceList.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = View.inflate(activity, R.layout.search_list, null)
        val tv_name_value = view.findViewById(R.id.tv_name_value) as TextView
        val tv_phone_value = view.findViewById(R.id.tv_phone_value) as TextView
        val tv_email_value = view.findViewById(R.id.tv_email_value) as TextView

        myloading()
        tv_name_value.text = mServiceList.get(position).first_name
        tv_phone_value.text = mServiceList.get(position).phone_number
        tv_email_value.text = mServiceList.get(position).email


        return view
    }

    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }



}
