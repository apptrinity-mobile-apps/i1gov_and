package com.gov.biil.model.apiresponses

/**
 * Created by indo67 on 8/2/2018.
 */
class StripePayServiceDataResponse {


    var payment_id:String?=null
    var user_id:String?=null
    var service_address:String?=null
    var biil_type:String?=null
    var account_id:String?=null
    var amount:String?=null
    var pay_with_wallet:String?=null
    var currency:String?=null
    var trans_id:String?=null
    var payment_status:String?=null


    var id:String?=null
    var service_id:String?=null
    var service_type:String?=null
    var message:String?=null
    var transaction_id:String?=null
    var amount_type:String?=null
    var time:String?=null
    var date:String?=null
    var created_at:String?=null


}