package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class SearchCityListResponse {
     val result: String? = null

     val data: Array<SearchCityListDataResponse>? = null

     val status: String? = null


}
