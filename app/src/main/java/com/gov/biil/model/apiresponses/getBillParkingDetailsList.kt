package com.gov.biil.model.apiresponses

/**
 * Created by indo67 on 7/26/2018.
 */
class getBillParkingDetailsList {

    var receipt_id:String?=null
    var transaction_id:String?=null
    var user_id:String?=null
    var no_plate:String?=null
    var state:String?=null
    var vehicle_type:String?=null
    var zone_rate_id:String?=null
    var lot_rate_id:String?=null
    var parkig_start_date:String?=null
    var parkig_end_date:String?=null
    var parking_status:String?=null
    var created_date:String?=null
    var message:String?=null
    var amount:String?=null
    var amount_type:String?=null
    var start_date:String?=null
    var end_date:String?=null
    var hour:String?=null

}