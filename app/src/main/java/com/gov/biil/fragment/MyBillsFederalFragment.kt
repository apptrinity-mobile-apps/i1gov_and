package com.gov.biil.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.TextView
import com.gov.a4print.Session.SessionManager
import com.gov.biil.MyBillsAccountUpdateActivity
import com.gov.biil.R
import com.gov.biil.ServicesDetailView
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.GetAllAccountsFederalResponseData
import com.gov.biil.model.apiresponses.GetAllAccountsResponse
import com.gov.biil.model.apiresponses.GetAllAccountsResponseData
import retrofit2.Call
import retrofit2.Callback
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class MyBillsFederalFragment : Fragment() {
    lateinit var fragmentName: Fragment
    var uri: Uri? = null
    lateinit var rv_mybillscity: RecyclerView
    lateinit var no_service: TextView
    internal lateinit var my_loader: Dialog
    internal var paylink: String? = ""
    internal lateinit var sessionManager: SessionManager
    public var mybillscitydataresponse: ArrayList<GetAllAccountsFederalResponseData> = ArrayList<GetAllAccountsFederalResponseData>()

    companion object {
        val TAG: String = MyBillsCityFragment::class.java.simpleName
        fun newInstance() = MyBillsCityFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = LayoutInflater.from(activity).inflate(R.layout.mybillscityfragment, container, false)
        rv_mybillscity = rootView.findViewById<RecyclerView>(R.id.rv_mybillscity)
        no_service = rootView.findViewById<TextView>(R.id.no_service)
        myloading()


        sessionManager = SessionManager(this.activity!!)

        callMyBillsCityAPI(sessionManager.isId)

        return rootView
    }


    private fun callMyBillsCityAPI(user_id: String) {

        my_loader.show()
        mybillscitydataresponse.clear()
        val apiService = ApiInterface.create()
        val call = apiService.myaccounts(user_id)
        Log.d("VoteForListingAPI", user_id)
        call.enqueue(object : Callback<GetAllAccountsResponse> {
            override fun onResponse(call: Call<GetAllAccountsResponse>, response: retrofit2.Response<GetAllAccountsResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_VoteForListing", response.body().toString())
                    if (response.body()!!.status.equals("1") && response.body()!!.data != null) {

                        val list: GetAllAccountsResponseData? = response.body()!!.data!!

                        val voting_list_array: Array<GetAllAccountsFederalResponseData>? = list!!.federal

                        for (item: GetAllAccountsFederalResponseData in voting_list_array!!.iterator()) {
                            mybillscitydataresponse.add(item)
                        }
                        if (voting_list_array!!.size > 0) {
                            no_service!!.visibility = View.GONE

                            rv_mybillscity!!.setHasFixedSize(true)
                            val layoutManager = LinearLayoutManager(activity);
                            rv_mybillscity!!.setLayoutManager(layoutManager)
                            rv_mybillscity!!.setItemAnimator(DefaultItemAnimator())
                            val details_adapter = VoteCountAdapter(mybillscitydataresponse, activity!!)
                            rv_mybillscity!!.setAdapter(details_adapter)
                            details_adapter.notifyDataSetChanged()

                        }

                        if (voting_list_array.size == 0) {
                             no_service!!.text = "No Data Found"
                             no_service!!.visibility = View.VISIBLE
                        }

                    } else if (response.body()!!.status.equals("2")) {
                         no_service!!.text = "No Data Found"
                         no_service!!.visibility = View.VISIBLE
                    }

                }
            }

            override fun onFailure(call: Call<GetAllAccountsResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val sd_id = view.findViewById<TextView>(R.id.sd_id)
        val sd_service_type = view.findViewById<TextView>(R.id.sd_service_type)
        val sd_subcategory = view.findViewById<TextView>(R.id.sd_subcategory)
        val sd_name = view.findViewById<TextView>(R.id.sd_name)
        val sd_account = view.findViewById<TextView>(R.id.sd_account)
        val btn_edit = view.findViewById<Button>(R.id.btn_edit)
        val btn_pay = view.findViewById<Button>(R.id.btn_pay)


    }

    class VoteCountAdapter(val title: ArrayList<GetAllAccountsFederalResponseData>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

        lateinit var sd_id: TextView
        lateinit var sd_service_type: TextView
        lateinit var sd_subcategory: TextView
        lateinit var sd_name: TextView
        lateinit var sd_account: TextView
        lateinit var btn_edit: Button
        lateinit var btn_pay: Button
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.listitemmybillscityfragment, parent, false))
        }

        @SuppressLint("NewApi", "ResourceAsColor")
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {


            holder?.sd_id.text = title.get(position).id
            holder?.sd_service_type.text = title.get(position).service_type
            holder?.sd_subcategory.text = title.get(position).service_name
            holder?.sd_name.text = title.get(position).name
            holder?.sd_account.text = title.get(position).account


            holder?.btn_edit.setOnClickListener(View.OnClickListener {

                val intent = Intent(this.context, MyBillsAccountUpdateActivity::class.java)
                intent.putExtra("account_id",title.get(position).id)
                intent.putExtra("account_name",title.get(position).name)
                intent.putExtra("account",title.get(position).account)
                context.startActivity(intent)

            })

            holder?.btn_pay.setOnClickListener(View.OnClickListener {

                val intent = Intent(this.context, ServicesDetailView::class.java)
                intent.putExtra("id",title.get(position).id)
                intent.putExtra("service_id",title.get(position).service_id)
                intent.putExtra("pay_type",title.get(position).pay_type)
                intent.putExtra("servive_amount",title.get(position).servive_amount)
                context.startActivity(intent)

            })


        }

        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return title.size
        }
    }


    private fun myloading() {
        my_loader = Dialog(activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }
}