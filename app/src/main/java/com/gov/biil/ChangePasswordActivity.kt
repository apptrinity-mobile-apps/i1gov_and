package com.gov.biil

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.ConnectionManager
import com.gov.biil.model.APIResponse.ChangePasswordResponse
import com.gov.biil.model.ApiInterface
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback

class ChangePasswordActivity : AppCompatActivity() {
    internal lateinit var et_pass_old_id: EditText
    internal lateinit var et_pass_new_id:EditText
    internal lateinit var et_pass_conf_id:EditText
    internal var old_pass_stg: String? = null
    internal var new_pass_stg:String? = null
    internal lateinit var conf_pass_stg:String
    internal lateinit var tv_change_pass_id: TextView
    internal lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_changepassword)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        var rootview = findViewById(R.id.rootview) as RelativeLayout
        myloading()
        toolbar.setNavigationOnClickListener { onBackPressed() }

        sessionManager = SessionManager(this)

        getAdminStatusInfo()


        et_pass_old_id = findViewById(R.id.et_pass_old_id) as EditText
        et_pass_new_id = findViewById(R.id.et_pass_new_id) as EditText
        et_pass_conf_id = findViewById(R.id.et_pass_conf_id) as EditText

        tv_change_pass_id = findViewById(R.id.tv_change_pass_id) as TextView
        //changePassword
        tv_change_pass_id.setOnClickListener(View.OnClickListener {
            old_pass_stg = et_pass_old_id.getText().toString()
            new_pass_stg = et_pass_new_id.getText().toString()
            conf_pass_stg = et_pass_conf_id.getText().toString()
            if (hasValidCredentials()) {
                if(ConnectionManager.checkConnection(applicationContext)) {
                    if(new_pass_stg == conf_pass_stg) {
                        callChangePasswordAPI(old_pass_stg, new_pass_stg, conf_pass_stg)
                    }else{
                        Toast.makeText(applicationContext,"Password and Confirm password not match",Toast.LENGTH_SHORT).show()
                    }
                } else{
                    ConnectionManager.snackBarNetworkAlert_Relative(rootview,applicationContext)

                }


            }
        })
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }
    private fun callChangePasswordAPI(old_pass_stg: String?, new_pass_stg: String?, conf_pass_stg: String) {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.changePassword(sessionManager.isId, old_pass_stg!!, new_pass_stg!!)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<ChangePasswordResponse> {
            override fun onResponse(call: Call<ChangePasswordResponse>, response: retrofit2.Response<ChangePasswordResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    Log.w("Result_Address","Result : "+response.body()!!.status)
                    if(response.body()!!.status.equals("1") && response.body()!!.Message!=null) {
                        Toast.makeText(applicationContext, "Password Changed Succesfully", Toast.LENGTH_SHORT).show()
                        onBackPressed()
                    }else{
                        Toast.makeText(applicationContext, response.body()!!.Message, Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<ChangePasswordResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(et_pass_old_id.text.toString())) {
            et_pass_old_id.setError("Old Password ")
        }else if (TextUtils.isEmpty(et_pass_new_id.text.toString())) {
            et_pass_new_id.setError("New Password ")
        }else if (TextUtils.isEmpty(et_pass_conf_id.text.toString())) {
            et_pass_conf_id.setError("Confirm New Password ")
        }else
            return true
        return false
    }

    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }



}
