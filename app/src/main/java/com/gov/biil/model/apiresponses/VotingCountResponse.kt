package com.gov.biil.model.apiresponses

class VotingCountResponse {

    val status: String? = null
    val result: String? = null
    val data:VotingCountDataResponse?=null

}
