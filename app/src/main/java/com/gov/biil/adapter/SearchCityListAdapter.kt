package com.gov.biil.adapter

import Helper.CustomTextView
import android.app.Activity
import android.app.Dialog
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.BaseAdapter
import com.gov.biil.R
import com.gov.biil.model.apiresponses.SearchCityListDataResponse


data class SearchCityListAdapter(val mServiceList: ArrayList<SearchCityListDataResponse>, val activity: Activity) : BaseAdapter() {


    override fun getItem(position: Int): Any {
        return mServiceList.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return mServiceList.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = View.inflate(activity, R.layout.city_search_list, null)
        val tv_city_name = view.findViewById(R.id.tv_city_name) as CustomTextView


        myloading()
        tv_city_name!!.text = mServiceList.get(position).city




        return view
    }

    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }



}
