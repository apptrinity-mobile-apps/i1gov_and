package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.ConnectionManager
import com.gov.a4printuser.Helper.CustomTextViewBold
import com.gov.biil.adapter.ServiceDetailsRecyclerAdapter
import com.gov.biil.model.APIResponse.UpdateProfileResponse
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.*
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class DashBoardFragment : Fragment() {

    private var mServiceView: RecyclerView? = null
    public var mServiceInfo: ArrayList<AllServicesByUserInfo> = ArrayList<AllServicesByUserInfo>()
    public var mServiceInfo_temp: ArrayList<AllServicesByUserInfo> = ArrayList<AllServicesByUserInfo>()
    private var mServiceDetailsAdapter: ServiceDetailsRecyclerAdapter? = null
    private var mNewsDetailsAdapter: NewsDetailsRecyclerAdapter? = null
    private var mProgressBar: ProgressBar? = null
    internal lateinit var sessionManager: SessionManager
    internal lateinit var noservice: TextView
    internal lateinit var reminder_count: TextView
    internal lateinit var tv_city_count: CustomTextView
    internal lateinit var tv_all_count: CustomTextView
    internal lateinit var tv_county_count: CustomTextView
    internal lateinit var tv_state_count: CustomTextView
    internal lateinit var tv_federal_count: CustomTextView

    internal lateinit var tv_addservice: ImageView
    internal lateinit var iv_down: ImageView
    internal lateinit var city_of_water: ImageView
    internal lateinit var iv_votefordown: ImageView
    internal lateinit var iv_paytoparkdown: ImageView
    internal lateinit var iv_payforservicedown: ImageView

    internal lateinit var iv_emailID: ImageView
    internal lateinit var iv_platePhone: ImageView
    internal lateinit var iv_911: ImageView
    internal lateinit var iv_dashEverything: ImageView
    internal lateinit var iv_censes: ImageView

    internal lateinit var iv_annualReport: ImageView
    internal lateinit var tv_remindercount: ImageView
    internal lateinit var iv_city_info: ImageView
    internal lateinit var iv_alertdown: ImageView
    internal lateinit var iv_my_bills: ImageView
    internal lateinit var search_autocomplete: CustomTextView
    internal lateinit var ll_citytabas: LinearLayout
    internal lateinit var btn_pay: Button
    internal lateinit var dialog_list: Dialog
    internal lateinit var dialog_list2: Dialog
    internal lateinit var recycler_id: RecyclerView
    internal lateinit var tv_nodata_id: CustomTextView
    internal lateinit var title_link_stg: String
    internal lateinit var author_link_stg: String
    lateinit var recyclerView_all: RecyclerView
    lateinit var iv_scroll_up: ImageView
    var arrowdownup = false
    var park_new_all = true
    var final_receipt_main_id = ""
    var total_main_balance = ""
    lateinit var shake: Animation
    public var parkingTimesData: java.util.ArrayList<getAllParkingDataResponse> = java.util.ArrayList<getAllParkingDataResponse>()

    internal lateinit var my_loader: Dialog
    var newslistArray = java.util.ArrayList<newsListDataResponse>()
    var newslistPositionArray = java.util.ArrayList<newsListDataResponse>()
    lateinit var rv_newslist: RecyclerView
    lateinit var no_datafound: TextView
    lateinit var newslistmanager: LinearLayoutManager
    var recycle_tabs_name: ArrayList<getUsTabsDataResponse>? = null
    var recycle_tab_items: ArrayList<getUsTabsDataResponse>? = null
    var lastVisibleItemIndex: Int? = null
    var tabsdata_adapter: TabsDataAdapter? = null


    internal lateinit var username_txt_view: CustomTextView
    internal lateinit var user_author_txt_view: CustomTextView
    internal lateinit var tv_user_title: CustomTextViewBold
    internal lateinit var tv_description_news: CustomTextView
    internal lateinit var tv_like_count: CustomTextView
    internal lateinit var iv_user_image: ImageView
    internal lateinit var iv_postimage: ImageView
    internal lateinit var tv_time_status: CustomTextView

    var city_count = 0
    var state_count = 0
    var county_count = 0
    var federal_count = 0
    var all_count = 0


    companion object {

        fun newInstance(): DashBoardFragment {
            return DashBoardFragment()
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = LayoutInflater.from(this.activity).inflate(R.layout.activity_dash_board, null, false)
        //getActivity().setTitle("BI-IL.GOV")
        myloading()
        sessionManager = SessionManager(this.activity!!)
        mServiceView = rootView.findViewById(R.id.rv_dash_list) as RecyclerView
        rv_newslist = rootView.findViewById(R.id.rv_newslist) as RecyclerView
        no_datafound = rootView.findViewById(R.id.no_datafound) as TextView
        recyclerView_all = rootView.findViewById(R.id.recyclerView_all)
        iv_scroll_up = rootView.findViewById(R.id.iv_scroll_up)

        mServiceView!!.isNestedScrollingEnabled = true
        rv_newslist.isNestedScrollingEnabled = true
        recyclerView_all.isNestedScrollingEnabled = true
        noservice = rootView.findViewById(R.id.no_service) as TextView


        tv_city_count = rootView!!.findViewById(R.id.tv_city_count) as CustomTextView
        tv_all_count = rootView!!.findViewById(R.id.tv_all_count) as CustomTextView
        tv_county_count = rootView.findViewById(R.id.tv_county_count) as CustomTextView
        tv_state_count = rootView.findViewById(R.id.tv_state_count) as CustomTextView
        tv_federal_count = rootView.findViewById(R.id.tv_federal_count) as CustomTextView
        tv_remindercount = rootView.findViewById(R.id.tv_remindercount) as ImageView

        tv_addservice = rootView.findViewById(R.id.tv_addservice) as ImageView
        iv_down = rootView.findViewById(R.id.iv_down) as ImageView
        // city_of_water = rootView.findViewById(R.id.city_of_water) as ImageView
        search_autocomplete = rootView.findViewById(R.id.search_autocomplete) as CustomTextView
        ll_citytabas = rootView.findViewById(R.id.ll_citytabas) as LinearLayout

        iv_paytoparkdown = rootView.findViewById(R.id.iv_paytoparkdown) as ImageView
        iv_votefordown = rootView.findViewById(R.id.iv_votefordown) as ImageView
        iv_payforservicedown = rootView.findViewById(R.id.iv_payforservicedown) as ImageView
        iv_alertdown = rootView.findViewById(R.id.iv_alertdown) as ImageView
        iv_my_bills = rootView.findViewById(R.id.iv_my_bills) as ImageView

        iv_emailID = rootView.findViewById(R.id.iv_emailID) as ImageView
        iv_platePhone = rootView.findViewById(R.id.iv_platePhone) as ImageView
        iv_911 = rootView.findViewById(R.id.iv_911) as ImageView
        iv_dashEverything = rootView.findViewById(R.id.iv_dashEverything) as ImageView
        iv_censes = rootView.findViewById(R.id.iv_censes) as ImageView
        iv_annualReport = rootView.findViewById(R.id.iv_annualReport) as ImageView
        iv_city_info = rootView.findViewById(R.id.iv_city_info) as ImageView




        reminder_count = rootView.findViewById(R.id.reminder_count) as TextView



        /*username_txt_view = rootView.findViewById(R.id.username_txt_view) as CustomTextView
        user_author_txt_view = rootView.findViewById(R.id.user_author_txt_view) as CustomTextView
        tv_user_title = rootView.findViewById(R.id.tv_user_title) as CustomTextViewBold
        tv_description_news = rootView.findViewById(R.id.tv_description_news) as CustomTextView*/




        shake = AnimationUtils.loadAnimation(context, R.anim.shake)
        tv_remindercount.startAnimation(shake)

        recycle_tab_items = ArrayList()
        recycle_tabs_name = ArrayList()
        noservice.visibility = View.GONE
        getProfileInfo()
        callgetUsTabsAPI()



        //recycle_tabs_name = resources.getStringArray(R.array.news_list)


        /*val prefsLayoutManager =
                LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)

        recyclerView_all.layoutManager = prefsLayoutManager
        recyclerView_all.setHasFixedSize(true)
        val editPrefsAdapter = EditPrefsAdapter(context!!, recycle_tab_items!!)
        recyclerView_all.adapter = editPrefsAdapter*/

        rv_newslist.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val currentFirstVisible =
                        newslistmanager.findLastCompletelyVisibleItemPosition()
                lastVisibleItemIndex = currentFirstVisible
                Log.d(
                        "scrolllist",
                        "lastVisibleItemIndex listener $lastVisibleItemIndex"
                )
                if ((lastVisibleItemIndex) == (newslistArray.size)) {
                    iv_scroll_up.visibility = View.GONE
                } else {
                    iv_scroll_up.visibility = View.VISIBLE
                }
            }
        })

        iv_scroll_up.setOnClickListener {
            val items_count = rv_newslist.adapter!!.itemCount
            /* lastVisibleItemIndex = newslistmanager.findLastCompletelyVisibleItemPosition()

             if ((lastVisibleItemIndex) == (items_count)) {
                 iv_scroll_up.visibility = View.GONE
             } else {
                 iv_scroll_up.visibility = View.VISIBLE
             }*/

            newslistmanager.smoothScrollToPosition(
                    rv_newslist,
                    null, (0)
                    /*items_count -1*/
            )
            rv_newslist.smoothScrollToPosition(0)
            // newslistmanager.scrollToPositionWithOffset(0,0)

        }




        iv_down.setOnClickListener(View.OnClickListener {

            Log.d("arrow", "" + arrowdownup)
            if (arrowdownup == false) {
                Log.d("arrow1", "" + arrowdownup)


                tv_city_count!!.visibility = View.VISIBLE
                tv_all_count!!.visibility = View.VISIBLE
                tv_county_count!!.visibility = View.VISIBLE
                tv_state_count!!.visibility = View.VISIBLE
                tv_federal_count!!.visibility = View.VISIBLE
                mServiceView!!.visibility = View.VISIBLE
                no_datafound!!.visibility = View.GONE
                ll_citytabas.visibility = View.VISIBLE
                iv_scroll_up.visibility = View.GONE
                iv_down.setImageResource(R.drawable.up_arrow)
                recyclerView_all.visibility = View.GONE
                rv_newslist.visibility = View.GONE
                arrowdownup = true
            } else {
                Log.d("arrow2", "" + arrowdownup)
                tv_city_count!!.visibility = View.GONE
                tv_all_count!!.visibility = View.GONE
                tv_county_count!!.visibility = View.GONE
                tv_state_count!!.visibility = View.GONE
                tv_federal_count!!.visibility = View.GONE
                mServiceView!!.visibility = View.GONE
                ll_citytabas.visibility = View.GONE

                iv_down.setImageResource(R.drawable.down_arrow)
                recyclerView_all.visibility = View.VISIBLE
                rv_newslist.visibility = View.VISIBLE
                arrowdownup = false
            }

        })



        iv_emailID.setOnClickListener(View.OnClickListener {

            val intent = Intent(this.activity, ComingSoonActivity::class.java)
            intent.putExtra("email", "email")
            startActivity(intent)

        })
        iv_platePhone.setOnClickListener(View.OnClickListener {

            val intent = Intent(this.activity, ComingSoonActivity::class.java)
            intent.putExtra("email", "platephone")
            startActivity(intent)

        })
        iv_911.setOnClickListener(View.OnClickListener {

            val intent = Intent(this.activity, ComingSoonActivity::class.java)
            intent.putExtra("email", "911")
            startActivity(intent)

        })
        iv_dashEverything.setOnClickListener(View.OnClickListener {

            val intent = Intent(this.activity, ComingSoonActivity::class.java)
            intent.putExtra("email", "everything")
            startActivity(intent)

        })
        iv_censes.setOnClickListener(View.OnClickListener {

            val intent = Intent(this.activity, ComingSoonActivity::class.java)
            intent.putExtra("email", "censes")
            startActivity(intent)

        })
        iv_annualReport.setOnClickListener(View.OnClickListener {

            val intent = Intent(this.activity, ComingSoonActivity::class.java)
            intent.putExtra("email", "annualreport")
            startActivity(intent)

        })

        iv_city_info.setOnClickListener(View.OnClickListener {

            val intent = Intent(this.activity, CityInfoActivity::class.java)
            startActivity(intent)

        })


        iv_votefordown.setOnClickListener(View.OnClickListener {
            val intent = Intent(this.activity, IVoteForActivity::class.java)
            startActivity(intent)

        })


        /*city_of_water.setOnClickListener(View.OnClickListener {
            val intent = Intent(this.activity, NewsFeedList::class.java)
            intent.putExtra("form_screen","city_of_water")
            startActivity(intent)

        })*/







        dialog_list = Dialog(activity)
        dialog_list.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list.setContentView(R.layout.dialog_recycler_view)
        dialog_list.setCancelable(true)
        val tv_main_parking_id = dialog_list.findViewById(R.id.tv_main_parking_id) as LinearLayout
        val tv_new_parking_id = dialog_list.findViewById(R.id.tv_new_parking_id) as LinearLayout
        val iv_dialog_down = dialog_list.findViewById(R.id.iv_dialog_down) as ImageView
        tv_nodata_id = dialog_list.findViewById(R.id.tv_nodata_id) as CustomTextView
        recycler_id = dialog_list.findViewById(R.id.recycler_id) as RecyclerView
        iv_paytoparkdown.setOnClickListener {
            if (!dialog_list.isShowing())
                dialog_list.show()
        }



        dialog_list2 = Dialog(activity)
        dialog_list2.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list2.setContentView(R.layout.dialog_payforservices)
        dialog_list2.setCancelable(true)
        val iv_payforserviceaddbillsarrow = dialog_list2.findViewById(R.id.iv_payforserviceaddbillsarrow) as LinearLayout
        val iv_payforservicemybillsarrow = dialog_list2.findViewById(R.id.iv_payforservicemybillsarrow) as LinearLayout
        iv_payforservicedown.setOnClickListener {
            if (!dialog_list2.isShowing())
                dialog_list2.show()
        }


        iv_payforserviceaddbillsarrow.setOnClickListener(View.OnClickListener {

            val intent = Intent(this.activity, PayForServicesActivity::class.java)
            startActivity(intent)

        })

        iv_payforservicemybillsarrow.setOnClickListener(View.OnClickListener {
            val intent = Intent(this.activity, MyBillsActivity::class.java)
            startActivity(intent)
        })

        /*  iv_paytoparkdown.setOnClickListener(View.OnClickListener {

              val intent = Intent(this.activity, PaytoParkActivty::class.java)
              startActivity(intent)

          })*/
        callgetAllParkingsAPI()
        tv_main_parking_id.setOnClickListener {
            if (park_new_all) {
                park_new_all = false
                recycler_id.visibility = View.VISIBLE
                iv_dialog_down.setImageResource(R.drawable.up_arrow)
            } else {
                park_new_all = true
                recycler_id.visibility = View.GONE
                iv_dialog_down.setImageResource(R.drawable.downarrow)
            }

        }
        tv_new_parking_id.setOnClickListener {
            dialog_list.dismiss()
            val intent = Intent(this.activity, PaytoParkActivty::class.java)
            startActivity(intent)
        }


        /*iv_votefordown.setOnClickListener(View.OnClickListener {

            Log.d("arrow", "" + arrowdownup)
            if (arrowdownup == false) {
                Log.d("arrow1", "" + arrowdownup)
                tv_city_count!!.visibility = View.GONE
                tv_county_count!!.visibility = View.GONE
                tv_state_count!!.visibility = View.GONE
                tv_federal_count!!.visibility = View.GONE
                mServiceView!!.visibility = View.GONE
                ll_citytabas.visibility = View.GONE
                noservice.visibility = View.GONE
                arrowdownup = true
            } else {
                Log.d("arrow2", "" + arrowdownup)
                tv_city_count!!.visibility = View.GONE
                tv_county_count!!.visibility = View.GONE
                tv_state_count!!.visibility = View.GONE
                tv_federal_count!!.visibility = View.GONE
                mServiceView!!.visibility = View.GONE
                ll_citytabas.visibility = View.GONE
                arrowdownup = false
                noservice.visibility = View.GONE
            }

        })*/


        iv_alertdown.setOnClickListener(View.OnClickListener {

            Log.d("arrow", "" + arrowdownup)
            if (arrowdownup == false) {
                Log.d("arrow1", "" + arrowdownup)
                tv_city_count!!.visibility = View.GONE
                tv_all_count!!.visibility = View.GONE
                tv_county_count!!.visibility = View.GONE
                tv_state_count!!.visibility = View.GONE
                tv_federal_count!!.visibility = View.GONE
                mServiceView!!.visibility = View.GONE
                no_datafound!!.visibility = View.GONE
                ll_citytabas.visibility = View.GONE
                noservice.visibility = View.GONE


                arrowdownup = true
            } else {
                Log.d("arrow2", "" + arrowdownup)
                tv_city_count!!.visibility = View.GONE
                tv_all_count!!.visibility = View.GONE
                tv_county_count!!.visibility = View.GONE
                tv_state_count!!.visibility = View.GONE
                tv_federal_count!!.visibility = View.GONE
                mServiceView!!.visibility = View.GONE
                ll_citytabas.visibility = View.GONE
                arrowdownup = false
                noservice.visibility = View.GONE
            }

        })


        iv_my_bills.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, PaymentHistory::class.java)
            startActivity(intent)

        })
        tv_addservice.setOnClickListener(View.OnClickListener {
            val intent = Intent(this.activity, AddServiceActivity::class.java)
            startActivity(intent)

        })

        search_autocomplete.setOnClickListener(View.OnClickListener {
            val intent = Intent(this.activity, SearchActivity::class.java)
            startActivity(intent)

        })






        if (ConnectionManager.checkConnection(this.activity!!)) {
            try {

                callServiceDetails()
            } catch (e: Exception) {
                e.printStackTrace()
                Log.w("Exceptiom", "" + e.toString())
            }

        } else {
            Toast.makeText(this.activity, resources.getString(R.string.network_error), Toast.LENGTH_SHORT).show()
        }

        if (ConnectionManager.checkConnection(this.activity!!)) {
            try {
                callReminderCount()

            } catch (e: Exception) {
                e.printStackTrace()
                Log.w("Exceptiom", "" + e.toString())
            }

        } else {
            Toast.makeText(this.activity, resources.getString(R.string.network_error), Toast.LENGTH_SHORT).show()
        }


        tv_state_count.setBackgroundResource(R.drawable.border_button_state_rem)
        tv_state_count.setTextColor(resources.getColor(R.color.dashboard_state))
        tv_city_count.setBackgroundResource(R.drawable.border_button_city_rem)
        tv_city_count.setTextColor(resources.getColor(R.color.dashboard_city))
        tv_county_count.setBackgroundResource(R.drawable.border_button_county_rem)
        tv_county_count.setTextColor(resources.getColor(R.color.dashboard_county))
        tv_all_count.setBackgroundResource(R.drawable.border_button_federal_rem)
        tv_all_count.setTextColor(resources.getColor(R.color.dashboard_federal))
        tv_federal_count.setBackgroundResource(R.drawable.border_button_federal_rem)
        tv_federal_count.setTextColor(resources.getColor(R.color.dashboard_federal))

        tv_city_count.setOnClickListener {

            tv_city_count.setBackgroundResource(R.drawable.border_button_city_sel_rem)
            tv_city_count.setTextColor(resources.getColor(R.color.white))

            tv_state_count.setBackgroundResource(R.drawable.border_button_state_rem)
            tv_state_count.setTextColor(resources.getColor(R.color.dashboard_state))
            tv_county_count.setBackgroundResource(R.drawable.border_button_county_rem)
            tv_county_count.setTextColor(resources.getColor(R.color.dashboard_county))
            tv_federal_count.setBackgroundResource(R.drawable.border_button_federal_rem)
            tv_federal_count.setTextColor(resources.getColor(R.color.dashboard_federal))
            tv_all_count.setBackgroundResource(R.drawable.border_button_federal_rem)
            tv_all_count.setTextColor(resources.getColor(R.color.dashboard_federal))

            Log.w("city_CITYYYYYY", "CITY")
            mServiceInfo_temp.clear()
            noservice!!.visibility = View.GONE
            if (mServiceInfo.size > 0) {
                for (i in mServiceInfo.indices) {
                    if (mServiceInfo.get(i).name.equals("City")) {
                        mServiceInfo_temp.add(mServiceInfo.get(i))
                        setProductAdapter(mServiceInfo_temp)
                        Log.w("city_size", "::" + mServiceInfo_temp.size)
                        //  tv_city_count.setText(resources.getString(R.string.city)+ mServiceInfo_temp.size)
                    }
                }
                if (mServiceInfo_temp.size == 0) {
                    setProductAdapter(mServiceInfo_temp)
                    noservice.text = "No City Found"
                    noservice!!.visibility = View.VISIBLE
                }
            }
        }




        tv_all_count.setOnClickListener {

            tv_all_count.setTextColor(resources.getColor(R.color.white))
            tv_all_count.setBackgroundResource(R.drawable.border_button_federal_sel_rem)

            tv_city_count.setBackgroundResource(R.drawable.border_button_city_rem)
            tv_city_count.setTextColor(resources.getColor(R.color.dashboard_city))
            tv_state_count.setBackgroundResource(R.drawable.border_button_state_rem)
            tv_state_count.setTextColor(resources.getColor(R.color.dashboard_state))
            tv_county_count.setBackgroundResource(R.drawable.border_button_county_rem)
            tv_county_count.setTextColor(resources.getColor(R.color.dashboard_county))
            tv_federal_count.setBackgroundResource(R.drawable.border_button_federal_rem)
            tv_federal_count.setTextColor(resources.getColor(R.color.dashboard_federal))



            Log.w("city_CITYYYYYY", "ALL")
            mServiceInfo_temp.clear()
            noservice!!.visibility = View.GONE
            if (mServiceInfo.size > 0) {
                for (i in mServiceInfo.indices) {
                        mServiceInfo_temp.add(mServiceInfo.get(i))
                        setProductAdapter(mServiceInfo_temp)
                        Log.w("city_size", "::" + mServiceInfo_temp.size)
                        //  tv_city_count.setText(resources.getString(R.string.city)+ mServiceInfo_temp.size)

                }
                if (mServiceInfo_temp.size == 0) {
                    setProductAdapter(mServiceInfo_temp)
                    noservice.text = "No News Found"
                    noservice!!.visibility = View.VISIBLE
                }
            }
        }

        tv_state_count.setOnClickListener {

            tv_state_count.setBackgroundResource(R.drawable.border_button_state_sel_rem)
            tv_state_count.setTextColor(resources.getColor(R.color.white))

            tv_city_count.setBackgroundResource(R.drawable.border_button_city_rem)
            tv_city_count.setTextColor(resources.getColor(R.color.dashboard_city))
            tv_county_count.setBackgroundResource(R.drawable.border_button_county_rem)
            tv_county_count.setTextColor(resources.getColor(R.color.dashboard_county))
            tv_federal_count.setBackgroundResource(R.drawable.border_button_federal_rem)
            tv_federal_count.setTextColor(resources.getColor(R.color.dashboard_federal))
            tv_all_count.setBackgroundResource(R.drawable.border_button_federal_rem)
            tv_all_count.setTextColor(resources.getColor(R.color.dashboard_federal))


            Log.w("city_CITYYYYYY", "STATE")
            mServiceInfo_temp.clear()
            noservice!!.visibility = View.GONE
            if (mServiceInfo.size > 0) {
                for (i in mServiceInfo.indices) {
                    if (mServiceInfo.get(i).name.equals("State")) {
                        mServiceInfo_temp.add(mServiceInfo.get(i))
                        setProductAdapter(mServiceInfo_temp)
                        Log.w("city_size", "::" + mServiceInfo_temp.size)
                        //tv_state_count.setText(resources.getString(R.string.state)+ mServiceInfo_temp.size)
                    }
                }
                if (mServiceInfo_temp.size == 0) {
                    setProductAdapter(mServiceInfo_temp)
                    noservice.text = "No State Found"
                    noservice!!.visibility = View.VISIBLE
                }
            }

        }
        tv_county_count.setOnClickListener {
            tv_county_count.setBackgroundResource(R.drawable.border_button_county_sel_rem)
            tv_county_count.setTextColor(resources.getColor(R.color.white))

            tv_state_count.setBackgroundResource(R.drawable.border_button_state_rem)
            tv_state_count.setTextColor(resources.getColor(R.color.dashboard_state))
            tv_city_count.setBackgroundResource(R.drawable.border_button_city_rem)
            tv_city_count.setTextColor(resources.getColor(R.color.dashboard_city))
            tv_federal_count.setBackgroundResource(R.drawable.border_button_federal_rem)
            tv_federal_count.setTextColor(resources.getColor(R.color.dashboard_federal))
            tv_all_count.setBackgroundResource(R.drawable.border_button_federal_rem)
            tv_all_count.setTextColor(resources.getColor(R.color.dashboard_federal))



            Log.w("city_CITYYYYYY", "COUNTY")
            mServiceInfo_temp.clear()
            noservice!!.visibility = View.GONE
            if (mServiceInfo.size > 0) {
                for (i in mServiceInfo.indices) {
                    if (mServiceInfo.get(i).name.equals("County")) {
                        mServiceInfo_temp.add(mServiceInfo.get(i))
                        setProductAdapter(mServiceInfo_temp)
                        Log.w("city_size", "::" + mServiceInfo_temp.size)
                        // tv_county_count.setText(resources.getString(R.string.country)+ mServiceInfo_temp.size)
                    }
                }
                if (mServiceInfo_temp.size == 0) {
                    setProductAdapter(mServiceInfo_temp)
                    noservice.text = "No County Found"
                    noservice!!.visibility = View.VISIBLE
                }
            }

        }
        tv_federal_count.setOnClickListener {

            tv_federal_count.setBackgroundResource(R.drawable.border_button_federal_sel_rem)
            tv_federal_count.setTextColor(resources.getColor(R.color.white))

            tv_state_count.setBackgroundResource(R.drawable.border_button_state_rem)
            tv_state_count.setTextColor(resources.getColor(R.color.dashboard_state))
            tv_city_count.setBackgroundResource(R.drawable.border_button_city_rem)
            tv_city_count.setTextColor(resources.getColor(R.color.dashboard_city))
            tv_county_count.setBackgroundResource(R.drawable.border_button_county_rem)
            tv_county_count.setTextColor(resources.getColor(R.color.dashboard_county))
            tv_all_count.setBackgroundResource(R.drawable.border_button_federal_rem)
            tv_all_count.setTextColor(resources.getColor(R.color.dashboard_federal))

            Log.w("city_CITYYYYYY", "FEDERAL")
            mServiceInfo_temp.clear()
            noservice!!.visibility = View.GONE
            if (mServiceInfo.size > 0) {
                for (i in mServiceInfo.indices) {
                    if (mServiceInfo.get(i).name.equals("Federal")) {
                        mServiceInfo_temp.add(mServiceInfo.get(i))
                        setProductAdapter(mServiceInfo_temp)
                        Log.w("city_size", "::" + mServiceInfo_temp.size)
                        // tv_federal_count.setText(resources.getString(R.string.federal)+ mServiceInfo_temp.size)
                    }
                }
                if (mServiceInfo_temp.size == 0) {
                    setProductAdapter(mServiceInfo_temp)
                    noservice.text = "No Federal Found"
                    noservice!!.visibility = View.VISIBLE
                }

            }
        }
        return rootView

    }

    private fun myloading() {
        my_loader = Dialog(activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun callServiceDetails() {
         my_loader.show()
        val apiService = ApiInterface.create()

        val call = apiService.getDashboardList(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<AllServicesByUserResponse> {
            override fun onResponse(call: Call<AllServicesByUserResponse>, response: retrofit2.Response<AllServicesByUserResponse>?) {
                try {
                    my_loader.dismiss()
                    if (response != null && response.body()!!.status.equals("1")) {
                        if (response.body()!!.newslist != null) {
                            val list: Array<AllServicesByUserInfo> = response.body()!!.newslist!!
                            for (item: AllServicesByUserInfo in list.iterator()) {
                                mServiceInfo.add(item)
                               // setProductAdapter(mServiceInfo)
                                //setdataDepartmentAdapter(mServiceInfo)

                                if(item.name.equals("City")){
                                    city_count++
                                }
                                if(item.name.equals("County")){
                                    county_count++
                                }
                                if(item.name.equals("State")){
                                    state_count++
                                }
                                if(item.name.equals("Federal")){
                                    federal_count++
                                }
                            }

                            mServiceView!!.setHasFixedSize(true)
                            val layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false);
                            mServiceView!!.setLayoutManager(layoutManager)
                            mServiceView!!.setItemAnimator(DefaultItemAnimator())
                            val dash_adapter = ServiceDetailsRecyclerAdapter(mServiceInfo, context!!)
                            mServiceView!!.setAdapter(dash_adapter)
                            dash_adapter.notifyDataSetChanged()


                            Log.e("COUNTCITY",""+city_count)
                            tv_city_count.setText(resources.getString(R.string.city) + city_count.toString())
                            tv_state_count.setText(resources.getString(R.string.state) + state_count.toString())
                            tv_county_count.setText(resources.getString(R.string.country) + county_count.toString())
                            tv_federal_count.setText(resources.getString(R.string.federal) + federal_count.toString())
                            tv_all_count.setText(resources.getString(R.string.All) + mServiceInfo.size.toString())
                            if (mServiceInfo.size == 0) {
                                //noservice!!.visibility = View.VISIBLE
                                // mServiceView!!.visibility = View.GONE
                            }

                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<AllServicesByUserResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun callReminderCount() {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getServicesCount(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UpdateProfileResponse> {
            override fun onResponse(call: Call<UpdateProfileResponse>, response: retrofit2.Response<UpdateProfileResponse>?) {
                try {
                    my_loader.dismiss()
                    if (response != null && response.body()!!.status.equals("1")) {
                        if (response.body()!!.count != null) {
                            val count = response.body()!!.count!!
                            reminder_count.setText(count)

                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<UpdateProfileResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun setcount(count: String) {
        tv_city_count.setText(resources.getString(R.string.city) + count)
        tv_state_count.setText(resources.getString(R.string.state) + count)
        tv_federal_count.setText(resources.getString(R.string.federal) + count)
        tv_county_count.setText(resources.getString(R.string.country) + count)
    }

    private fun setProductAdapter(mServiceList: ArrayList<AllServicesByUserInfo>) {
        mServiceDetailsAdapter = ServiceDetailsRecyclerAdapter(mServiceList, context!!)
        mServiceView!!.adapter = mServiceDetailsAdapter
        mServiceDetailsAdapter!!.notifyDataSetChanged()
    }

    fun closeAPP() {
        activity!!.finish()
    }

    private fun callgetAllParkingsAPI() {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAllParkings(sessionManager.isId)
        Log.d("REQUEST", call.toString() + sessionManager.isId)
        parkingTimesData.clear()
        call.enqueue(object : Callback<getAllParkingResponse> {
            override fun onResponse(call: Call<getAllParkingResponse>, response: retrofit2.Response<getAllParkingResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        tv_nodata_id.visibility = View.GONE
                        my_loader.dismiss()
                        if (response.body()!!.data != null) {
                            val list: Array<getAllParkingDataResponse> = response.body()!!.data!!
                            for (item: getAllParkingDataResponse in list.iterator()) {
                                parkingTimesData.add(item)
                            }

                            val details_adapter = MapDetailsAdapter(parkingTimesData, activity!!)
                            recycler_id.setAdapter(details_adapter)
                            details_adapter.notifyDataSetChanged()
                            recycler_id.setHasFixedSize(true)
                            recycler_id.setLayoutManager(LinearLayoutManager(activity))
                            recycler_id.setItemAnimator(DefaultItemAnimator())

                        }

                    } else if (response.body()!!.status.equals("2")) {
                        tv_nodata_id.visibility = View.VISIBLE
                    }

                }
            }

            override fun onFailure(call: Call<getAllParkingResponse>, t: Throwable) {
                my_loader.dismiss()
                Log.w("Result_Address_Profile", t.toString())
            }


        })
    }

    private fun CallgetParkingData() {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getParkingData(final_receipt_main_id)
        Log.e("final_receipt_LL", final_receipt_main_id)
        call.enqueue(object : Callback<getParkingDataResponse> {
            override fun onResponse(call: Call<getParkingDataResponse>, response: retrofit2.Response<getParkingDataResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        my_loader.dismiss()
                        //  if (response.body()!!.data != null) {
                        val list: parkingDataResponse = response.body()!!.data!!

                        Log.e("final_receipt_id", final_receipt_main_id + "--" + list.remain_hour + "--" + response.body()!!.data!!.remain_min + "--" + list!!.zone_id)
                        val intent = Intent(activity, TimerActivity::class.java)
                        intent.putExtra("timer_hrs", list.remain_min)
                        intent.putExtra("receipt_id", list.receipt_id)
                        intent.putExtra("user_id", sessionManager.isId)
                        intent.putExtra("no_plate", "")
                        intent.putExtra("state", "")
                        intent.putExtra("vehicle_type", "")
                        intent.putExtra("zone_rate_id", "")
                        intent.putExtra("lot_rate_id", "")
                        intent.putExtra("parkig_start_date", "")
                        intent.putExtra("parkig_end_date", "")
                        intent.putExtra("zone_id", list!!.zone_id)
                        intent.putExtra("lot_id", list!!.lot_id)
                        intent.putExtra("from", "all_current")
                        intent.putExtra("total_main_balance", total_main_balance)
                        startActivity(intent)

                        // }

                    } else if (response.body()!!.status.equals("2")) {
                        my_loader.dismiss()

                    }

                }
            }

            override fun onFailure(call: Call<getParkingDataResponse>, t: Throwable) {
                my_loader.dismiss()
                Log.w("Result_Address_Profile", t.toString())
            }

        })
    }

    private fun getProfileInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getUserProfile(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UserProfileResponse> {
            override fun onResponse(call: Call<UserProfileResponse>, response: retrofit2.Response<UserProfileResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    // Log.w("Result_Address_Profile","Result : "+response.body()!!.user_info)
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            Log.w("Result_Address_Profile", "Result : " + response.body()!!.user_info)
                            total_main_balance = response.body()!!.user_info!!.wallet_amount!!

                            sessionManager.wallet_update(total_main_balance)
                        }

                    }

                }
            }

            override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_main_parking_list_id = view.findViewById<CustomTextView>(R.id.tv_main_parking_list_id)
        val ll_current_parking_timing = view.findViewById<LinearLayout>(R.id.ll_current_parking_timing)
        // var userSelected = true;
    }

    inner class MapDetailsAdapter(val title: java.util.ArrayList<getAllParkingDataResponse>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_paytopark_timers, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.tv_main_parking_list_id.setText(title.get(position).no_plate)
            holder.ll_current_parking_timing.setOnClickListener {
                final_receipt_main_id = title.get(position).receipt_id!!
                CallgetParkingData()
                dialog_list.dismiss()
            }
        }

        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return title.size
        }
    }


    private fun getNewsListAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getNewsList(sessionManager.isId)
        Log.d("NEWSLIST", sessionManager.isId)
        newslistArray.clear()
        call.enqueue(object : Callback<getNewsListResponse> {
            override fun onResponse(call: Call<getNewsListResponse>, response: retrofit2.Response<getNewsListResponse>?) {

                try {
                    if (response != null) {
                        my_loader.dismiss()
                        if (response.body()!!.status.equals("1")) {
                            Log.w("NEWSLIST_RESPONSE_STATUS", "Result : " + response.body()!!.status)
                            if (response.body()!!.newslist != null) {
                                Log.w("NEWSLIST_RESPONSE_RESULT", "Result : " + response.body()!!.newslist)
                                // setProfileDatatoView(response.body()!!.user_info)
                                val list: Array<newsListDataResponse>? = response.body()!!.newslist!!
                                for (item: newsListDataResponse in list!!.iterator()) {
                                    newslistArray.add(item)
                                }

                                newslistPositionArray = ArrayList()
                                tabsdata_adapter!!.setSelectedIndex(0)

                                for(i in 0 until  newslistArray.size){
                                    if (newslistArray[i].title!!.equals(recycle_tabs_name!![0].title.toString())) {
                                        newslistPositionArray.add(newslistArray[i])

                                        val news_list_adapter = NewsDetailsRecyclerAdapter(newslistPositionArray, activity!!)
                                        rv_newslist!!.setAdapter(news_list_adapter)
                                        news_list_adapter!!.notifyDataSetChanged()
                                        rv_newslist!!.setHasFixedSize(true)
                                        rv_newslist!!.setItemAnimator(DefaultItemAnimator())

                                    }
                                }

                                Log.w("NEWSLISTARRAYSIZE", "Result : " + newslistArray.size)
                                newslistmanager =
                                        LinearLayoutManager(context, LinearLayout.VERTICAL, false)
                                rv_newslist!!.setLayoutManager(newslistmanager)


                                /* val news_list_adapter = NewsDetailsRecyclerAdapter(newslistArray, activity!!)
                                 rv_newslist!!.setAdapter(news_list_adapter)
                                 news_list_adapter!!.notifyDataSetChanged()
                                 rv_newslist!!.setHasFixedSize(true)
                                 rv_newslist!!.setItemAnimator(DefaultItemAnimator())*/

                                Log.d("NEWSARRAYSIZE", newslistArray.size.toString())


                                if (newslistArray.size == 0) {
                                    Log.d("NEWSARRAYSIZE0", "NODATAFOUND")
                                    noservice!!.visibility = View.VISIBLE
                                    rv_newslist!!.visibility = View.GONE
                                }
                            }

                        } else {
                            Toast.makeText(context, "Response Failed", Toast.LENGTH_SHORT).show()
                        }

                    } else {
                        Toast.makeText(context, "Response Failed", Toast.LENGTH_SHORT).show()
                    }

                } catch (e: java.lang.Exception) {
                    Log.w("Result_Profile_Exception", e.toString())
                }
            }

            override fun onFailure(call: Call<getNewsListResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }

        })
    }

    inner class NewsDetailsRecyclerAdapter(val mServiceList: ArrayList<newsListDataResponse>, val context: Context) : RecyclerView.Adapter<ViewHolder2>() {


        internal lateinit var my_loader: Dialog
        internal lateinit var sessionManager: SessionManager
        var arrowdownup = false
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder2 {
            return ViewHolder2(LayoutInflater.from(context).inflate(R.layout.dashboard_news_details_adapter_layout, parent, false))

        }

        override fun onBindViewHolder(holder: ViewHolder2, position: Int) {


            sessionManager = SessionManager(context)
            myloading()

            holder?.username_txt_view.text = mServiceList.get(position).posted_by
            holder?.user_author_txt_view.text = mServiceList.get(position).author_profession
            holder?.tv_user_title.text = mServiceList.get(position).title
            // holder?.tv_description_news.text = mServiceList.get(position).news
            holder?.tv_like_count.text = mServiceList.get(position).likes_count


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder?.tv_description_news.setText(Html.fromHtml(mServiceList.get(position).news, Html.FROM_HTML_MODE_COMPACT))
            } else {
                holder?.tv_description_news.setText(Html.fromHtml(mServiceList.get(position).news))
            }

            Log.e("NEWSARRAYSIZE_ADAPTER", "" + mServiceList.size);

            if (mServiceList.get(position).like_status.equals("1")) {

                holder?.iv_like_status.setImageDrawable(context.resources.getDrawable(R.drawable.like))

            } else {
                holder?.iv_like_status.setImageDrawable(context.resources.getDrawable(R.drawable.unlike))
            }


            Picasso.with(context)
                    .load("https://www.i1gov.com/uploads/news/" + mServiceList.get(position).image)
                    .into(holder?.iv_postimage)



            Picasso.with(context)
                    .load("https://www.i1gov.com/uploads/news/" + mServiceList.get(position).posted_by_image)
                    .error(R.drawable.user_icon)
                    .into(holder?.iv_user_image)


            val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

            val currentDateandTime = format.format(Date())

            var d1: Date? = null
            var d2: Date? = null

            try {
                d1 = format.parse(currentDateandTime)
                d2 = format.parse(mServiceList.get(position).created_date)

                //in milliseconds
                val diff = d1!!.time - d2!!.time

                val diffSeconds = diff / 1000 % 60
                val diffMinutes = diff / (60 * 1000) % 60
                val diffHours = diff / (60 * 60 * 1000) % 24
                val diffDays = diff / (24 * 60 * 60 * 1000)

                Log.e("FINALNEWSTIME", "DAYS" + diffDays + "HOURS" + diffHours + "MIN" + diffMinutes + "SEC" + diffSeconds);

                holder?.tv_time_status.setText(diffHours.toString() + "hrs" + diffMinutes.toString() + "min")
                /* if(diffHours.toString()!=null){
                     holder?.tv_time_status.setText(diffHours.toString() + "hrs ago")
                 }else if(diffMinutes.toString()!=null){
                     holder?.tv_time_status.setText(diffMinutes.toString() + "min")
                 }else if(diffSeconds.toString()!=null){
                     holder?.tv_time_status.setText(diffSeconds.toString() + "sec")
                 }*/
                //holder?.tv_time_status.setText(diffHours.toString() + "hrs" + diffMinutes.toString()+ "min" )


            } catch (e: Exception) {
                e.printStackTrace()
            }

            holder?.iv_postimage.setOnClickListener {
                val intent = Intent(context, DashboardPostDetailViewActivity::class.java)
                intent.putExtra("news_id", mServiceList.get(position).id)
                intent.putExtra("title_link", mServiceList.get(position).title_link)
                intent.putExtra("author_link", mServiceList.get(position).author_link)
                intent.putExtra("title_name", mServiceList.get(position).title)
                context.startActivity(intent)


            }


            holder?.tv_user_title.setOnClickListener {
                if (mServiceList.get(position).title_link == "" || mServiceList.get(position).title_link == null || mServiceList.get(position).title_link == "null") {

                } else {
                    val intent = Intent(context, NewsTitleWebActivity::class.java)
                    intent.putExtra("title_link", mServiceList.get(position).title_link)
                    intent.putExtra("title_name", mServiceList.get(position).title)
                    context.startActivity(intent)
                }

            }

            holder?.username_txt_view.setOnClickListener {
                if (mServiceList.get(position).author_link == "" || mServiceList.get(position).author_link == null || mServiceList.get(position).author_link == "null") {

                } else {
                    val intent = Intent(context, NewsTitleWebActivity::class.java)
                    intent.putExtra("title_link", mServiceList.get(position).author_link)
                    intent.putExtra("title_name", mServiceList.get(position).title)
                    context.startActivity(intent)
                }
            }

            /* holder?.tv_user_title.setOnClickListener {
                 val intent = Intent(context, DashboardPostDetailViewActivity::class.java)
                 intent.putExtra("news_id", mServiceList.get(position).id)
                 context.startActivity(intent)


             }
 */
            holder?.iv_like_status.setOnClickListener {
                getNewsLikeStatusAPI(sessionManager.isId, mServiceList.get(position).id.toString())

            }

            holder?.iv_news_share.setOnClickListener {
                whatsApp_sendMsg(mServiceList.get(position).title.toString(), "https://www.i1gov.com/uploads/news/" + mServiceList.get(position).image)
                // messenger_sendMsg("Bi-il.gov")

                //shareImageWhatsApp()
            }


            holder?.ll_comment.setOnClickListener {
                val intent = Intent(context, DashboardPostDetailViewActivity::class.java)
                intent.putExtra("news_id", mServiceList.get(position).id)
                intent.putExtra("title_link", mServiceList.get(position).title_link)
                intent.putExtra("author_link", mServiceList.get(position).author_link)
                intent.putExtra("title_name", mServiceList.get(position).title)
                context.startActivity(intent)


            }


        }

        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return mServiceList.size
        }


        private fun myloading() {
            my_loader = Dialog(context)
            my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
            my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            my_loader.setCancelable(false);
            my_loader.setContentView(R.layout.mkloader_dialog)
        }

        private fun getNewsLikeStatusAPI(user_id: String, news_id: String) {
            my_loader.show()
            val apiService = ApiInterface.create()
            val call = apiService.getNewsLikeStatus(user_id, news_id)
            Log.d("NEWSLIKEPARAMS", sessionManager.isId + "-------" + news_id)
            call.enqueue(object : Callback<getNewsListResponse> {
                override fun onResponse(call: Call<getNewsListResponse>, response: retrofit2.Response<getNewsListResponse>?) {
                    if (response != null) {
                        my_loader.dismiss()
                        Log.w("NEWSLIKESTATUS_RESPONSE_STATUS", "Result : " + response.body()!!.status)
                        if (response.body()!!.status.equals("1")) {

                            getNewsListAPI()
                            notifyDataSetChanged()
                            //  holder?.iv_like_status.setImageDrawable(context.resources.getDrawable(R.drawable.like))
                            /* val intent = Intent(context, MainActivity::class.java)
                             context.startActivity(intent)*/


                        } else if (response.body()!!.status.equals("2")) {
                            //holder?.iv_like_status.setImageDrawable(context.resources.getDrawable(R.drawable.unlike))
                            getNewsListAPI()
                            notifyDataSetChanged()
                            /* val intent = Intent(context, MainActivity::class.java)
                             context.startActivity(intent)*/
                        }

                    }
                }

                override fun onFailure(call: Call<getNewsListResponse>, t: Throwable) {
                    Log.w("Result_Address_Profile", t.toString())
                }

            })
        }


/*String text =  + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(R.string.invite_earn_label_share_messgae2) + " " + referral_code + " to ride free.Enjoy!" + "https://play.google.com/store/apps/details?id=" + package_name + "&hl=en";*/

        private fun whatsApp_sendMsg(text: String, s: String) {
            val pm = context.getPackageManager()
            val shareIntent = Intent();
            shareIntent.setType("image/*");
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.setAction(Intent.ACTION_SEND);
            //without the below line intent will show error
            shareIntent.setType("text/plain")
            shareIntent.putExtra(Intent.EXTRA_TEXT, text + "\n" + s)
            // Target whatsapp:
            Intent.createChooser(shareIntent, "Share via")
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivity(shareIntent);


        }


        fun shareImageWhatsApp() {

            val adv = BitmapFactory.decodeResource(getResources(), R.drawable.ic_cvc);


            val share = Intent(Intent.ACTION_SEND);
            share.setType("image/*");
            val bytes = ByteArrayOutputStream();
            adv.compress(Bitmap.CompressFormat.PNG, 100, bytes);

            val f = File(Environment.getExternalStorageDirectory().toString() + File.separator + "temporary_file.jpg");
            try {
                f.createNewFile();
                FileOutputStream(f).write(bytes.toByteArray());
            } catch (e: IOException) {
                e.printStackTrace();
            }
            share.putExtra(Intent.EXTRA_STREAM,
                    Uri.parse(Environment.getExternalStorageDirectory().toString() + File.separator + "temporary_file.jpg"));

            share.setPackage("com.whatsapp");
            startActivity(Intent.createChooser(share, "Share Image"));


        }


        /*private fun messenger_sendMsg(text: String) {
            val pm = context.getPackageManager()
            try {
                val waIntent = Intent(Intent.ACTION_SEND)
                waIntent.type = "text/plain"
                val info = pm.getPackageInfo("com.facebook.orca", PackageManager.GET_META_DATA)  //Check if package exists or not. If not then codein catch block will be called
                waIntent.setPackage("com.facebook.orca")
                waIntent.putExtra(Intent.EXTRA_TEXT, text)
                context.startActivity(Intent.createChooser(waIntent, "Share with"))
            } catch (e: PackageManager.NameNotFoundException) {

            }

        }*/

        // Inflates the item views

    }


    class ViewHolder2(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val username_txt_view = view.findViewById(R.id.username_txt_view) as CustomTextView
        val user_author_txt_view = view.findViewById(R.id.user_author_txt_view) as CustomTextView
        val tv_user_title = view.findViewById(R.id.tv_user_title) as CustomTextViewBold
        val tv_description_news = view.findViewById(R.id.tv_description_news) as CustomTextView
        val tv_like_count = view.findViewById(R.id.tv_like_count) as CustomTextView
        val iv_user_image = view.findViewById(R.id.iv_user_image) as ImageView
        val iv_postimage = view.findViewById(R.id.iv_postimage) as ImageView
        val iv_news_share = view.findViewById(R.id.iv_news_share) as ImageView
        val iv_like_status = view.findViewById(R.id.iv_like_status) as ImageView
        val tv_time_status = view.findViewById(R.id.tv_time_status) as CustomTextView
        val ll_comment = view.findViewById(R.id.ll_comment) as LinearLayout

    }


    /*inner class EditPrefsAdapter(context: Context, list: java.util.ArrayList<String>) :
            RecyclerView.Adapter<EditPrefsAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: java.util.ArrayList<String> = java.util.ArrayList()
        private var selectedIndex = -1

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(
                parent: ViewGroup,
                position: Int
        ): EditPrefsAdapter.ViewHolder {

            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycle_dash_tabs, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {

            return mList.size

        }

        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
        override fun onBindViewHolder(holder: EditPrefsAdapter.ViewHolder, position: Int) {

            val title = mList[position]

            if(position==0){
                holder.tv_prefs_name.text = title
                holder.view_id.visibility=View.VISIBLE
                holder.tv_prefs_name.setTextColor(ContextCompat.getColor(context!!, R.color.blue))
            }else{
                holder.tv_prefs_name.text=title
                holder.view_id.visibility=View.GONE
                holder.tv_prefs_name.setTextColor(ContextCompat.getColor(context!!, R.color.gray))

            }

            holder.tv_prefs_name.setOnClickListener {
                if(position !=0 ){
                    val intent =Intent(context,NewsFeedList::class.java)
                    intent.putExtra("form_screen",mList[position])
                    startActivity(intent)
                }
            }

        }
        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            var tv_prefs_name: TextView = view.findViewById(R.id.tv_prefs_name)
            var ll_match_prefs: LinearLayout = view.findViewById(R.id.ll_match_prefs)
            var view_id: View = view.findViewById(R.id.view_id)


        }

        fun getItem(position: Int): String? {
            return mList[position]
        }

        fun setSelectedIndex(index: Int) {
            selectedIndex = index
            notifyDataSetChanged()
        }

        fun getSelectedIndex(): Int {
            return selectedIndex
        }

    }*/


    private fun callgetUsTabsAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getUstabs()
        recycle_tabs_name!!.clear()
        call.enqueue(object : Callback<getUsTabsResponse> {
            override fun onResponse(call: Call<getUsTabsResponse>, response: retrofit2.Response<getUsTabsResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        tv_nodata_id.visibility = View.GONE
                        my_loader.dismiss()
                        if (response.body()!!.data != null) {
                            val list: Array<getUsTabsDataResponse> = response.body()!!.data!!
                            for (item: getUsTabsDataResponse in list.iterator()) {
                                recycle_tabs_name!!.add(item)
                                for (i in 0 until recycle_tabs_name!!.size) {
                                    recycle_tab_items!!.add(recycle_tabs_name!![i])
                                }
                            }

                            getNewsListAPI()
                             tabsdata_adapter = TabsDataAdapter(recycle_tabs_name!!, activity!!)
                            recyclerView_all.setAdapter(tabsdata_adapter)
                            tabsdata_adapter!!.notifyDataSetChanged()
                            recyclerView_all.setHasFixedSize(true)
                            recyclerView_all.setLayoutManager(LinearLayoutManager(activity, LinearLayout.HORIZONTAL, false))
                            recyclerView_all.setItemAnimator(DefaultItemAnimator())



                        }

                    } else if (response.body()!!.status.equals("2")) {
                        tv_nodata_id.visibility = View.VISIBLE
                    }

                }
            }

            override fun onFailure(call: Call<getUsTabsResponse>, t: Throwable) {
                my_loader.dismiss()
                Log.w("Result_Address_Profile", t.toString())
            }


        })
    }


    class ViewHolder4(view: View) : RecyclerView.ViewHolder(view) {
        val tv_tab_name = view.findViewById<CustomTextView>(R.id.tv_tab_name)
        val view_id = view.findViewById<View>(R.id.view_id)
        // val ll_current_parking_timing = view.findViewById<LinearLayout>(R.id.ll_current_parking_timing)
        // var userSelected = true;
    }

    inner class TabsDataAdapter(val title: java.util.ArrayList<getUsTabsDataResponse>, val context: Context) : RecyclerView.Adapter<ViewHolder4>() {

        var pos = -1
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder4 {
            return ViewHolder4(LayoutInflater.from(context).inflate(R.layout.item_recycle_dash_tabs, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder4, position: Int) {
           /* holder.tv_tab_name.setText(title.get(position).title)
            holder.tv_tab_name.setOnClickListener {
                Log.e("AfterCLICKING",""+newslistArray.size)
                try{
                    no_datafound.visibility = View.GONE
                    rv_newslist.visibility = View.VISIBLE
                    pos = position
                    this.setSelectedIndex(position)
                    newslistPositionArray = ArrayList()
                  *//*  if (title[position].title.toString().equals(newslistArray[position].title!!)) {
                        newslistPositionArray.add(newslistArray[position])

                    } else {
                        //holder.view_id.visibility=View.GONE
                        Log.e("No Data",""+newslistArray.size)
                    }*//*
                    Log.e("AfterCLICKINGINSIDE",""+newslistPositionArray.size)

                    for(i in 0 until  newslistArray.size){
                        if (newslistArray[i].title!!.equals(recycle_tabs_name!![position].title.toString())) {
                            newslistPositionArray.add(newslistArray[i])
                        }else{
                            no_datafound.visibility = View.VISIBLE
                        }
                    }
                    val news_list_adapter = NewsDetailsRecyclerAdapter(newslistPositionArray, activity!!)
                    rv_newslist!!.setAdapter(news_list_adapter)
                    news_list_adapter!!.notifyDataSetChanged()
                    rv_newslist!!.setHasFixedSize(true)
                    rv_newslist!!.setItemAnimator(DefaultItemAnimator())
                }catch (e:Exception){
                    no_datafound.visibility = View.VISIBLE
                    rv_newslist.visibility = View.GONE

                }
            }

            if (position == pos) {
                holder.tv_tab_name.text = title[position].title
                holder.view_id.visibility = View.VISIBLE
                holder.tv_tab_name.setTextColor(ContextCompat.getColor(context!!, R.color.blue))
            } else {
                holder.tv_tab_name.text = title[position].title
                holder.view_id.visibility = View.GONE
                holder.tv_tab_name.setTextColor(ContextCompat.getColor(context!!, R.color.gray))

            }*/


            holder.tv_tab_name.setText(title.get(position).title)


            if(position==0){
                holder.tv_tab_name.text = title[position].title
                holder.view_id.visibility=View.VISIBLE
                holder.tv_tab_name.setTextColor(ContextCompat.getColor(context!!, R.color.blue))
            }else{
                holder.tv_tab_name.text= title[position].title
                holder.view_id.visibility=View.GONE
                holder.tv_tab_name.setTextColor(ContextCompat.getColor(context!!, R.color.gray))

            }

            holder.tv_tab_name.setOnClickListener {
                val tab_intent =Intent(context,NewsFeedList::class.java)
                tab_intent.putExtra("form_screen",title[position].title.toString())
                tab_intent.putExtra("url",title[position].url.toString())
                startActivity(tab_intent)
            }



        }

        fun setSelectedIndex(index: Int) {
            pos = index
            notifyDataSetChanged()
        }

        fun getSelectedIndex(): Int {
            return pos
        }

        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return title.size
        }
    }


}


