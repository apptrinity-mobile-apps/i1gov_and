package com.gov.biil.model.apiresponses

class SaveVoteResponse {

    val result: String? = null

    val data: SaveVoteDataResponse? = null

    val status: String? = null

}
