package com.gov.biil.model.apiresponses

class VotingListResponse {

    val status: String? = null
    val result: String? = null
    val data:VotingListDataResponse?=null

}
