package com.gov.biil.model.apiresponses

class GetCityWiseUsersResponse {

    val result: String? = null

    val data: GetCityWiseUsersDataResponse? = null

    val status: String? = null

}
