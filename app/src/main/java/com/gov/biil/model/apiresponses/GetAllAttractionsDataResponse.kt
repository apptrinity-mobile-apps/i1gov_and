package com.gov.biil.model.apiresponses

class GetAllAttractionsDataResponse {

    val id: String? = null
    val title: String? = null
    val text: String? = null
    val image: String? = null
    val status: String? = null
    val created_date: String? = null


}
