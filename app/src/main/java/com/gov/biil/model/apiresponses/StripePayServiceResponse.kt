package com.gov.biil.model.apiresponses

/**
 * Created by indo67 on 8/2/2018.
 */
class StripePayServiceResponse {

    var status:String?=null
    var result:String?=null
    var data:StripePayServiceDataResponse?=null

}