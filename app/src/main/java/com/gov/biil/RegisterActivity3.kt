package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.a4printuser.Helper.ConnectionManager
import com.gov.a4printuser.Helper.CustomTextViewBold
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.RegistrationStepOneResponse
import com.gov.biil.model.apiresponses.UserProfileResponse
import retrofit2.Call
import retrofit2.Callback


class RegisterActivity3 : AppCompatActivity() {


    var dialog_list: Dialog? = null
    internal lateinit var sessionManager: SessionManager

    internal lateinit var reg_cellphone: CustomTextView

    internal lateinit var reg_email: EditText
    internal lateinit var reg_otp: EditText
    internal lateinit var reg_intent_email: String
    internal lateinit var reg_intent_phone: String
    internal lateinit var reg_intent_password: String
    internal var user_id: String = ""
    internal lateinit var reg_intent_otp: String
    internal lateinit var resend_otp: CustomTextView
    internal lateinit var create_account: CustomTextView
    internal lateinit var tv_verifyemail_id: CustomTextView
    internal lateinit var tv_resendtimer: CustomTextView
    internal lateinit var header_title: CustomTextViewBold
    internal var otp: String? = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_account)
        sessionManager = SessionManager(this)
        timer()
        /*  val toolbar = findViewById(R.id.toolbar) as Toolbar
          setSupportActionBar(toolbar)
          supportActionBar!!.setHomeButtonEnabled(true)
          supportActionBar!!.setDisplayHomeAsUpEnabled(true)
          supportActionBar!!.setDisplayShowTitleEnabled(false)
          toolbar.setNavigationOnClickListener { onBackPressed() }*/


        reg_intent_email = intent.getStringExtra("email")
        reg_intent_phone = intent.getStringExtra("phonenumber")
        reg_intent_password = intent.getStringExtra("password")
        reg_intent_otp = intent.getStringExtra("otp")


        //var rootview = findViewById(R.id.rootview) as LinearLayout
        dialog_list = Dialog(this)
        dialog_list!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list!!.setContentView(R.layout.dialog_view)
        dialog_list!!.setCancelable(true)


        reg_email = findViewById(R.id.et_user_name) as EditText
        reg_cellphone = findViewById(R.id.et_number_id) as CustomTextView
        reg_otp = findViewById(R.id.et_verify_otp) as EditText
        resend_otp = findViewById(R.id.tv_resendotp) as CustomTextView
        create_account = findViewById(R.id.tv_sign_in_id) as CustomTextView
        tv_verifyemail_id = findViewById(R.id.tv_verifyemail_id) as CustomTextView
        tv_resendtimer = findViewById(R.id.tv_resendtimer) as CustomTextView
        header_title = findViewById(R.id.header_title) as CustomTextViewBold

        tv_verifyemail_id.visibility = View.GONE

        if (reg_intent_phone.equals("")) {

            user_id = intent.getStringExtra("user_id")
            header_title.setText("Verify Email")
            reg_cellphone.visibility = View.GONE
            reg_otp.visibility = View.GONE
            resend_otp.visibility = View.GONE
            tv_resendtimer.visibility = View.GONE
            reg_email.visibility = View.VISIBLE
            reg_email.setText(reg_intent_email)
            tv_verifyemail_id.visibility = View.VISIBLE
            create_account.visibility = View.GONE
        }


        reg_cellphone.setText(reg_intent_phone)

        myloading()

        create_account.setOnClickListener(View.OnClickListener {
            if (hasValidCredentials()) {
                if (true) {
                    if (ConnectionManager.checkConnection(applicationContext)) {
                        if (reg_otp.text.equals("")) {
                            Toast.makeText(applicationContext, "Please Enter Otp", Toast.LENGTH_SHORT).show()
                        } else {

                            if (reg_otp.text.toString().equals(reg_intent_otp)) {
                                callRegisterAPI("1")
                            }
                        }

                        // callRegisterAPI()
                    } else {
                        //ConnectionManager.snackBarNetworkAlert(rootview, applicationContext)
                    }
                } else {
                    Toast.makeText(applicationContext, "Please select terms and conditions", Toast.LENGTH_SHORT).show()
                }
            }
        })



        resend_otp.setOnClickListener(View.OnClickListener {

            if (ConnectionManager.checkConnection(applicationContext)) {
                if (reg_otp.text.equals("")) {
                    Toast.makeText(applicationContext, "Please Enter Otp", Toast.LENGTH_SHORT).show()
                } else {
                    if (reg_otp.text.toString().equals(otp)) {
                        callRegisterOtpAPI()
                    }
                }
            } else {
                //ConnectionManager.snackBarNetworkAlert(rootview, applicationContext)
            }

        })

        tv_verifyemail_id.setOnClickListener(View.OnClickListener {

                callRegisterEmailAPI(user_id)

        })


    }

    private fun callRegisterAPI(mobile_status: String) {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.saveUserStepOne(reg_intent_email, reg_intent_password, reg_intent_phone, mobile_status)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<RegistrationStepOneResponse> {
            override fun onResponse(call: Call<RegistrationStepOneResponse>, response: retrofit2.Response<RegistrationStepOneResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.usetr_info!!.email_status.equals("1") && response.body()!!.usetr_info!!.mobile_status.equals("1")) {
                            Toast.makeText(applicationContext, "Registration Success", Toast.LENGTH_SHORT).show()
                            val intent = Intent(this@RegisterActivity3, LoginActivity::class.java)
                            finish()
                            startActivity(intent)
                        } else {

                            user_id = response.body()!!.usetr_info!!.id!!
                            header_title.setText("Verify Email")
                            reg_cellphone.visibility = View.GONE
                            reg_otp.visibility = View.GONE
                            resend_otp.visibility = View.GONE
                            tv_resendtimer.visibility = View.GONE
                            reg_email.visibility = View.VISIBLE
                            reg_email.setText(reg_intent_email)
                            tv_verifyemail_id.visibility = View.VISIBLE
                            create_account.visibility = View.GONE


                        }


                    } else {
                        Toast.makeText(applicationContext, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<RegistrationStepOneResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun callRegisterOtpAPI() {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.mobileVerification(reg_intent_phone, reg_intent_email)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<RegistrationStepOneResponse> {
            override fun onResponse(call: Call<RegistrationStepOneResponse>, response: retrofit2.Response<RegistrationStepOneResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    // Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        otp = response.body()!!.otp
                        reg_intent_otp = otp.toString()
                    } else if (response.body()!!.status.equals("3")) {
                        Toast.makeText(applicationContext, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(applicationContext, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<RegistrationStepOneResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun callRegisterEmailAPI(user_id: String) {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.userProfile(user_id)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UserProfileResponse> {
            override fun onResponse(call: Call<UserProfileResponse>, response: retrofit2.Response<UserProfileResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info!!.email_status.equals("1") && response.body()!!.user_info!!.mobile_status.equals("1")) {
                            Toast.makeText(applicationContext, "Registration Success", Toast.LENGTH_SHORT).show()
                            val intent = Intent(this@RegisterActivity3, RegisterActivity::class.java)
                            intent.putExtra("user_id",user_id)
                            intent.putExtra("email",reg_intent_email)
                            intent.putExtra("phone",reg_intent_phone)
                            finish()
                            startActivity(intent)
                        } else {

                            Toast.makeText(applicationContext, "Please Verify Your Email", Toast.LENGTH_SHORT).show()
                            header_title.setText("Verify Email")
                            reg_cellphone.visibility = View.GONE
                            reg_otp.visibility = View.GONE
                            resend_otp.visibility = View.GONE
                            tv_resendtimer.visibility = View.GONE
                            reg_email.visibility = View.VISIBLE
                            reg_email.setText(reg_intent_email)
                            tv_verifyemail_id.isEnabled

                        }

                    } else if (response.body()!!.status.equals("3")) {
                        Toast.makeText(applicationContext, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(applicationContext, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(reg_otp.text.toString())) {
            reg_otp.setError("Enter Otp ")
        }  /*else if (TextUtils.isEmpty(reg_cellphone.text.toString())) {
           reg_cellphone.setError("Enter phone number")
        } */ else
            return true
        return false
    }


    private fun timer() {
        object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                tv_resendtimer.setText("00:" + millisUntilFinished / 1000)
            }

            override fun onFinish() {
                tv_resendtimer.visibility = View.GONE
                resend_otp.isEnabled = true
                resend_otp.setTextColor(resources.getColor(R.color.blue))
            }

        }.start()
    }

    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    override fun onBackPressed() {
        val intent = Intent(this@RegisterActivity3, RegisterActivity2::class.java)
        startActivity(intent)
    }


}
