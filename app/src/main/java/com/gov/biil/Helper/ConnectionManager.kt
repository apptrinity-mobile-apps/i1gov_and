package com.gov.a4printuser.Helper

import android.content.Context
import android.net.ConnectivityManager
import android.support.design.widget.Snackbar
import android.widget.LinearLayout
import android.widget.RelativeLayout


object ConnectionManager {

    /** CHECK WHETHER INTERNET CONNECTION IS AVAILABLE OR NOT  */

    fun checkConnection(context: Context): Boolean {
        val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetworkInfo = connMgr.activeNetworkInfo

        if (activeNetworkInfo != null) { // connected to the internet
            //Toast.makeText(context, activeNetworkInfo.typeName, Toast.LENGTH_SHORT).show()

            if (activeNetworkInfo.type == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                return true
            } else if (activeNetworkInfo.type == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                return true
            }
        }
        return false
    }
    fun snackBarNetworkAlert(rootview: LinearLayout, context: Context) {
        val mSnackbar = Snackbar.make(rootview, "please check your network connection", Snackbar.LENGTH_INDEFINITE)
                .setAction("RETRY") {
                    if(!ConnectionManager.checkConnection(context)){
                        if(!rootview.equals(0)) {
                            snackBarNetworkAlert(rootview, context)
                        }else{
                            snackBarNetworkAlert(rootview, context)
                        }
                    }
                }
        mSnackbar.show()
    }
    fun snackBarNetworkAlert_Relative(rootview: RelativeLayout, context: Context) {
        val mSnackbar = Snackbar.make(rootview, "please check your network connection", Snackbar.LENGTH_INDEFINITE)
                .setAction("RETRY") {
                    if(!ConnectionManager.checkConnection(context)){
                        if(!rootview.equals(0)) {
                            snackBarNetworkAlert_Relative(rootview, context)
                        }else{
                            snackBarNetworkAlert_Relative(rootview, context)
                        }
                    }
                }
        mSnackbar.show()
    }
    fun showNetworkErrorToast(context: Context){
        //Toast.makeText(context,context.resources.getString(R.string.network_error),Toast.LENGTH_SHORT).show()
    }
}