package com.gov.biil.model.apiresponses

class SaveVoteListDataResponse {

    val id: String? = null
    val candidate_name: String? = null
    val party_name: String? = null
    val ind_party_name: String? = null
    val category_id: String? = null
    val sub_category_id: String? = null
    val state_id: String? = null
    val city_id: String? = null
    val status: String? = null
    val created_date: String? = null
    val count: String? = null


}
