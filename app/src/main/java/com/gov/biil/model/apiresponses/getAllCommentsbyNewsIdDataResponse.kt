package model.apiresponses

class getAllCommentsbyNewsIdDataResponse {


    val comment_id: String? = null
    val user_id: String? = null
    val news_id: String? = null
    val comment: String? = null
    val created_at: String? = null
    val username: String? = null
    val user_type: String? = null
    val reply_data: Array<getAllCommentsReplyDataResponse>?=null

}
