package com.gov.biil

import android.annotation.SuppressLint
import android.app.Dialog
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Window
import android.webkit.WebView
import android.webkit.WebViewClient
import com.gov.a4print.Session.SessionManager

class CountywebActivity : AppCompatActivity() {

    internal  var paylink:String?=""
    internal lateinit var id:String
    var mywebview: WebView? = null
    internal lateinit var sessionManager: SessionManager
    lateinit  var uri: Uri

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_countyweb)
        mywebview = findViewById<WebView>(R.id.webview_pricecalc)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        myloading()

       // paylink = "https://www.cookcountyil.gov/"


        Log.e("COUNTYHTTP","COUNTYWEB")
        sessionManager = SessionManager(this)

        if(sessionManager.countyurl!=""){
            paylink = sessionManager.countyurl
            Log.e("COUNTYHTTP",paylink)
            if (paylink!!.contains("http") || paylink!!.contains("https")) {
                if(!paylink!!.startsWith("http://")){
                    paylink = "https://"+paylink;
                    Log.e("COUNTYHTTP",paylink)
                    uri = Uri.parse(paylink)
                }

            } else {
                Log.e("COUNTYHTTP",paylink)
                uri = Uri.parse("https://" + paylink)
            }

        }

        mywebview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }
        mywebview!!.getSettings().setJavaScriptEnabled(true);
        mywebview!!.setHorizontalScrollBarEnabled(false);
        mywebview!!.loadUrl(uri.toString())

    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    override fun onBackPressed() {
        if (mywebview!!.canGoBack()) {
            mywebview!!.goBack()
        } else {
            super.onBackPressed()
        }
    }
}
