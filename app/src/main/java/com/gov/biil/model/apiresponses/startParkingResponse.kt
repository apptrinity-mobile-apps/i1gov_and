package model.apiresponses

class startParkingResponse {

    val data: StartParkingDataResponse? = null

    val result: String? = null

    val status: String? = null

}
