package com.gov.biil.model.apiresponses

class getAllParkingResponse {

    val data:Array<getAllParkingDataResponse>?=null
    val status: String? = null
    val result: String? = null

}
