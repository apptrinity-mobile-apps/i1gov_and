package com.gov.biil

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.biil.fragment.MyBillsCityFragment
import com.gov.biil.fragment.MyBillsCountyFragment
import com.gov.biil.fragment.MyBillsFederalFragment
import com.gov.biil.fragment.MyBillsStateFragment
import com.gov.biil.model.ApiInterface
import kotlinx.android.synthetic.main.app_bar_main.*
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback


class MyBillsActivity : AppCompatActivity()  {

    lateinit var tabLayout: TabLayout
    lateinit var viewPager: ViewPager
    internal lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mybills)

        sessionManager = SessionManager(this)
        myloading()
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        viewPager = findViewById(R.id.container) as ViewPager
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tabs) as TabLayout
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        getAdminStatusInfo()

    }


    private fun setupTabIcons() {

        tabLayout.getTabAt(0)!!.setCustomView(R.layout.tab_selector_city)
        tabLayout.getTabAt(1)!!.setCustomView(R.layout.tab_selector_county)
        tabLayout.getTabAt(2)!!.setCustomView(R.layout.tab_selector_state)
        tabLayout.getTabAt(3)!!.setCustomView(R.layout.tab_selector_federal)

    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        //adapter.addFrag(CityPayForServiceFragment(), "PayForService")
        adapter.addFrag(MyBillsCityFragment(), "City")
        adapter.addFrag(MyBillsCountyFragment(), "County")
        adapter.addFrag(MyBillsStateFragment(), "State")
        adapter.addFrag(MyBillsFederalFragment(), "Federal")
        viewPager.adapter = adapter

        viewPager.addOnPageChangeListener(
                TabLayout.TabLayoutOnPageChangeListener(tabs))
        for (i in 0 until tabs.tabCount) {
            val view = tabs.getTabAt(i) as TextView
            view.setTextColor(resources.getColor(R.color.gray))
            if(i==1){
                val view = tabs.getTabAt(i) as TextView
                view.setTextColor(resources.getColor(R.color.green))
            }else if(i==2){
                val view = tabs.getTabAt(i) as TextView
                view.setTextColor(resources.getColor(R.color.yellow_color))
            }else if(i==3){
                val view = tabs.getTabAt(i) as TextView
                view.setTextColor(resources.getColor(R.color.red_color))
            }

        }
        tabs.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })

    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList.get(position)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this@MyBillsActivity, MainActivity::class.java)
        startActivity(intent)

    }

    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


}
