package com.gov.a4printuser.Helper

class BundleDataInfo {

    companion object {


        val PROFILE_USERID = "userid"
        val PROFILE_FULLNAME = "fullname"
        val PROFILE_DOB = "dob"
        val PROFILE_GENDER = "gender"
        val PROFILE_SSN = "ssn"
        val PROFILE_ADDRESS = "address"
        val PROFILE_CITY = "city"
        val PROFILE_CITYURL = "cityurl"
        val PROFILE_COUNTRY = "country"
        val PROFILE_COUNTRYURL = "countryurl"
        val PROFILE_STATE = "state"
        val PROFILE_STATEURL = "stateurl"
        val PROFILE_EMAIL = "email"
        val PROFILE_ZIPCODE = "zipcode"
        val PROFILE_CELLPHONE = "cellphone"
        val PROFILE_USERTYPE = "usertype"
        val PROFILE_ORGANIZATION = "organization"


        val SERVICE_VIEW_ID = "id"
        val SERVICE_VIEW_TYPE = "stype"
        val SERVICE_PAY_TYPE = "stype"


        val STATES_LIST = arrayListOf<String>("Alabama"
                ,"Alaska"
                ,"Arizona"
                ,"Arkanans"
                ,"California"
                ,"Colorado"
                ,"Connecticut"
                ,"Delaware"
                ,"Florida"
                ,"Georgia"
                ,"Hawaii"
                ,"Idaho"
                ,"Illinois"
                ,"Indiana"
                ,"Iowa"
                ,"Kansas"
                ,"Kentucky"
                ,"Louisiana"
                ,"Maine"
                ,"Maryland"
                ,"Massachusetts"
                ,"Michigan"
                ,"Minnesota"
                ,"Mississippi"
                ,"Missouri"
                ,"Montana"
                ,"Nebraska"
                ,"Nevada"
                ,"New Hampshire"
                ,"New Mexico"
                ,"New York"
                ,"New Jersey"
                ,"North Carolina"
                ,"North Dakota"
                ,"Ohio"
                ,"Oklahoma"
                ,"Oregon"
                ,"Pennsylvania"
                ,"Rhode Island"
                ,"South Carolina"
                ,"South Dakota"
                ,"Tennessee"
                ,"Texas"
                ,"Utah"
                ,"Vermont"
                ,"Virginia"
                ,"Washington"
                ,"Washington, D.C."
                ,"West Virginia"
                ,"Wisconsin"
                ,"Wyoming")

        val STATES_URLLIST = arrayListOf<String>(
                "www.alabama.gov", "www.alaska.gov", "https://az.gov", "https://arkansas.gov",
                "www.ca.gov", "https://www.colorado.gov", "www.ct.gov", "https://delaware.gov", "www.myflorida.com",
                "https://georgia.gov", "https://portal.ehawaii.gov", "https://www.idaho.gov", "https://www2.illinois.gov", "www.in.gov",
                "https://www.iowa.gov", "https://www.kansas.gov", "www.kentucky.gov", "www.louisiana.gov", "www.maine.gov",
                "www.maryland.gov", "https://www.mass.gov", "https://michigan.gov", "https://mn.gov", "www.ms.gov",
                "https://www.mo.gov", "www.mt.gov", "www.nebraska.gov", "www.nv.gov", "https://www.nh.gov", "www.newmexico.gov",
                "www.ny.gov", "www.nj.gov", "https://www.nc.gov", "https://www.nd.gov", "https://ohio.gov", "https://www.okgov",
                "www.oregon.gov", "https://pa.gov", "https://www.ri.gov", "www.sc.gov", "www.sd.gov", "https://www.tn.gov",
                "https://texas.gov", "https://www.utah.gov", "www.vermont.gov", "https://virginia.gov", "www.access.wa.gov","https://dc.gov",
                "https://www.wv.gov", "https://wisconsin.gov", "www.wyo.gov")


    }
}
