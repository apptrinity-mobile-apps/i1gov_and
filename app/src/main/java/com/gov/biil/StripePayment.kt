package com.gov.biil

import Helper.CustomTextView
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.gov.a4print.Session.SessionManager
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.PaymentResponseAPI
import com.gov.biil.model.apiresponses.StripePayServiceDataResponse
import com.gov.biil.model.apiresponses.StripePayServiceResponse
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.stripe.android.model.Token
import com.stripe.android.view.CardInputWidget
import model.apiresponses.getAdminStatusInfo
import retrofit2.Call
import retrofit2.Callback


class StripePayment : AppCompatActivity() {
    val PUBLISHABLE_KEY = "pk_test_0I3IGKx3mcL97rMJ0Dbt5pm9"
    /* val APPLICATION_ID = "RKNck9SdN6sqcznBvy5lqnN2ln1FrrSabNcq8YEK"
     val CLIENT_KEY = "zWtkaYFS0Ia91jKkgmIHJql30cARcrDmKUGAXLTY"
     val ARMINFO = "https://arminfo.com/"*/
    var amount_stg = ""
    var from_stg = ""
    var ptype_wallet = ""
    var service_id = ""
    var address_stg = ""
    var due_current_past_stg = ""
    var after_amount = ""
    var id = ""
    lateinit var success_Dialog: Dialog
    lateinit var tv_message_id: CustomTextView
    lateinit var tv_ok_id: CustomTextView
    lateinit var tv_cancel_id: CustomTextView
    lateinit var sessionManager: SessionManager
    lateinit var card_input_widget: CardInputWidget
    lateinit var et_card_name_id: EditText
    lateinit var tv_pay_amount_id: TextView
    private var card: Card? = null
    private var progress: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stripe_payment)

        sessionManager = SessionManager(this)
        myloading()
        getAdminStatusInfo()

        successalert()
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }


        et_card_name_id = findViewById(R.id.et_card_name_id)
        tv_pay_amount_id = findViewById(R.id.tv_pay_amount_id)
        card_input_widget = findViewById(R.id.card_input_widget)

        amount_stg = intent.getStringExtra("amount")
        from_stg = intent.getStringExtra("from")
        if (from_stg.equals("services")) {
            id = intent.getStringExtra("id")
            ptype_wallet = intent.getStringExtra("ptype_wallet")
            service_id = intent.getStringExtra("service_id")
            address_stg = intent.getStringExtra("address_stg")
            due_current_past_stg = intent.getStringExtra("due_current_past_stg")
            after_amount = intent.getStringExtra("after_amount")
        }


        if (from_stg.equals("services")) {
            tv_pay_amount_id.setText("PAY " + "$ " + after_amount)
        } else {
            tv_pay_amount_id.setText("PAY " + "$ " + amount_stg)
        }

        /* Parse.initialize(Parse.Configuration.Builder(this)
                 .applicationId(APPLICATION_ID)
                 .clientKey(CLIENT_KEY)
                 .server(ARMINFO).build())
         Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE)*/

        // Create a demo test credit Card
        // You can pass the payment form data to create a Real Credit card
        // But you need to implement youself.


        progress = ProgressDialog(this)
        val purchase = findViewById(R.id.ll_pay_amount_id) as LinearLayout
        purchase.setOnClickListener {

            val cardToSave = card_input_widget.getCard()
            if (cardToSave == null) {
                Toast.makeText(this@StripePayment, "Invalid Card Data", Toast.LENGTH_SHORT).show()
            } else if (et_card_name_id.text.toString().equals("")) {
                Toast.makeText(this@StripePayment, "Please Enter Cardholder's Name", Toast.LENGTH_SHORT).show()
            } else {
                card = card_input_widget.card
                buy()
            }

        }

    }

    private fun buy() {
        val validation = card!!.validateCard()
        if (validation) {
            startProgress("Validating Credit Card")
            Stripe(this).createToken(
                    card!!,
                    PUBLISHABLE_KEY,
                    object : TokenCallback {
                        override fun onError(error: Exception) {
                            Log.d("Stripe", error.toString())
                        }

                        override fun onSuccess(token: Token) {
                            finishProgress()
                            // charge(token)
                            Log.e("cardToken_lll", token.getId() + "--" + token.card.customerId + "---" + token.card.last4 + "---" + token.card.expMonth + "---" + token.card.expYear)
                            if (from_stg.equals("load_funds")) {
                                callPaymentAPI(token.getId()!!)
                            } else if (from_stg.equals("services")) {
                                callStripePayServiceAPI(token.getId()!!)
                            }


                        }
                    })
        } else if (!card!!.validateNumber()) {
            Log.d("Stripe", "The card number that you entered is invalid")
        } else if (!card!!.validateExpiryDate()) {
            Log.d("Stripe", "The expiration date that you entered is invalid")
        } else if (!card!!.validateCVC()) {
            Log.d("Stripe", "The CVC code that you entered is invalid")
        } else {
            Log.d("Stripe", "The card details that you entered are invalid")
        }
    }

    /*private fun charge(cardToken: Token) {
        val params = HashMap<String, Any>()
        params["itemName"] = "test" //This part you need to pass your item detail, implement yourself
        params["cardToken"] = cardToken.getId()
        params["name"] = "Dominic Wong"
        params["email"] = "dominwong4@gmail.com"
        params["address"] = "HIHI"
        params["zip"] = "99999"
        params["city_state"] = "CA"
        startProgress("Purchasing Item")
        Log.e("cardToken_lll",cardToken.getId())
        ParseCloud.callFunctionInBackground("purchaseItem", params, object : FunctionCallback<Any> {
            override fun done(response: Any, e: ParseException?) {
                finishProgress()
                if (e == null) {
                    Log.d("Cloud Response", "There were no exceptions! " + response.toString())
                    Toast.makeText(applicationContext,
                            "Item Purchased Successfully ",
                            Toast.LENGTH_LONG).show()
                } else {
                    Log.d("Cloud Response", "Exception: " + e!!)
                    Toast.makeText(applicationContext, e.toString(),
                            Toast.LENGTH_LONG).show()
                }
            }
        })
    }*/

    private fun startProgress(title: String) {
        progress!!.setTitle(title)
        progress!!.setMessage("Please Wait")
        progress!!.show()
    }

    private fun finishProgress() {
        progress!!.dismiss()
    }

    private fun callPaymentAPI(token: String) {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.stripPayment(token, et_card_name_id.text.toString(), amount_stg, sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")

        call.enqueue(object : Callback<PaymentResponseAPI> {
            override fun onResponse(call: Call<PaymentResponseAPI>, response: retrofit2.Response<PaymentResponseAPI>?) {
                if (response != null) {
                    // my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.data != null) {
                            // setProfileDatatoView(response.body()!!.user_info)
                            val list = response.body()!!.data!!

                            Log.e("list_data", list.amount + "--" + list.payment_status + "--" + list.trans_id + "--" + list.currency)
                            success_Dialog.show()
                        }

                    } else {
                        tv_message_id.setTextColor(resources.getColor(R.color.red_color))
                        tv_message_id.setText("Failed")
                        success_Dialog.show()
                    }

                }
            }

            override fun onFailure(call: Call<PaymentResponseAPI>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }


    ///services Bill Payment

    private fun callStripePayServiceAPI(token: String) {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.StripePayServiceAPI(id,service_id, sessionManager.isId, amount_stg, ptype_wallet, address_stg, token, due_current_past_stg)
        call.enqueue(object : Callback<StripePayServiceResponse> {
            override fun onResponse(call: Call<StripePayServiceResponse>, response: retrofit2.Response<StripePayServiceResponse>?) {
                //my_loader.dismiss()
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        val list: StripePayServiceDataResponse = response.body()!!.data!!


                        Toast.makeText(this@StripePayment, "Payment Success", Toast.LENGTH_SHORT).show()

                        val intent = Intent(this@StripePayment, MainActivity::class.java)
                        startActivity(intent)
                    }

                } else {
                    //Toast.makeText(activity, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<StripePayServiceResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    fun successalert() {
        success_Dialog = Dialog(this)
        success_Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        success_Dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        success_Dialog.setCancelable(false);
        success_Dialog.setContentView(R.layout.stop_dialog_alert)
        tv_message_id = success_Dialog.findViewById(R.id.tv_message_id)
        tv_message_id.setTextColor(resources.getColor(R.color.green))
        tv_message_id.setText("Success")
        tv_ok_id = success_Dialog.findViewById(R.id.tv_ok_id)
        tv_cancel_id = success_Dialog.findViewById(R.id.tv_cancel_id)
        tv_cancel_id.visibility = View.GONE
        tv_ok_id.setOnClickListener {
            success_Dialog.dismiss()
            val intent = Intent(this@StripePayment, MainActivity::class.java)
            startActivity(intent)
        }

    }


    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


    private fun getAdminStatusInfo() {
        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getAdminStatus(sessionManager.isId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<getAdminStatusInfo> {
            override fun onResponse(call: Call<getAdminStatusInfo>, response: retrofit2.Response<getAdminStatusInfo>?) {
                if (response != null) {
                    my_loader.dismiss()
                    if (response.body()!!.status.equals("1")) {
                        if (response.body()!!.user_info != null) {
                            if (response.body()!!.user_info!!.admin_status!!.equals("1")) {

                            } else {

                                Toast.makeText(applicationContext, "Oh No! \n" +
                                        "We are sorry to inform you that your account has been deactivated. To know more, contact our customer support.\n", Toast.LENGTH_LONG).show()
                                sessionManager.logoutUser()
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<getAdminStatusInfo>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }
        })
    }

}
