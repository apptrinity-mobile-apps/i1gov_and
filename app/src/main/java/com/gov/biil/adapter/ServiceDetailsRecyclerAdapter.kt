package com.gov.biil.adapter

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.gov.a4print.Session.SessionManager
import com.gov.biil.*
import com.gov.biil.model.ApiInterface
import com.gov.biil.model.apiresponses.AllServicesByUserInfo
import com.gov.biil.model.apiresponses.getNewsListResponse
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*


class ServiceDetailsRecyclerAdapter(val mServiceList: ArrayList<AllServicesByUserInfo>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

    internal lateinit var my_loader: Dialog
    internal var likestatus: String = ""
    internal lateinit var sessionManager: SessionManager
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        sessionManager = SessionManager(context)
        myloading()
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.dashboard_details_adapter_layout, parent, false))

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.username_txt_view.text = mServiceList.get(position).posted_by
        holder.user_author_txt_view.text = mServiceList.get(position).author_profession
        holder.tv_user_title.text = mServiceList.get(position).title
        // holder?.tv_description_news.text = mServiceList.get(position).news
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.tv_description_news.setText(Html.fromHtml(mServiceList.get(position).news, Html.FROM_HTML_MODE_COMPACT))
        } else {
            holder.tv_description_news.setText(Html.fromHtml(mServiceList.get(position).news))
        }

        Picasso.with(context)
                .load("https://www.i1gov.com/uploads/news/" + mServiceList.get(position).image)
                .into(holder.iv_postimage)

        Picasso.with(context)
                .load("https://www.i1gov.com/uploads/news/" + mServiceList.get(position).posted_by_image)
                .error(R.drawable.user_icon)
                .into(holder.iv_user_image)


        holder?.iv_postimage.setOnClickListener {
            val intent = Intent(context, DashboardPostDetailViewActivity::class.java)
            intent.putExtra("news_id", mServiceList.get(position).id)
            intent.putExtra("title_link", mServiceList.get(position).title_link)
            intent.putExtra("author_link", mServiceList.get(position).author_link)
            intent.putExtra("title_name", mServiceList.get(position).title)
            context.startActivity(intent)
        }

        holder?.username_txt_view.setOnClickListener {
            if (mServiceList.get(position).author_link == "" || mServiceList.get(position).author_link == null || mServiceList.get(position).author_link == "null") {
            } else {
                val intent = Intent(context, NewsTitleWebActivity::class.java)
                intent.putExtra("title_link", mServiceList.get(position).author_link)
                intent.putExtra("title_name", mServiceList.get(position).title)
                context.startActivity(intent)
            }
        }

        if (mServiceList.get(position).like_status.equals("1")) {
            holder?.iv_like_status.setImageDrawable(context.resources.getDrawable(R.drawable.like))
        } else {
            holder?.iv_like_status.setImageDrawable(context.resources.getDrawable(R.drawable.unlike))
        }


        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val currentDateandTime = format.format(Date())
        var d1: Date? = null
        var d2: Date? = null
        try {
            d1 = format.parse(currentDateandTime)
            d2 = format.parse(mServiceList.get(position).created_date)

            //in milliseconds
            val diff = d1!!.time - d2!!.time
            val diffSeconds = diff / 1000 % 60
            val diffMinutes = diff / (60 * 1000) % 60
            val diffHours = diff / (60 * 60 * 1000) % 24
            val diffDays = diff / (24 * 60 * 60 * 1000)
            Log.e("FINALNEWSTIME", "DAYS" + diffDays + "HOURS" + diffHours + "MIN" + diffMinutes + "SEC" + diffSeconds);
            holder.tv_time_status.setText(diffHours.toString() + "hrs" + diffMinutes.toString() + "min")
        } catch (e: Exception) {
            e.printStackTrace()
        }

        holder?.iv_like_status.setOnClickListener {

            this.likedStatus(position)
            getNewsLikeStatusAPI(sessionManager.isId, mServiceList.get(position).id.toString())

        }

        holder?.iv_news_share.setOnClickListener {
            whatsApp_sendMsg(mServiceList.get(position).title.toString(), "https://www.i1gov.com/uploads/news/" + mServiceList.get(position).image)
            // messenger_sendMsg("Bi-il.gov")

            //shareImageWhatsApp()
        }


        holder?.ll_comment.setOnClickListener {
            val intent = Intent(context, DashboardPostDetailViewActivity::class.java)
            intent.putExtra("news_id", mServiceList.get(position).id)
            intent.putExtra("title_link", mServiceList.get(position).title_link)
            intent.putExtra("author_link", mServiceList.get(position).author_link)
            intent.putExtra("title_name", mServiceList.get(position).title)
            context.startActivity(intent)


        }

        holder?.btn_pay_news.setOnClickListener {

            val intent = Intent(context, NewsFeedList::class.java)
            intent.putExtra("pay_link", mServiceList.get(position).pay_link)
            intent.putExtra("form_screen", mServiceList.get(position).title)
            context.startActivity(intent)


        }


    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return mServiceList.size
    }



    private fun getNewsLikeStatusAPI(user_id: String, news_id: String) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getNewsLikeStatus(user_id, news_id)
        Log.d("NEWSLIKEPARAMS", sessionManager.isId + "-------" + news_id)
        call.enqueue(object : Callback<getNewsListResponse> {
            override fun onResponse(call: Call<getNewsListResponse>, response: retrofit2.Response<getNewsListResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("NEWSLIKESTATUS_RESPONSE_STATUS", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        notifyDataSetChanged()
                        likestatus = "2"
                    } else if (response.body()!!.status.equals("2")) {
                        notifyDataSetChanged()
                        likestatus = "1"
                    }

                }
            }

            override fun onFailure(call: Call<getNewsListResponse>, t: Throwable) {
                Log.w("Result_Address_Profile", t.toString())
            }

        })
    }

    private fun whatsApp_sendMsg(text: String, s: String) {
        val pm = context.getPackageManager()
        val shareIntent = Intent();
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.setAction(Intent.ACTION_SEND);
        //without the below line intent will show error
        shareIntent.setType("text/plain")
        shareIntent.putExtra(Intent.EXTRA_TEXT, text + "\n" + s)
        // Target whatsapp:
        Intent.createChooser(shareIntent, "Share via")
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        context.startActivity(shareIntent);


    }

    private fun myloading() {
        my_loader = Dialog(context)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


    fun likedStatus(position: Int){

        this.mServiceList.get(position).like_status = likestatus
        notifyDataSetChanged()
    }


    // Inflates the item views

}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val iv_user_image = view.findViewById(R.id.iv_user_image) as ImageView
    val username_txt_view = view.findViewById(R.id.username_txt_view) as TextView
    val user_author_txt_view = view.findViewById(R.id.user_author_txt_view) as TextView
    val tv_time_status = view.findViewById(R.id.tv_time_status) as TextView
    val tv_user_title = view.findViewById(R.id.tv_user_title) as TextView
    val tv_description_news = view.findViewById(R.id.tv_description_news) as TextView
    val iv_postimage = view.findViewById(R.id.iv_postimage) as ImageView
    val iv_news_share = view.findViewById(R.id.iv_news_share) as ImageView
    val iv_like_status = view.findViewById(R.id.iv_like_status) as ImageView
    val btn_pay_news = view.findViewById(R.id.btn_pay_news) as Button
    val ll_comment = view.findViewById(R.id.ll_comment) as LinearLayout

}
