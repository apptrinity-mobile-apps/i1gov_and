package com.gov.biil.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.gov.biil.model.apiresponses.Service_Info

@JsonIgnoreProperties(ignoreUnknown = true)
class UpdateProfileResponse {


    val user_info: Service_Info? = null

    val result: String? = null

    val status: String? = null

    val count: String? = null

}
