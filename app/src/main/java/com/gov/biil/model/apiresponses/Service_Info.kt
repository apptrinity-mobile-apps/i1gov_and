package com.gov.biil.model.apiresponses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Service_Info {

    val  id:String ? = null

    val  service_link:String ? = null

    val  city_name:String ? = null

    val  zip_code:String ? = null

    val  county:String ? = null

    val  address:String ? = null

    val  service_type:String ? = null

    val  state:String ? = null

    val  apt_unit_no:String ? = null

    val  due_date:String ? = null

    val  user_id:String ? = null

    val  remider_date:String ? = null

    val  city:String ? = null

    val  state_url:String ? = null
    val  county_url:String ? = null
    val  city_url:String ? = null


}
