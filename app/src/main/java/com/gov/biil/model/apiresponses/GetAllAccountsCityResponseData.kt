package com.gov.biil.model.apiresponses

class GetAllAccountsCityResponseData {

    val id: String? = null
    val user_id: String? = null
    val service_type: String? = null
    val service_id: String? = null
    val city_id: String? = null
    val name: String? = null
    val account: String? = null
    val status: String? = null
    val created_date: String? = null
    val service_name: String? = null
    val pay_type: String? = null
    val servive_amount: String? = null


}
